using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using ClosedXML.Excel;
using FluentValidation.AspNetCore;
using Hangfire;
using Hangfire.PostgreSql;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Configurations;
using Krafter.Logist.WebApp.Hubs;
using Krafter.Logist.WebApp.Hubs.Services;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.Basic.UserRequest;
using Krafter.Logist.WebApp.Models.HealthCheck.HealthCheckReponse;
using Krafter.Logist.WebApp.Models.HealthCheck.IndividualHealthCheckResponse;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserRequestDto;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.Configurations;
using Krafter.ServiceCommon.Middlewares;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using static Microsoft.OData.ODataUrlKeyDelimiter;
using Krafter.Client.Atisu.Configuration;
using Krafter.Logist.WebApp.Models.Basic.Atisu.Filters;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction;
using Krafter.Logist.WebApp.Models.ReadOnly.ActionDto;
using Krafter.Logist.WebApp.Models.ReadOnly.EmployerDto;
using Krafter.ServiceCommon.Extensions;
using Krafter.ServiceCommon.Services.Files;
using Krafter.ServiceCommon.Services.JwtSecurityStampValidator;
using Role = Krafter.Logist.WebApp.Models.Basic.Role.Role;
using User = Krafter.Logist.WebApp.Models.Basic.User.User;
using Employer = Krafter.Logist.WebApp.Models.Basic.Employer.Employer;
using Crafter.Vat.Configuration;
using Krafter.ServiceCommon.Filters;
using Crafter.Client.CIS.Configurations;
using Crafter.Vat.Configuration;
using EasyCaching.Core;
using EasyCaching.Core;
using Krafter.ServiceCommon.Filters;
using System.Collections.Generic;
using Crafter.CarrierOffer.WebApp.Configuration;
using Hangfire.Dashboard;
using Krafter.Logist.WebApp.Configurations.Atisu;
using Microsoft.AspNetCore.Http.Connections;

namespace Krafter.Logist.WebApp
{
    /// <summary>
    ///     Represents the startup process for the application.
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        private IConfiguration Configuration { get; }
        private IWebHostEnvironment Env { get; }

        /// <summary>
        ///     Configures services for the application.
        /// </summary>
        /// <param name="services">The collection of services to configure the application with.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureIdentityServerClient(Configuration);

            services.AddIdentityForWebApi<User, Role>()
                .AddEntityFrameworkStores<LogistDbContext>()
                .AddRoles<Role>()
                .AddDefaultTokenProviders();

            services.Configure<SecurityStampValidatorOptions>(options =>
                options.ValidationInterval = TimeSpan.FromSeconds(10));

            // @TODO полный треш, такого быть не должно
            services.AddJwtSecurityStampValidator(options => { options.UseEasyCacheStorage(); });

            if (!Env.IsDevelopment())
            {
                services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.ForwardedHeaders =
                        ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;

                    options.KnownNetworks.Clear();
                    options.KnownProxies.Clear();
                });
            }

            services.AddCors();
            // the sample application always uses the latest version, but you may want an explicit version such as Version_2_2
            // note: Endpoint Routing is enabled by default; however, it is unsupported by OData and MUST be false
            services.AddControllers(options =>
                {
                    options.EnableEndpointRouting = false;
                    options.Filters.Add(new AtisuResponseExceptionFilter());
                })
                .AddNewtonsoftJson(options=>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.ReferenceHandler=ReferenceHandler.IgnoreCycles;
                })
                .AddFluentValidation(s => { s.RegisterValidatorsFromAssemblyContaining<Startup>(); });

            services.AddCarrierOffer(Configuration);
            
            services.AddApiVersioning(options => options.ReportApiVersions = true);
            services.AddOData().EnableApiVersioning();
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "wwwroot"; });

            services.AddODataApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major[.minor][-status]"
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });

            if (!Env.IsProduction())
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.ConfigureSwaggerBearer(Configuration, xmlPath);
            }

            services.AddHealthChecks();
            // .AddDbContextCheck<LogistDbContext>();


            services.ConfigureCrafterExternalClient(Configuration);

            services.AddHttpContextAccessor();

            services.ConfigureDatabase(Configuration);

            /*if (Env.IsProduction() || Env.IsPreprod())
            {
                services.ConfigureRedisCache(Configuration);
            }
            else
            {*/
            services.ConfigureMemoryCache();
            /*}*/

            services.ConfigureSqlKata(Configuration);

            if (Env.IsDevelopment())
            {
                services.ConfigureS3Files(Configuration);
            }
            else
            {
                services.ConfigureCsiFiles(Configuration);
            }

            services.ConfigureAtisu(Configuration);

            services.ConfigureEmail(Configuration);

            services.ConfigureRazorTemplateEngine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            
            services.AddSingleton<HangfireFilter>();
            
            services.AddHangfire(config =>
            {
                config.UseNLogLogProvider();
                config.UseFilter(services.BuildServiceProvider().GetRequiredService<HangfireFilter>());
                if (!Env.IsDevelopment())
                {
                    var connectionString = Configuration.GetConnectionString("Krafter");
                    config.UsePostgreSqlStorage(connectionString, new PostgreSqlStorageOptions()
                    {
                        SchemaName = "hangfire-logist"
                    });
                }
                else
                {
                    config.UseInMemoryStorage();
                }
            });

            services.AddScoped<CacheSubscriberService>();
            services.AddSignalR(hubOptions =>
            {
                hubOptions.EnableDetailedErrors = true;
                hubOptions.KeepAliveInterval = TimeSpan.FromSeconds(15);
            }).AddNewtonsoftJsonProtocol(options =>
                {
                    options.PayloadSerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                    options.PayloadSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.PayloadSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.PayloadSerializerSettings.Converters.Add(new StringEnumConverter());
                }
            );

            services.AddScoped<RequestsHubService>();

            services.ConfigureDbEventHostedService(Configuration);

            services.AddScoped<ChangesTrackService>();
            services.AddScoped<XLWorkbook>();
            services.AddScoped<KrafterRequestReplyService>();
            services.AddScoped<KrafterRequestService>();
            services.AddScoped<ReisService>();
            services.AddScoped<KontragentContractService>();
            services.AddScoped<ISuitableTransportService, SuitableTransportService>();

            services.AddTransient<IRequestDtoFilterService, RequestDtoFilterService>();
            
            services.AddRepositories();

            if (Env.IsDevelopment())
            {
                services.ConfigureMassTransitInMemory(Configuration);
            }
            else
            {
                services.ConfigureMassTransit(Configuration);
            }

            services.AddScoped<SpotService>();

            services.AddAutoMapper(config =>
            {
                config.CreateMap<User, UserDto>();

                config.CreateMap<Employer, EmployerDto>();

                config.CreateMap<KrafterRequestAction, RequestActionDto>();

                config.CreateMap<UserRequest, UserRequestDto>();

                config.CreateMap<KrafterRequestReplyAction, MinimalRequestActionDto>();
            }, typeof(Startup));

            services.ConfigureVat(Configuration);
        }

        /// <summary>
        ///     Configures the application using the provided builder, hosting environment, and logging factory.
        /// </summary>
        /// <param name="app">The current application builder.</param>
        /// <param name="modelBuilder">
        ///     The <see cref="VersionedODataModelBuilder">model builder</see> used to create OData entity
        ///     data models (EDMs).
        /// </param>
        /// <param name="provider">The API version descriptor provider used to enumerate defined API versions.</param>
        /// <param name="env">
        ///     Provides the information about the web hosting environment an application is running in.
        /// </param>
        public void Configure(
            IApplicationBuilder app,
            VersionedODataModelBuilder modelBuilder,
            IApiVersionDescriptionProvider provider)
        {
            if (!Env.IsDevelopment())
            {
                app.UseHsts();
                app.UseForwardedHeaders();
            }

            if (!Env.IsProduction())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseHeaderPropagation();
            app.UseCors(b => b.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseWebSockets();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseJwtSecurityStampValidator<User>();

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                Predicate = healthCheck => !healthCheck.Tags.Contains("masstransit"),
                ResponseWriter = async (context, report) =>
                {
                    context.Response.ContentType = "application/json";
                    var response = new HealthCheckReponse
                    {
                        Status = report.Status.ToString(),
                        HealthChecks = report.Entries.Select(x => new IndividualHealthCheckResponse
                        {
                            Component = x.Key,
                            Status = x.Value.Status.ToString(),
                            Description = x.Value.Description
                        }),
                        HealthCheckDuration = report.TotalDuration
                    };
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                }
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapWebClientConfig(Configuration);

                endpoints.MapHealthChecks("/health");
                
                void ConfigureOptions(HttpConnectionDispatcherOptions opt)
                {
                    opt.Transports = HttpTransportType.LongPolling;
                    opt.LongPolling.PollTimeout = TimeSpan.FromSeconds(20);
                }
                
                endpoints.MapHub<NotificationsHub>("/notifications", ConfigureOptions);
                endpoints.MapHub<CargomartsHub>("/cargomarts", ConfigureOptions);
                endpoints.MapHub<TorgTransHub>("/torgTrans", ConfigureOptions);
                endpoints.MapHub<TrafficsHub>("/traffics", ConfigureOptions);
                endpoints.MapHub<TransporeonsHub>("/transporeons", ConfigureOptions);
                endpoints.MapHub<RequestsHub>("/requests", ConfigureOptions);
                endpoints.MapHub<MessagesHub>("/messages", ConfigureOptions);
                endpoints.MapHub<BaltikaHub>("/baltika", ConfigureOptions);
                endpoints.MapHub<AtrucksHub>("/atrucks", ConfigureOptions);

                endpoints.MapGet("/EasyCaching/", async (IEasyCachingProviderFactory cacheProvider, HttpContext context) =>
                {
                    IEasyCachingProvider cache = cacheProvider.GetCachingProvider(CacheConfiguration.ProviderMemory);

                    await context.Response.WriteAsync(cache.GetProviderInfo().ProviderName+"\n");
                    
                    var result = await cache.GetCountAsync("");

                    await context.Response.WriteAsync($"allCount: {result.ToString()}\n\n");

                    var keysRequests = await cache.GetByPrefixAsync<RequestSubscriber>(nameof(RequestsHub));

                    await context.Response.WriteAsync($"RequestSubscriber: {keysRequests.Count()}\n");
                    foreach (var keyItem in keysRequests)
                    {
                        await context.Response.WriteAsync($"Key:{keyItem.Key} UserId:{keyItem.Value.Value.UserId} RequestId:{keyItem.Value.Value.RequestId}\n");
                    }

                    var keysCargomartsHub = await cache.GetByPrefixAsync<RequestSubscriber>(nameof(CargomartsHub));
                    await context.Response.WriteAsync($"\nCargomartsSubscriber: {keysCargomartsHub.Count()}\n");
                    foreach (var keyItem in keysCargomartsHub)
                    {
                        await context.Response.WriteAsync($"Key:{keyItem.Key} UserId:{keyItem.Value.Value.UserId} RequestId:{keyItem.Value.Value.RequestId}\n");
                    }

                    await context.Response.WriteAsync($"\n All Keys \n");

                    var provider = cache.Database;
                    var memoryField = provider.GetType().GetField("_memory", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                    if (memoryField != null)
                    {
                        var memoryValue = memoryField.GetValue(provider);

                        foreach (var item in (System.Collections.ICollection)memoryValue)
                        {
                            var keyField = item.GetType().GetField("key", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                            if (keyField != null)
                            {
                                var keyValue = keyField.GetValue(item);

                                await context.Response.WriteAsync($"keyValue:{keyValue}\n");
                            }
                        }
                    }
                    await context.Response.WriteAsync($"\n\n=============================================================\n");

                    cache = cacheProvider.GetCachingProvider(CacheConfiguration.ProviderSessions);

                    await context.Response.WriteAsync(cache.GetProviderInfo().ProviderName+"\n");

                    result = await cache.GetCountAsync("");

                    await context.Response.WriteAsync($"allCount: {result.ToString()}\n\n");

                    keysRequests = await cache.GetByPrefixAsync<RequestSubscriber>(nameof(RequestsHub));

                    await context.Response.WriteAsync($"RequestSubscriber: {keysRequests.Count()}\n");
                    foreach (var keyItem in keysRequests)
                    {
                        await context.Response.WriteAsync($"Key:{keyItem.Key} UserId:{keyItem.Value.Value.UserId} RequestId:{keyItem.Value.Value.RequestId}\n");
                    }

                    keysCargomartsHub = await cache.GetByPrefixAsync<RequestSubscriber>(nameof(CargomartsHub));
                    await context.Response.WriteAsync($"\nCargomartsSubscriber: {keysCargomartsHub.Count()}\n");
                    foreach (var keyItem in keysCargomartsHub)
                    {
                        await context.Response.WriteAsync($"Key:{keyItem.Key} UserId:{keyItem.Value.Value.UserId} RequestId:{keyItem.Value.Value.RequestId}\n");
                    }

                    
                });

                if (!Env.IsDevelopment())
                {
                    endpoints.MapHangfireDashboard("/hangfire", new DashboardOptions()
                    {
                        Authorization = new IDashboardAuthorizationFilter[] { new HangfireAuthorizationFilter() }
                    });
                }
                else
                {
                    endpoints.MapHangfireDashboard("/hangfire", new DashboardOptions()
                    {
                        Authorization = new List<IDashboardAuthorizationFilter>()
                    });
                }

            });

            if (!Env.IsProduction())
            {
                app.UseSwagger();
                app.UseSwaggerUI(
                    options =>
                    {
                        options.OAuthClientId(Configuration.GetValue<string>("Web:IDENTITY_CLIENT_ID"));
                        options.OAuthScopes(Configuration.GetValue<string>("Web:IDENTITY_SCOPES").Split(' '));
                        options.OAuthUsePkce();

                    // build a swagger endpoint for each discovered API version
                        foreach (var description in provider.ApiVersionDescriptions)
                            options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                                description.GroupName.ToUpperInvariant());
                    });
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseHangfireServer(new BackgroundJobServerOptions()
            {
                // порядковый номер в очереди это приоритет для HF
                Queues = new[] { "to_front", "repository", "digital", "logist", "default" },
                WorkerCount = 30
            });

            var defaultRetryFilter = GlobalJobFilters.Filters.FirstOrDefault(f => f.Instance is AutomaticRetryAttribute);

            if (defaultRetryFilter != null && defaultRetryFilter.Instance != null)
            {
                GlobalJobFilters.Filters.Remove(defaultRetryFilter.Instance);
            }

            GlobalJobFilters.Filters.Add(new HangfireUseCorrectQueueFilter { Order = 1 });
            GlobalJobFilters.Filters.Add(new HangfireRetryJobFilter
            {
                Order = 2,
                ["to_front"] = new HangfireQueueSettings
                {
                    DelayInSeconds = 10,
                    RetryAttempts = 3
                },
                ["repository"] = new HangfireQueueSettings
                {
                    DelayInSeconds = 0,
                    RetryAttempts = 0
                },
                ["digital"] = new HangfireQueueSettings
                {
                    DelayInSeconds = 60,
                    RetryAttempts = 5
                },
                ["logist"] = new HangfireQueueSettings
                {
                    DelayInSeconds = 60,
                    RetryAttempts = 5
                },
                ["default"] = new HangfireQueueSettings
                {
                    DelayInSeconds = 60,
                    RetryAttempts = 5
                }
            });

            app.UseRepositoriesAutoUpdate();

            app.UseMvc(
                routeBuilder =>
                {
                    routeBuilder.SetTimeZoneInfo(TimeZoneInfo.Local);

                    routeBuilder.ServiceProvider.GetRequiredService<ODataOptions>().UrlKeyDelimiter = Parentheses;

                    // global odata query options
                    routeBuilder.Count().MaxTop(1000).Select().Expand().Filter();

                    routeBuilder.MapVersionedODataRoutes("odata", "api", modelBuilder.GetEdmModels());
                });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "../Krafter.Web/packages/krafter-logist";

                if (Env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
                }
            });
        }
    }
}