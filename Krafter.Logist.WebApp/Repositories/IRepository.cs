using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;

namespace Krafter.Logist.WebApp.Repositories
{
    public interface IRepository<TModel> : IRepositoryBase where TModel : class
    {
        public Task<TModel> GetAsync();
    }
    
    public interface IRepositoryBase
    {
        public Task SetAsync();
    }
}