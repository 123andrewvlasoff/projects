using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Dictionaries;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Krafter.Enums;
using Dapper;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Krafter.Logist.WebApp.Models.ReadOnly.AtrucksRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.AtrucksRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.BaltikaRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.BaltikaRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.CargomartRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.CargomartRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.EmployerDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KontragentDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KontragentUserDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestReplyDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.TorgTransRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.TrafficRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.TransporeonRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.TransporeonRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserRequestDto;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services.ChangesTrackService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Nito.AsyncEx;
using SqlKata.Execution;
using Z.EntityFramework.Plus;
using Region = DAL.Models.Requests.Dictionaries.Region;

namespace Krafter.Logist.WebApp.Repositories
{
    [Queue("repository")]
    public class RequestDtoRepository : IRepository<IEnumerable<RequestDto>>
    {
        private readonly LogistReadOnlyDbContext logistReadOnlyDbContext;
        private readonly ConcurrentDictionary<Guid, RequestDto> storage;
        private readonly RequestDtoRepositoryConfiguration configuration;
        private readonly AsyncReaderWriterLock cacheLock;
        private readonly QueryFactory db;

        public RequestDtoRepository(LogistReadOnlyDbContext logistReadOnlyDbContext, AsyncReaderWriterLock cacheLock,
            IOptions<RequestDtoRepositoryConfiguration> configuration, ConcurrentDictionary<Guid, RequestDto> storage,
            QueryFactory db)
        {
            this.logistReadOnlyDbContext = logistReadOnlyDbContext;
            this.cacheLock = cacheLock;
            this.storage = storage;
            this.db = db;
            this.configuration = configuration.Value;
        }

        public async Task<IEnumerable<RequestDto>> GetAsync()
        {
            using (await cacheLock.ReaderLockAsync())
            {
                return storage.Values;
            }
        }

        public async Task<TRequest> GetAsync<TRequest>(Guid key) where TRequest : RequestDto
        {
            using (await cacheLock.ReaderLockAsync())
            {
                return storage[key] as TRequest;
            }
        }

        public async Task<RequestDto> GetAsync(Guid key)
        {
            return await GetAsync<RequestDto>(key);
        }

        public async Task SetAsync()
        {
            SqlMapper.AddTypeHandler(typeof(IEnumerable<PropertyChanges>), new JsonTypeHandler());
                
            var queryKrafterOrder = db.Query("Requests")
                .WhereIn("Requests.Discriminator", new[]{ RequestDiscriminator.Krafter })
                .WhereIn("RequestStatus", new[]
                {
                    RequestStatus.IsActive, RequestStatus.DataTreatment, RequestStatus.DataInput,
                    RequestStatus.Error
                })
                .OrWhereIn("KrafterRequestStatus",
                    new[] { KrafterRequestStatus.CancelledByCarrier, KrafterRequestStatus.CancelledByClient })
                .RightJoin("RequestRoutePoints", "RequestId", "Requests.Id")
                .LeftJoin("kRegions", "kRegions.Id", "RequestRoutePoints.kRegionId")
                .LeftJoin("Settlements", "Settlements.Id", "RequestRoutePoints.SettlementId")
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .Select("*").As("InProgressUser"),
                    j => j.On("InProgressUser.Id", "Requests.InProgressUserId"))
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .Select("*").As("AssignedUser"),
                    j => j.On("AssignedUser.Id", "Requests.AssignedUserId"))
                .LeftJoin("Kontragents", "Kontragents.Id", "Requests.ClientId")
                .LeftJoin(
                    db.Query("KKontragentUsers").Where("RelationType", RelationType.Manager)
                        .Select("*").As("KKontragentUsers"),
                    j => j.On("KKontragentUsers.KKontragentID", "Kontragents.Id"))
                .LeftJoin("AspNetUsers", "AspNetUsers.Id", "KKontragentUsers.UID")
                .LeftJoin("Employs", "Employs.UserId", "AspNetUsers.Id")
                .LeftJoin("RequestReplies", "RequestReplies.RequestId", "Requests.Id")
                .LeftJoin(
                    db.Query("Actions")
                        .Where("Discriminator", nameof(KrafterRequestAction))
                        .Select("*")
                        .As("Actions"),
                    j => j.On("Actions.RequestId", "Requests.Id"))
                .LeftJoin("UserRequests", "UserRequests.RequestId", "Requests.Id")
                .Select("*");

            var queryExternalOrder = db.Query("Requests")
                .WhereIn("Requests.Discriminator", new[]{ RequestDiscriminator.Cargomart, RequestDiscriminator.Transporeon, 
                                                          RequestDiscriminator.TrafficOnline, RequestDiscriminator.Baltika, RequestDiscriminator.Atrucks,
                                                          RequestDiscriminator.TorgTrans})
                .Where("ActiveUntil", ">", DateTime.Now)
                .WhereIn("RequestStatus", new[]
                {
                    RequestStatus.IsActive, RequestStatus.DataTreatment, RequestStatus.DataInput,
                    RequestStatus.Error
                })
                .OrWhereIn("KrafterRequestStatus",
                    new[] { KrafterRequestStatus.CancelledByCarrier, KrafterRequestStatus.CancelledByClient })
                .OrWhereRaw("(\"Requests\".\"Discriminator\" = ? AND \"RequestStatus\" IN (? , ?))", RequestDiscriminator.Cargomart, RequestStatus.DataTreatment, RequestStatus.DataInput)
                .RightJoin("RequestRoutePoints", "RequestId", "Requests.Id")
                .LeftJoin("kRegions", "kRegions.Id", "RequestRoutePoints.kRegionId")
                .LeftJoin("Settlements", "Settlements.Id", "RequestRoutePoints.SettlementId")
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .Select("*").As("InProgressUser"),
                    j => j.On("InProgressUser.Id", "Requests.InProgressUserId"))
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .Select("*").As("AssignedUser"),
                    j => j.On("AssignedUser.Id", "Requests.AssignedUserId"))
                .LeftJoin("Kontragents", "Kontragents.Id", "Requests.ClientId")
                .LeftJoin(
                    db.Query("KKontragentUsers").Where("RelationType", RelationType.Manager)
                        .Select("*").As("KKontragentUsers"),
                    j => j.On("KKontragentUsers.KKontragentID", "Kontragents.Id"))
                .LeftJoin("AspNetUsers", "AspNetUsers.Id", "KKontragentUsers.UID")
                .LeftJoin("Employs", "Employs.UserId", "AspNetUsers.Id")
                .LeftJoin("RequestReplies", "RequestReplies.RequestId", "Requests.Id")
                .LeftJoin(
                    db.Query("Actions")
                        .Where("Discriminator", nameof(KrafterRequestAction))
                        .Select("*")
                        .As("Actions"),
                    j => j.On("Actions.RequestId", "Requests.Id"))
                .LeftJoin("UserRequests", "UserRequests.RequestId", "Requests.Id")
                .Select("*");

            var query = queryKrafterOrder.Union(queryExternalOrder);
                
            var requests = new Dictionary<Guid, RequestDto>();

            var guids = new Hashtable();

            await db.Connection.QueryAsync(
                db.Compiler.Compile(query).ToString(),
                new[]
                {
                typeof(object),
                typeof(object),
                typeof(Region),
                typeof(Settlement),
                typeof(UserDto),
                typeof(UserDto),
                typeof(KontragentDto),
                typeof(KontragentUserDto),
                typeof(UserDto),
                typeof(EmployerDto),
                typeof(RequestReplyDto),
                typeof(object),
                typeof(UserRequestDto)
                },
                item =>
                {
                    var requestDict = item[0] as IDictionary<string, object>;

                    RequestDto request = null;

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Krafter)
                    {
                        request = requestDict.ToType<KrafterRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Cargomart)
                    {
                        request = requestDict.ToType<CargomartRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.TorgTrans)
                    {
                        request = requestDict.ToType<TorgTransRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.TrafficOnline)
                    {
                        request = requestDict.ToType<TrafficRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Transporeon)
                    {
                        request = requestDict.ToType<TransporeonRequestDto>();
                    }
                    
                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Baltika)
                    {
                        request = requestDict.ToType<BaltikaRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Atrucks)
                    {
                        request = requestDict.ToType<AtrucksRequestDto>();
                    }

                    request.InProgressUser = item[4] as UserDto;
                    request.AssignedUser = item[5] as UserDto;
                    request.Client = item[6] as KontragentDto;


                    var routePointDict = item[1] as IDictionary<string, object>;

                    RequestRoutePointDto routePoint = null;

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Krafter)
                    {
                        routePoint = routePointDict.ToType<KrafterRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Cargomart)
                    {
                        routePoint = routePointDict.ToType<CargomartRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.TorgTrans)
                    {
                        routePoint = routePointDict.ToType<AuctionRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.TrafficOnline)
                    {
                        routePoint = routePointDict.ToType<AuctionRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Transporeon)
                    {
                        routePoint = routePointDict.ToType<TransporeonRequestRoutePointDto>();
                    }
                    
                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Baltika)
                    {
                        routePoint = routePointDict.ToType<BaltikaRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Atrucks)
                    {
                        routePoint = routePointDict.ToType<AtrucksRequestRoutePointDto>();
                    }

                    routePoint.Region = item[2] as Region;
                    routePoint.Settlement = item[3] as Settlement;

                    var kontragentUser = item[7] as KontragentUserDto;

                    if (kontragentUser != null)
                    {
                        kontragentUser.User = item[8] as UserDto;
                        kontragentUser.User.Employer = item[9] as EmployerDto;
                    }

                    var reply = item[10] as RequestReplyDto;

                    var actionDict = item[11] as IDictionary<string, object>;

                    RequestActionDto action = null;

                    if (actionDict != null)
                    {
                        action = actionDict.ToType<RequestActionDto>();
                    }

                    var userRequest = item[12] as UserRequestDto;


                    if (requests.ContainsKey(request.Id))
                    {
                        request = requests[request.Id];
                    }
                    else
                    {
                        request.Replies = new List<RequestReplyDto>();
                        request.RoutePoints = new List<RequestRoutePointDto>();
                        request.UserRequests = new List<UserRequestDto>();
                        request.Actions = new List<RequestActionDto>();
                        request.Client.KontragentUsers = new List<KontragentUserDto>();
                    }


                    if (!guids.ContainsKey(routePoint.Id))
                    {
                        guids.Add(routePoint.Id, null);
                        request.RoutePoints.Add(routePoint);
                    }

                    if (reply != null && !guids.ContainsKey(reply.Id))
                    {
                        guids.Add(reply.Id, null);
                        request.Replies.Add(reply);
                    }

                    if (action != null && !guids.ContainsKey(action.Id))
                    {
                        guids.Add(action.Id, null);
                        request.Actions.Add(action);
                    }

                    if (userRequest != null && !guids.ContainsKey(userRequest.Id))
                    {
                        guids.Add(userRequest.Id, null);
                        request.UserRequests.Add(userRequest);
                    }

                    if (kontragentUser != null)
                    {
                        request.Client.KontragentUsers.Add(kontragentUser);
                    }

                    requests[request.Id] = request;

                    return request;
                });
                
            using (await cacheLock.WriterLockAsync())
            {
                storage.Clear();

                foreach (var request in requests.Values)
                {
                    storage.AddOrUpdate(request.Id, request, (guid, dto) => request);
                }
            }
        }
        
         public async Task SetAsync(Guid key)
        {
             var query = db.Query("Requests")
                 .Where("Requests.Id",key)
                .RightJoin("RequestRoutePoints", "RequestId", "Requests.Id")
                .LeftJoin("kRegions", "kRegions.Id", "RequestRoutePoints.kRegionId")
                .LeftJoin("Settlements", "Settlements.Id", "RequestRoutePoints.SettlementId")
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .Select("*").As("InProgressUser"),
                    j => j.On("InProgressUser.Id", "Requests.InProgressUserId"))
                .LeftJoin("Kontragents", "Kontragents.Id", "Requests.ClientId")
                .LeftJoin(
                    db.Query("RequestReplies")
                        .Select("*").As("RequestReplies"),
                    j => j.On("RequestReplies.RequestId", "Requests.Id"))
                .LeftJoin("UserRequests", "UserRequests.RequestId", "Requests.Id")
                .LeftJoin(db.Query("Actions").Select("Id","Discriminator","RequestId","State","ActionUserId","ActionDate")
                    .As("Actions"),j=>j.On("Actions.RequestId","Requests.Id"))
                .Select("*");

            var requests = new Dictionary<Guid, RequestDto>();

            var guids = new Hashtable();

            await db.Connection.QueryAsync(
                db.Compiler.Compile(query).ToString(),
                new[]
                {
                    typeof(object),
                    typeof(object),
                    typeof(Region),
                    typeof(Settlement),
                    typeof(UserDto),
                    typeof(KontragentDto),
                    typeof(RequestReplyDto),
                    typeof(UserRequestDto),
                    typeof(RequestActionDto)
                },
                item =>
                {
                    var requestDict = item[0] as IDictionary<string, object>;

                    RequestDto request = null;

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Krafter)
                    {
                        request = requestDict.ToType<KrafterRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Cargomart)
                    {
                        request = requestDict.ToType<CargomartRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.TorgTrans)
                    {
                        request = requestDict.ToType<TorgTransRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.TrafficOnline)
                    {
                        request = requestDict.ToType<TrafficRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Transporeon)
                    {
                        request = requestDict.ToType<TransporeonRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Baltika)
                    {
                        request = requestDict.ToType<BaltikaRequestDto>();
                    }

                    if ((RequestDiscriminator)requestDict["Discriminator"] == RequestDiscriminator.Atrucks)
                    {
                        request = requestDict.ToType<AtrucksRequestDto>();
                    }

                    request.InProgressUser = item[4] as UserDto;
                    request.Client = item[5] as KontragentDto;


                    var routePointDict = item[1] as IDictionary<string, object>;

                    RequestRoutePointDto routePoint = null;

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Krafter)
                    {
                        routePoint = routePointDict.ToType<KrafterRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Cargomart)
                    {
                        routePoint = routePointDict.ToType<CargomartRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.TorgTrans)
                    {
                        routePoint = routePointDict.ToType<AuctionRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.TrafficOnline)
                    {
                        routePoint = routePointDict.ToType<AuctionRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Transporeon)
                    {
                        routePoint = routePointDict.ToType<TransporeonRequestRoutePointDto>();
                    }
                    
                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Baltika)
                    {
                        routePoint = routePointDict.ToType<BaltikaRequestRoutePointDto>();
                    }

                    if ((RequestDiscriminator)routePointDict["Discriminator"] == RequestDiscriminator.Atrucks)
                    {
                        routePoint = routePointDict.ToType<AtrucksRequestRoutePointDto>();
                    }

                    var action = item[8] as RequestActionDto;


                    routePoint.Region = item[2] as Region;
                    routePoint.Settlement = item[3] as Settlement;


                    var reply = item[6] as RequestReplyDto;
                    var userRequest = item[7] as UserRequestDto;

                    if (requests.ContainsKey(request.Id))
                    {
                        request = requests[request.Id];
                    }
                    else
                    {
                        request.Replies = new List<RequestReplyDto>();
                        request.RoutePoints = new List<RequestRoutePointDto>();
                        request.UserRequests = new List<UserRequestDto>();
                        request.Actions = new List<RequestActionDto>();
                    }


                    if (!guids.ContainsKey(routePoint.Id))
                    {
                        guids.Add(routePoint.Id, null);
                        request.RoutePoints.Add(routePoint);
                    }

                    if (action != null && !guids.ContainsKey(action.Id))
                    {
                        guids.Add(action.Id, null);
                        request.Actions.Add(action);
                    }

                    if (reply != null && !guids.ContainsKey(reply.Id))
                    {
                        guids.Add(reply.Id, null);
                        request.Replies.Add(reply);
                    }

                    if (userRequest != null && !guids.ContainsKey(userRequest.Id))
                    {
                        guids.Add(userRequest.Id, null);
                        request.UserRequests.Add(userRequest);
                    }

                    requests[request.Id] = request;

                    return request;
                });
            
            if (requests.Values.FirstOrDefault() != null)
            {
                await SetAsync(requests.Values.First());
            }
        }
         

        public async Task SetAsync(RequestDto item)
        {
            using (await cacheLock.WriterLockAsync())
            {
                storage.AddOrUpdate(item.Id, item, (guid, dto) => item);
            }
        }

        public async Task<bool> ExistAsync(Guid key)
        {
            using (await cacheLock.ReaderLockAsync())
            {
                return storage.ContainsKey(key);
            }
        }

        public async Task RemoveAsync(Guid key)
        {
            using (await cacheLock.WriterLockAsync())
            {
                storage.Remove(key, out _);
            }
        }
    }
}