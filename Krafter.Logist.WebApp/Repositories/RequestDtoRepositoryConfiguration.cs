using System;
using Krafter.Logist.WebApp.Configurations;

namespace Krafter.Logist.WebApp.Repositories
{
    public class RequestDtoRepositoryConfiguration
    {
        public TimeSpan ItemsLifeTime { get; set; } = TimeSpan.FromDays(1);
    }
}