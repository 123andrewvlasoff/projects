using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using KellermanSoftware.CompareNetObjects;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.ServiceCommon.Services.DbEventHandlerService;

namespace Krafter.Logist.WebApp.Repositories.Services
{
    public class RequestsRepositoryService
    {
        private readonly RequestDtoRepository repository;

        public RequestsRepositoryService(RequestDtoRepository repository)
        {
            this.repository = repository;
        }

        public async Task SetRequestAsync(DbEvent dbEvent)
        {
            var key = Guid.Parse(dbEvent.EntryId);
            var item = dbEvent.GetNew<RequestDto>();
            AuctionRequestDto auctionRequest;
            AuctionRequestDto auction;

            switch (dbEvent.Operation)
            {
                case DbOperation.Update:
                    if (item.RequestStatus != RequestStatus.Done &&
                        item.RequestStatus != RequestStatus.Cancelled)
                    {
                        var compareLogic = new CompareLogic();
                        if (compareLogic.Compare(dbEvent.GetNew<RequestDto>(),
                                dbEvent.GetOld<RequestDto>()).Differences
                            .Any(i =>
                                new[]
                                    {
                                        nameof(RequestDto.ClientId),
                                        nameof(RequestDto.TransportTypeId),
                                        nameof(RequestDto.RollingStockTypeId),
                                        nameof(RequestDto.LoadingTypeId),
                                        nameof(RequestDto.AssignedUserId),
                                        nameof(RequestDto.InProgressUserId)
                                    }
                                    .Contains(i.PropertyName)))
                        {
                            await repository.SetAsync(key);
                            return;
                        }

                        switch (item.Discriminator)
                        {
                            case RequestDiscriminator.Krafter:
                                var krafterRequest = dbEvent.GetNew<KrafterRequestDto>();
                                var request = await repository.GetAsync<KrafterRequestDto>(key);

                                request.KrafterRequestStatus = krafterRequest.KrafterRequestStatus;
                                request.RequestStatus = krafterRequest.RequestStatus;
                                request.Price = krafterRequest.Price;
                                request.PalletsQuantity = krafterRequest.PalletsQuantity;
                                request.Distance = krafterRequest.Distance;
                                request.Volume = krafterRequest.Volume;
                                request.Weight = krafterRequest.Weight;
                                request.Comment = krafterRequest.Comment;
                                request.Description = krafterRequest.Description;
                                request.ActiveUntil = krafterRequest.ActiveUntil;
                                request.ActualPrice = krafterRequest.ActualPrice;
                                request.ClientPrice = krafterRequest.ClientPrice;
                                request.IsActive = krafterRequest.IsActive;
                                request.RequestCode = krafterRequest.RequestCode;
                                request.Number1C = krafterRequest.Number1C;
                                request.TemperatureMin = krafterRequest.TemperatureMin;
                                request.TemperatureMax = krafterRequest.TemperatureMax;
                                request.VatType = krafterRequest.VatType;

                                await repository.SetAsync(request);

                                break;
                            case RequestDiscriminator.Cargomart:
                            case RequestDiscriminator.TorgTrans:
                            case RequestDiscriminator.TrafficOnline:
                            case RequestDiscriminator.Baltika:
                                auctionRequest = dbEvent.GetNew<AuctionRequestDto>();
                                auction = await repository.GetAsync<AuctionRequestDto>(key);

                                auction.AuctionRequestStatus = auctionRequest.AuctionRequestStatus;
                                auction.RequestStatus = auctionRequest.RequestStatus;
                                auction.Price = auctionRequest.Price;
                                auction.PalletsQuantity = auctionRequest.PalletsQuantity;
                                auction.Distance = auctionRequest.Distance;
                                auction.Volume = auctionRequest.Volume;
                                auction.Weight = auctionRequest.Weight;
                                auction.Comment = auctionRequest.Comment;
                                auction.Description = auctionRequest.Description;
                                auction.ActiveUntil = auctionRequest.ActiveUntil;
                                auction.ActualPrice = auctionRequest.ActualPrice;
                                auction.ClientPrice = auctionRequest.ClientPrice;
                                auction.IsActive = auctionRequest.IsActive;
                                auction.RequestCode = auctionRequest.RequestCode;
                                auction.Number1C = auctionRequest.Number1C;
                                auction.TemperatureMin = auctionRequest.TemperatureMin;
                                auction.TemperatureMax = auctionRequest.TemperatureMax;
                                auction.VatType = auctionRequest.VatType;

                                await repository.SetAsync(auction);

                                break;
                            case RequestDiscriminator.Transporeon:
                                auctionRequest = dbEvent.GetNew<AuctionRequestDto>();
                                auction = await repository.GetAsync<AuctionRequestDto>(key);

                                auction.AuctionRequestStatus = auctionRequest.AuctionRequestStatus;
                                auction.RequestStatus = auctionRequest.RequestStatus;
                                auction.Price = auctionRequest.Price;
                                auction.PalletsQuantity = auctionRequest.PalletsQuantity;
                                auction.Distance = auctionRequest.Distance;
                                auction.Volume = auctionRequest.Volume;
                                auction.Weight = auctionRequest.Weight;
                                auction.Comment = auctionRequest.Comment;
                                auction.Description = auctionRequest.Description;
                                auction.ActiveUntil = auctionRequest.ActiveUntil;
                                auction.ActualPrice = auctionRequest.ActualPrice;
                                auction.ClientPrice = auctionRequest.ClientPrice;
                                auction.IsActive = auctionRequest.IsActive;
                                auction.RequestCode = auctionRequest.RequestCode;
                                auction.Number1C = auctionRequest.Number1C;
                                auction.TemperatureMin = auctionRequest.TemperatureMin;
                                auction.TemperatureMax = auctionRequest.TemperatureMax;
                                auction.VatType = auctionRequest.VatType;

                                await repository.SetAsync(auction);

                                break;
                        }
                    }
                    else if (await repository.ExistAsync(key))
                    {
                        await repository.RemoveAsync(key);
                    }

                    break;

                case DbOperation.Insert:
                    switch (item.Discriminator)
                    {
                        case RequestDiscriminator.Krafter:
                            break;
                        case RequestDiscriminator.Cargomart:
                        case RequestDiscriminator.TorgTrans:
                        case RequestDiscriminator.TrafficOnline:
                        case RequestDiscriminator.Baltika:
                            break;
                        case RequestDiscriminator.Transporeon:
                            await repository.SetAsync(key);
                            return;
                    }

                    break;
            }
        }
    }
}