﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Transport
{
    public class PointInfo
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
    }
}
