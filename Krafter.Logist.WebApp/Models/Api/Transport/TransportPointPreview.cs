﻿using DAL.Models.Drivers;
using DAL.Models.Kontragents;
using DAL.Models.Transports.Enums;
using DAL.Models.Users;
using System;
using Krafter.Logist.WebApp.Models.Api.Basic;
using Krafter.Logist.WebApp.Models.Basic.Transport;

namespace Krafter.Logist.WebApp.Models.Api.Transport
{
    public class TransportPointPreview
    {
        public Guid Id { get; set; }
        public Guid ReisId { get; set; }
        public string DeliveryNumber { get; set; }
        public string UnloadAddress { get; set; }
        public CarrierApi Carrier { get; set; }
        public EmployerApi PinnedEmployer { get; set; }
        public bool IsCanReadReis { get; set; }

    }
}
