using System;
using ClosedXML.Excel;

namespace Krafter.Logist.WebApp.Models.Api.Basic;

public class ReisRoutePointApi
{
    public double? Lat { get; set; }
    public double? Lon { get; set; }
    public string Address { get; set; }
    public DateTime LeaveDatePlan { get; set; }
    public DateTime ArrivalDatePlan { get; set; }
    
}