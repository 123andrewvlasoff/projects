﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class BranchApi
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}
