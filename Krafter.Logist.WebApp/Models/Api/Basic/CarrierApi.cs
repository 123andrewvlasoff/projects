﻿using System;
using System.Collections.Generic;
using DAL.Models.Requests.Reis.Enums;
using Krafter.Logist.WebApp.Models.Api.Transport;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class CarrierApi
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public IEnumerable<KontragentContactApi> Contacts { get; set; }

        public IEnumerable<VatType> VatTypes { get; set; }
    }
}
