using System;
using DAL.Models.Comment.Enums;
using DAL.Models.Users;
using Microsoft.AspNetCore.Identity;

namespace Krafter.Logist.WebApp.Models.Api.Basic;

public class BundleCommentApi : DAL.Models.Comment.Comment
{
    public Guid RequestId { get; set; }

    public RequestApi Request { get; set; }

    public Guid TransportId { get; set; }

    public  TransportApi Transport { get; set; }

    public Guid? ConnectedTransportId { get; set; }

    public TransportApi ConnectedTransport { get; set; }
    
    public double? Remoteness { get; set; }

    public Guid Id { get; set; }

    public string Text { get; set; }
    

    public IdentityRole Role { get; set; }

    public DateTime CreatedAt { get; set; }
        
    public DateTime? UpdatedAt { get; set; }
    
    public string CreatorId { get; set; }
    public UserApi Creator { get; set; }

    public string UpdaterId { get; set; }
    
    public UserApi Updater { get; set; }

    public CommentType CommentType { get; set; }

    public bool IsDeleted { get; set; }
}