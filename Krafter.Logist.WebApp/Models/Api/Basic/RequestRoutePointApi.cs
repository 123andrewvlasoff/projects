using System;
using System.Collections.Generic;
using DAL.Models.Requests.Dictionaries;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Krafter.Actions;

namespace Krafter.Logist.WebApp.Models.Api.Basic;

public class RequestRoutePointApi
{
    public Guid Id { get; set; }

    public DateTime ArrivalDate { get; set; }

    public DateTime LeaveDate { get; set; }

    public DateTime? ArrivalDateFact { get; set; }

    public DateTime? LeaveDateFact { get; set; }

    public PointType PointType { get; set; }

    public string Address { get; set; }

    public string Contacts { get; set; }
    
    public int DeltaHours { get; set; }

    public int RegionId { get; set; }

    public Region Region { get; set; }

    public double? Lat { get; set; }

    public double? Lon { get; set; }

    public int OrderInRoute { get; set; }

    public string Client { get; set; }

    public int? SettlementId { get; set; }

    public Settlement Settlement { get; set; }

    public Guid? AddressId { get; set; }

    public string PostalCode { get; set; }

    public RequestDiscriminator Discriminator { get; set; }

    protected internal ICollection<RequestRoutePointAction> Actions { get; set; }
}