﻿
using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class DriverApi
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string FullName => $"{LastName} {FirstName} {Patronymic}";
        public string PhoneNumber { get; set; }
        public string WorkPhone { get; set; }

        public DateTime? BirthDate { get; set; }

    }
}
