﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class KontragentContactApi
    {
        public int Id { get; set; }

        public string Position { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}
