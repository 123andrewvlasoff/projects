namespace Krafter.Logist.WebApp.Models.Api.Basic;

public class UserApi
{
    public string PersonName { get; set; }

    public string LastName { get; set; }

    public string Patronymic { get; set; }

    public string FullName => $"{LastName} {PersonName} {Patronymic}";

    public string Position { get; set; }

    public bool HasDigitalSign { get; set; }

    public bool IsActive { get; set; }
}