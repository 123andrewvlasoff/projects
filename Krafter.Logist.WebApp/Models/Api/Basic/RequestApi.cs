using System;
using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Api.Basic;

public class RequestApi
{
    public Guid Id { get; set; }
    protected internal ICollection<RequestRoutePointApi> RoutePoints { get; set; }
}