﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class EmployerApi
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string FIO { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public BranchApi Branch { get; set; }

        public string Phone { get; set; }
    }
}
