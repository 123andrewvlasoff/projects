﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class TransportStatusApi
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}
