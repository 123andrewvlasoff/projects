﻿using System;

namespace Krafter.Logist.WebApp.Models.Api.Basic
{
    public class TransportApi
    {
        public Guid Id { get; set; }
        public int? TransportTypeId { get; set; }
        public bool IsConnectedTransport { get; set; }
        public string RegNumber { get; set; }
        public string Model { get; set; }
        public double Volume { get; set; }
        public double Weight { get; set; }
        public int? RollingStockTypeId { get; set; }
    }
}
