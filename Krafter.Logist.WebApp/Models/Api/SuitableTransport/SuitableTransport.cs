using System;
using Krafter.Logist.WebApp.Models.Api.Basic;
using DAL.Models.Transports.Enums;

namespace Krafter.Logist.WebApp.Models.Api.SuitableTransport;

public class SuitableTransport
{
    public Guid Id { get; set; }
    
    public TimeSpan? RequestDateInterval { get; set; }

    public Guid DriverId { get; set; }

    public DriverApi Driver { get; set; }
    
    public Guid TransportId { get; set; }
    
    public TransportApi Transport { get; set; }

    public Guid? ConnectedTransportId { get; set; }

    public TransportApi ConnectedTransport { get; set; }

    public Guid CarrierId { get; set; }

    public CarrierApi Carrier { get; set; }

    public string Address { get; set; }

    public int? RollingStockTypeId { get; set; }
    
    public DateTime UnloadDate { get; set; }

    public double? Remoteness { get; set; }

    public EmployerApi PinnedEmployer { get; set; }

    public Owner? Owner { get; set; }

    public Guid? StatusId { get; set; }

    public ReisStatusApi Status { get; set; }

    public Guid? TransportStatusId { get; set; }

    public TransportStatusApi TransportStatus { get; set; }
    
    public string DeliveryNumber { get; set; }
    
    public double? Lat { get; set; }
    
    public double? Lon { get; set; }

    public bool IsCanReadReis { get; set; }
}