using System;
using System.Collections.Generic;
using DAL.Models.Requests.Reis.Enums;
using DAL.Models.Transports.Enums;
using NetTopologySuite.Geometries;

namespace Krafter.Logist.WebApp.Models.Api.SuitableTransport;

public class SuitableTransportFilter
{
    public DateTime? StartPeriod { get; set; }
    public DateTime? EndPeriod { get; set; }
    public int? StartInterval { get; set; }
    public int? EndInterval { get; set; }
    public double? Lat { get; set; }
    public double? Lon { get; set; }
    public DateTime? ArrivalDate { get; set; }
    public DateTime? LeaveDate { get; set; }
    public double? Weight { get; set; }
    public int? LoadingTypeId { get; set; }
    public int? Remoteness { get; set; }
    public VatType? VatType { get; set; }
    public bool IsConnectedTransport { get; set; }
    public bool IsNotConnectedTransport { get; set; }
    public List<int?> RollingStockTypes { get; set; } = new();

    public double? MinWeight { get; set; }
    public double? MaxWeight { get; set; }
    public double? MinVolume { get; set; }
    public double? MaxVolume { get; set; }

    public bool IsNds { get; set; }
    public bool IsNotNds { get; set; }
    public Owner Owner { get; set; }
}