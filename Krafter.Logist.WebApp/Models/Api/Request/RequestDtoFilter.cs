using System;
using Krafter.ServiceCommon.Models;

namespace Krafter.Logist.WebApp.Models.Api.Request;


public class RequestDtoFilter : RequestDtoFilterBase
{
    public bool? IsInProgress { get; set; }
    public string[] ClientCode { get; set; }
    public Guid[] ClientManager { get; set; }
    public decimal? PriceMin { get; set; }
    public decimal? PriceMax { get; set; }
}