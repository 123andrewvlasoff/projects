namespace Krafter.Logist.WebApp.Models.Basic.PaymentTransaction
{
    public class PaymentTransaction : DAL.Models.Transactions.PaymentTransaction
    {
        public new Reis.Reis Reis { get; set; }

        public new Kontragent.Kontragent Carrier { get; set; }
    }
}