using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.PaymentTransaction
{
    public class PaymentTransactionContextConfiguration : IEntityTypeConfiguration<PaymentTransaction>
    {
        public void Configure(EntityTypeBuilder<PaymentTransaction> item)
        {
            item.HasBaseType((Type)null);

            item.HasQueryFilter(i => i.IsDeleted == false && i.ReisId != null);
        }
    }
}