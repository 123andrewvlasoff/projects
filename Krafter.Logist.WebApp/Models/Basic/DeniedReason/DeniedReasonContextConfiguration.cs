﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.DeniedReason
{
    public class
        DeniedReasonContextConfiguration : IEntityTypeConfiguration<
            DAL.Models.Requests.Krafter.Dictionaries.DeniedReason>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Requests.Krafter.Dictionaries.DeniedReason> item)
        {
        }
    }
}