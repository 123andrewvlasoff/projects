using System;

namespace Krafter.Logist.WebApp.Models.Basic.ReisesPriceByQuarter
{
    public class ReisesPriceByQuarter
    {
        public YearQuarter YearQuarter { get; set; }

        public double Min { get; set; }

        public double Max { get; set; }

        public double Average { get; set; }
    }

    public class ReisPrice
    {
        public double Price { get; set; }

        public DateTime Date { get; set; }
    }

    public class YearQuarter
    {
        public int Year { get; set; }

        public int Quarter { get; set; }
    }
}