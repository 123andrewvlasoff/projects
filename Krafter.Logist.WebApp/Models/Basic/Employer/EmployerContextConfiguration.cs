using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Employer
{
    public class EmployerContextConfiguration : IEntityTypeConfiguration<Employer>
    {
        public void Configure(EntityTypeBuilder<Employer> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(p => p.Position);

            item.Ignore(p => p.IsActive);

            item.Ignore(p => p.CreatedAt);
            
            item.Ignore(p => p.UpdatedAt);
        }
    }
}