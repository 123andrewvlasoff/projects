﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.Employer
{
    /// <summary>
    ///     Represents the model configuration for Employer.
    /// </summary>
    public class EmployerOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<Employer>(nameof(Employer)).EntityType;

            item.DerivesFromNothing();

            item.Ignore(p => p.Position);

            item.Ignore(p => p.IsActive);

            item.Ignore(p => p.CreatedAt);
            
            item.Ignore(p => p.UpdatedAt);

            item.Ignore(p => p.ExternalId);

            item.Ignore(p => p.Login);

            item.HasKey(p => p.Id);
        }
    }
}