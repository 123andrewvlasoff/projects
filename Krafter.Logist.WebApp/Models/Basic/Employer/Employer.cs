namespace Krafter.Logist.WebApp.Models.Basic.Employer
{
    public class Employer : DAL.Models.Users.Employer
    {
        public new Employer ParentEmployer { get; set; }

        public new User.User User { get; set; }
    }
}