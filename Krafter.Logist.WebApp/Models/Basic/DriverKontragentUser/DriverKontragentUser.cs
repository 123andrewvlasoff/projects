namespace Krafter.Logist.WebApp.Models.Basic.DriverKontragentUser
{
    public class DriverKontragentUser : DAL.Models.Drivers.DriverKontragentUser
    {
        public new DriverKontragent.DriverKontragent DriverKontragent { get; set; }
    }
}