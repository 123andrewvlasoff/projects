using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.DriverKontragentUser
{
    public class DriverKontragentUserContextConfiguration : IEntityTypeConfiguration<DriverKontragentUser>
    {
        public void Configure(EntityTypeBuilder<DriverKontragentUser> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}