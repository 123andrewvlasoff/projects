﻿namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyComment
{
    public class KrafterRequestReplyComment : DAL.Models.Requests.Krafter.RequestReplyComment
    {
        public new KrafterRequestReply.KrafterRequestReply RequestReply { get; set; }

        public new KrafterRequestReplyAction.KrafterRequestReplyAction Action { get; set; }
    }
}