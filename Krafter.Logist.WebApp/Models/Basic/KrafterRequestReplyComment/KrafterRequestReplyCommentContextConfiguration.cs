﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyComment
{
    public class KrafterRequestReplyCommentContextConfiguration : IEntityTypeConfiguration<KrafterRequestReplyComment>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestReplyComment> item)
        {
            item.HasOne(i => i.Action);

            item.Property(b => b.DeniedReasonId).HasColumnName("DeniedReasonId");
        }
    }
}