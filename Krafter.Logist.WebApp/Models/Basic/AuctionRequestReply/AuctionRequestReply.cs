﻿namespace Krafter.Logist.WebApp.Models.Basic.AuctionRequestReply
{
    public class AuctionRequestReply : RequestReply.RequestReply
    {
        public decimal? MinPrice { get; set; }

        public int? AutoBidding { get; set; }
    }
}