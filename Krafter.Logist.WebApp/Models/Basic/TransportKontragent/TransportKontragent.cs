using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragent
{
    public class TransportKontragent : DAL.Models.Transports.TransportKontragent
    {
        public new Transport.Transport Transport { get; set; }

        public new Kontragent.Kontragent Kontragent { get; set; }

        public new ICollection<TransportKontragentUser.TransportKontragentUser> TransportKontragentUsers { get; set; }

        [NotMapped] public RelationType RelationType { get; set; }
    }
}