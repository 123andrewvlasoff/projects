using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragent
{
    public class TransportKontragentContextConfiguration : IEntityTypeConfiguration<TransportKontragent>
    {
        public void Configure(EntityTypeBuilder<TransportKontragent> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(i => i.TransportId1C);
            item.Ignore(i => i.KontragentId1C);
        }
    }
}