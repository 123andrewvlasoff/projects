using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragent
{
    /// <summary>
    ///     Represents the model configuration for TransportKontragent.
    /// </summary>
    public class TransportKontragentOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<TransportKontragent>(nameof(TransportKontragent)).EntityType;

            item.OrderBy();

            item.Ignore(p => p.TransportId1C);
            item.Ignore(p => p.KontragentId1C);

            item.EnumProperty(p => p.RelationType);

            item.Collection.Function("SuitableInfo")
                .ReturnsFromEntitySet<SuitableInfoDto.SuitableInfoDto>(nameof(SuitableInfoDto));

            item.Function("SuitableRequests")
                .ReturnsCollectionFromEntitySet<TransportSuitableRequest.TransportSuitableRequest>(
                    nameof(TransportSuitableRequest.TransportSuitableRequest));

            item.HasKey(p => p.TransportId);
            item.HasKey(p => p.KontragentId);
        }
    }
}