﻿using System;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragent.SuitableInfoDto
{
    public class SuitableInfoDto
    {
        public DateTime? LastReisDate { get; set; }

        public TransportKontragent TransportKontragent { get; set; }

        public string LastAddress { get; set; }

        public double Volume { get; set; }

        public double Weight { get; set; }

        public int RollingStockType { get; set; }

        public double? LastReisLat { get; set; }

        public double? LastReisLon { get; set; }
    }
}