using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyFile
{
    public class RequestReplyFileContextConfiguration : IEntityTypeConfiguration<RequestReplyFile>
    {
        public void Configure(EntityTypeBuilder<RequestReplyFile> item)
        {
            item.HasDiscriminator().HasValue(Folders.RequestReply);
        }
    }
}