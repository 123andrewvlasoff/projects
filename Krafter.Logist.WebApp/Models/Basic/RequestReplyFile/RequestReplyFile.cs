namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyFile
{
    public class RequestReplyFile : DAL.Models.Requests.RequestReplyFile
    {
        public new RequestReply.RequestReply Reply { get; set; }
    }
}