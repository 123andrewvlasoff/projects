﻿namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestFile
{
    public class TransporeonRequestFile : RequestFile.RequestFile
    {
        public new TransporeonRequest.TransporeonRequest Request { get; set; }
    }
}
