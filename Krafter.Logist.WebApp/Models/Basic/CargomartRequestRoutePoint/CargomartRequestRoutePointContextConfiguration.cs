﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestRoutePoint
{
    public class CargomartRequestRoutePointContextConfiguration : IEntityTypeConfiguration<CargomartRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<CargomartRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<CargomartRequestRoutePoint>(RequestDiscriminator.Cargomart);
        }
    }
}