﻿namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestRoutePoint
{
    public class CargomartRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new CargomartRequest.CargomartRequest Request { get; set; }
    }
}