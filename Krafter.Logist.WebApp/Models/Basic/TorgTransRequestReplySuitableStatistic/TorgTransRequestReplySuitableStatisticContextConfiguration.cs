﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReplySuitableStatistic
{
    public class TorgTransRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<
        TorgTransRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<TorgTransRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}