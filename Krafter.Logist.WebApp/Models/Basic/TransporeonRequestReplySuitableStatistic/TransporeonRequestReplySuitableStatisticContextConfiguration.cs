﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReplySuitableStatistic
{
    public class
    TransporeonRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<TransporeonRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<TransporeonRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
