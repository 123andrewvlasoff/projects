﻿using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReplySuitableStatistic
{
    public class TransporeonRequestReplySuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestReplyStatistic Data { get; set; }
    }
}
