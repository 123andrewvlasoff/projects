﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestRoutePoint
{
    public class TrafficRoutePointContextConfiguration : IEntityTypeConfiguration<TrafficRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<TrafficRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TrafficRequestRoutePoint>(RequestDiscriminator.TrafficOnline);
        }
    }
}