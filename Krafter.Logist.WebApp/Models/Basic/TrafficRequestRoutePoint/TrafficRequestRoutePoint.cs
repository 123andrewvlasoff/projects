﻿using System;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestRoutePoint
{
    public class TrafficRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new TrafficRequest.TrafficRequest Request { get; set; }

        public DateTime? CarSupplyAt { get; set; }
    }
}