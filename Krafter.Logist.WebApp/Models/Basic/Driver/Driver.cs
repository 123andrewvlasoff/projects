using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.Driver
{
    public class Driver : DAL.Models.Drivers.Driver
    {
        public new ICollection<DriverKontragent.DriverKontragent> DriverKontragents { get; set; }
    }
}