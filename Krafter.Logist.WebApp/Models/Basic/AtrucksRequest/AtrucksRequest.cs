﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequest
{
    public class AtrucksRequest : AuctionRequest.AuctionRequest
    {
        [NotMapped]
        [Computed]
        public AtrucksRequestRoutePoint.AtrucksRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public AtrucksRequestRoutePoint.AtrucksRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<AtrucksRequestRoutePoint.AtrucksRequestRoutePoint> RoutePoints { get; set; }

        public new ICollection<AtrucksRequestReply.AtrucksRequestReply> Replies { get; set; }
    }
}
