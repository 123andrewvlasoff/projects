﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequest
{
    public class AtrucksRequestContextConfiguration : IEntityTypeConfiguration<AtrucksRequest>
    {
        public void Configure(EntityTypeBuilder<AtrucksRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<AtrucksRequest>(RequestDiscriminator.Atrucks);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}
