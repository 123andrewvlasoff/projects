﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.DriverKontragent
{
    /// <summary>
    ///     Represents the model configuration for DriverKontragent.
    /// </summary>
    public class DriverKontragentOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<DriverKontragent>(nameof(DriverKontragent)).EntityType;

            item.OrderBy();

            item.Ignore(p => p.DriverId1C);
            item.Ignore(p => p.KontragentId1C);

            item.EnumProperty(p => p.RelationType);


            item.HasKey(p => p.DriverId);
            item.HasKey(p => p.KontragentId);
        }
    }
}