using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.DriverKontragent
{
    public class DriverKontragentContextConfiguration : IEntityTypeConfiguration<DriverKontragent>
    {
        public void Configure(EntityTypeBuilder<DriverKontragent> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(p => p.DriverId1C);
            item.Ignore(p => p.KontragentId1C);
        }
    }
}