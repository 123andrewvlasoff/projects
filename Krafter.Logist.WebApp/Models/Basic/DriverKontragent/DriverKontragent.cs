using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.DriverKontragent
{
    public class DriverKontragent : DAL.Models.Drivers.DriverKontragent
    {
        public new Driver.Driver Driver { get; set; }

        public new Kontragent.Kontragent Kontragent { get; set; }

        public new ICollection<DriverKontragentUser.DriverKontragentUser> DriverKontragentUsers { get; set; }

        [NotMapped] [Computed] public RelationType RelationType { get; set; }
    }
}