using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyChoice
{
    public class RequestReplyChoiceContextConfiguration : IEntityTypeConfiguration<RequestReplyChoice>
    {
        public void Configure(EntityTypeBuilder<RequestReplyChoice> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}