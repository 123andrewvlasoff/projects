namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyChoice
{
    public class RequestReplyChoice : DAL.Models.Requests.Guaranties.RequestReplyChoice
    {
        public new Driver.Driver Driver { get; set; }

        public new KontragentContact.KontragentContact KontragentContact { get; set; }

        public new Kontragent.Kontragent Kontragent { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        public new User.User User { get; set; }
    }
}