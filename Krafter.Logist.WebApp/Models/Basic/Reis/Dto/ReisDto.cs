﻿using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.Reis.Dto
{
    public class ReisDto : Reis
    {
        public Employer.Employer PinnedEmployer { get; set; }
        
        public ICollection<BundleComment.BundleComment> BundleComments { get; set; }
    }
}