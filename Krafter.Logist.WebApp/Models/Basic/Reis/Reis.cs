using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Enums;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.Reis
{
    public class Reis : DAL.Models.Requests.Reis.Reis
    {
        public new KontragentContact.KontragentContact KontragentContact { get; set; }

        public new Kontragent.Kontragent Client { get; set; }

        public new KrafterRequest.KrafterRequest Request { get; set; }

        public new Kontragent.Kontragent Carrier { get; set; }

        public new Driver.Driver Driver { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        [Computed][NotMapped] public bool? HasProblems => Problems.Any(x => x.Status == 0);

        [Computed][NotMapped] public bool? HasBlocks => PaymentTransactions.Any(c => c.PayTypeId == 17);

        [NotMapped]
        [Computed]
        public bool? HasClaims => Claims.Any(x => x.Status == 1);

        [NotMapped]
        [Computed]
        public ReisRoutePoint.ReisRoutePoint Load => RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public ReisRoutePoint.ReisRoutePoint Unload => RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<ReisRoutePoint.ReisRoutePoint> RoutePoints { get; set; }
        
        public new ICollection<ReisDispatchingPoint.ReisDispatchingPoint> DispatchingPoints { get; set; }

        [NotMapped]
        [Computed]
        public ReisRoutePoint.ReisRoutePoint DocsReady =>
            RoutePoints?.FirstOrDefault(y => y.PointType == PointType.DocsReady);

        [NotMapped]
        [Computed]
        public PaymentTransaction.PaymentTransaction PayFact => PaymentTransactions?.OrderByDescending(o => o.CreatedAt)
            .FirstOrDefault(pt => pt.PayTypeId != 7 && pt.PayTypeId != 17 && pt.PaySum < 0);

        [NotMapped]
        [Computed]
        public PaymentTransaction.PaymentTransaction PayPlan =>
            PaymentTransactions?.FirstOrDefault(pt => pt.PayTypeId == 7);

        public new ICollection<PaymentTransaction.PaymentTransaction> PaymentTransactions { get; set; } =
            new List<PaymentTransaction.PaymentTransaction>();

        public new ICollection<Problem.Problem> Problems { get; set; } =
            new List<Problem.Problem>();

        public new ICollection<ReisFile.ReisFile> Files { get; set; }

        public new ICollection<Claim.Claim> Claims { get; set; } = new List<Claim.Claim>();
    }
}