﻿using Krafter.Logist.WebApp.Models.Basic.Reis.Dto;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.Reis
{
    /// <summary>
    ///     Represents the model configuration for RollingStockType.
    /// </summary>
    public class ReisOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<Reis>(nameof(Reis)).EntityType;

            item.DerivesFromNothing();

            item.OrderBy();

            item.Ignore(i => i.ExternalId);

            item.Collection.Function("List").ReturnsCollectionFromEntitySet<ReisDto>(nameof(ReisDto));

            item.Function("Cancel")
                .ReturnsFromEntitySet<KrafterRequestAction.KrafterRequestAction>(
                    nameof(KrafterRequestAction.KrafterRequestAction));

            item.HasRequired(i => i.Unload).OrderBy();

            item.HasRequired(i => i.Load).OrderBy();

            item.Property(p => p.HasBlocks);

            item.Property(p => p.HasClaims);

            item.Property(p => p.HasProblems);

            item.ContainsOptional(i => i.DocsReady).OrderBy();
            item.ContainsOptional(i => i.PayFact).OrderBy();
            item.ContainsOptional(i => i.PayPlan).OrderBy();

            item.HasKey(p => p.Id);
        }
    }
}