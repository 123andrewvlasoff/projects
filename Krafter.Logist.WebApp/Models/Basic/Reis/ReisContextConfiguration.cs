using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Reis
{
    public class ReisContextConfiguration : IEntityTypeConfiguration<Reis>
    {
        public void Configure(EntityTypeBuilder<Reis> item)
        {
            item.Ignore(i => i.UpdaterId);

            item.Ignore(i => i.AssistantId);
        }
    }
}