using System;
using AutoMapper;

namespace Krafter.Logist.WebApp.Models.Basic.Reis
{
    public class ReisValidator : Profile
    {
        public ReisValidator()
        {
            CreateMap<Reis, Reis>()
                .ForMember(dest => dest.PaymentDatePlan,
                    opt => opt.MapFrom<ReisPaymentPlanDateResolver>());
        }

        public class ReisPaymentPlanDateResolver : IValueResolver<Reis, Reis, DateTime?>
        {
            public DateTime? Resolve(Reis source, Reis destination, DateTime? date, ResolutionContext context)
            {
                return source?.DocsReady != null ? source?.PaymentDatePlan : null;
            }
        }
    }
}