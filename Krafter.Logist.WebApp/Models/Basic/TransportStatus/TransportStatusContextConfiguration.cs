using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransportStatus
{
    public class
        TransportStatusContextConfiguration : IEntityTypeConfiguration<
            DAL.Models.Transports.Dictionaries.TransportStatus>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Transports.Dictionaries.TransportStatus> item)
        {
        }
    }
}