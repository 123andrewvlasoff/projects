﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequest
{
    public class TrafficRequestContextConfiguration : IEntityTypeConfiguration<TrafficRequest>
    {
        public void Configure(EntityTypeBuilder<TrafficRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator).HasValue<TrafficRequest>(RequestDiscriminator.TrafficOnline);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}