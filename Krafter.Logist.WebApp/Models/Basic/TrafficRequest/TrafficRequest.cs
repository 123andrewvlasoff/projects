﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequest
{
    public class TrafficRequest : AuctionRequest.AuctionRequest
    {
        public int Signature { get; set; }

        public int IsInterval { get; set; }

        public int ResourceAssignmentTime { get; set; }

        public int CircularRoute { get; set; }

        [NotMapped]
        [Computed]
        public TrafficRequestRoutePoint.TrafficRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public TrafficRequestRoutePoint.TrafficRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<TrafficRequestRoutePoint.TrafficRequestRoutePoint> RoutePoints { get; set; }

        public new ICollection<TrafficRequestReply.TrafficRequestReply> Replies { get; set; }

        public new ICollection<TrafficRequestFile.TrafficRequestFile> Files { get; set; }
    }
}