﻿using System;
using DAL.Models.Requests.Reis.Enums;
using Mapster.Utils;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.Kontragent
{
    /// <summary>
    ///     Represents the model configuration for Kontragent.
    /// </summary>
    public class KontragentOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<Kontragent>(nameof(Kontragent)).EntityType;

            item.OrderBy();

            item.Ignore(i => i.ExternalId);

            item.Ignore(i => i.KontragentDrafts);

            item.Collection.Function("UserReisClients").ReturnsCollectionFromEntitySet<Kontragent>(nameof(Kontragent));

            item.Collection.Function("RequestsClients").ReturnsCollectionFromEntitySet<Kontragent>(nameof(Kontragent));

            item.Ignore(i => i.Kpp);

            item.Ignore(i => i.Ogrn);

            item.Ignore(i => i.Okpo);

            item.Ignore(i => i.ManagerFio);

            item.Ignore(i => i.CreatedAt);

            item.Ignore(i => i.UpdatedAt);

            item.EnumProperty(p => p.RelationType);

            item.EnumProperty(p => p.VatType);

            item.Action("UserAgreementPdf");

            item.Action("PublicOfferPdf");

            item.Action("AgreementOnTheProcessingOfPersonalDataPdf");

            item.Action("PolicyTheProcessingOfPersonalDataPdf");

            item.HasKey(p => p.Id);
        }
    }
}