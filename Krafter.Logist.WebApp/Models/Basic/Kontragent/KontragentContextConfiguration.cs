using DAL.Models.Kontragents.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Kontragent
{
    public class KontragentContextConfiguration : IEntityTypeConfiguration<Kontragent>
    {
        public void Configure(EntityTypeBuilder<Kontragent> item)
        {
            item.HasDiscriminator(i => i.Discriminator).HasValue(CompanyType.Kontragent);

            item.Ignore(i => i.ManagerFio);

            item.Ignore(i => i.CreatedAt);
            
            item.Ignore(i => i.UpdatedAt);
        }
    }
}