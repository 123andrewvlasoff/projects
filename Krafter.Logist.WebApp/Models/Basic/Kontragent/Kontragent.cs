using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models;
using DAL.Models.Drafts.Enums;
using DAL.Models.Drafts.Kontragents;
using DAL.Models.Enums;
using DAL.Models.Requests.Reis.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.Kontragent
{
    public class Kontragent : DAL.Models.Kontragents.Kontragent
    {
        public new ICollection<KontragentContact.KontragentContact> Contacts { get; set; }

        public new ICollection<TransportKontragent.TransportKontragent> TransportKontragents { get; set; }

        [NotMapped] public RelationType RelationType { get; set; }

        public new ICollection<KontragentUser.KontragentUser> KontragentUsers { get; set; }

        public new ICollection<KontragentDraft> KontragentDrafts { get; set; }

        public new ICollection<DriverKontragent.DriverKontragent> DriverKontragents { get; set; }

        public new ICollection<KontragentFile.KontragentFile> KontragentFiles { get; set; }

        public new ICollection<KontragentContract.KontragentContract> KontragentContracts { get; set; }

        [NotMapped]
        public VatType? VatType => KontragentContracts != null && KontragentContracts.Any(x => x.IsActive == 1)
            ? KontragentContracts.Where(x => x.IsActive == 1)
                .OrderBy(x => x.CrafterCompany?.PriorityVat)
                .Select(x => x.VatType).FirstOrDefault()
            : DAL.Models.Requests.Reis.Enums.VatType.None;
    }
}