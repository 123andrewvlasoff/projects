﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Notification
{
    public class NotificationContextConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> item)
        {
            item.HasBaseType((Type)null);

            item.HasOne(i => i.User);
        }
    }
}