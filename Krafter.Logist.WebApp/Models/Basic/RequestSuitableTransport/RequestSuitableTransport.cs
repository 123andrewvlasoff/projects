﻿using System;
using System.Collections.Generic;
using DAL.Models.Transports.Enums;
using Microsoft.AspNet.OData.Query;

namespace Krafter.Logist.WebApp.Models.Basic.RequestSuitableTransport
{
    [OrderBy]
    public class RequestSuitableTransport
    {
        public Guid Id { get; set; }

        public Guid TransportId { get; set; }

        public TimeSpan RequestDateInterval { get; set; }

        public Guid DriverId { get; set; }

        public Driver.Driver Driver { get; set; }

        public Transport.Transport Transport { get; set; }

        public Guid? ConnectedTransportId { get; set; }

        public Transport.Transport ConnectedTransport { get; set; }

        public Guid CarrierId { get; set; }

        public Kontragent.Kontragent Carrier { get; set; }

        public string UnloadAddress { get; set; }

        public int? RollingStockTypeId { get; set; }

        public DateTime UnloadDate { get; set; }

        public double Remoteness { get; set; }

        public Employer.Employer PinnedEmployer { get; set; }

        public Owner? Owner { get; set; }

        public Guid? StatusId { get; set; }

        public DAL.Models.Requests.Reis.Dictionaries.ReisStatus Status { get; set; }

        public Guid? TransportStatusId { get; set; }

        public DAL.Models.Transports.Dictionaries.TransportStatus TransportStatus { get; set; }

        public ICollection<BundleComment.BundleComment> BundleComments { get; set; }

        public string UnloadPointAddress { get; set; }
        public DateTime? UnloadPointDate { get; set; }
        public double? UnloadPointLat { get; set; }
        public double? UnloadPointLon { get; set; }
        public int? BundleCommentCount { get; set; }
        
        public  int? KontragentContactId { get; set; }
        public  KontragentContact.KontragentContact  KontragentContact { get; set; }
    }
}