﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePointAction
{
    public class KrafterRequestRoutePointAction : DAL.Models.Requests.Krafter.Actions.RequestRoutePointAction
    {
        [ForeignKey("ActionUserId")]
        public new User.User User { get; set; }
    }
}