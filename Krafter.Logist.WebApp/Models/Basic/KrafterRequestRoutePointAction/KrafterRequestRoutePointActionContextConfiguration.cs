﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePointAction
{
    public class
        KrafterRequestRoutePointActionContextConfiguration : IEntityTypeConfiguration<KrafterRequestRoutePointAction>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestRoutePointAction> item)
        {
            item.HasBaseType(typeof(Action));

            item.HasDiscriminator().HasValue("RequestRoutePointAction");
        }
    }
}