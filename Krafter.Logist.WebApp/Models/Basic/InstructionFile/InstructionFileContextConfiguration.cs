﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.InstructionFile
{
    public class InstructionFileContextConfiguration : IEntityTypeConfiguration<InstructionFile>
    {
        public void Configure(EntityTypeBuilder<InstructionFile> item)
        {
            item.HasDiscriminator();
        }
    }
}