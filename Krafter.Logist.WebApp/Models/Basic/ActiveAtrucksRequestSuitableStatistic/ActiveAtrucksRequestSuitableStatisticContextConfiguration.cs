﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace Krafter.Logist.WebApp.Models.Basic.ActiveAtrucksRequestSuitableStatistic
{
    public class
ActiveAtrucksRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
    ActiveAtrucksRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveAtrucksRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
