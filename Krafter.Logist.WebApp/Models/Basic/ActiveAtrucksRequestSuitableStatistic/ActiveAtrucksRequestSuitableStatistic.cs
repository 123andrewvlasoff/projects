﻿using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveAtrucksRequestSuitableStatistic
{
    public class ActiveAtrucksRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic Data { get; set; }
    }
}
