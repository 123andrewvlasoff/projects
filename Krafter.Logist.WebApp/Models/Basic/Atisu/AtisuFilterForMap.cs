using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.ExtendedProperties;

namespace Krafter.Logist.WebApp.Models.Basic.Atisu;

public class AtisuFilterForMap
{
    public ICollection<Guid> Ids { get; set; }
    public int Remoteness { get; set; }
}