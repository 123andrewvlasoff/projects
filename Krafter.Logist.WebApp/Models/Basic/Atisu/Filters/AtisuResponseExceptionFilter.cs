﻿using Krafter.Client.Atisu.Client.Models;
using Krafter.ServiceCommon.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krafter.Logist.WebApp.Models.Basic.Atisu.Filters
{
    public class AtisuResponseExceptionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is AtisuApiException exception)
            {
                context.Result = new ObjectResult(exception.Response)
                {
                    StatusCode = HttpHelper.Status299Custom
                };
                context.ExceptionHandled = true;
            }
        }
    }
}
