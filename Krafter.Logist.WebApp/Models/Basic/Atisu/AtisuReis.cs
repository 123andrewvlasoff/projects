﻿using System;

namespace Krafter.Logist.WebApp.Models.Basic.Atisu
{
    internal class AtisuReis
    {
        public Guid Id { get; set; }
        public double Volume { get; set; }
        public double Weight { get; set; }
        public int? RollingStockTypeId { get; set; }
        public Point Load { get; set; }
        public Point Unload { get; set; }

        internal class Point
        {
            public string Title { get; set; }
            public double? Lat { get; set; }
            public double? Lon { get; set; }
            public DateTime ArrivalDatePlan { get; set; }
            public DateTime LeaveDatePlan { get; set; }
        }
    }
}
