﻿namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestFile
{
    public class TrafficRequestFile : RequestFile.RequestFile
    {
        public new TrafficRequest.TrafficRequest Request { get; set; }
    }
}