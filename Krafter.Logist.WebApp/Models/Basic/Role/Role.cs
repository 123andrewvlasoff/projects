using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.Role
{
    public class Role : DAL.Models.Users.Role
    {
        protected internal new ICollection<UserRole.UserRole> UserRoles { get; set; }
    }
}