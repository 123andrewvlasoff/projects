﻿using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.MessageType
{
    public class MessageType : DAL.Models.SupportTickets.MessageType
    {
        public new MessageType Parent { get; set; }

        public new ICollection<MessagesEmailType.MessagesEmailType> MessagesEmailTypes { get; set; }
    }
}