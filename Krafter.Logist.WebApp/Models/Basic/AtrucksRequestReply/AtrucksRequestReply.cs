﻿namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestReply
{
    public class AtrucksRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new AtrucksRequest.AtrucksRequest Request { get; set; }
    }
}
