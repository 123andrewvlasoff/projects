﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestReply
{
    public class AtrucksRequestReplyContextConfiguration : IEntityTypeConfiguration<AtrucksRequestReply>
    {
        public void Configure(EntityTypeBuilder<AtrucksRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<AtrucksRequestReply>(RequestDiscriminator.Atrucks);
        }
    }
}
