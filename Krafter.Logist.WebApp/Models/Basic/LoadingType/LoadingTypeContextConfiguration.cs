using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.LoadingType
{
    public class LoadingTypeContextConfiguration : IEntityTypeConfiguration<DAL.Models.Dictionaries.LoadingType>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Dictionaries.LoadingType> item)
        {
            item.Ignore(i => i.SortOrder);
        }
    }
}