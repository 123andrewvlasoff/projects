﻿namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestFile
{
    public class TorgTransRequestFile : RequestFile.RequestFile
    {
        public new TorgTransRequest.TorgTransRequest Request { get; set; }
    }
}