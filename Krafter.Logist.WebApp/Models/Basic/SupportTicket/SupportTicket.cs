﻿using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.SupportTicket
{
    public class SupportTicket : DAL.Models.SupportTickets.SupportTicket
    {
        public new User.User User { get; set; }

        public new User.User SupportUser { get; set; }

        public new MessageType.MessageType MessageType { get; set; }

        public new ICollection<Message.Message> Messages { get; set; }
    }
}