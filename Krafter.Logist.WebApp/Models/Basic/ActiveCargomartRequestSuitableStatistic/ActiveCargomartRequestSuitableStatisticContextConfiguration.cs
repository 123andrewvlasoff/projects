﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveCargomartRequestSuitableStatistic
{
    public class
        ActiveCargomartRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
            ActiveCargomartRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveCargomartRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}