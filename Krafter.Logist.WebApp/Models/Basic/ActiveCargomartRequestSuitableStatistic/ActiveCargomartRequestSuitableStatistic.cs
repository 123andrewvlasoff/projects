﻿using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveCargomartRequestSuitableStatistic
{
    public class ActiveCargomartRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic Data { get; set; }
    }
}