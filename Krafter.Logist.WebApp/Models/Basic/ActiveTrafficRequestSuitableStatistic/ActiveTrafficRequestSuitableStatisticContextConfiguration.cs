﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveTrafficRequestSuitableStatistic
{
    public class ActiveTrafficRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
        ActiveTrafficRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveTrafficRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}