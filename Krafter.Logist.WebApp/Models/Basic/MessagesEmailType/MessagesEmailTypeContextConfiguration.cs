﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.MessagesEmailType
{
    public class MessagesEmailTypeContextConfiguration : IEntityTypeConfiguration<MessagesEmailType>
    {
        public void Configure(EntityTypeBuilder<MessagesEmailType> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}