using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models;
using DAL.Models.Atisu;
using DAL.Models.Comment;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests;
using Krafter.Logist.WebApp.Models.Basic.ActiveCargomartRequestSuitableStatistic;
using DAL.Models.Requests.Krafter.Actions;
using DAL.Models.Requests.Reis;
using Krafter.Carrier.WebApp.Models.Basic.ReisDispatchingPoint;
using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveTorgTransRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveTrafficRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.AuctionRequest;
using Krafter.Logist.WebApp.Models.Basic.BundleComment;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequest;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestFile;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestReply;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.CarrierKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.CarrierUserRequest;
using Krafter.Logist.WebApp.Models.Basic.DeniedReason;
using Krafter.Logist.WebApp.Models.Basic.Claim;
using Krafter.Logist.WebApp.Models.Basic.Driver;
using Krafter.Logist.WebApp.Models.Basic.DriverKontragent;
using Krafter.Logist.WebApp.Models.Basic.DriverKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.InstructionFile;
using Krafter.Logist.WebApp.Models.Basic.Kontragent;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.KontragentContactUser;
using Krafter.Logist.WebApp.Models.Basic.KontragentUser;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyComment;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyDraft;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePointAction;
using Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.LoadingType;
using Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.LogistUserRequest;
using Krafter.Logist.WebApp.Models.Basic.Message;
using Krafter.Logist.WebApp.Models.Basic.MessageFile;
using Krafter.Logist.WebApp.Models.Basic.MessagesEmailType;
using Krafter.Logist.WebApp.Models.Basic.MessageType;
using Krafter.Logist.WebApp.Models.Basic.Notification;
using Krafter.Logist.WebApp.Models.Basic.PaymentBlock;
using Krafter.Logist.WebApp.Models.Basic.PaymentTransaction;
using Krafter.Logist.WebApp.Models.Basic.Problem;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Models.Basic.ReisFile;
using Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.ReisStatus;
using Krafter.Logist.WebApp.Models.Basic.Request;
using Krafter.Logist.WebApp.Models.Basic.RequestFile;
using Krafter.Logist.WebApp.Models.Basic.RequestReply;
using Krafter.Logist.WebApp.Models.Basic.RequestReplyChoice;
using Krafter.Logist.WebApp.Models.Basic.RequestReplyComment;
using Krafter.Logist.WebApp.Models.Basic.RequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.RequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.Role;
using Krafter.Logist.WebApp.Models.Basic.RollingStockType;
using Krafter.Logist.WebApp.Models.Basic.SuitableRequestsSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.SuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.SupportTicket;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequest;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestFile;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequest;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestFile;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestReply;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragent;
using Krafter.Logist.WebApp.Models.Basic.TransportStatus;
using Krafter.Logist.WebApp.Models.Basic.TransportType;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.Basic.UserRequest;
using Krafter.Logist.WebApp.Models.Basic.UserRole;
using Krafter.ServiceCommon.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Krafter.Logist.WebApp.Models.Basic.KontragentContract;
using Krafter.ServiceCommon.Services.ChangesTrackService.Helpers;
using Krafter.ServiceCommon.Services.ChangesTrackService.Models;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequest;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReply;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveTransporeonRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.BaltikaRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.Company;
using Krafter.Logist.WebApp.Models.Basic.CrafterCompany;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestFile;
using Krafter.ServiceCommon.Models;

namespace Krafter.Logist.WebApp.Models.Basic
{
    public class LogistDbContext : IdentityDbContext<
        User.User, Role.Role, string,
        IdentityUserClaim<string>, UserRole.UserRole, IdentityUserLogin<string>,
        IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public string UserId { get; set; }

        public LogistDbContext(DbContextOptions<LogistDbContext> options) : base(options)
        {
        }

        public Task<IEnumerable<PropertyChanges>> SaveChangesWithTrackingAsync<T>(
            CancellationToken cancellationToken = new CancellationToken(), bool acceptAllChangesOnSuccess = true)
            where T : class
        {
            var changes =
                ChangesTrackHelper.TrackChanges(ChangeTracker.Entries()
                    .FirstOrDefault(i => i.Entity.GetType().FullName == typeof(T).FullName));

            var task = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

            return task.ContinueWith(t => t.Result > 0 && changes != null ? changes : new List<PropertyChanges>(),
                cancellationToken);
        }

        public DbSet<ReisDispatchingPoint.ReisDispatchingPoint> ReisDispatchingPoints { get; set; }

        public DbSet<Kontragent.Kontragent> Kontragents { get; set; }

        public DbSet<KontragentContact.KontragentContact> KontragentContacts { get; set; }
        public DbSet<SupportTicket.SupportTicket> SupportTickets { get; set; }

        public DbSet<KrafterRequest.KrafterRequest> KrafterRequests { get; set; }

        public DbSet<KrafterRequestAction.KrafterRequestAction> RequestActions { get; set; }

        public DbSet<KrafterRequestReplyComment.KrafterRequestReplyComment> RequestReplyComments { get; set; }

        public DbSet<KrafterRequestReplyAction.KrafterRequestReplyAction> RequestReplyActions { get; set; }

        public DbSet<KrafterRequestReply.KrafterRequestReply> KrafterRequestReplies { get; set; }

        public DbSet<RequestReplyChoice.RequestReplyChoice> RequestReplyChoices { get; set; }

        public DbSet<KrafterRequestReplyDraft.KrafterRequestReplyDraft> KrafterRequestReplyDrafts { get; set; }

        public DbSet<UserRequest.UserRequest> UserRequests { get; set; }

        public DbSet<RequestRoutePoint.RequestRoutePoint> RequestRoutePoints { get; set; }

        public DbSet<KrafterRequestRoutePoint.KrafterRequestRoutePoint> KrafterRequestRoutePoints { get; set; }

        public DbSet<DAL.Models.Dictionaries.RollingStockType> RollingStockTypes { get; set; }

        public DbSet<DAL.Models.Dictionaries.CargoType> CargoTypes { get; set; }

        public DbSet<DAL.Models.Dictionaries.LoadingType> LoadingTypes { get; set; }

        public DbSet<DAL.Models.Dictionaries.TransportType> TransportTypes { get; set; }

        public DbSet<MessageType.MessageType> MessageTypes { get; set; }

        public DbSet<MessagesEmailType.MessagesEmailType> MessagesEmailTypes { get; set; }

        public DbSet<Message.Message> Messages { get; set; }

        public DbSet<MessageFile.MessageFile> MessageFiles { get; set; }

        public DbSet<RequestReplyFile.RequestReplyFile> RequestReplyFiles { get; set; }

        public DbSet<DAL.Models.Requests.Dictionaries.Region> Regions { get; set; }

        public DbSet<DAL.Models.Requests.Dictionaries.Settlement> Settlements { get; set; }

        public DbSet<DAL.Models.Transports.Dictionaries.TransportStatus> TransportStatuses { get; set; }

        public DbSet<DAL.Models.Requests.Krafter.Dictionaries.DeniedReason> DeniedReasons { get; set; }

        public DbSet<Reis.Reis> Reises { get; set; }

        public DbSet<ReisRoutePoint.ReisRoutePoint> ReisRoutePoints { get; set; }

        public DbSet<Transport.Transport> Transports { get; set; }

        public DbSet<TransportKontragent.TransportKontragent> TransportKontragents { get; set; }

        public DbSet<Driver.Driver> Drivers { get; set; }

        public DbSet<DriverKontragent.DriverKontragent> DriverKontragents { get; set; }

        public DbSet<DriverKontragentUser.DriverKontragentUser> DriverKontragentUsers { get; set; }

        public DbSet<KontragentContactUser.KontragentContactUser> KontragentContactUsers { get; set; }

        public DbSet<KontragentContract.KontragentContract> KontragentContracts { get; set; }

        public DbSet<TransportKontragentUser.TransportKontragentUser> TransportKontragentUsers { get; set; }

        public DbSet<CargomartRequest.CargomartRequest> CargomartRequests { get; set; }

        public DbSet<CargomartRequestReply.CargomartRequestReply> CargomartRequestReplies { get; set; }

        public DbSet<TrafficRequestReply.TrafficRequestReply> TrafficRequestReplies { get; set; }

        public DbSet<KontragentFile.KontragentFile> KontragentFiles { get; set; }

        public DbSet<TrafficRequest.TrafficRequest> TrafficRequests { get; set; }

        public DbSet<TrafficRequestRoutePoint.TrafficRequestRoutePoint> TrafficRequestRoutePoints { get; set; }

        public DbSet<File> Files { get; set; }

        public DbSet<Notification.Notification> Notifications { get; set; }

        public DbSet<Employer.Employer> Employers { get; set; }

        public DbSet<ReisFile.ReisFile> ReisFiles { get; set; }

        public DbSet<TorgTransRequest.TorgTransRequest> TorgTransRequests { get; set; }

        public DbSet<TorgTransRequestRoutePoint.TorgTransRequestRoutePoint> TorgTransRoutePoints { get; set; }

        public DbSet<TorgTransRequestReply.TorgTransRequestReply> TorgTransRequestReplies { get; set; }

        public DbSet<DAL.Models.Requests.Reis.Dictionaries.ReisStatus> ReisStatuses { get; set; }

        public DbSet<DAL.Models.Transactions.Dictionaries.PayType> PayTypes { get; set; }

        public DbSet<DAL.Models.Kontragents.Dictionaries.Branch> Branches { get; set; }

        public DbSet<Claim.Claim> Claims { get; set; }

        public DbSet<PaymentBlock.PaymentBlock> PaymentBlocks { get; set; }

        public DbSet<PaymentTransaction.PaymentTransaction> PaymentTransactions { get; set; }

        public DbSet<Problem.Problem> Problems { get; set; }

        public DbSet<LogistKontragentUser.LogistKontragentUser> LogistKontragentUsers { get; set; }

        public DbSet<KontragentUser.KontragentUser> KontragentUsers { get; set; }

        public DbSet<CarrierKontragentUser.CarrierKontragentUser> CarrierKontragentUsers { get; set; }

        public DbSet<ActiveRequestSuitableStatistic.ActiveRequestSuitableStatistic> ActiveRequestSuitableStatistics
        {
            get;
            set;
        }

        public DbSet<ActiveTorgTransRequestSuitableStatistic.ActiveTorgTransRequestSuitableStatistic>
            ActiveTorgTransRequestSuitableStatistics
        { get; set; }

        public DbSet<ActiveCargomartRequestSuitableStatistic.ActiveCargomartRequestSuitableStatistic>
            ActiveCargomartRequestSuitableStatistics
        { get; set; }

        public DbSet<ActiveTrafficRequestSuitableStatistic.ActiveTrafficRequestSuitableStatistic>
            ActiveTrafficRequestSuitableStatistics
        { get; set; }

        public DbSet<RequestReplySuitableStatistic.RequestReplySuitableStatistic> RequestReplySuitableStatistics
        {
            get;
            set;
        }

        public DbSet<SuitableRequestsSuitableStatistic.SuitableRequestsSuitableStatistic>
            SuitableRequestsSuitableStatistics
        { get; set; }

        public DbSet<ListWithSuitableReisesSuitableStatistic.ListWithSuitableReisesSuitableStatistic>
            ListWithSuitableReisesSuitableStatistics
        { get; set; }

        public DbSet<SuitableStatistic.SuitableStatistic> SuitableStatistics { get; set; }

        public DbSet<CarrierUserRequest.CarrierUserRequest> CarrierUserRequests { get; set; }

        public DbSet<LogistUserRequest.LogistUserRequest> LogistUserRequests { get; set; }

        public DbSet<CargomartRequestReplySuitableStatistic.CargomartRequestReplySuitableStatistic>
            CargomartRequestReplySuitableStatistics
        { get; set; }

        public DbSet<TrafficRequestReplySuitableStatistic.TrafficRequestReplySuitableStatistic>
            TrafficRequestReplySuitableStatistics
        { get; set; }

        public DbSet<TorgTransRequestReplySuitableStatistic.TorgTransRequestReplySuitableStatistic>
            TorgTransRequestReplySuitableStatistics
        { get; set; }

        public DbSet<KrafterRequestReplyFile.KrafterRequestReplyFile> KrafterRequestReplyFiles { get; set; }

        public DbSet<CargomartRequestFile.CargomartRequestFile> CargomartRequestFiles { get; set; }

        public DbSet<InstructionFile.InstructionFile> InstructionFiles { get; set; }

        public DbSet<TrafficRequestFile.TrafficRequestFile> TrafficRequestFiles { get; set; }

        public DbSet<TorgTransRequestFile.TorgTransRequestFile> TorgTransRequestFiles { get; set; }

        public DbSet<RequestFile.RequestFile> RequestFiles { get; set; }

        public DbSet<RequestReply.RequestReply> RequestReplies { get; set; }

        public DbSet<KrafterRequestRoutePointAction.KrafterRequestRoutePointAction> KrafterRequestRoutePointActions
        {
            get;
            set;
        }

        public DbSet<KrafterRequestReplyAction.KrafterRequestReplyAction> KrafterRequestReplyActions { get; set; }

        public DbSet<Request.Request> Requests { get; set; }

        public DbSet<AuctionRequest.AuctionRequest> AuctionRequests { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<BundleComment.BundleComment> BundleComments { get; set; }


        public DbSet<TransporeonRequest.TransporeonRequest> TransporeonRequests { get; set; }
        public DbSet<TransporeonRequestRoutePoint.TransporeonRequestRoutePoint> TransporeonRoutePoints { get; set; }
        public DbSet<TransporeonRequestReply.TransporeonRequestReply> TransporeonRequestReplies { get; set; }
        public DbSet<TransporeonRequestReplySuitableStatistic.TransporeonRequestReplySuitableStatistic> TransporeonRequestReplySuitableStatistics { get; set; }
        public DbSet<ActiveTransporeonRequestSuitableStatistic.ActiveTransporeonRequestSuitableStatistic> ActiveTransporeonRequestSuitableStatistics { get; set; }
        public DbSet<TransporeonRequestFile.TransporeonRequestFile> TransporeonRequestFiles { get; set; }

        public DbSet<Addresses> Addresses { get; set; }

        public DbSet<Marginality> Marginality { get; set; }

        public DbSet<Vat> Vat { get; set; }

        public DbSet<CrafterCompany.CrafterCompany> CrafterCompanies { get; set; }

        public DbSet<BaltikaRequest.BaltikaRequest> BaltikaRequests { get; set; }
        public DbSet<ActiveBaltikaRequestSuitableStatistic.ActiveBaltikaRequestSuitableStatistic>
            ActiveBaltikaRequestSuitableStatistics
        { get; set; }

        public DbSet<AtrucksRequest.AtrucksRequest> AtrucksRequests { get; set; }
        public DbSet<ActiveAtrucksRequestSuitableStatistic.ActiveAtrucksRequestSuitableStatistic>
            ActiveAtrucksRequestSuitableStatistics
        { get; set; }

        public DbSet<MonitoringPointType> MonitoringPointTypes { get; set; }

        public DbSet<AtisuClientHistory> AtisuClientHistories { get; set; }
        public DbSet<AtisuCity> AtisuCities { get; set; }
        public DbSet<AtisuLoadingType> AtisuLoadingTypes { get; set; }
        public DbSet<AtisuCarType> AtisuCarTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Request.Request>().HasQueryFilter(i =>
                i.RequestStatus == RequestStatus.IsActive ||
                (i.RequestStatus == RequestStatus.DataTreatment ||
                 i.RequestStatus == RequestStatus.DataInput ||
                 i.RequestStatus == RequestStatus.Cancelled ||
                 i.RequestStatus == RequestStatus.Error) && (i.InProgressUserId == UserId || i.AssignedUserId == UserId));

            modelBuilder.Entity<SupportTicket.SupportTicket>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<Reis.Reis>().HasQueryFilter(r => !r.IsDeleted);

            modelBuilder.Entity<File>().HasQueryFilter(r => !r.IsDeleted);

            modelBuilder.Entity<Notification.Notification>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<KontragentUser.KontragentUser>().HasQueryFilter(p =>
                p.UserId == UserId || p.Discriminator == KontragentUserType.Carrier);

            modelBuilder.Entity<DriverKontragentUser.DriverKontragentUser>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<TransportKontragentUser.TransportKontragentUser>()
                .HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<KontragentContactUser.KontragentContactUser>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<KontragentContact.KontragentContact>().HasQueryFilter(i => i.IsActive);

            modelBuilder.Entity<RequestReply.RequestReply>()
                .HasQueryFilter(p => p.RequestId != null && !p.IsDeleted);

            modelBuilder.Entity<KrafterRequestReplyDraft.KrafterRequestReplyDraft>()
                .HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<RequestReplyChoice.RequestReplyChoice>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<UserRequest.UserRequest>().HasQueryFilter(p => p.UserId == UserId);

            modelBuilder.Entity<Comment>();

            modelBuilder.ApplyConfiguration(new DriverKontragentContextConfiguration());
            modelBuilder.ApplyConfiguration(new AuctionRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new TorgTransRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestRoutePointActionContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new DriverContextConfiguration());
            modelBuilder.ApplyConfiguration(new EmployerContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentContactUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new DriverKontragentUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new TransportKontragentUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentContactContextConfiguration());
            modelBuilder.ApplyConfiguration(new LoadingTypeContextConfiguration());
            modelBuilder.ApplyConfiguration(new ReisContextConfiguration());
            modelBuilder.ApplyConfiguration(new ReisFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new ReisRoutePointContextConfiguration());
            modelBuilder.ApplyConfiguration(new ReisStatusContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestReplyFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyDraftContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestReplyChoiceContextConfiguration());
            modelBuilder.ApplyConfiguration(new RollingStockTypeContextConfiguration());
            modelBuilder.ApplyConfiguration(new TransportContextConfiguration());
            modelBuilder.ApplyConfiguration(new TransportKontragentContextConfiguration());
            modelBuilder.ApplyConfiguration(new TransportStatusContextConfiguration());
            modelBuilder.ApplyConfiguration(new TransportTypeContextConfiguration());
            modelBuilder.ApplyConfiguration(new CargomartRequestReplyContextConfiguration());
            modelBuilder.ApplyConfiguration(new TrafficRequestReplyContextConfiguration());
            modelBuilder.ApplyConfiguration(new SupportTicketContextConfiguration());
            modelBuilder.ApplyConfiguration(new MessagesEmailTypeContextConfiguration());
            modelBuilder.ApplyConfiguration(new MessageTypeContextConfiguration());
            modelBuilder.ApplyConfiguration(new MessageContextConfiguration());
            modelBuilder.ApplyConfiguration(new MessageFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new CargomartRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new TrafficRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationContextConfiguration());
            modelBuilder.ApplyConfiguration(new UserContextConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleContextConfiguration());
            modelBuilder.ApplyConfiguration(new RoleContextConfiguration());
            modelBuilder.ApplyConfiguration(new MessageFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new ClaimContextConfiguration());
            modelBuilder.ApplyConfiguration(new PaymentBlockContextConfiguration());
            modelBuilder.ApplyConfiguration(new ProblemContextConfiguration());
            modelBuilder.ApplyConfiguration(new PaymentTransactionContextConfiguration());
            modelBuilder.ApplyConfiguration(new DeniedReasonContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyActionContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestReplyCommentContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyCommentContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestRoutePointActionContextConfiguration());
            modelBuilder.ApplyConfiguration(new LogistKontragentUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new CarrierKontragentUserContextConfiguration());
            modelBuilder.ApplyConfiguration(new SuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new ActiveRequestSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestReplySuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new SuitableRequestsSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new ListWithSuitableReisesSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new UserRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new CarrierUserRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new LogistUserRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new LogistUserRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new ActiveCargomartRequestSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new ActiveTorgTransRequestSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new ActiveTrafficRequestSuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new CargomartRequestReplySuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new TrafficRequestReplySuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new TorgTransRequestReplySuitableStatisticContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestRoutePointContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestReplyFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new CargomartRequestFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new TrafficRequestFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new TorgTransRequestFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestReplyContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestRoutePointContextConfiguration());
            modelBuilder.ApplyConfiguration(new CargomartRequestRoutePointContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestActionContextConfiguration());
            modelBuilder.ApplyConfiguration(new InstructionFileContextConfiguration());
            modelBuilder.ApplyConfiguration(new BundleCommentContextConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyContextConfiguration());
            modelBuilder.ApplyConfiguration(new ReisDispatchingPointContextConfiguration());
        }
    }
}