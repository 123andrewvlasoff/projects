using DAL;
using DAL.Models.Requests.Enums;
using Krafter.ServiceCommon.Services.ChangesTrackService;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequest
{
    public class KrafterRequestContextConfiguration : IEntityTypeConfiguration<KrafterRequest>
    {
        public void Configure(EntityTypeBuilder<KrafterRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator).HasValue(RequestDiscriminator.Krafter);

            item.Property(i => i.RequestStatus)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.KrafterRequestStatus)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.UpdatedAt)
                .HasAnnotation(Annotations.NoTracking, true);

            item.HasMany(i => i.Reises).WithOne(i => i.Request).HasForeignKey(i => i.RequestId);
            item.HasMany(i => i.RoutePoints).WithOne(i => i.Request).HasForeignKey(i => i.RequestId);
            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
            item.HasMany(i => i.Actions).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}