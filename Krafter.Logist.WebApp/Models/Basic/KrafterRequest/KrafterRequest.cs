using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Guaranties.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequest
{
    public class KrafterRequest : Request.Request
    {
        public new KontragentContract.KontragentContract KontragentContract { get; set; }

        [Computed]
        [NotMapped]
        public decimal? PriceForKm => Distance != null && Distance != 0 ? Price / (decimal)Distance : 0;


        public new ICollection<Reis.Reis> Reises { get; set; }

        public DAL.Models.Requests.Krafter.Enums.KrafterRequestStatus? KrafterRequestStatus { get; set; }

        [NotMapped]
        [Computed]
        public new KrafterRequestRoutePoint.KrafterRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public new KrafterRequestRoutePoint.KrafterRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<KrafterRequestRoutePoint.KrafterRequestRoutePoint> RoutePoints { get; set; }

        public new ICollection<KrafterRequestReply.KrafterRequestReply> Replies { get; set; }

        [NotMapped] public DateTime? LastActionDate { get; set; }

        [NotMapped] public DateTime BookUntil { get; set; }

        [NotMapped] public int ActionCount { get; set; }

        public new ICollection<KrafterRequestAction.KrafterRequestAction> Actions { get; set; }

        [NotMapped] public decimal MaxPrice { get; set; }
    }
}