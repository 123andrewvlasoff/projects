﻿using System.Collections.Generic;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequest
{
    /// <summary>
    ///     Represents the model configuration for Request.
    /// </summary>
    public class KrafterRequestOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<KrafterRequest>(nameof(KrafterRequest)).EntityType;

            item.OrderBy();

            item.Property(i => i.PriceForKm);

            item.Property(i => i.LastActionDate);

            item.Property(i => i.ActionCount);

            item.Property(i => i.BookUntil);

            item.Ignore(i => i.UpdatedAt);

            item.Ignore(i => i.ActualPrice);

            item.Ignore(i => i.ExternalId);

            item.Property(i => i.MaxPrice);

            item.Function("CancelByClient")
                .ReturnsFromEntitySet<KrafterRequestAction.KrafterRequestAction>(
                    nameof(KrafterRequestAction.KrafterRequestAction));
            
            
            item.Function("CancelByCarrier")
                .ReturnsFromEntitySet<KrafterRequestAction.KrafterRequestAction>(
                    nameof(KrafterRequestAction.KrafterRequestAction));

            item.Action("Instruction");

            item.Function("ReisesPriceByQuarter").ReturnsCollection<ReisesPriceByQuarter.ReisesPriceByQuarter>();

            item.Function("History")
                .ReturnsCollectionFromEntitySet<KrafterRequestAction.KrafterRequestAction>(
                    nameof(KrafterRequestAction.KrafterRequestAction));

            item.Function("SuitableTransport")
                .ReturnsCollectionFromEntitySet<RequestSuitableTransport.RequestSuitableTransport>(
                    nameof(RequestSuitableTransport.RequestSuitableTransport));

            item.Function("GetBundleComments")
                .ReturnsCollectionFromEntitySet<BundleComment.BundleComment>(
                    nameof(BundleComment.BundleComment));
            
            item.Function("NextRequest").ReturnsCollectionFromEntitySet<RequestDto>(nameof(RequestDto));

            item.Function("TakeInProgress").ReturnsCollection<IEnumerable<string>>();

            item.Function("Book").ReturnsCollection<IEnumerable<string>>();

            item.Function("FreeInProgress").ReturnsCollection<IEnumerable<string>>();

            item.ContainsRequired(i => i.Unload).OrderBy();
            item.ContainsRequired(i => i.Load).OrderBy();
        }
    }
}