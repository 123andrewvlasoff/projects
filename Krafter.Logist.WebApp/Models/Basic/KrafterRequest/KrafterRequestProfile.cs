﻿using System;
using System.Linq;
using AutoMapper;
using DAL.Models.Requests.Guaranties.Enums;
using DocumentFormat.OpenXml.Spreadsheet;
using Krafter.Logist.WebApp.Helpers;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.Helpers;
using Microsoft.Extensions.Configuration;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequest
{
    public class KrafterRequestProfile : Profile
    {
        public KrafterRequestProfile()
        {
            CreateMap<KrafterRequest, KrafterRequest>()
                .ForMember(dest => dest.ActionCount,
                    opt => opt.MapFrom<ActionCountDateResolver>())
                .ForMember(dest => dest.LastActionDate,
                    opt => opt.MapFrom<LastActionDateResolver>())
                .ForMember(dest => dest.BookUntil,
                    opt => opt.MapFrom<BookUntilDateResolver>())
                .ForMember(dest => dest.MaxPrice,
                    opt => opt.MapFrom<KrafterRequestMaxPriceResolver>());
        }
    }

    public class BookUntilDateResolver : IValueResolver<KrafterRequest, KrafterRequest, DateTime>
    {
        public DateTime Resolve(KrafterRequest source, KrafterRequest destination, DateTime destMember,
            ResolutionContext context)
        {
            return source?.Load?.ArrivalDate!=null ? TakeInProgressUntilHelper.GetTakeUntilDate(source.Load.ArrivalDate) : DateTime.MinValue;
        }
    }

    public class LastActionDateResolver : IValueResolver<KrafterRequest, KrafterRequest, DateTime?>
    {
        private readonly IConfiguration configuration;

        public LastActionDateResolver(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public DateTime? Resolve(KrafterRequest source, KrafterRequest destination, DateTime? destMember,
            ResolutionContext context)
        {
            var actionTimes = source.Actions?.Where(i =>
                    i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == source.InProgressUserId)
                .Select(i => i.ActionDate)
                .OrderBy(o => o)
                .ToList();

            if (actionTimes == null || !actionTimes.Any())
            {
                return null;
            }

            if (source.Actions.Any(i =>
                    (i.State == RequestActionStatus.FreeInProgress ||
                     i.State == RequestActionStatus.FreeInProgressAutomatically) &&
                    i.ActionUserId == source.InProgressUserId))
            {
                return source.Actions.OrderByDescending(i => i.ActionDate).FirstOrDefault(i =>
                        i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == source.InProgressUserId)
                    ?.ActionDate
                    .AddMinutes(configuration.GetMinutesInterval());
            }

            var totalMinutes = default(double);

            for (var i = 0; i < actionTimes.Count - 1; i++)
            {
                totalMinutes += (actionTimes[i].AddMinutes(configuration.GetMinutesInterval()) - actionTimes[i + 1])
                    .TotalMinutes;
            }

            var lastAction = actionTimes.OrderByDescending(i => i).FirstOrDefault();

            return lastAction.AddMinutes(totalMinutes + configuration.GetMinutesInterval());
        }
    }

    public class ActionCountDateResolver : IValueResolver<KrafterRequest, KrafterRequest, int>
    {
        public int Resolve(KrafterRequest source, KrafterRequest destination, int destMember,
            ResolutionContext context)
        {
            var userId = context.Items["UserId"].ToString();

            if (source.Actions == null)
            {
                return 0;
            }

            return source.Actions
                .Count(i => i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == userId);
        }
    }

    public class KrafterRequestMaxPriceResolver : IValueResolver<KrafterRequest, KrafterRequest, decimal>
    {
        private readonly IConfiguration configuration;

        public KrafterRequestMaxPriceResolver(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public decimal Resolve(KrafterRequest source, KrafterRequest destination, decimal destMember,
            ResolutionContext context)
        {
            return source.ClientPrice * configuration.CarrierPercentage();
        }
    }
}