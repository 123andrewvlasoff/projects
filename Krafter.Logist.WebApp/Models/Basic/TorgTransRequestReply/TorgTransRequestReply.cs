﻿namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReply
{
    public class TorgTransRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new TorgTransRequest.TorgTransRequest Request { get; set; }
    }
}