﻿using System;
using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReply
{
    public class TorgTransRequestReplyContextConfiguration : IEntityTypeConfiguration<TorgTransRequestReply>
    {
        public void Configure(EntityTypeBuilder<TorgTransRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TorgTransRequestReply>(RequestDiscriminator.TorgTrans);
        }
    }
}