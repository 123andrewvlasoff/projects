﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestReply
{
    public class CargomartRequestReplyContextConfiguration : IEntityTypeConfiguration<CargomartRequestReply>
    {
        public void Configure(EntityTypeBuilder<CargomartRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<CargomartRequestReply>(RequestDiscriminator.Cargomart);
        }
    }
}