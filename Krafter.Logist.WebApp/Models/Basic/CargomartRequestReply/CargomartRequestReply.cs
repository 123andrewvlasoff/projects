﻿namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestReply
{
    public class CargomartRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new CargomartRequest.CargomartRequest Request { get; set; }
    }
}