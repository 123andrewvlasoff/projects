using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ReisStatus
{
    public class
        ReisStatusContextConfiguration : IEntityTypeConfiguration<DAL.Models.Requests.Reis.Dictionaries.ReisStatus>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Requests.Reis.Dictionaries.ReisStatus> modelBuilder)
        {
        }
    }
}