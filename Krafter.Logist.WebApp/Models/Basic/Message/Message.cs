﻿using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.Message
{
    public class Message : DAL.Models.SupportTickets.Message
    {
        public new User.User User { get; set; }

        public new SupportTicket.SupportTicket SupportTicket { get; set; }

        public new ICollection<MessageFile.MessageFile> Files { get; set; }
    }
}