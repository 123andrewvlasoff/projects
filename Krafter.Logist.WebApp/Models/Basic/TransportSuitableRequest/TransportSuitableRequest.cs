﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.TransportSuitableRequest
{
    public class TransportSuitableRequest
    {
        [Key] public Guid Id { get; set; }

        public int RollingStockTypeId { get; set; }

        public string InProgressUserId { get; set; }

        public DAL.Models.Requests.Krafter.Enums.KrafterRequestStatus? KrafterRequestStatus { get; set; }

        public decimal? PriceForKm { get; set; }

        public DAL.Models.Dictionaries.RollingStockType RollingStockType { get; set; }

        public int LoadingTypeId { get; set; }

        public DAL.Models.Dictionaries.LoadingType LoadingType { get; set; }

        public string RequestNumber { get; set; }

        public double? Volume { get; set; }

        public double? Weight { get; set; }

        public double? TemperatureMin { get; set; }

        public double? TemperatureMax { get; set; }

        public decimal Price { get; set; }

        public decimal ActualPrice { get; set; }

        public string Number1C { get; set; }

        public string RequestCode { get; set; }

        public int? PalletsQuantity { get; set; }

        public int? TransportTypeId { get; set; }

        public virtual DAL.Models.Dictionaries.TransportType TransportType { get; set; }

        public Guid ClientId { get; set; }

        public Kontragent.Kontragent Client { get; set; }

        [Computed]
        public KrafterRequestRoutePoint.KrafterRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [Computed]
        public KrafterRequestRoutePoint.KrafterRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public ICollection<KrafterRequestRoutePoint.KrafterRequestRoutePoint> RoutePoints { get; set; }

        public ICollection<UserRequest.UserRequest> UserRequests { get; set; }

        public TimeSpan RequestDateInterval { get; set; }

        public double Remoteness { get; set; }
    }
}