﻿namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestFile
{
    public class CargomartRequestFile : RequestFile.RequestFile
    {
        public new CargomartRequest.CargomartRequest Request { get; set; }
    }
}