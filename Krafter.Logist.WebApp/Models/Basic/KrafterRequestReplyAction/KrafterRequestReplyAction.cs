﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction
{
    public class KrafterRequestReplyAction : DAL.Models.Requests.Krafter.Actions.RequestReplyAction
    {
        public new KrafterRequestReply.KrafterRequestReply RequestReply { get; set; }
        public new ICollection<KrafterRequestReplyComment.KrafterRequestReplyComment> RequestReplyComments { get; set; }

        [ForeignKey("ActionUserId")]
        public new User.User User { get; set; }
    }
}