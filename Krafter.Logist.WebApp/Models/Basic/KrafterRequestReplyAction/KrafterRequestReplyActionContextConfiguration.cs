﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction
{
    public class KrafterRequestReplyActionContextConfiguration : IEntityTypeConfiguration<KrafterRequestReplyAction>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestReplyAction> item)
        {
            item.HasBaseType(typeof(DAL.Models.Action));

            item.HasDiscriminator().HasValue("KrafterRequestReplyAction");
        }
    }
}