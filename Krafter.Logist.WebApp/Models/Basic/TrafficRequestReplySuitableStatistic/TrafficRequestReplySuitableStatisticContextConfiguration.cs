﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestReplySuitableStatistic
{
    public class
        TrafficRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<
            TrafficRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<TrafficRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}