﻿// ReSharper disable once RedundantUsingDirective

using DAL.Models;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentFile
{
    public class KontragentFileContextConfiguration : IEntityTypeConfiguration<KontragentFile>
    {
        public void Configure(EntityTypeBuilder<KontragentFile> item)
        {
            item.HasDiscriminator().HasValue(Folders.Kontragent);
        }
    }
}