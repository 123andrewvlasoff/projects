﻿namespace Krafter.Logist.WebApp.Models.Basic.KontragentFile
{
    public class KontragentFile : DAL.Models.Kontragents.KontragentFile
    {
        public new Kontragent.Kontragent Kontragent { get; set; }
    }
}