﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequest
{
    public class BaltikaRequestContextConfiguration : IEntityTypeConfiguration<BaltikaRequest>
    {
        public void Configure(EntityTypeBuilder<BaltikaRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<BaltikaRequest>(RequestDiscriminator.Baltika);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}