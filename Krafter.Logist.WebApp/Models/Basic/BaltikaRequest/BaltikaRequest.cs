﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequest
{
    public class BaltikaRequest : AuctionRequest.AuctionRequest
    {
        [NotMapped]
        [Computed]
        public BaltikaRequestRoutePoint.BaltikaRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public BaltikaRequestRoutePoint.BaltikaRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<BaltikaRequestRoutePoint.BaltikaRequestRoutePoint> RoutePoints { get; set; }
        
        public new ICollection<BaltikaRequestReply.BaltikaRequestReply> Replies { get; set; }
    }
}
