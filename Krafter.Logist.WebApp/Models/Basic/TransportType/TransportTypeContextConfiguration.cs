using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransportType
{
    public class TransportTypeContextConfiguration : IEntityTypeConfiguration<DAL.Models.Dictionaries.TransportType>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Dictionaries.TransportType> item)
        {
            item.Ignore(i => i.ExternalId);
            item.Ignore(i => i.SortOrder);
        }
    }
}