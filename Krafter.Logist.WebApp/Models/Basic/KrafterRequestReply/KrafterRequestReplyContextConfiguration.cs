using DAL;
using DAL.Models.Requests.Enums;
using Krafter.ServiceCommon.Services.ChangesTrackService;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply
{
    public class KrafterRequestReplyContextConfiguration : IEntityTypeConfiguration<KrafterRequestReply>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<KrafterRequestReply>(RequestDiscriminator.Krafter);

            item.Property(i => i.Id)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.Discriminator)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.RequestId)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.IsAccepted)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.CreatorId)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.UpdaterId)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.CreatedAt)
                .HasAnnotation(Annotations.NoTracking, true);
        }
    }
}