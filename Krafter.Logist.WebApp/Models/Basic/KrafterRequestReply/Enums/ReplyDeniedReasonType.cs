namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply.Enums
{
    public enum ReplyDeniedReasonType
    {
        None,
        ByClientDecision,
        ByLogistDecision,
        ByLogistDecisionLogistCreatedReply
    }
}