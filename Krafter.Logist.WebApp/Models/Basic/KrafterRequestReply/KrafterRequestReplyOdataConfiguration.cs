﻿using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply.Enums;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply
{
    /// <summary>
    ///     Represents the model configuration for RequestReply.
    /// </summary>
    public class KrafterRequestReplyOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<KrafterRequestReply>(nameof(KrafterRequestReply)).EntityType;

            item.DerivesFromNothing();

            item.Ignore(i => i.CreatedAt);

            item.Ignore(i => i.Actions);

            item.Ignore(i => i.CreatorId);

            item.Ignore(i => i.Status);

            item.Function("SendToSign")
                .ReturnsFromEntitySet<KrafterRequestReply>(nameof(KrafterRequestReply));

            item.Action("Reload");

            item.Action("Cancel");

            item.Action("Accept");
            
            item.Action("AcceptAfterCarrierSigned");

            item.Function("Denied")
                .ReturnsFromEntitySet<KrafterRequestAction.KrafterRequestAction>(
                    nameof(KrafterRequestAction.KrafterRequestAction));

            item.Action("RejectFile");

              item.Property(r => r.CreatedByCarrier);
        }
    }
}