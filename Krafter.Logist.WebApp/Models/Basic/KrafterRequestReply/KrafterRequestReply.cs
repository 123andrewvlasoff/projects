using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Requests.Krafter.Enums;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply
{
    public class KrafterRequestReply : RequestReply.RequestReply
    {
        public new KrafterRequest.KrafterRequest Request { get; set; }

        public string UpdaterId { get; set; }

        public User.User Updater { get; set; }

        public RequestReplyType RequestReplyType { get; set; }
        
        [NotMapped]
        public bool CreatedByCarrier { get; set; }

        public ICollection<KrafterRequestReplyAction.KrafterRequestReplyAction> Actions { get; set; }

        public new ICollection<KrafterRequestReplyFile.KrafterRequestReplyFile> Files { get; set; }

        public ICollection<KrafterRequestReplyComment.KrafterRequestReplyComment> Comments { get; set; }
    }
}