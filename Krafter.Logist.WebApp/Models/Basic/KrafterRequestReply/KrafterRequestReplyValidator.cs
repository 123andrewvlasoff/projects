﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Kontragents.Enums;
using FluentValidation;
using Krafter.ServiceCommon.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply
{
    public class KrafterRequestReplyValidator : AbstractValidator<KrafterRequestReply>
    {
        private readonly IConfiguration configuration;
        private readonly LogistDbContext context;

        public KrafterRequestReplyValidator(LogistDbContext context, IConfiguration configuration)
        {
            this.configuration = configuration;
            this.context = context;

            RuleFor(i => i.Price)
                .GreaterThan(1)
                .WithMessage("Цена отклика должна быть больше чем 0");

            RuleFor(i => i)
                .Must(x => BeAValidReplyPrice(x.RequestId, x.Price))
                .WithMessage(
                    $"Цена должна быть менее чем {configuration.CarrierPercentage() * 100} процентов от цены заказчика");

            RuleFor(i => i)
                .Must(x => BeAValidKontragent(x.KontragentId.Value))
                .WithMessage("Контрагент не активен");

            RuleFor(x => x)
                .Must(x => BeOneOrMoreActiveKontragentContracts(x.KontragentId.Value))
                .WithMessage("Не найден действующий договор");
        }

        private bool BeAValidReplyPrice(Guid requestId, decimal price)
        {
            return price <= context.Requests.First(i => i.Id == requestId).ClientPrice *
                configuration.CarrierPercentage();
        }

        private bool BeAValidKontragent(Guid kontragentId)
        {
            return context.Kontragents.First(i => i.Id == kontragentId).Status == KontragentStatus.Active;
        }

        private bool BeOneOrMoreActiveKontragentContracts(Guid kontragentId)
        {
            return context.KontragentContracts
                .Any(x => x.KontragentId == kontragentId && x.IsActive == 1);
        }
    }
}