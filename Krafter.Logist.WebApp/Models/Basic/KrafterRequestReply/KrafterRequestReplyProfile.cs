﻿using System.Linq;
using AutoMapper;
using DAL.Models.KontragentUser.Enums;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply
{
    public class KrafterRequestReplyProfile : Profile
    {
        public KrafterRequestReplyProfile()
        {
            CreateMap<KrafterRequestReply, KrafterRequestReply>()
                .ForMember(dest => dest.CreatedByCarrier,
                    opt => opt.MapFrom<CreatedByCarrierResolver>());
        }
    }

     public class CreatedByCarrierResolver : IValueResolver<KrafterRequestReply, KrafterRequestReply, bool>
    {
        public CreatedByCarrierResolver()
        {
        }

        public bool Resolve(KrafterRequestReply source, KrafterRequestReply destination, bool destMember, ResolutionContext context)
        {
            
            return (source.Creator?.KontragentUsers?.Any(c => c.UserId == source.CreatorId &&
                                                             c.Discriminator ==
                                                             KontragentUserType.Carrier)).GetValueOrDefault();
        }
    }
     
}