﻿using DAL.Models.Kontragents.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Company
{
    public class CompanyContextConfiguration : IEntityTypeConfiguration<DAL.Models.Kontragents.Company>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Kontragents.Company> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                            .HasValue(CompanyType.Base)
                            .HasValue<Kontragent.Kontragent>(CompanyType.Kontragent)
                            .HasValue<CrafterCompany.CrafterCompany>(CompanyType.CrafterCompany);
        }
    }
}