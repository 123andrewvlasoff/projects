﻿using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestReplySuitableStatistic
{
    public class AtrucksRequestReplySuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestReplyStatistic Data { get; set; }
    }
}
