﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestReplySuitableStatistic
{
    public class
AtrucksRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<AtrucksRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<AtrucksRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
