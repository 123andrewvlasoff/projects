﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic
{
    public class
        ListWithSuitableReisesSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
            ListWithSuitableReisesSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ListWithSuitableReisesSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}