﻿using System;
using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic.ListWithSuitableReisesStatistic
{
    public class ListWithSuitableReisesStatistic
    {
        public Dictionary<Guid, int> Map { get; set; }
    }
}