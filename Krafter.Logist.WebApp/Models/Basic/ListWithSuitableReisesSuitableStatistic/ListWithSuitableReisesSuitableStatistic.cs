﻿namespace Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic
{
    public class ListWithSuitableReisesSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new ListWithSuitableReisesStatistic.ListWithSuitableReisesStatistic Data { get; set; }
    }
}