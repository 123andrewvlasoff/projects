﻿namespace Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic
{
    public class RequestStatistic
    {
        public int SuitableTransportCount { get; set; }

        public int SuitableTransportCrafterCount { get; set; }

        public int SuitableTransportPartnerCount { get; set; }
    }
}