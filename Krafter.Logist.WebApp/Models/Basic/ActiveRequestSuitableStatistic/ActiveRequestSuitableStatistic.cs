﻿namespace Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic
{
    public class ActiveRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic.RequestStatistic Data { get; set; }
    }
}