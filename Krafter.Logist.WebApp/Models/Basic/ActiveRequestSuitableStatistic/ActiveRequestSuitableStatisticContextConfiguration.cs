﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic
{
    public class
        ActiveRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<ActiveRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}