﻿using System;
using DAL.Models.KontragentUser.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentUser
{
    public class KontragentUserContextConfiguration : IEntityTypeConfiguration<KontragentUser>
    {
        public void Configure(EntityTypeBuilder<KontragentUser> item)
        {
            item.HasDiscriminator("Discriminator", typeof(KontragentUserType))
                .HasValue<KontragentUser>(KontragentUserType.Base)
                .HasValue<CarrierKontragentUser.CarrierKontragentUser>(KontragentUserType.Carrier)
                .HasValue<LogistKontragentUser.LogistKontragentUser>(KontragentUserType.Logist);
        }
    }
}