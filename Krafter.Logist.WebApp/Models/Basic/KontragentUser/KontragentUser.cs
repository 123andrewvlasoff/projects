﻿namespace Krafter.Logist.WebApp.Models.Basic.KontragentUser
{
    public class KontragentUser : DAL.Models.KontragentUser.KontragentUser
    {
        public new Kontragent.Kontragent Kontragent { get; set; }

        public new User.User User { get; set; }
    }
}