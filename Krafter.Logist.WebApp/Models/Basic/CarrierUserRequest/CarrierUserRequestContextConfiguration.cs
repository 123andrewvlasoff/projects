﻿using DAL.Models.Requests.UserRequest.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CarrierUserRequest
{
    public class CarrierUserRequestContextConfiguration : IEntityTypeConfiguration<CarrierUserRequest>
    {
        public void Configure(EntityTypeBuilder<CarrierUserRequest> item)
        {
            item.HasDiscriminator("Discriminator", typeof(UserRequestType)).HasValue(UserRequestType.Carrier);
        }
    }
}