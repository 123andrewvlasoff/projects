﻿using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveBaltikaRequestSuitableStatistic
{
    public class ActiveBaltikaRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic Data { get; set; }
    }
}
