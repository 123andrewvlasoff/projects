﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveBaltikaRequestSuitableStatistic
{
    public class
    ActiveBaltikaRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
        ActiveBaltikaRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveBaltikaRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
