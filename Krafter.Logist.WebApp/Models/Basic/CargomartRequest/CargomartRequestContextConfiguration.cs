﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequest
{
    public class CargomartRequestContextConfiguration : IEntityTypeConfiguration<CargomartRequest>
    {
        public void Configure(EntityTypeBuilder<CargomartRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator).HasValue<CargomartRequest>(RequestDiscriminator.Cargomart);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}