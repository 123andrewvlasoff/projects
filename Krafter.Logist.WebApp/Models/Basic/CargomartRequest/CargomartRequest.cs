﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequest
{
    public class CargomartRequest : AuctionRequest.AuctionRequest
    {
        [NotMapped]
        [Computed]
        public CargomartRequestRoutePoint.CargomartRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public CargomartRequestRoutePoint.CargomartRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<CargomartRequestRoutePoint.CargomartRequestRoutePoint> RoutePoints { get; set; }
        public new ICollection<CargomartRequestFile.CargomartRequestFile> Files { get; set; }

        public new ICollection<CargomartRequestReply.CargomartRequestReply> Replies { get; set; }
    }
}