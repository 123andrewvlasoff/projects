using System.ComponentModel.DataAnnotations.Schema;

namespace Krafter.Logist.WebApp.Models.Basic.BundleComment
{
    public class BundleComment : DAL.Models.Drafts.Transports.BundleComment
    {
        public new KrafterRequest.KrafterRequest Request { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        public new User.User Creator { get; set; }
        
        public new User.User Updater { get; set; }

        [NotMapped]
        public double Remoteness { get; set; }
    }
}