using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.BundleComment
{
    public class BundleCommentContextConfiguration : IEntityTypeConfiguration<BundleComment>
    {
        public void Configure(EntityTypeBuilder<BundleComment> item)
        {
            item.HasDiscriminator();

            item.HasOne(i => i.Transport)
                .WithMany(x => x.BundleComments);

            item.Property(i => i.RequestId).HasColumnName("RequestId");

            item.HasOne(i => i.Request);

            item.HasOne(i => i.ConnectedTransport);
        }
    }
}