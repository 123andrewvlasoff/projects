﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestRoutePoint
{
    public class
        BaltikaRequestRoutePointContextConfiguration : IEntityTypeConfiguration<BaltikaRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<BaltikaRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<BaltikaRequestRoutePoint>(RequestDiscriminator.Baltika);
        }
    }
}