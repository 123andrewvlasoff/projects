﻿namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestRoutePoint
{
    public class BaltikaRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new BaltikaRequest.BaltikaRequest Request { get; set; }
    }
}
