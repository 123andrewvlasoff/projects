﻿namespace Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic
{
    public class RequestReplySuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestReplyStatistic.RequestReplyStatistic Data { get; set; }
    }
}