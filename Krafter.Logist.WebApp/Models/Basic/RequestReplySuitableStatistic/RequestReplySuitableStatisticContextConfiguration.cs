﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic
{
    public class
        RequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<RequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<RequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}