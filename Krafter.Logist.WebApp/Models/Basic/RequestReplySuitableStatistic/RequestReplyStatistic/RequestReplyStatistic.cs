﻿namespace Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic
{
    public class RequestReplyStatistic
    {
        public int SuitableTransportCount { get; set; }

        public int SuitableTransportCrafterCount { get; set; }

        public int SuitableTransportPartnerCount { get; set; }

        public bool SuitableTransportSelected { get; set; }
    }
}