using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.User
{
    public class User : DAL.Models.Users.User
    {
        public new ICollection<UserRole.UserRole> UserRoles { get; set; }

        public new Employer.Employer Employer { get; set; }
        
        public new ICollection<KontragentUser.KontragentUser> KontragentUsers { get; set; }
    }
}