﻿using System;
using DAL.Models.Requests.Enums;
using Krafter.Logist.WebApp.Models.Basic.User.UserStaticDto.Enums;

namespace Krafter.Logist.WebApp.Models.Basic.User.UserStaticDto
{
    public class UserStaticDto
    {
        public bool Status { get; set; }

        public DateTime? CreatedAt { get; set; }

        public UserStatisticTypes Type { get; set; }
    }
}