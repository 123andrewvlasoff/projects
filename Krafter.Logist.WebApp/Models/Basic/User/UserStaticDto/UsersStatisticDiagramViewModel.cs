﻿namespace Krafter.Logist.WebApp.Models.Basic.User.UserStaticDto
{
    public class UsersStatisticDiagramViewModel
    {
        public int RequestCount { get; set; }

        public int RequestDoneCount { get; set; }

        public int AuctionCount { get; set; }

        public int AuctionDoneCount { get; set; }
    }
}