using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.User.UserProfileDto
{
    public class UserProfileDto : User
    {
        public List<string> Roles { get; set; }
    }
}