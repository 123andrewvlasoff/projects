﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyComment
{
    public class RequestReplyCommentContextConfiguration : IEntityTypeConfiguration<RequestReplyComment>
    {
        public void Configure(EntityTypeBuilder<RequestReplyComment> item)
        {
            item.HasBaseType(typeof(DAL.Models.Comment.Comment));
        }
    }
}