﻿namespace Krafter.Logist.WebApp.Models.Basic.RequestReplyComment
{
    public class RequestReplyComment : DAL.Models.Requests.Krafter.RequestReplyComment
    {
        public new RequestReply.RequestReply RequestReply { get; set; }

        public new KrafterRequestReplyAction.KrafterRequestReplyAction Action { get; set; }

        public new DAL.Models.Requests.Krafter.Dictionaries.DeniedReason DeniedReason { get; set; }
    }
}