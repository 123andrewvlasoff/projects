﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequest
{
    public class TransporeonRequestOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<TransporeonRequest>(nameof(TransporeonRequest)).EntityType;

            item.OrderBy();

            item.ContainsRequired(i => i.Unload).OrderBy();
            item.ContainsRequired(i => i.Load).OrderBy();

            item.Action("Cancel");
            item.Action("Break");
            item.Action("Sign");
            item.Action("Instruction");

            item.Function("SuitableTransport")
                .ReturnsCollectionFromEntitySet<RequestSuitableTransport.RequestSuitableTransport>(
                    nameof(RequestSuitableTransport.RequestSuitableTransport));
        }
    }
}
