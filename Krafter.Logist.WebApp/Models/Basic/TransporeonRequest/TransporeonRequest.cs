﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequest
{
    public class TransporeonRequest : AuctionRequest.AuctionRequest
    {
        [NotMapped]
        [Computed]
        public TransporeonRequestRoutePoint.TransporeonRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public TransporeonRequestRoutePoint.TransporeonRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<TransporeonRequestRoutePoint.TransporeonRequestRoutePoint> RoutePoints { get; set; }

        public new ICollection<TransporeonRequestFile.TransporeonRequestFile> Files { get; set; }

        public new ICollection<TransporeonRequestReply.TransporeonRequestReply> Replies { get; set; }
    }
}
