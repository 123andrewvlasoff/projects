﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequest
{
    public class TransporeonRequestContextConfiguration : IEntityTypeConfiguration<TransporeonRequest>
    {
        public void Configure(EntityTypeBuilder<TransporeonRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TransporeonRequest>(RequestDiscriminator.Transporeon);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}