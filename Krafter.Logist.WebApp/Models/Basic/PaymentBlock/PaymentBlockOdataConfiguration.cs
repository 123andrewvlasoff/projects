﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.PaymentBlock
{
    public class PaymentBlockOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<PaymentBlock>(nameof(PaymentBlock)).EntityType;
            item.DerivesFromNothing();

            item.Ignore(i => i.ExternalId);

            item.OrderBy();
            item.ContainsRequired(i => i.Carrier).OrderBy();
            item.HasKey(p => p.Id);
        }
    }
}