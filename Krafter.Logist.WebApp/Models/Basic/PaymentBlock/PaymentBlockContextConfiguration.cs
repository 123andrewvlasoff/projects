﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.PaymentBlock
{
    public class PaymentBlockContextConfiguration : IEntityTypeConfiguration<PaymentBlock>
    {
        public void Configure(EntityTypeBuilder<PaymentBlock> item)
        {
            item.HasBaseType((Type)null);
            item.HasQueryFilter(f => f.ReisId != null && f.IsDeleted == false);
        }
    }
}