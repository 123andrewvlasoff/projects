﻿namespace Krafter.Logist.WebApp.Models.Basic.PaymentBlock
{
    public class PaymentBlock : DAL.Models.Transactions.PaymentBlock
    {
        public new Reis.Reis Reis { get; set; }

        public new Kontragent.Kontragent Carrier { get; set; }
    }
}