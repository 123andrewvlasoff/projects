namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser
{
    public class TransportKontragentUser : DAL.Models.Transports.TransportKontragentUser
    {
        public new TransportKontragent.TransportKontragent TransportKontragent { get; set; }
    }
}