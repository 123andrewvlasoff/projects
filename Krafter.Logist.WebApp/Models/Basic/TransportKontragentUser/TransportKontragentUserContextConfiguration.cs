using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser
{
    public class TransportKontragentUserContextConfiguration : IEntityTypeConfiguration<TransportKontragentUser>
    {
        public void Configure(EntityTypeBuilder<TransportKontragentUser> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}