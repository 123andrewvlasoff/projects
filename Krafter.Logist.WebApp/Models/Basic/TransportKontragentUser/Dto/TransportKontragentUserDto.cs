using System;
using DAL.Models.Enums;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser.Dto
{
    public class TransportKontragentUserDto
    {
        public Guid TransportKontragentUserId { get; set; }

        public Guid KontragentId { get; set; }

        public Guid TransportKontragentId { get; set; }

        public Guid TransportId { get; set; }

        public string RegNumber { get; set; }

        public int? RollingStockTypeId { get; set; }

        public int? TransportTypeId { get; set; }

        public Guid? TransportStatusId { get; set; }

        public DateTime? LastReisDate { get; set; }

        public double? LastReisLat { get; set; }

        public double? LastReisLon { get; set; }

        public string Comment { get; set; }

        public string LastAddress { get; set; }
        
        public Guid? LastConnectedTransportId { get; set; }
        
        public Transport.Transport LastConnectedTransport { get; set; }

        public RelationType RelationType { get; set; }
        
        public int SuitableReisesCount { get; set; }

        public double? Volume { get; set; }

        public double? Weight { get; set; }

        public Guid? ConnectedTransportId { get; set; }

        public Guid? ConnectedTransportKontragentsId { get; set; }

        public DateTime? ReisLeaveDatePlan { get; set; }
        public DateTime? RentStart { get; set; }
        public DateTime? RentStop { get; set; }
    }
}