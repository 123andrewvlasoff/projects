using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser.Dto
{
    /// <summary>
    ///     Represents the model configuration for DriverKontragent.
    /// </summary>
    public class TransportKontragentUserDtoOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<TransportKontragentUserDto>(nameof(TransportKontragentUserDto)).EntityType;

            item.HasKey(i => i.TransportKontragentUserId);
        }
    }
}