﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile
{
    public class KrafterRequestReplyFileContextConfiguration : IEntityTypeConfiguration<KrafterRequestReplyFile>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestReplyFile> item)
        {
        }
    }
}