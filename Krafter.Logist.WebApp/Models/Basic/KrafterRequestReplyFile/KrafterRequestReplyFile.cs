﻿namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile
{
    public class KrafterRequestReplyFile : RequestReplyFile.RequestReplyFile
    {
        public new KrafterRequestReply.KrafterRequestReply Reply { get; set; }
    }
}