﻿namespace Krafter.Logist.WebApp.Models.Basic.SuitableRequestsSuitableStatistic.SuitableRequestStatistic
{
    public class SuitableRequestStatistic
    {
        public int RequestCount { get; set; }

        public decimal RequestMin { get; set; }

        public decimal RequestMax { get; set; }

        public int RequestNotInProgressCount { get; set; }

        public decimal RequestNotInProgressMin { get; set; }

        public decimal RequestNotInProgressMax { get; set; }
    }
}