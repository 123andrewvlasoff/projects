﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.SuitableRequestsSuitableStatistic
{
    public class
        SuitableRequestsSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
            SuitableRequestsSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<SuitableRequestsSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}