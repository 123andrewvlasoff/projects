using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Models.Requests.Enums;

namespace Krafter.Logist.WebApp.Models.Basic.UserRequest
{
    public class UserRequest : DAL.Models.Requests.UserRequest.UserRequest
    {
        public new User.User User { get; set; }
    }
}