using DAL.Models.Requests.UserRequest.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.UserRequest
{
    public class UserRequestContextConfiguration : IEntityTypeConfiguration<UserRequest>
    {
        public void Configure(EntityTypeBuilder<UserRequest> item)
        {
            item.HasDiscriminator("Discriminator", typeof(UserRequestType))
                .HasValue<UserRequest>(UserRequestType.Base)
                .HasValue<CarrierUserRequest.CarrierUserRequest>(UserRequestType.Carrier)
                .HasValue<LogistUserRequest.LogistUserRequest>(UserRequestType.Logist);
        }
    }
}