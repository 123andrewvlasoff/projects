using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ReisFile
{
    public class ReisFileContextConfiguration : IEntityTypeConfiguration<ReisFile>
    {
        public void Configure(EntityTypeBuilder<ReisFile> item)
        {
            item.HasDiscriminator().HasValue(Folders.Reises);
        }
    }
}