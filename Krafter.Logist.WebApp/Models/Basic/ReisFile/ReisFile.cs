namespace Krafter.Logist.WebApp.Models.Basic.ReisFile
{
    public class ReisFile : DAL.Models.Requests.Reis.ReisFile
    {
        public new Reis.Reis Reis { get; set; }

        public new User.User SignedBy { get; set; }
    }
}