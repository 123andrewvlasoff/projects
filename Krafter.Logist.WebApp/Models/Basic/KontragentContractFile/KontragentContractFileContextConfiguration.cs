﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContractFile
{
    public class KontragentContractFileContextConfiguration : IEntityTypeConfiguration<KontragentContractFile>
    {
        public void Configure(EntityTypeBuilder<KontragentContractFile> item)
        {
            item.HasDiscriminator();
        }
    }
}