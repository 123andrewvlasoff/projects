﻿namespace Krafter.Logist.WebApp.Models.Basic.KontragentContractFile
{
    public class KontragentContractFile : DAL.Models.Kontragents.KontragentContractFile
    {
        public new KontragentContract.KontragentContract KontragentContract { get; set; }
    }
}