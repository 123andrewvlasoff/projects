﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction
{
    public class KrafterRequestActionContextConfiguration : IEntityTypeConfiguration<KrafterRequestAction>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestAction> item)
        {
            item.HasBaseType(typeof(DAL.Models.Action));

            item.HasDiscriminator().HasValue("KrafterRequestAction");
        }
    }
}