﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Requests.Krafter;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction
{
    public class KrafterRequestAction : DAL.Models.Requests.Krafter.Actions.RequestAction
    {
        [ForeignKey("ActionUserId")]
        public new User.User User { get; set; }

        public new ICollection<KrafterRequestComment> Comments { get; set; }
    }
}