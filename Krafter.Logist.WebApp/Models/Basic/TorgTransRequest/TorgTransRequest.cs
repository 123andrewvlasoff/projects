﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequest
{
    public class TorgTransRequest : AuctionRequest.AuctionRequest
    {
        public int ExtLastOurBidId { get; set; }

        public new ICollection<TorgTransRequestFile.TorgTransRequestFile> Files { get; set; }

        public new ICollection<TorgTransRequestReply.TorgTransRequestReply> Replies { get; set; }

        [NotMapped]
        [Computed]
        public TorgTransRequestRoutePoint.TorgTransRequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public TorgTransRequestRoutePoint.TorgTransRequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<TorgTransRequestRoutePoint.TorgTransRequestRoutePoint> RoutePoints { get; set; }
    }
}