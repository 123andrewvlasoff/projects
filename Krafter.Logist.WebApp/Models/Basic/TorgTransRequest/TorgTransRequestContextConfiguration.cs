﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequest
{
    public class TorgTransRequestContextConfiguration : IEntityTypeConfiguration<TorgTransRequest>
    {
        public void Configure(EntityTypeBuilder<TorgTransRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
               .HasValue<TorgTransRequest>(RequestDiscriminator.TorgTrans);

            item.HasMany(i => i.UserRequests).WithOne().HasForeignKey(i => i.RequestId);
        }
    }
}