﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContact
{
    /// <summary>
    ///     Represents the model configuration for KontragentContact.
    /// </summary>
    public class KontragentContactOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<KontragentContact>(nameof(KontragentContact)).EntityType;

            item.DerivesFromNothing();

            item.Ignore(i => i.ExternalId);

            item.Ignore(i => i.CreatedAt);

            item.Ignore(i => i.UpdatedAt);

            item.EnumProperty(p => p.RelationType);

            item.HasKey(i => i.Id);

            item.Expand().OrderBy();
        }
    }
}