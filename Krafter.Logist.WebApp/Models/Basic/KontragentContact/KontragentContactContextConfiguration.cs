using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContact
{
    public class KontragentContactContextConfiguration : IEntityTypeConfiguration<KontragentContact>
    {
        public void Configure(EntityTypeBuilder<KontragentContact> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(i => i.CreatedAt);
        }
    }
}