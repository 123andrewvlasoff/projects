using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models;
using DAL.Models.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContact
{
    public class KontragentContact : DAL.Models.Kontragents.KontragentContact
    {
        public new virtual Kontragent.Kontragent Kontragent { get; set; }

        [NotMapped] [Computed] public RelationType RelationType { get; set; }

        public new ICollection<KontragentContactUser.KontragentContactUser> KontragentContactUsers { get; set; }

        public new virtual Position Position { get; set; }
}
}