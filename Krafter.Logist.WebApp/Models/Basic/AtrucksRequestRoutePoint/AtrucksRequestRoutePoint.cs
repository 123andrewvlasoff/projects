﻿using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestRoutePoint
{
    public class AtrucksRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new AtrucksRequest.AtrucksRequest Request { get; set; }
    }
}
