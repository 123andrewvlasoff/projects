﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.AtrucksRequestRoutePoint
{
    public class
    AtrucksRequestRoutePointContextConfiguration : IEntityTypeConfiguration<AtrucksRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<AtrucksRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<AtrucksRequestRoutePoint>(RequestDiscriminator.Atrucks);
        }
    }
}
