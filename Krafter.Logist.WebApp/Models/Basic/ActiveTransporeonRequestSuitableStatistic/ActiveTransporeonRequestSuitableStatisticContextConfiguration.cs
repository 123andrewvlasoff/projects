﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveTransporeonRequestSuitableStatistic
{
    public class
    ActiveTransporeonRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
        ActiveTransporeonRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveTransporeonRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
