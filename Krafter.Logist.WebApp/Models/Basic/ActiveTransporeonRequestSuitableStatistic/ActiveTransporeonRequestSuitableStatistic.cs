﻿using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveTransporeonRequestSuitableStatistic
{
    public class ActiveTransporeonRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic Data { get; set; }
    }
}
