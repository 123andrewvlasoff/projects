﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Claim
{
    public class ClaimContextConfiguration : IEntityTypeConfiguration<Claim>
    {
        public void Configure(EntityTypeBuilder<Claim> item)
        {
            item.HasBaseType((Type)null);
            item.HasQueryFilter(f => f.ReisId != null && f.IsDeleted == false);
        }
    }
}