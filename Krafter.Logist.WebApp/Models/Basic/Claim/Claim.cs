﻿namespace Krafter.Logist.WebApp.Models.Basic.Claim
{
    public class Claim : DAL.Models.Transactions.Claim
    {
        public new Reis.Reis Reis { get; set; }

        public new Kontragent.Kontragent Carrier { get; set; }
    }
}