﻿using DAL.Models.Requests.UserRequest.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.LogistUserRequest
{
    public class LogistUserRequestContextConfiguration : IEntityTypeConfiguration<LogistUserRequest>
    {
        public void Configure(EntityTypeBuilder<LogistUserRequest> item)
        {
            item.HasDiscriminator("Discriminator", typeof(UserRequestType)).HasValue(UserRequestType.Logist);
        }
    }
}