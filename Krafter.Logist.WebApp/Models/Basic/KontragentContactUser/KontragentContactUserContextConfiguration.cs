using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContactUser
{
    public class KontragentContactUserContextConfiguration : IEntityTypeConfiguration<KontragentContactUser>
    {
        public void Configure(EntityTypeBuilder<KontragentContactUser> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}