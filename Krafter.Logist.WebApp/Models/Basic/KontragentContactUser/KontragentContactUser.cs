namespace Krafter.Logist.WebApp.Models.Basic.KontragentContactUser
{
    public class KontragentContactUser : DAL.Models.Kontragents.KontragentContactUser
    {
        public new KontragentContact.KontragentContact KontragentContact { get; set; }
    }
}