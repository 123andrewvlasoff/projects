﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krafter.Logist.WebApp.Models.Basic.KontragentContract
{
    public class KontragentContract : DAL.Models.Kontragents.KontragentContract
    {
        public new KontragentContractFile.KontragentContractFile ShippingDocsRequirements { get; set; }
        public new CrafterCompany.CrafterCompany CrafterCompany { get; set; }
    }
}