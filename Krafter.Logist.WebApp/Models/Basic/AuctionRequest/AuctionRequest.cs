﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Requests.Auction.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.AuctionRequest
{
    public class AuctionRequest : Request.Request
    {
        [MaxLength(100)] public string ExternalLogin { get; set; }

        [MaxLength(100)] public string ExternalUrl { get; set; }

        public decimal MinPrice { get; set; }

        public decimal? PredictedFinalPrice { get; set; }

        public decimal Step { get; set; }

        public int BidCount { get; set; }

        [MaxLength(100)] public string ContactName { get; set; }

        [MaxLength(100)] public string ContactPhone { get; set; }

        [MaxLength(100)] public string ContactMail { get; set; }

        public AuctionRequestStatus? AuctionRequestStatus { get; set; }

        public bool BetWasPlaced { get; set; }

        [NotMapped] [Computed] public decimal? PredictedFinalPriceForKm => PredictedFinalPrice / (decimal?)Distance;

        [NotMapped] [Computed] public decimal CurrentPrice => ActualPrice <= Price ? ActualPrice : Price;

        [NotMapped] [Computed] public decimal BidPrice => CurrentPrice - Step;

        [NotMapped]
        [Computed]
        public decimal? PriceForKm => Distance != null && Distance != 0 ? Price / (decimal)Distance : 0;
    }
}