﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.AuctionRequest
{
    public class AuctionRequestContextConfiguration : IEntityTypeConfiguration<AuctionRequest>
    {
        public void Configure(EntityTypeBuilder<AuctionRequest> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.AuctionBase)
                .HasValue<TrafficRequest.TrafficRequest>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequest.CargomartRequest>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequest.TransporeonRequest>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequest.TorgTransRequest>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequest.BaltikaRequest>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequest.AtrucksRequest>(RequestDiscriminator.Atrucks);
        }
    }
}