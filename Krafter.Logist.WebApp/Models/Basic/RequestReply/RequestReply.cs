using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReply
{
    public class RequestReply : DAL.Models.Requests.RequestReply
    {
        public new Kontragent.Kontragent Kontragent { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        public new DAL.Models.Transports.Dictionaries.TransportStatus TransportStatus { get; set; }

        public new Driver.Driver Driver { get; set; }

        public new KontragentContact.KontragentContact KontragentContact { get; set; }

        public new User.User Creator { get; set; }

        protected internal ICollection<RequestReplyFile.RequestReplyFile> Files { get; set; }
    }
}