using DAL;
using DAL.Models.Requests.Enums;
using Krafter.ServiceCommon.Services.ChangesTrackService;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReply
{
    public class RequestReplyContextConfiguration : IEntityTypeConfiguration<RequestReply>
    {
        public void Configure(EntityTypeBuilder<RequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<TrafficRequestReply.TrafficRequestReply>(RequestDiscriminator.TrafficOnline)
                .HasValue<KrafterRequestReply.KrafterRequestReply>(RequestDiscriminator.Krafter)
                .HasValue<CargomartRequestReply.CargomartRequestReply>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestReply.TransporeonRequestReply>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequestReply.TorgTransRequestReply>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequestReply.BaltikaRequestReply>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequestReply.AtrucksRequestReply>(RequestDiscriminator.Atrucks);

            item.Property(i => i.Id)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.CreatorId)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.CreatedAt)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.UpdatedAt)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.RequestId)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Property(i => i.IsDeleted)
                .HasAnnotation(Annotations.NoTracking, true);

            item.Ignore(i => i.Status);
        }
    }
}