﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.RequestReply
{
    /// <summary>
    ///     Represents the model configuration for RequestReply.
    /// </summary>
    public class RequestReplyOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<RequestReply>(nameof(RequestReply)).EntityType;

            item.Ignore(i => i.CreatedAt);

            item.Ignore(i => i.UpdatedAt);

            item.Ignore(i => i.Status);

            item.Ignore(i => i.IsDeleted);

            item.Function("SendToSign")
                .ReturnsFromEntitySet<RequestReply>(nameof(RequestReply));

            item.Action("Reload");

            item.Action("Cancel");

            item.Action("Accept");

            item.Action("Denied");

            item.Action("RejectFile");

            item.HasKey(p => p.Id);
        }
    }
}