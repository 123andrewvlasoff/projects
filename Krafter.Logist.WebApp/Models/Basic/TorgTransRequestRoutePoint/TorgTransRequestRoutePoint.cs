﻿namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestRoutePoint
{
    public class TorgTransRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new TorgTransRequest.TorgTransRequest Request { get; set; }
    }
}