﻿using System;
using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TorgTransRequestRoutePoint
{
    public class TorgTransRequestRoutePointContextConfiguration : IEntityTypeConfiguration<TorgTransRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<TorgTransRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.TorgTrans);
        }
    }
}