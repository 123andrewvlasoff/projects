using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DelegateDecompiler;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;

namespace Krafter.Logist.WebApp.Models.Basic.Transport
{
    public class Transport : DAL.Models.Transports.Transport
    {
        public new ICollection<TransportKontragent.TransportKontragent> TransportKontragents { get; set; }

        public new DAL.Models.Dictionaries.RollingStockType RollingStockType { get; set; }

        public new DAL.Models.Dictionaries.TransportType TransportType { get; set; }

        public new ICollection<Reis.Reis> Reises { get; set; }

        public new Transport ConnectedTransport { get; set; }

        [NotMapped]
        [Computed]
        public Reis.Reis LastReis => Reises != null
            ? Reises?.Where(r => r.RoutePoints != null).Where(r =>
                    r.Unload?.LeaveDatePlan > DateTime.Now.AddMonths(-6) &&
                    r.StatusId != Cancelled && r.IsActive).OrderByDescending(r => r.Unload?.LeaveDatePlan)
                .FirstOrDefault()
            : null;

        public new ICollection<BundleComment.BundleComment> BundleComments { get; set; }
    }
}