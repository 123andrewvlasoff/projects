using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Transport
{
    public class TransportContextConfiguration : IEntityTypeConfiguration<Transport>
    {
        public void Configure(EntityTypeBuilder<Transport> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(i => i.OsagoSeries);
            item.Ignore(i => i.OsagoNumber);
            item.Ignore(i => i.OsagoIssuedAt);
            item.Ignore(i => i.OsagoValidUntil);

            item.Ignore(i => i.StsSeries);
            item.Ignore(i => i.StsNumber);
            item.Ignore(i => i.StsIssuedAt);
            item.Ignore(i => i.StsValidUntil);

            item.Ignore(i => i.PtsSeries);
            item.Ignore(i => i.PtsNumber);
            item.Ignore(i => i.PtsIssuedAt);
            item.Ignore(i => i.PtsValidUntil);

            item.Ignore(i => i.StsSeries);
            item.Ignore(i => i.StsNumber);
            item.Ignore(i => i.StsIssuedAt);
            item.Ignore(i => i.StsValidUntil);

            item.Ignore(i => i.Inn);
            item.Ignore(i => i.Kpp);
            item.Ignore(i => i.Vin);

            item.Ignore(i => i.OwnerTitle);
            item.Ignore(i => i.OwnerType);
            item.Ignore(i => i.OwnerFirstName);
            item.Ignore(i => i.OwnerLastName);
            item.Ignore(i => i.OwnerPatronymic);
            item.Ignore(i => i.OwnerPhone);
            item.Ignore(i => i.OwnerBirthDate);

            item.HasMany(i => i.Reises)
                .WithOne(r => r.Transport);
        }
    }
}