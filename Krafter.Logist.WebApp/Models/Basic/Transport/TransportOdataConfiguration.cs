﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.Basic.Transport
{
    /// <summary>
    ///     Represents the model configuration for Transport.
    /// </summary>
    public class TransportOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<Transport>(nameof(Transport)).EntityType;

            item.OrderBy();
            item.Ignore(i => i.ExternalId);

            item.Ignore(i => i.OsagoSeries);
            item.Ignore(i => i.OsagoNumber);
            item.Ignore(i => i.OsagoIssuedAt);
            item.Ignore(i => i.OsagoValidUntil);

            item.Ignore(i => i.StsSeries);
            item.Ignore(i => i.StsNumber);
            item.Ignore(i => i.StsIssuedAt);
            item.Ignore(i => i.StsValidUntil);

            item.Ignore(i => i.PtsSeries);
            item.Ignore(i => i.PtsNumber);
            item.Ignore(i => i.PtsIssuedAt);
            item.Ignore(i => i.PtsValidUntil);

            item.Ignore(i => i.StsSeries);
            item.Ignore(i => i.StsNumber);
            item.Ignore(i => i.StsIssuedAt);
            item.Ignore(i => i.StsValidUntil);

            item.Ignore(i => i.Inn);
            item.Ignore(i => i.Kpp);
            item.Ignore(i => i.Vin);

            item.Ignore(i => i.OwnerTitle);
            item.Ignore(i => i.OwnerType);
            item.Ignore(i => i.OwnerFirstName);
            item.Ignore(i => i.OwnerLastName);
            item.Ignore(i => i.OwnerPatronymic);
            item.Ignore(i => i.OwnerPhone);
            item.Ignore(i => i.OwnerBirthDate);

            item.ContainsOptional(i => i.LastReis).OrderBy();

            item.HasKey(p => p.Id);
        }
    }
}