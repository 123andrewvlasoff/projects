﻿using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestReplySuitableStatistic
{
    public class CargomartRequestReplySuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestReplyStatistic Data { get; set; }
    }
}