﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CargomartRequestReplySuitableStatistic
{
    public class
        CargomartRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<
            CargomartRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<CargomartRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}