﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestRoutePoint
{
    public class
        TransporeonRequestRoutePointContextConfiguration : IEntityTypeConfiguration<TransporeonRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<TransporeonRequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TransporeonRequestRoutePoint>(RequestDiscriminator.Transporeon);
        }
    }
}