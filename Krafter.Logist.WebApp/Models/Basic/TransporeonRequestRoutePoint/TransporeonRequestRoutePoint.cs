﻿namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestRoutePoint
{
    public class TransporeonRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new TransporeonRequest.TransporeonRequest Request { get; set; }

    }
}
