﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveTorgTransRequestSuitableStatistic
{
    public class ActiveTorgTransRequestSuitableStatisticContextConfiguration : IEntityTypeConfiguration<
        ActiveTorgTransRequestSuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<ActiveTorgTransRequestSuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}