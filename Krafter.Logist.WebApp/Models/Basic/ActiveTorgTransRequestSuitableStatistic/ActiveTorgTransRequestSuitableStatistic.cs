﻿using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.ActiveTorgTransRequestSuitableStatistic
{
    public class ActiveTorgTransRequestSuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestStatistic Data { get; set; }
    }
}