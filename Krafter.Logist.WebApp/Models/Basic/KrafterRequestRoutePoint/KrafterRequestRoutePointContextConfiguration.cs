using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint
{
    public class KrafterRequestRoutePointContextConfiguration : IEntityTypeConfiguration<KrafterRequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestRoutePoint> item)
        {
            item.HasMany(i => i.Actions).WithOne().HasForeignKey(i => i.RequestId);

            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.Krafter);
        }
    }
}