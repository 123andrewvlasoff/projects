using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint
{
    public class KrafterRequestRoutePoint : RequestRoutePoint.RequestRoutePoint
    {
        public new KrafterRequest.KrafterRequest Request { get; set; }

        public new ICollection<KrafterRequestRoutePointAction.KrafterRequestRoutePointAction> Actions { get; set; }
    }
}