using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Carrier.WebApp.Models.Basic.ReisDispatchingPoint
{
    public class ReisDispatchingPointContextConfiguration : IEntityTypeConfiguration<Logist.WebApp.Models.Basic.ReisDispatchingPoint.ReisDispatchingPoint>
    {
        public void Configure(EntityTypeBuilder<Logist.WebApp.Models.Basic.ReisDispatchingPoint.ReisDispatchingPoint> item)
        {
            item.HasOne(i => i.MonitoringPointType);
            item
                .HasOne(t => t.RoutePoint)
                .WithMany(t => t.ReisDispatchingPoints)
                .HasForeignKey(t => t.ReisRoutePointId);
        }
    }
}