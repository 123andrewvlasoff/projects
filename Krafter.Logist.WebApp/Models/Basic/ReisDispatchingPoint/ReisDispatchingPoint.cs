﻿namespace Krafter.Logist.WebApp.Models.Basic.ReisDispatchingPoint
{
	public class ReisDispatchingPoint : DAL.Models.Requests.Reis.ReisDispatchingPoint
	{
		public new Reis.Reis Reis { get; set; }

		public new ReisRoutePoint.ReisRoutePoint RoutePoint { get; set; }
	}
}
