namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyDraft
{
    public class KrafterRequestReplyDraft : DAL.Models.Requests.Krafter.RequestReplyDraft
    {
        public new KrafterRequest.KrafterRequest Request { get; set; }

        public new Kontragent.Kontragent Kontragent { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        public new Driver.Driver Driver { get; set; }

        public new KontragentContact.KontragentContact KontragentContact { get; set; }
    }
}