using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyDraft
{
    public class KrafterRequestReplyDraftContextConfiguration : IEntityTypeConfiguration<KrafterRequestReplyDraft>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestReplyDraft> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}