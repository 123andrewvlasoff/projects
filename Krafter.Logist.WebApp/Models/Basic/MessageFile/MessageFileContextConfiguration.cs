﻿using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.MessageFile
{
    public class MessageFileContextConfiguration : IEntityTypeConfiguration<MessageFile>
    {
        public void Configure(EntityTypeBuilder<MessageFile> item)
        {
            item.HasDiscriminator().HasValue(Folders.Message);
        }
    }
}