using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.CrafterCompany;

public class CrafterCompany : DAL.Models.Kontragents.CrafterCompany
{
    public new ICollection<KontragentContract.KontragentContract> Contracts { get; set; }
}