﻿using DAL.Models.KontragentUser.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.CarrierKontragentUser
{
    public class CarrierKontragentUserContextConfiguration : IEntityTypeConfiguration<CarrierKontragentUser>
    {
        public void Configure(EntityTypeBuilder<CarrierKontragentUser> item)
        {
            item.HasDiscriminator("Discriminator", typeof(KontragentUserType)).HasValue(KontragentUserType.Carrier);
        }
    }
}