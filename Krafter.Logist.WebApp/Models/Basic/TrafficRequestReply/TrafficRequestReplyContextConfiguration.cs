﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestReply
{
    public class TrafficRequestReplyContextConfiguration : IEntityTypeConfiguration<TrafficRequestReply>
    {
        public void Configure(EntityTypeBuilder<TrafficRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TrafficRequestReply>(RequestDiscriminator.TrafficOnline);
        }
    }
}