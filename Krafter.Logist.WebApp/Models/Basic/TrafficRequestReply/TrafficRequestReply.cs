﻿namespace Krafter.Logist.WebApp.Models.Basic.TrafficRequestReply
{
    public class TrafficRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new Kontragent.Kontragent Kontragent { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }
        public new TrafficRequest.TrafficRequest Request { get; set; }
    }
}