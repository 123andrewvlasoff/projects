﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Problem
{
    public class ProblemContextConfiguration : IEntityTypeConfiguration<Problem>
    {
        public void Configure(EntityTypeBuilder<Problem> item)
        {
            item.HasBaseType((Type) null);
            item.HasQueryFilter(f => f.ReisId != null && f.IsDeleted == false);
        }
    }
}