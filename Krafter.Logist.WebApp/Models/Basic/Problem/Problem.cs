﻿using System.ComponentModel.DataAnnotations.Schema;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.Problem
{
    public class Problem : DAL.Models.Transactions.Problem
    {
        public new Reis.Reis Reis { get; set; }

        [Computed] [NotMapped] public Kontragent.Kontragent Carrier => Reis.Carrier;
    }
}