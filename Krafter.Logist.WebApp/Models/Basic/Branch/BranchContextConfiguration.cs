﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Branch
{
    public class BranchContextConfiguration : IEntityTypeConfiguration<
        DAL.Models.Kontragents.Dictionaries.Branch>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Kontragents.Dictionaries.Branch> item)
        {
        }
    }
}