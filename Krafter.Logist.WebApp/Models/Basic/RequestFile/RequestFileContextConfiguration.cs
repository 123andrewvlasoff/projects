﻿using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestFile
{
    public class RequestFileContextConfiguration : IEntityTypeConfiguration<RequestFile>
    {
        public void Configure(EntityTypeBuilder<RequestFile> item)
        {
            item.HasDiscriminator().HasValue(Folders.Request);
        }
    }
}