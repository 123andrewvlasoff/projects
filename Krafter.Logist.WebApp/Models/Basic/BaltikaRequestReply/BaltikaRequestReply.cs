﻿namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestReply
{
    public class BaltikaRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new BaltikaRequest.BaltikaRequest Request { get; set; }
    }
}
