﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestReply
{
    public class BaltikaRequestReplyContextConfiguration : IEntityTypeConfiguration<BaltikaRequestReply>
    {
        public void Configure(EntityTypeBuilder<BaltikaRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<BaltikaRequestReply>(RequestDiscriminator.Baltika);
        }
    }
}
