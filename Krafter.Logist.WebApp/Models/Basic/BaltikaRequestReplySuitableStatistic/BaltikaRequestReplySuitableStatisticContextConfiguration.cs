﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestReplySuitableStatistic
{
    public class
    BaltikaRequestReplySuitableStatisticContextConfiguration : IEntityTypeConfiguration<BaltikaRequestReplySuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<BaltikaRequestReplySuitableStatistic> item)
        {
            item.Property(i => i.Data).HasColumnName("Data").HasColumnType("jsonb");

            item.HasDiscriminator();
        }
    }
}
