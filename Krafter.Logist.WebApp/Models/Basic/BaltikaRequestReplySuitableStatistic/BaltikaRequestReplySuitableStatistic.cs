﻿using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;

namespace Krafter.Logist.WebApp.Models.Basic.BaltikaRequestReplySuitableStatistic
{
    public class BaltikaRequestReplySuitableStatistic : SuitableStatistic.SuitableStatistic
    {
        public new RequestReplyStatistic Data { get; set; }
    }
}
