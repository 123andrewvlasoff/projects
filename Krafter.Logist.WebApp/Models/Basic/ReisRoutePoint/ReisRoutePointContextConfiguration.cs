using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint
{
    public class ReisRoutePointContextConfiguration : IEntityTypeConfiguration<ReisRoutePoint>
    {
        public void Configure(EntityTypeBuilder<ReisRoutePoint> item)
        {
            item.HasBaseType((Type)null);
            item
                .HasMany(t => t.ReisDispatchingPoints)
                .WithOne(t => t.RoutePoint)
                .HasForeignKey(t => t.ReisRoutePointId);
        }
    }
}