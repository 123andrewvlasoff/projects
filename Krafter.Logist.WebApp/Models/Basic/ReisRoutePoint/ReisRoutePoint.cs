﻿using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint
{
    public class ReisRoutePoint : DAL.Models.Requests.Reis.ReisRoutePoint
    {
        public new Reis.Reis Reis { get; set; }
        public new ICollection<ReisDispatchingPoint.ReisDispatchingPoint> ReisDispatchingPoints { get; set; }
    }
}