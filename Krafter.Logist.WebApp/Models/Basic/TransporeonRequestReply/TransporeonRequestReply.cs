﻿namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReply
{
    public class TransporeonRequestReply : AuctionRequestReply.AuctionRequestReply
    {
        public new TransporeonRequest.TransporeonRequest Request { get; set; }
    }
}
