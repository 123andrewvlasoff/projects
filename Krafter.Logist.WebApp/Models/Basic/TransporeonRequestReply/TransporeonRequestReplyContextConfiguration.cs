﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReply
{
    public class TransporeonRequestReplyContextConfiguration : IEntityTypeConfiguration<TransporeonRequestReply>
    {
        public void Configure(EntityTypeBuilder<TransporeonRequestReply> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<TransporeonRequestReply>(RequestDiscriminator.Transporeon);
        }
    }
}
