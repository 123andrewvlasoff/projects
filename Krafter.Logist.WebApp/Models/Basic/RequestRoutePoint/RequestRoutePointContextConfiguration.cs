using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RequestRoutePoint
{
    public class RequestRoutePointContextConfiguration : IEntityTypeConfiguration<RequestRoutePoint>
    {
        public void Configure(EntityTypeBuilder<RequestRoutePoint> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<TrafficRequestRoutePoint.TrafficRequestRoutePoint>(RequestDiscriminator.TrafficOnline)
                .HasValue<KrafterRequestRoutePoint.KrafterRequestRoutePoint>(RequestDiscriminator.Krafter)
                .HasValue<CargomartRequestRoutePoint.CargomartRequestRoutePoint>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestRoutePoint.TransporeonRequestRoutePoint>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequestRoutePoint.TorgTransRequestRoutePoint>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequestRoutePoint.BaltikaRequestRoutePoint>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequestRoutePoint.AtrucksRequestRoutePoint>(RequestDiscriminator.Atrucks);

            item.Ignore(i => i.ExternalId);
        }
    }
}