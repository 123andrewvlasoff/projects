﻿using DAL.Models.KontragentUser.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser
{
    public class LogistKontragentUserContextConfiguration : IEntityTypeConfiguration<LogistKontragentUser>
    {
        public void Configure(EntityTypeBuilder<LogistKontragentUser> item)
        {
            item.HasDiscriminator("Discriminator", typeof(KontragentUserType)).HasValue(KontragentUserType.Logist);
        }
    }
}