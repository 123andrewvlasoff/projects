﻿namespace Krafter.Logist.WebApp.Models.Basic.SuitableStatistic
{
    public class SuitableStatistic : DAL.Models.SuitableStatistic.SuitableStatistic
    {
        public new Kontragent.Kontragent Kontragent { get; set; }

        public new Transport.Transport Transport { get; set; }

        public new Transport.Transport ConnectedTransport { get; set; }

        public new User.User Creator { get; set; }
    }
}