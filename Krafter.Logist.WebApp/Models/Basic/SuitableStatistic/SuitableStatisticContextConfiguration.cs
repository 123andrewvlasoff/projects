﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.SuitableStatistic
{
    public class SuitableStatisticContextConfiguration : IEntityTypeConfiguration<SuitableStatistic>
    {
        public void Configure(EntityTypeBuilder<SuitableStatistic> item)
        {
            item.HasBaseType((Type)null);
        }
    }
}