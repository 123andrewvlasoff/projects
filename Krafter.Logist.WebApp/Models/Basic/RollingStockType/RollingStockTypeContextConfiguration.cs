using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.RollingStockType
{
    public class
        RollingStockTypeContextConfiguration : IEntityTypeConfiguration<DAL.Models.Dictionaries.RollingStockType>
    {
        public void Configure(EntityTypeBuilder<DAL.Models.Dictionaries.RollingStockType> item)
        {
            item.Ignore(i => i.SortOrder);
        }
    }
}