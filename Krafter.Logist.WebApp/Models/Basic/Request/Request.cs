﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.Basic.Request
{
    public class Request : DAL.Models.Requests.Request
    {
        public new Kontragent.Kontragent Client { get; set; }

        protected internal new ICollection<RequestReply.RequestReply> Replies { get; set; }
        
        protected internal new ICollection<RequestRoutePoint.RequestRoutePoint> RoutePoints { get; set; }

        
        [NotMapped]
        [Computed]
        protected internal RequestRoutePoint.RequestRoutePoint Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        protected internal RequestRoutePoint.RequestRoutePoint Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);
        
        public new User.User AssignedUser { get; set; }

        public new User.User InProgressUser { get; set; }

        public new Employer.Employer СreatedBy1C { get; set; }

        public new ICollection<UserRequest.UserRequest> UserRequests { get; set; }
    }
}