﻿using System;
using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.Basic.Request
{
    public class RequestContextConfiguration : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> item)
        {
            item.HasBaseType((Type)null);

            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<AuctionRequest.AuctionRequest>(RequestDiscriminator.AuctionBase)
                .HasValue<TrafficRequest.TrafficRequest>(RequestDiscriminator.TrafficOnline)
                .HasValue<KrafterRequest.KrafterRequest>(RequestDiscriminator.Krafter)
                .HasValue<CargomartRequest.CargomartRequest>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequest.TransporeonRequest>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequest.TorgTransRequest>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequest.BaltikaRequest>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequest.AtrucksRequest>(RequestDiscriminator.Atrucks);
        }
    }
}