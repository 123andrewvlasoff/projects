﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestRoutePointDto
{
    public class AuctionRequestRoutePointDtoContextConfiguration : IEntityTypeConfiguration<AuctionRequestRoutePointDto>
    {
        public void Configure(EntityTypeBuilder<AuctionRequestRoutePointDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.AuctionBase)
                .HasValue<TrafficRequestRoutePointDto.TrafficRequestRoutePointDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequestRoutePointDto.CargomartRequestRoutePointDto>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestRoutePointDto.TransporeonRequestRoutePointDto>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRoutePointDto.TorgTransRoutePointDto>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequestRoutePointDto.BaltikaRequestRoutePointDto>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequestRoutePointDto.AtrucksRequestRoutePointDto>(RequestDiscriminator.Atrucks);
        }
    }
}