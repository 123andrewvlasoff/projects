using System;
using DAL.Models.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.EmployerDto
{
    public class EmployerDtoContextConfiguration : IEntityTypeConfiguration<EmployerDto>
    {
        public void Configure(EntityTypeBuilder<EmployerDto> item)
        {
        }
    }
}