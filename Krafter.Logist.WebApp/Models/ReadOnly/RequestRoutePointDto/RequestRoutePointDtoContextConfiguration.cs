using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto
{
    public class RequestRoutePointDtoContextConfiguration : IEntityTypeConfiguration<RequestRoutePointDto>
    {
        public void Configure(EntityTypeBuilder<RequestRoutePointDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<KrafterRequestRoutePointDto.KrafterRequestRoutePointDto>(RequestDiscriminator.Krafter)
                .HasValue<AuctionRequestRoutePointDto.AuctionRequestRoutePointDto>(RequestDiscriminator.AuctionBase)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<TrafficRequestRoutePointDto.TrafficRequestRoutePointDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequestRoutePointDto.CargomartRequestRoutePointDto>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestRoutePointDto.TransporeonRequestRoutePointDto>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRoutePointDto.TorgTransRoutePointDto>(RequestDiscriminator.TorgTrans);

            item.Ignore(i => i.ExternalId);
        }
    }
}