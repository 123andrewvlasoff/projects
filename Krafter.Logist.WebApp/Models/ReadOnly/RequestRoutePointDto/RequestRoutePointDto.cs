namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto
{
    public class RequestRoutePointDto : DAL.Models.Requests.RequestRoutePoint
    {
        public RequestDto.RequestDto Request { get; set; }
    }
}