﻿using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models.Requests.Auction.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto
{
    public class AuctionRequestDto : RequestDto.RequestDto
    {
        [NotMapped] [Computed] public decimal? PredictedFinalPriceForKm => PredictedFinalPrice / (decimal?)Distance;

        [NotMapped] [Computed] public decimal CurrentPrice => ActualPrice <= Price ? ActualPrice : Price;

        [NotMapped] [Computed] public decimal BidPrice => CurrentPrice - Step;

        public string ExternalUrl { get; set; }

        public decimal? MinPrice { get; set; }

        public decimal? PredictedFinalPrice { get; set; }

        public decimal Step { get; set; }

        public int BidCount { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactMail { get; set; }

        public AuctionRequestStatus? AuctionRequestStatus { get; set; }

        public bool? BetWasPlaced { get; set; }
    }
}