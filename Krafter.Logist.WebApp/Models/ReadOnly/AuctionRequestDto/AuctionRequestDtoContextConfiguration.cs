﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto
{
    public class AuctionRequestDtoContextConfiguration : IEntityTypeConfiguration<AuctionRequestDto>
    {
        public void Configure(EntityTypeBuilder<AuctionRequestDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.AuctionBase)
                .HasValue<TrafficRequestDto.TrafficRequestDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequestDto.CargomartRequestDto>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestDto.TransporeonRequestDto>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequestDto.TorgTransRequestDto>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequestDto.BaltikaRequestDto>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequestDto.AtrucksRequestDto>(RequestDiscriminator.Atrucks);
        }
    }
}