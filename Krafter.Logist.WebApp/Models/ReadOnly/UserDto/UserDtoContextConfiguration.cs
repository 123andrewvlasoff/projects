using System;
using DAL.Models.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.UserDto
{
    public class UserDtoContextConfiguration : IEntityTypeConfiguration<UserDto>
    {
        public void Configure(EntityTypeBuilder<UserDto> item)
        {
            item.ToTable("AspNetUsers");
            item.HasOne(i => i.Employer).WithOne().HasForeignKey<EmployerDto.EmployerDto>(i => i.UserId);
        }
    }
}