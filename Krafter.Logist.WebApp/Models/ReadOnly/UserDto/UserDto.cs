using System.Collections.Generic;
using DAL.Models.Users;

namespace Krafter.Logist.WebApp.Models.ReadOnly.UserDto
{
    public class UserDto : User
    {
        public new EmployerDto.EmployerDto Employer { get; set; }
    }
}