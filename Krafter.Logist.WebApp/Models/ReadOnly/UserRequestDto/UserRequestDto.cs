using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Models.Requests.Enums;

namespace Krafter.Logist.WebApp.Models.ReadOnly.UserRequestDto
{
    public class UserRequestDto : DAL.Models.Requests.UserRequest.UserRequest, IValidatableObject
    {
        public new UserDto.UserDto User { get; set; }

        public new RequestDto.RequestDto Request { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Status != UserRequestStatus.Favorite && Status != UserRequestStatus.Hidden)
            {
                yield return new ValidationResult("Invalid Status", new[] { nameof(Status) });
            }
        }
    }
}