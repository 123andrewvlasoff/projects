using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.UserRequestDto
{
    public class UserRequestDtoContextConfiguration : IEntityTypeConfiguration<UserRequestDto>
    {
        public void Configure(EntityTypeBuilder<UserRequestDto> item)
        {
            item.HasNoDiscriminator();
        }
    }
}