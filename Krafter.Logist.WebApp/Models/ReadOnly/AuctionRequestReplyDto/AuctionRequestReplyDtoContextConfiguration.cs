﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestReplyDto
{
    public class AuctionRequestReplyDtoContextConfiguration : IEntityTypeConfiguration<AuctionRequestReplyDto>
    {
        public void Configure(EntityTypeBuilder<AuctionRequestReplyDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.AuctionBase)
                .HasValue<TrafficRequestReplyDto.TrafficRequestReplyDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequestReplyDto.CargomartRequestReplyDto>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestReplyDto.TransporeonRequestReplyDto>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequestReplyDto.TorgTransRequestReplyDto>(RequestDiscriminator.TorgTrans)
                .HasValue<BaltikaRequestReplyDto.BaltikaRequestReplyDto>(RequestDiscriminator.Baltika)
                .HasValue<AtrucksRequestReplyDto.AtrucksRequestReplyDto>(RequestDiscriminator.Atrucks);
        }
    }
}