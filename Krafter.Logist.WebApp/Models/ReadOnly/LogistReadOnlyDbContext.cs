using System;
using System.Linq;
using DAL.Models.Requests;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestReplyDto;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KontragentDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KontragentUserDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestReplyDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserDto;
using Krafter.ServiceCommon.Models;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Models.ReadOnly
{
    public class LogistReadOnlyDbContext : DbContext
    {
        public LogistReadOnlyDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<RequestRoutePointDto.RequestRoutePointDto> RequestRoutePoints { get; set; }

        public DbSet<AuctionRequestRoutePointDto.AuctionRequestRoutePointDto> AuctionRequestRoutePoints { get; set; }

        public DbSet<KrafterRequestRoutePointDto.KrafterRequestRoutePointDto> KrafterRequestRoutePoints { get; set; }

        public DbSet<RequestReplyDto.RequestReplyDto> RequestReplies { get; set; }

        public DbSet<RequestActionDto.RequestActionDto> RequestActions { get; set; }

        public DbSet<DAL.Models.Requests.Dictionaries.Region> Regions { get; set; }

        public DbSet<DAL.Models.Requests.Dictionaries.Settlement> Settlements { get; set; }

        public DbSet<RequestDto.RequestDto> Requests { get; set; }

        public DbSet<AuctionRequestDto.AuctionRequestDto> AuctionRequests { get; set; }

        public DbSet<KrafterRequestDto.KrafterRequestDto> KrafterRequests { get; set; }

        public DbSet<KontragentDto.KontragentDto> Kontragents { get; set; }

        public DbSet<UserRequestDto.UserRequestDto> UserRequests { get; set; }

        public DbSet<UserDto.UserDto> Users { get; set; }

        public DbSet<EmployerDto.EmployerDto> Employers { get; set; }

        public DbSet<KontragentUserDto.KontragentUserDto> KontragentUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RequestReplyDto.RequestReplyDto>().HasQueryFilter(p =>
                !p.IsDeleted);

            modelBuilder.Entity<RequestDto.RequestDto>().HasQueryFilter(i =>
                new[]
                {
                    RequestStatus.IsActive, RequestStatus.DataTreatment, RequestStatus.DataInput, RequestStatus.Error
                }.Contains(i.RequestStatus)||i.RequestStatus==RequestStatus.Cancelled&&i.ActiveUntil>DateTime.UtcNow.AddHours(3));

            modelBuilder.ApplyConfiguration(new RequestReplyDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new AuctionRequestDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new AuctionRequestRoutePointDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new AuctionRequestReplyDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestRoutePointDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new UserDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new RequestActionDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new KontragentUserDtoContextConfiguration());
            modelBuilder.ApplyConfiguration(new KrafterRequestDtoContextConfiguration());
        }
    }
}