﻿using System;
using AutoMapper;
using Krafter.Logist.WebApp.Helpers;
using Krafter.ServiceCommon.Helpers;
using Microsoft.AspNetCore.Http;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto
{
    public class KrafterRequestDtoProfile : Profile
    {
        public KrafterRequestDtoProfile()
        {
            CreateMap<KrafterRequestDto, KrafterRequestDto>()
                .ForMember(dest => dest.BookUntil,
                    opt => opt.MapFrom<BookUntilDateResolver>());
        }
    }

    public class BookUntilDateResolver : IValueResolver<KrafterRequestDto, KrafterRequestDto, DateTime>
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public BookUntilDateResolver(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public DateTime Resolve(KrafterRequestDto source, KrafterRequestDto destination, DateTime destMember,
            ResolutionContext context)
        {
            return TakeInProgressUntilHelper.GetTakeUntilDate(source.Load.ArrivalDate);
        }
    }
}