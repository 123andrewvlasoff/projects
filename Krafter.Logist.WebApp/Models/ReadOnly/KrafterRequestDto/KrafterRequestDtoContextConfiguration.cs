using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto
{
    public class KrafterRequestDtoContextConfiguration: IEntityTypeConfiguration<KrafterRequestDto>
    {
        public void Configure(EntityTypeBuilder<KrafterRequestDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue(RequestDiscriminator.Krafter);
        }
    }
}