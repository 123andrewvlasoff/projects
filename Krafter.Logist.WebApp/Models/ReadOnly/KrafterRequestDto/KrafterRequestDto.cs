using System.ComponentModel.DataAnnotations.Schema;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto
{
    public class KrafterRequestDto : RequestDto.RequestDto
    {
        [Computed]
        [NotMapped]
        public decimal? PriceForKm => Distance != null && Distance != 0 ? ClientPrice / (decimal) Distance : 0;

        public DAL.Models.Requests.Krafter.Enums.KrafterRequestStatus? KrafterRequestStatus { get; set; }
    }
}