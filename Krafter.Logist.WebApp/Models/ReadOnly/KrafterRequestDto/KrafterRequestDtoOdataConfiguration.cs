﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto
{
    public class KrafterRequestDtoOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<KrafterRequestDto>(nameof(KrafterRequestDto)).EntityType;

            item.OrderBy();

            item.Property(i => i.PriceForKm);

            item.Property(i => i.ActionCount);
            
            item.Property(i => i.TotalActionCount);

            item.Property(i => i.LastActionDate);

            item.Property(i => i.BookUntil);

            item.Ignore(i => i.UpdatedAt);

            item.Ignore(i => i.ActualPrice);

            item.ContainsRequired(i => i.Unload).OrderBy();

            item.ContainsRequired(i => i.Load).OrderBy();
        }
    }
}