using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KontragentDto
{
    public class KontragentDto : DAL.Models.Kontragents.Kontragent
    {
        public new ICollection<KontragentUserDto.KontragentUserDto> KontragentUsers { get; set; }
    }
}