﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KontragentDto
{
    public class KontragentDtoOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<KontragentDto>(nameof(KontragentDto)).EntityType;

            item.DerivesFromNothing();
            
            item.OrderBy();
            
            item.Ignore(i => i.ExternalId);

            item.Ignore(i => i.Kpp);
            
            item.Ignore(i => i.Ogrn);
            
            item.Ignore(i => i.Okpo);

            item.Ignore(i => i.ManagerFio);
            
            item.Ignore(i => i.CreatedAt);
            
            item.Ignore(i => i.UpdatedAt);
        }
    }
}