using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KontragentDto
{
    public class KontragentDtoContextConfiguration : IEntityTypeConfiguration<KontragentDto>
    {
        public void Configure(EntityTypeBuilder<KontragentDto> item)
        {
            item.HasBaseType((Type)null);

            item.Ignore(i => i.Inn);
            
            item.Ignore(i => i.Kpp);
            
            item.Ignore(i => i.Ogrn);
            
            item.Ignore(i => i.Okpo);

            item.Ignore(i => i.ManagerFio);

            item.Ignore(i => i.CreatedAt);
            
            item.Ignore(i => i.UpdatedAt);
        }
    }
}