﻿using System;
using System.Linq;
using AutoMapper;
using DAL.Models.Requests.Guaranties.Enums;
using Krafter.Logist.WebApp.Helpers;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestDto
{
    public class RequestDtoProfile : Profile
    {
        public RequestDtoProfile()
        {
            CreateMap<RequestDto, RequestDto>()
                .ForMember(dest => dest.BookUntil,
                    opt => opt.MapFrom<BookUntilDateResolver>())
                .ForMember(dest => dest.LastActionDate,
                    opt => opt.MapFrom<LastActionDateResolver>())
                .ForMember(dest => dest.ActionCount,
                    opt => opt.MapFrom<ActionCountDateResolver>())
                .ForMember(dest => dest.TotalActionCount,
                    opt => opt.MapFrom<TotalActionCountDateResolver>())
                .ForMember(dest => dest.PriceForKm, opt => opt.MapFrom<KrafterRequestDtoPriceForKmResolver>())
                .AddTransform<DateTime?>(dt => dt.HasValue ? new DateTime(dt.Value.Ticks, DateTimeKind.Local).ToLocalTime() : null)
                .AddTransform<DateTime>(dt => new DateTime(dt.Ticks, DateTimeKind.Local).ToLocalTime());
        }
    }

    public class BookUntilDateResolver : IValueResolver<RequestDto, RequestDto, DateTime>
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly UserManager<User> userManager;

        public BookUntilDateResolver(IHttpContextAccessor httpContextAccessor, UserManager<User> userManager)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.userManager = userManager;
        }

        public DateTime Resolve(RequestDto source, RequestDto destination, DateTime destMember,
            ResolutionContext context)
        {
            return TakeInProgressUntilHelper.GetTakeUntilDate(source.Load.ArrivalDate);
        }
    }

    public class LastActionDateResolver : IValueResolver<RequestDto, RequestDto, DateTime?>
    {
        private readonly IConfiguration configuration;

        public LastActionDateResolver(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public DateTime? Resolve(RequestDto source, RequestDto destination, DateTime? destMember,
            ResolutionContext context)
        {
            var actionTimes = source.Actions?.Where(i =>
                    i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == source.InProgressUserId)
                .Select(i => i.ActionDate)
                .OrderBy(o => o)
                .ToList();

            if (actionTimes == null || !actionTimes.Any())
            {
                return null;
            }

            if (source.Actions.Any(i => (i.State == RequestActionStatus.FreeInProgress ||
                                         i.State == RequestActionStatus.FreeInProgressAutomatically) &&
                                        i.ActionUserId == source.InProgressUserId))
            {
                return source.Actions.OrderByDescending(i => i.ActionDate).FirstOrDefault(i =>
                        i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == source.InProgressUserId)
                    ?.ActionDate
                    .AddMinutes(configuration.GetMinutesInterval());
            }

            var totalMinutes = default(double);

            for (var i = 0; i < actionTimes.Count - 1; i++)
            {
                totalMinutes += (actionTimes[i].AddMinutes(configuration.GetMinutesInterval()) - actionTimes[i + 1])
                    .TotalMinutes;
            }

            var lastAction = actionTimes.OrderByDescending(i => i).FirstOrDefault();

            return lastAction.AddMinutes(totalMinutes + configuration.GetMinutesInterval());
        }
    }

    public class ActionCountDateResolver : IValueResolver<RequestDto, RequestDto, int>
    {
        public int Resolve(RequestDto source, RequestDto destination, int destMember,
            ResolutionContext context)
        {
            var userId = context.Items["UserId"].ToString();

            if (source.Actions == null)
            {
                return 0;
            }

            return source.Actions
                .Count(i => i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == userId);
        }
    }

    public class TotalActionCountDateResolver : IValueResolver<RequestDto, RequestDto, int>
    {
        public int Resolve(RequestDto source, RequestDto destination, int destMember, ResolutionContext context)
        {
            if (source.Actions == null)
            {
                return 0;
            }

            return source.Actions.Count(i =>
                i.State == RequestActionStatus.TakeInProgress && i.ActionUserId == source.InProgressUserId);
        }
    }

    public class KrafterRequestDtoPriceForKmResolver : IValueResolver<RequestDto, RequestDto, decimal?>
    {
        private readonly IConfiguration configuration;

        public KrafterRequestDtoPriceForKmResolver(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public decimal? Resolve(RequestDto source, RequestDto destination, decimal? destMember, ResolutionContext context)
        {
            return source.Distance != null && source.Distance != 0 ? source.ClientPrice  / (decimal)source.Distance : 0;
        }
    }
}