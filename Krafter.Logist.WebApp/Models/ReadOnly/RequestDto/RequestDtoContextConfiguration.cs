﻿using System;
using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestDto
{
    public class RequestDtoContextConfiguration : IEntityTypeConfiguration<RequestDto>
    {
        public void Configure(EntityTypeBuilder<RequestDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<KrafterRequestDto.KrafterRequestDto>(RequestDiscriminator.Krafter)
                .HasValue<AuctionRequestDto.AuctionRequestDto>(RequestDiscriminator.AuctionBase)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<TrafficRequestDto.TrafficRequestDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<CargomartRequestDto.CargomartRequestDto>(RequestDiscriminator.Cargomart)
                .HasValue<TransporeonRequestDto.TransporeonRequestDto>(RequestDiscriminator.Transporeon)
                .HasValue<TorgTransRequestDto.TorgTransRequestDto>(RequestDiscriminator.TorgTrans);
        }
    }
}