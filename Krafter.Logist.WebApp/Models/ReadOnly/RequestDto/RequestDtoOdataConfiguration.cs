﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestDto
{
    public class RequestDtoOdataConfiguration : IModelConfiguration
    {
        /// <summary>
        ///     Applies model configurations using the provided builder for the specified API version.
        /// </summary>
        /// <param name="builder">The <see cref="ODataModelBuilder">builder</see> used to apply configurations.</param>
        /// <param name="apiVersion">The <see cref="ApiVersion">API version</see> associated with the <paramref name="builder" />.</param>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var item = builder.EntitySet<RequestDto>(nameof(RequestDto)).EntityType;

            item.Collection.Function("TypesCount").Returns<RequestsTypesCountDto.RequestsTypesCountDto>();

            item.OrderBy();

            item.Property(i => i.PriceForKm);

            item.Property(i => i.LastActionDate);
            
            item.Property(i => i.ActionCount);
            
            item.Property(i => i.TotalActionCount);

            item.Property(i => i.BookUntil);
           
            item.Property(i => i.Remoteness);

            item.Property(i => i.RequestDateInterval);

            item.Property(i => i.CarrierOffersCount);

            item.Ignore(i => i.UpdatedAt);

            item.Ignore(i => i.ActualPrice);

            item.ContainsRequired(i => i.Unload).OrderBy();

            item.ContainsRequired(i => i.Load).OrderBy();
        }
    }
}