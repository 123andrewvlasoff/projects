﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestDto
{
    public class RequestDto : DAL.Models.Requests.Request
    {
        [Computed]
        [NotMapped]
        public decimal? PriceForKm => Distance != null && Distance != 0 ? Price / (decimal)Distance : 0;

        public new KontragentDto.KontragentDto Client { get; set; }

        [NotMapped]
        [Computed]
        public RequestRoutePointDto.RequestRoutePointDto Load =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Load);

        [NotMapped]
        [Computed]
        public RequestRoutePointDto.RequestRoutePointDto Unload =>
            RoutePoints.FirstOrDefault(y => y.PointType == PointType.Unload);

        public new ICollection<RequestRoutePointDto.RequestRoutePointDto> RoutePoints { get; set; } =
            new List<RequestRoutePointDto.RequestRoutePointDto>();

        public new ICollection<RequestReplyDto.RequestReplyDto> Replies { get; set; }

        public new ICollection<UserRequestDto.UserRequestDto> UserRequests { get; set; }

        public new UserDto.UserDto AssignedUser { get; set; }

        public new UserDto.UserDto InProgressUser { get; set; }

        [NotMapped] public DateTime? LastActionDate { get; set; }

        [NotMapped] public int ActionCount { get; set; }
        
        [NotMapped] public int TotalActionCount { get; set; }

        [NotMapped] public DateTime BookUntil { get; set; }
        
        [NotMapped]
        public double Remoteness { get; set; }

        [NotMapped]
        public TimeSpan RequestDateInterval { get; set; }

        public new ICollection<RequestActionDto.RequestActionDto> Actions { get; set; }
        [NotMapped] public int CarrierOffersCount { get; set; }
    }
}