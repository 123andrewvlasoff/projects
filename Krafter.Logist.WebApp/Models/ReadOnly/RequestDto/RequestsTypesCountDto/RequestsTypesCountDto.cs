namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestDto.RequestsTypesCountDto
{
    public class RequestsTypesCountDto
    {
        public int All { get; set; }
        public int Favorite { get; set; }
        public int MyRequest { get; set; }
        public int InProgress { get; set; }
        public int OnSign { get; set; }
        public int ApproveInput { get; set; }
        public int Cancelled { get; set; }
        public int Bidding { get; set; }
    }
}