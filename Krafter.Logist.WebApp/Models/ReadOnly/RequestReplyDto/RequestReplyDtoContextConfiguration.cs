﻿using DAL.Models.Requests.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestReplyDto
{
    public class RequestReplyDtoContextConfiguration : IEntityTypeConfiguration<RequestReplyDto>
    {
        public void Configure(EntityTypeBuilder<RequestReplyDto> item)
        {
            item.HasDiscriminator(i => i.Discriminator)
                .HasValue<KrafterRequestReplyDto.KrafterRequestReplyDto>(RequestDiscriminator.Krafter)
                .HasValue<AuctionRequestReplyDto.AuctionRequestReplyDto>(RequestDiscriminator.AuctionBase)
                .HasValue(RequestDiscriminator.Base)
                .HasValue<TrafficRequestReplyDto.TrafficRequestReplyDto>(RequestDiscriminator.TrafficOnline)
                .HasValue<TransporeonRequestReplyDto.TransporeonRequestReplyDto>(RequestDiscriminator.Transporeon)
                .HasValue<CargomartRequestReplyDto.CargomartRequestReplyDto>(RequestDiscriminator.Cargomart)
                .HasValue<TorgTransRequestReplyDto.TorgTransRequestReplyDto>(RequestDiscriminator.TorgTrans);

            item.Ignore(i => i.Status);
        }
    }
}