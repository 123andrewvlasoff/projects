namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestReplyDto
{
    public class RequestReplyDto : DAL.Models.Requests.RequestReply
    {
        public RequestDto.RequestDto Request { get; set; }

        public new UserDto.UserDto Creator { get; set; }
    }
}