﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto
{
    public class RequestActionDtoOdataConfiguration : IEntityTypeConfiguration<RequestActionDto>
    {
        public void Configure(EntityTypeBuilder<RequestActionDto> item)
        {
            item.Ignore(i => i.Changes);
        }
    }
}