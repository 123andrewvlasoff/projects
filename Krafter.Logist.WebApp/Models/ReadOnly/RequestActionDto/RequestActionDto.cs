﻿namespace Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto
{
    public class RequestActionDto : DAL.Models.Requests.Krafter.Actions.RequestAction
    {
        public new RequestDto.RequestDto Request { get; set; }
    }
}