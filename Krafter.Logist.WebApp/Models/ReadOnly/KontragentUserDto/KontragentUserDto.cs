namespace Krafter.Logist.WebApp.Models.ReadOnly.KontragentUserDto
{
    public class KontragentUserDto : DAL.Models.KontragentUser.KontragentUser
    {
        public new UserDto.UserDto User { get; set; }

        public new KontragentDto.KontragentDto Kontragent { get; set; }
    }
}