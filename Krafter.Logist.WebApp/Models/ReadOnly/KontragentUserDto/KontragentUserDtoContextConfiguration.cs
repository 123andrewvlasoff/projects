using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Krafter.Logist.WebApp.Models.ReadOnly.KontragentUserDto
{
    public class KontragentUserDtoContextConfiguration : IEntityTypeConfiguration<KontragentUserDto>
    {
        public void Configure(EntityTypeBuilder<KontragentUserDto> item)
        {
            item.HasBaseType((Type) null);
        }
    }
}