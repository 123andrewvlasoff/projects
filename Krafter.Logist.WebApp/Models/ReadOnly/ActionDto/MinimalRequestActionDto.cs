using System;
using DAL.Models.Requests.Guaranties.Enums;

namespace Krafter.Logist.WebApp.Models.ReadOnly.ActionDto;

public class MinimalRequestActionDto
{
    public Guid Id { get; set; }
    
    public RequestActionStatus State { get; set; }
}