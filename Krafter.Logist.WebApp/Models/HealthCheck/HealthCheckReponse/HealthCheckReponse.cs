﻿using System;
using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Models.HealthCheck.HealthCheckReponse
{
    public class HealthCheckReponse
    {
        public string Status { get; set; }

        public TimeSpan HealthCheckDuration { get; set; }

        public IEnumerable<IndividualHealthCheckResponse.IndividualHealthCheckResponse> HealthChecks { get; set; }
    }
}