﻿namespace Krafter.Logist.WebApp.Models.HealthCheck.IndividualHealthCheckResponse
{
    public class IndividualHealthCheckResponse
    {
        public string Status { get; set; }

        public string Component { get; set; }

        public string Description { get; set; }
    }
}