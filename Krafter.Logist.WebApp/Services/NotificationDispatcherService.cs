using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Drafts.Enums;
using DAL.Models.KontragentUser.Enums;
using DelegateDecompiler;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Helpers;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Helpers;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Services
{
    public class NotificationDispatcherService
    {
        //Определяет каналы отправки необязательных уведомлений
        protected readonly LogistDbContext dbContext;

        public NotificationDispatcherService(LogistDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
       
        public async Task NotifyReisProblemRevealed(Guid reisId)
        {
            var reis = await dbContext.Reises.IgnoreQueryFilters()
                .Include(d => d.DispatchingPoints)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            var driverKontragents = await dbContext.DriverKontragents
                .Where(item => item.DriverId == reis.DriverId)
                .Include(item => item.Kontragent)
                .ThenInclude(item => item.KontragentUsers)
                .ThenInclude(item => item.User)
                .ThenInclude(item => item.Employer).ToListAsync();
            
            var logist = driverKontragents.Select(item => item.Kontragent.KontragentUsers
                    .Where(p => p.RelationType == DAL.Models.Enums.RelationType.Pinned)
                    .Select(p => p.User?.Employer))
                .SelectMany(item => item).FirstOrDefault();

            if (logist != null)
            {
                BackgroundJob.Enqueue<EmailSenderService>(j =>
                    j.SendReisDocsProblemRevealedEmail(reisId, logist.Email));
                // //
                BackgroundJob.Enqueue<NotificationsService>(j =>
                    j.AddReisDocsProblemRevealedNotification(reisId, logist.UserId));
            }

        }
        
        public async Task NotifyReisCompleted(Guid reisId)
        {
            var reis = await dbContext.Reises.IgnoreQueryFilters()
                .Include(d => d.DispatchingPoints)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            var driverKontragents = await dbContext.DriverKontragents
                .Where(item => item.DriverId == reis.DriverId)
                .Include(item => item.Kontragent)
                .ThenInclude(item => item.KontragentUsers)
                .ThenInclude(item => item.User)
                .ThenInclude(item => item.Employer).ToListAsync();
            
            var logist = driverKontragents.Select(item => item.Kontragent.KontragentUsers
                    .Where(p => p.RelationType == DAL.Models.Enums.RelationType.Pinned)
                    .Select(p => p.User?.Employer))
                .SelectMany(item => item).FirstOrDefault();
            
            if (logist != null)
            {
                BackgroundJob.Enqueue<EmailSenderService>(j =>
                    j.SendReisCompletedEmail(reisId, logist.Email));
                //
                BackgroundJob.Enqueue<NotificationsService>(j =>
                    j.AddReisCompletedNotification(reisId, logist.UserId));
            }
            
            
        }
        
        public async Task NotifyReisDocsOnVerification(Guid reisId)
        {
            var reis = await dbContext.Reises.IgnoreQueryFilters()
                .Include(d => d.DispatchingPoints)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            var driverKontragents = await dbContext.DriverKontragents
                .Where(item => item.DriverId == reis.DriverId)
                .Include(item => item.Kontragent)
                .ThenInclude(item => item.KontragentUsers)
                .ThenInclude(item => item.User)
                .ThenInclude(item => item.Employer).ToListAsync();
            
            var logist = driverKontragents.Select(item => item.Kontragent.KontragentUsers
                    .Where(p => p.RelationType == DAL.Models.Enums.RelationType.Pinned)
                    .Select(p => p.User?.Employer))
                .SelectMany(item => item).FirstOrDefault();
            
            if (logist != null)
            {
                BackgroundJob.Enqueue<EmailSenderService>(j => j.SendDocsOnVerificationEmail(reisId, logist.Email));
        
                BackgroundJob.Enqueue<NotificationsService>(j =>
                    j.AddDocsOnVerificationNotification(reisId, logist.UserId));
            }
        }
        
        public async Task NotifyDriverDontGaveDocumentsUp(Guid reisId)
        {
            var point = await dbContext.ReisDispatchingPoints
                .IgnoreQueryFilters()
                .IncludeOptimized(r => r.MonitoringPointType)
                .IncludeOptimized(r => r.Reis)
                .IncludeOptimized(r => r.Reis.Driver)
                .IncludeOptimized(r => r.Reis.Client)
                .Where(r => r.ReisId == reisId
                            && r.MonitoringPointTypeId == MonitoringPointTypeGuids.DeliveryOfDocumentsIsConfirmedbyTheDriver
                            && r.MonitoringDateFact == null
                            && !r.Rejected)
                .Select(x => new
                {
                    DriverId = x.Reis.DriverId
                })
                .FirstOrDefaultAsync();
            
            if (point == null)
            {
                return;
            }

            var driverKontragents = await dbContext.DriverKontragents
                .Where(item => item.DriverId == point.DriverId)
                .Include(item => item.Kontragent)
                .ThenInclude(item => item.KontragentUsers)
                .ThenInclude(item => item.User)
                .ThenInclude(item => item.Employer).ToListAsync();

            var logist = driverKontragents.Select(item => item.Kontragent.KontragentUsers
                    .Where(p => p.RelationType == DAL.Models.Enums.RelationType.Pinned)
                    .Select(p => p.User?.Employer))
                .SelectMany(item => item).FirstOrDefault();

            
            if (logist != null)
            {
                BackgroundJob.Enqueue<EmailSenderService>(j =>
                                    j.SendDriverDontGaveDocumentsUp(reisId, logist.Email));
            
                BackgroundJob.Enqueue<NotificationsService>(j =>
                    j.AddDriverDontGaveDocumentsUpNotification(reisId, logist.UserId));
            }
            
        }

    }
}