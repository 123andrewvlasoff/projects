using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Notifications.Enums;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Notification;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Services
{
    [Queue("logist")]
    public class NotificationsService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IMapper mapper;

        public NotificationsService(LogistDbContext logistDbContext,
            IMapper mapper)
        {
            this.logistDbContext = logistDbContext;
            this.mapper = mapper;
        }

        public async Task AddRequestReplyReceivedNotification(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestReplyReceived,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Создана заявка по заданию"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddReloadReisInfoFrom1CReceivedNotificationAsync(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestReplyReceived,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Информация по заявке была обновлена"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddNotificationSignedByCarrier(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.SignedByCarrier,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Заявка успешно подписана ПЭП"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestReplyProcessErrorNotification(Guid key, string userId, string error)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestReplyReceived,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Ошибка в обработке заявки {error}"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestReplyEditedNotificationAsync(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestReplyReceived,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Request.Id,
                Message = $"{item.Request.Number1C}: Обратите Ваше внимание, заявка была отредактирована"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestReplyDocuments(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestReplyReceived,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Заявка отправлена, ожидается согласование логистом"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddAcceptRequestReplyDocument(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(r=>r.Reises)
                .FirstOrDefaultAsync(i => i.Id == key);
            var reis = item.Reises.FirstOrDefault(x => x.StatusId != ReisStatus.Cancelled &&
                                                       x.IsActive == true &&
                                                       x.IsDeleted == false &&
                                                       x.IsComplete == false);
            if (reis == null)
            {
                return;
            }
            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.AcceptedByLogist,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = reis.Id,
                Message = $"{item.Number1C}: Документ согласован логистом"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddSupportTicketReceivedNotificationAsync(Guid key, string userId)
        {
            var item = await logistDbContext.SupportTickets.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.NewMessage,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"Поступило новое обращение (заявка №{item.Number})"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestRequestedChangesNotification(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters().FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestedChanges,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Запрошены изменения"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestCancelledByClientNotification(Guid key, string userId)
        {
            var item = await logistDbContext.Requests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.CancelledByClient,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Заявка отменена"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddReisCancelledByLogistNotification(Guid key, string userId)
        {
            var item = await logistDbContext.Reises.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ReisCancelled,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.DeliveryNumber}: Реис отменён логистом"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestReplyDeniedByLogistNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key);


            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.CancelledByLogist,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Request.Id,
                Message = $"{item.Request.Number1C}: Отказано в одобрении"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestCancelByCarrierNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.CarrierSignRefused,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"{item.Number1C}: Отмена по вине перевозчика"
            });

            await logistDbContext.SaveChangesAsync();
        }


        public async Task AddChangeUnloadInfoNotification(Guid key, string logistId, string carrierId)
        {
            var logist = logistDbContext.Users.First(i => i.Id == logistId);

            var item = logistDbContext.Transports.FirstOrDefault(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ChangeUnloadInfo,
                CreatedAt = DateTime.Now,
                UserId = carrierId,
                EventEntityId = item.Id,
                Message =
                    $"Логист: {logist.FullName}/ {logist.Email}, {logist.PhoneNumber}" +
                    $" изменил желаемое место и время погрузки для ТС: {item.RegNumber} "
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddChangeUnloadInfoNotificationLogist(Guid key, string logistId, string mainLogistId)
        {
            var logist = logistDbContext.Users.First(i => i.Id == logistId);

            var item = logistDbContext.Transports.FirstOrDefault(i => i.Id == key);

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ChangeUnloadInfo,
                CreatedAt = DateTime.Now,
                UserId = mainLogistId,
                EventEntityId = item.Id,
                Message =
                    $"Логист: {logist.FullName}/ {logist.Email}, {logist.PhoneNumber}" +
                    $" изменил желаемое место и время погрузки для ТС: {item.RegNumber} "
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddTakeInProgressForAssignedUserNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.InProgressUser)
                .IncludeOptimized(i => i.RoutePoints.Select(s => s.Settlement))
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var settlementLoad = item.Load.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload = item.Unload.SettlementId != null ? item.Unload.Settlement.Title : "";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.TakeInProgress,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"Ваше задание № {item.Number1C}, {settlementLoad} - {settlementUnload}," +
                          $" {item.Client.Title}  {(item.InProgressUserId == item.AssignedUserId ? "взято в работу" : "забронировано")} " +
                          $"логистом {item.InProgressUser.FullName}" +
                          " на платформе crafter.online."
            });

            await logistDbContext.SaveChangesAsync();
        }

        [AutomaticRetry(Attempts = 0)]
        public async Task AddFreeInProgressForAssignedUserNotification(Guid key, string userIdFrom, string userIdTo)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.InProgressUser)
                .IncludeOptimized(i => i.RoutePoints.Select(s => s.Settlement))
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var settlementLoad =
                item.Load?.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload =
                item.Unload?.SettlementId != null ? item.Unload.Settlement.Title : "";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.FreeInProgress,
                CreatedAt = DateTime.Now,
                UserId = userIdTo,
                EventEntityId = item.Id,
                Message =
                    $"Ваше задание № {item.Number1C}, {settlementLoad} - {settlementUnload}," +
                    $" {item.Client.Title} было освобождено логистом {logistDbContext.Users.FirstOrDefault(x => x.Id == userIdFrom)?.FullName}" +
                    " на платформе crafter.online."
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddSetAssignedUserNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.RoutePoints.Select(s => s.Settlement))
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var settlementLoad =
                item.Load.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload =
                item.Unload.SettlementId != null ? item.Unload.Settlement.Title : "";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.TakeInProgress,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = $"Задание № {item.Number1C}, {settlementLoad} - {settlementUnload}," +
                          $" {item.Client.Title} автоматически закреплено ЗА ВАМИ."
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestReservationExpiredNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.RoutePoints.Select(s => s.Settlement))
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var settlementLoad =
                item.Load.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload =
                item.Unload.SettlementId != null ? item.Unload.Settlement.Title : "";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.FreeInProgress,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message =
                    $"Время бронирования по заданию № {item.Number1C}, {settlementLoad} - {settlementUnload}," +
                    $" {item.Client.Title} истекло, задание освобождено автоматически"
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddChangeAssignedUserNotification(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.AssignedUser)
                .IncludeOptimized(i => i.RoutePoints.Select(s => s.Settlement))
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var settlementLoad =
                item.Load.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload =
                item.Unload.SettlementId != null ? item.Unload.Settlement.Title : "";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.FreeInProgress,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message =
                    $"По заданию № {item.Number1C}, {settlementLoad} - {settlementUnload}, {item.Client.Title}" +
                    $" изменен ответственный логист {item.AssignedUser.FullName} на платформе crafter.online."
            });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task AddRequestUnblockedNotification(Guid key)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.RoutePoints)
                .FirstOrDefaultAsync(i => i.Id == key);

            string message;

            if (item.InProgressUserId == item.AssignedUserId)
            {
                var deltaTime = TimeSpan.FromMinutes((item.Load.ArrivalDate - DateTime.Now).TotalMinutes)
                    .ToString(@"hh\:mm\:ss");

                message =
                    $"Задание  № {item.Number1C} разблокировано.  " +
                    $"До погрузки осталось {deltaTime}, Вы назначены ответственным логистом за закрытие данного задания";
            }
            else
            {
                message =
                    $"Задание № {item.Number1C} разблокировано оператором. Вы можете оформлять заявку с перевозчиком";
            }

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.RequestUnblocked,
                CreatedAt = DateTime.Now,
                UserId = item.InProgressUserId,
                EventEntityId = item.Id,
                Message = message,
            });

            await logistDbContext.SaveChangesAsync();
        }
        
        public async Task AddDriverDontGaveDocumentsUpNotification(Guid reisId, string userId)
        {
            var reis = await logistDbContext.Reises
                .IncludeOptimized(r => r.Client)
                .IncludeOptimized(r => r.Carrier)
                .IncludeOptimized(r => r.Driver)
                .FirstOrDefaultAsync(r=>r.Id==reisId);

            if (reis == null)
            {
                return;
            }
            
            var text =
                $"Водитель {reis.Driver.FullName} не сдал документы по рейсу №{reis.DeliveryNumber}, заказчик \"{reis.Client.Title}\", перевозчик \"{reis.Carrier.Title ?? ""}\"";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ReisDriverDontGaveDocsUp,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = reisId,
                Message = text
            });

            await logistDbContext.SaveChangesAsync();
        }
        public async Task AddDocsOnVerificationNotification(Guid key, string userId)
        {
            var item = await logistDbContext.Reises.IgnoreQueryFilters()
                .Include(d => d.DispatchingPoints)
                .FirstOrDefaultAsync(i => i.Id == key);
            
            if (item == null)
            {
                return;
            }
            var pushNotifBody = $"Документы по рейсу №{item.DeliveryNumber} приняты на проверку";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ReisDocsOnVerification,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = item.Id,
                Message = pushNotifBody
            });
            
            await logistDbContext.SaveChangesAsync();
        }
        
        public async Task AddReisCompletedNotification(Guid reisId, string userId)
        {
            var reis = await logistDbContext.Reises.IgnoreQueryFilters()
                .IncludeOptimized(r=>r.Driver)
                .IncludeOptimized(r=>r.Carrier)
                .IncludeOptimized(r=>r.Client)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            
            if (reis == null)
            {
                return;
            }
            
            var text = $"Сдача документы по рейсу №{reis.DeliveryNumber} , Водитель: {reis.Driver.FirstName} {reis.Driver.LastName}; Заказчик: {reis.Client.Title}; Перевозчик: {reis.Carrier.Title} подтверждена. Рейс выполнен";
            
            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ReisCompleted,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = reisId,
                Message = text
            });

            await logistDbContext.SaveChangesAsync();
        }
        
        public async Task AddReisDocsProblemRevealedNotification(Guid key, string userId)
        {
            var reis = await logistDbContext.Reises.IgnoreQueryFilters()
                .Include(d => d.DispatchingPoints)
                .FirstOrDefaultAsync(i => i.Id == key);
            
            if (reis == null)
            {
                return;
            }
            
            var text = $"По рейсу №{reis.DeliveryNumber} Водитель: {reis.Driver.FirstName} {reis.Driver.LastName}; Заказчик: {reis.Client.Title}; Перевозчик: {reis.Carrier.Title} зафиксированы проблемы с ТСД";

            await logistDbContext.Notifications.AddAsync(new Notification
            {
                Kind = EventKind.ReisProblemRevealed,
                CreatedAt = DateTime.Now,
                UserId = userId,
                EventEntityId = reis.Id,
                Message = text
            });

            await logistDbContext.SaveChangesAsync();
        }
    }
}