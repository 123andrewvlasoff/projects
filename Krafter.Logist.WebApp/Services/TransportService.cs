﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;

namespace Krafter.Logist.WebApp.Services
{
    [Queue("logist")]
    public class TransportService
    {
        private readonly LogistDbContext logistDbContext;

        public TransportService(LogistDbContext logistDbContext)
        {
            this.logistDbContext = logistDbContext;
        }

        public async Task CleanUnloadInfo(Guid key)
        {
            var item = logistDbContext.Transports.First(i => i.Id == key);

            if (item.LastReisDate != null && item.LastReisDate.Value.AddHours(48) > DateTime.Now)
            {
                return;
            }

            item.LastReisLat = null;
            item.LastReisLon = null;
            item.LastAddress = null;
            item.LastReisDate = null;
            item.ConnectedTransportId = null;

            await logistDbContext.SaveChangesAsync();
        }
    }
}