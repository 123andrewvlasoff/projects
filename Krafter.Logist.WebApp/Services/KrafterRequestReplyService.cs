﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Crafter.Client.CIS.Clients;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Enums;
using DAL.Models.Requests.Reis.Constants;
using DocumentFormat.OpenXml.Spreadsheet;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply.Enums;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Models.Basic.ReisFile;
using Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services;
using Krafter.ServiceCommon.Services.ChangesTrackService.Helpers;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Services
{
    public class KrafterRequestReplyService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IFileService fileService;
        private readonly IKrafterExternalClient krafterExternalClient;
        private readonly KontragentContractService kontragentContractService;

        public KrafterRequestReplyService(LogistDbContext logistDbContext,
            IFileService fileService, IKrafterExternalClient krafterExternalClient,
            KontragentContractService kontragentContractService)
        {
            this.logistDbContext = logistDbContext;
            this.fileService = fileService;
            this.krafterExternalClient = krafterExternalClient;
            this.kontragentContractService = kontragentContractService;
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task PrepareRequestReplyDocument(Guid key, string userId, RequestReplyDocumentType type)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.Request.Reises.Where(x => x.StatusId != ReisStatus.Cancelled))
                .IncludeOptimized(i => i.Request.RoutePoints)
                .IncludeOptimized(i => i.Request.InProgressUser.Employer)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Driver)
                .IncludeOptimized(i => i.ConnectedTransport)
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.TransportStatus)
                .FirstOrDefaultAsync(i => i.Id == key);

            var userIds = new[] { item.Request.AssignedUserId, item.Request.InProgressUserId }.Distinct();

            try
            {
                var employer = await logistDbContext.Employers.FirstOrDefaultAsync(i => i.UserId == userId);

                var addressIds = item.Request.RoutePoints.Select(x => x.AddressId).ToArray();
                var addresses = await logistDbContext.Addresses.Where(x => addressIds.Contains(x.Id)).ToArrayAsync();
                
                FreightHaul externalRequest = null;

                switch (type)
                {
                    case RequestReplyDocumentType.Create:
                    {
                        var assignResult = await krafterExternalClient.AssignAsync(item.Request.ExternalId,
                            item.PetrolSum,
                            item.RequestReplyType == RequestReplyType.Digital, new FreightHaulBase()
                            {
                                CarrierGuid = item.Kontragent.ExternalId.ToString(),
                                CarrierContactGuid = item.KontragentContact.ExternalId.ToString(),
                                DriverGuid = item.Driver.ExternalId.ToString(),
                                TruckGuid = item.Transport.ExternalId.ToString(),
                                EmployeeGuid = employer.ExternalId.ToString(),
                                Price = (double)item.Price,
                                SemitrailerGuid = item.ConnectedTransport?.ExternalId.ToString(),
                                TruckStatusAfterHaulId = item.TransportStatus.ExternalId,
                                RoutePointUpdates = item.Request.RoutePoints.Select(i => new RoutePointUpdate()
                                {
                                    PointTypeId = (int)i.PointType,
                                    AddressGuid = addresses.FirstOrDefault(x => x.Id == i.AddressId)?.ExternalId?.ToString(),
                                    ArrivalTimePlan = i.ArrivalDate,
                                    ArrivalTimePlanUpdated = i.ArrivalDateFact ?? i.ArrivalDate,
                                    LeaveTimePlan = i.LeaveDate,
                                    LeaveTimePlanUpdated = i.LeaveDateFact ?? i.LeaveDate
                                }).ToList()
                            });

                        externalRequest = assignResult.FreightHaul;

                        break;
                    }
                    case RequestReplyDocumentType.Update:

                        var replaceResult = await krafterExternalClient.ReplaceAsync(item.Request.ExternalId,
                            item.PetrolSum,
                            item.RequestReplyType == RequestReplyType.Digital, new FreightHaulBase()
                            {
                                CarrierGuid = item.Kontragent.ExternalId.ToString(),
                                CarrierContactGuid = item.KontragentContact.ExternalId.ToString(),
                                DriverGuid = item.Driver.ExternalId.ToString(),
                                TruckGuid = item.Transport.ExternalId.ToString(),
                                EmployeeGuid = employer.ExternalId.ToString(),
                                Price = (double)item.Price,
                                SemitrailerGuid = item.ConnectedTransport?.ExternalId.ToString(),
                                TruckStatusAfterHaulId = item.TransportStatus.ExternalId,
                                RoutePointUpdates = item.Request.RoutePoints.Select(i => new RoutePointUpdate()
                                {
                                    PointTypeId = (int)i.PointType,
                                    AddressGuid = addresses.FirstOrDefault(x => x.Id == i.AddressId)?.ExternalId?.ToString(),
                                    ArrivalTimePlan = i.ArrivalDate,
                                    ArrivalTimePlanUpdated = i.ArrivalDateFact ?? i.ArrivalDate,
                                    LeaveTimePlan = i.LeaveDate,
                                    LeaveTimePlanUpdated = i.LeaveDateFact ?? i.LeaveDate
                                }).ToList()
                            });

                        externalRequest = replaceResult.FreightHaul;

                        break;
                }

                var file = externalRequest.Documents.FirstOrDefault(i => i.Type == DocumentType.CarrierRequest);
                if (file == null)
                {
                    throw new Exception("request contract file not found");
                }

                var fileName = Guid.NewGuid().ToString();

                var result = new MemoryStream(file.Content);

                if (item.RequestReplyType == RequestReplyType.Digital)
                {
                    var typeIsProviderForAgent =
                        await kontragentContractService.TypeIsProviderForAgent1CAsync(
                            new Guid(externalRequest.CarrierContractGuid));

                    result = typeIsProviderForAgent
                        ? DocumentWatermarkHelper.AddSignedByLogistForProviderAgentWatermark(result)
                        : DocumentWatermarkHelper.AddSignedByLogistWatermark(result);
                }

                await fileService.PutFileAsync(result, Folders.Request, fileName);

                switch (type)
                {
                    case RequestReplyDocumentType.Create:
                        item.Files = new List<KrafterRequestReplyFile>
                        {
                            new KrafterRequestReplyFile
                            {
                                CreatedAt = DateTime.Now,
                                OriginalName = file.Name,
                                UploaderId = userId,
                                ContentType = "application/pdf",
                                DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                Path = Folders.Request + "/" + fileName
                            }
                        };
                        break;
                    case RequestReplyDocumentType.Update:
                        var old = item.Files.FirstOrDefault(i =>
                            i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);

                        if (old != null)
                        {
                            old.IsDeleted = true;
                            item.Files.Add(
                                new KrafterRequestReplyFile
                                {
                                    CreatedAt = DateTime.Now,
                                    OriginalName = file.Name,
                                    UploaderId = userId,
                                    ContentType = "application/pdf",
                                    DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                    Path = Folders.Request + "/" + fileName
                                }
                            );
                        }
                        else
                        {
                            item.Files = new List<KrafterRequestReplyFile>
                            {
                                new KrafterRequestReplyFile
                                {
                                    CreatedAt = DateTime.Now,
                                    OriginalName = file.Name,
                                    UploaderId = userId,
                                    ContentType = "application/pdf",
                                    DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                    Path = Folders.Request + "/" + fileName
                                }
                            };
                        }

                        break;
                }

                await logistDbContext.SaveChangesAsync();

                await SetReis(externalRequest, item, userId);

                item.Request.RequestStatus = RequestStatus.DataInput;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.Verification;

                var assignedUser1c =
                    (await logistDbContext.Employers.FirstOrDefaultAsync(i =>
                        i.ExternalId == Guid.Parse(externalRequest.EmployeeGuid)))?.UserId;
                item.Request.AssignedUserId = assignedUser1c != null ? assignedUser1c : item.Request.InProgressUserId;

                await logistDbContext.SaveChangesAsync();

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyReceivedNotification(item.RequestId, item.Request.InProgressUserId));

                BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendRequestReply(item.Id, userId));
            }
            catch (ApiException<AssignFreightHaulResponse> e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.ErrorReceivingDocuments;

                await logistDbContext.SaveChangesAsync();

                SendErrorNotifications(item.RequestId, userIds, e.Result.Messages.First().Text);

                throw;
            }
            catch (ApiException<ReplaceFreightHaulResponse> e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.ErrorReceivingDocuments;

                await logistDbContext.SaveChangesAsync();

                SendErrorNotifications(item.RequestId, userIds, e.Result.Messages.First().Text);
                
                throw;
            }
            catch (Exception e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.ErrorReceivingDocuments;

                await logistDbContext.SaveChangesAsync();

                SendErrorNotifications(item.RequestId, userIds, "");

                throw;
            }
        }
        
        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task ReloadReisInfoFrom1C(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request.Reises.Where(x => x.StatusId != ReisStatus.Cancelled))
                .IncludeOptimized(i => i.Request.Replies.Select(s => s.Files.Where(x => !x.IsDeleted)))
                .FirstOrDefaultAsync(i => i.Id == key);

            var type = RequestReplyDocumentType.Create;

            if (item.Request.Reises.Any())
            {
                type = RequestReplyDocumentType.Update;
            }

            try
            {
                var reloadResult =
                    await krafterExternalClient.FreighthaulsAsync(item.Request.ExternalId,
                        item.Request.Reises.First().ExternalId.ToString(),
                        item.Request.Replies.First().RequestReplyType == RequestReplyType.Digital);

                var externalRequest = reloadResult.FreightHaul;

                var file = externalRequest.Documents.FirstOrDefault(i => i.Type == DocumentType.CarrierRequest);

                if (file == null)
                {
                    throw new Exception("request contract file not found");
                }

                var fileName = Guid.NewGuid().ToString();

                var result = new MemoryStream(file.Content);

                if (item.RequestReplyType == RequestReplyType.Digital)
                {
                    var typeIsProviderForAgent =
                        await kontragentContractService.TypeIsProviderForAgent1CAsync(
                            new Guid(externalRequest.CarrierContractGuid));

                    result = typeIsProviderForAgent
                        ? DocumentWatermarkHelper.AddSignedByLogistForProviderAgentWatermark(result)
                        : DocumentWatermarkHelper.AddSignedByLogistWatermark(result);
                }

                await fileService.PutFileAsync(result, Folders.Request, fileName);

                switch (type)
                {
                    case RequestReplyDocumentType.Create:
                        item.Files = new List<KrafterRequestReplyFile>
                        {
                            new KrafterRequestReplyFile
                            {
                                CreatedAt = DateTime.Now,
                                OriginalName = file.Name,
                                UploaderId = userId,
                                ContentType = "application/pdf",
                                DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                Path = Folders.Request + "/" + fileName
                            }
                        };
                        break;
                    case RequestReplyDocumentType.Update:
                        var old = item.Files.FirstOrDefault(i =>
                            i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);
                        if (old != null)
                        {
                            old.IsDeleted = true;
                            item.Files.Add(
                                new KrafterRequestReplyFile
                                {
                                    CreatedAt = DateTime.Now,
                                    OriginalName = file.Name,
                                    UploaderId = userId,
                                    ContentType = "application/pdf",
                                    DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                    Path = Folders.Request + "/" + fileName
                                }
                            );
                        }
                        else
                        {
                            item.Files = new List<KrafterRequestReplyFile>
                            {
                                new KrafterRequestReplyFile
                                {
                                    CreatedAt = DateTime.Now,
                                    OriginalName = file.Name,
                                    UploaderId = userId,
                                    ContentType = "application/pdf",
                                    DocumentType = DAL.Models.Enums.DocumentType.RequestContractTemplate,
                                    Path = Folders.Request + "/" + fileName
                                }
                            };
                        }

                        break;
                }

                await logistDbContext.SaveChangesAsync();

                await SetReis(externalRequest, item, userId);
                await SetRequestReply(externalRequest, item);

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddReloadReisInfoFrom1CReceivedNotificationAsync(item.RequestId, item.Request.InProgressUserId));
            }
            catch (Exception e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.ErrorReceivingDocuments;
                await logistDbContext.SaveChangesAsync();

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId, ""));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId, ""));

                throw;
            }
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task SendRequestReplyEmails(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.Request.RoutePoints)
                .IncludeOptimized(i => i.Request.AssignedUser.Employer)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Driver)
                .IncludeOptimized(i => i.ConnectedTransport)
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.TransportStatus)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item.Request.AssignedUserId != null)
            {
                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistReplyForEmployer,
                        item.Request.AssignedUser.Employer.Email)
                );
            }

            var kontragentUser = item.Kontragent.KontragentUsers
                .FirstOrDefault(x => x.Discriminator == KontragentUserType.Carrier);

            var email = kontragentUser?.User?.Email ?? item.KontragentContact.Email;

            if (item.RequestReplyType == RequestReplyType.Digital)
            {
                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistDigitalSignWaitingForCarrier, email));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistDigitalSignWaitingForLogist,
                        item.Request.InProgressUser.Email));
            }
            else
            {
                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistPaperSignWaitingForCarrier, email));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistPaperSignWaitingForLogist,
                        item.Request.InProgressUser.Email));
            }
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task DeleteRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.Request.Reises.Where(x => x.StatusId != ReisStatus.Cancelled))
                .IncludeOptimized(i => i.Request.RoutePoints)
                .IncludeOptimized(i => i.Request.Actions)
                .IncludeOptimized(i => i.Request.AssignedUser)
                .FirstOrDefaultAsync(i => i.Id == key);
            try
            {
                switch (item.RequestReplyType)
                {
                    case RequestReplyType.Paper:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);

                        break;
                    }

                    case RequestReplyType.Digital:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);
                        var document =
                            item.Files.FirstOrDefault(i =>
                                i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);

                        if (document != null)
                        {
                            var attachDocumentToFreightHaulResponse =
                                await SetCancelRequestReplyDocumentWatermark(item);

                            await SetReis(attachDocumentToFreightHaulResponse.FreightHaul, item, userId);
                        }

                        break;
                    }
                }

                if (item.Request.Reises.Any())
                {
                    item.Request.Reises.First().IsActive = false;
                    item.Request.Reises.First().IsDeleted = true;
                }

                item.IsDeleted = true;
                item.Request.RequestStatus = RequestStatus.IsActive;
                item.Request.KrafterRequestStatus = null;
                item.Request.InProgressUserId = null;
                item.UpdatedAt = DateTime.Now;

                item.Request.Actions.Add(new KrafterRequestAction
                {
                    ActionDate = DateTime.Now,
                    ActionUserId = userId,
                    State = RequestActionStatus.FreeInProgress,
                });

                foreach (var rr in item.Request.RoutePoints)
                {
                    rr.ArrivalDateFact = null;
                    rr.LeaveDateFact = null;
                }

                await logistDbContext.SaveChangesAsync();

                if (item.Request.AssignedUserId != null)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddFreeInProgressForAssignedUserNotification(item.Id, userId, item.Request.AssignedUserId));

                    BackgroundJob.Enqueue<EmailSenderService>(i =>
                        i.SendRequestEmail(item.Id, SubjectType.LogistDeleteUserRequest,
                            item.Request.AssignedUser.Email, userId));
                }
            }

            catch (ApiException<CancelPickUpRequestResponse> e)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId,
                        e.Result.Messages.First().Text));

                throw;
            }

            catch (ApiException<CancelFreightHaulResponse> e)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId,
                        e.Result.Messages.First().Text));

                throw;
            }

            catch (ApiException<AttachDocumentToFreightHaulResponse> e)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId,
                        e.Result.Messages.First().Text));

                throw;
            }
            catch (Exception)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId, ""));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId, ""));

                throw;
            }
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task CancelRequestReplyDocument(Guid key, string userId, ReplyDeniedReasonType reasonType)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.Kontragent.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Request.Reises.Where(x => x.StatusId != ReisStatus.Cancelled))
                .IncludeOptimized(i => i.Request.DeniedReason)
                .IncludeOptimized(i => i.Request.RoutePoints)
                .IncludeOptimized(i => i.Request.Replies)
                .IncludeOptimized(i => i.Request.InProgressUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            var recipientIds = new[] { item.Request.InProgressUserId, item.Request.AssignedUserId }.Distinct();

            try
            {
                switch (item.RequestReplyType)
                {
                    case RequestReplyType.Paper:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);
                        if (item.Request.DeniedReasonId != null)
                            await krafterExternalClient.Cancel2Async(item.Request.ExternalId,
                                new CancellationReason
                                {
                                    Guid = item.Request.DeniedReason.ExternalId.ToString()
                                });

                        break;
                    }

                    case RequestReplyType.Digital:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);
                        var document =
                            item.Files.FirstOrDefault(i =>
                                i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);

                        if (document != null)
                        {
                            var attachDocumentToFreightHaulResponse =
                                await SetCancelRequestReplyDocumentWatermark(item, reasonType);

                            await SetReis(attachDocumentToFreightHaulResponse.FreightHaul, item, userId);
                        }

                        // if(item.Request.DeniedReasonId!=null)
                        //     await krafterExternalClient.Cancel2Async(item.Request.ExternalId,
                        //     new CancellationReason
                        //     {
                        //         Guid = item.Request.DeniedReason.ExternalId.ToString()
                        //     });

                        break;
                    }
                }

                if (item.Request.Reises.Any())
                {
                    item.Request.Reises.First().StatusId = ReisStatus.Cancelled;
                    item.Request.Reises.First().IsActive = true;
                    item.Request.Reises.First().IsComplete = true;
                    item.Request.Reises.First().IsDeleted = false;
                    item.Request.Reises.First().AcceptorId = userId;
                }

                switch (reasonType)
                {
                    case ReplyDeniedReasonType.ByClientDecision:
                    {
                        item.Request.RequestStatus = RequestStatus.Cancelled;
                        item.Request.KrafterRequestStatus = KrafterRequestStatus.CancelledByClient;
                        break;
                    }

                    case ReplyDeniedReasonType.ByLogistDecision:
                    {
                        item.Request.RequestStatus = RequestStatus.IsActive;
                        item.Request.KrafterRequestStatus = null;
                        item.Request.InProgressUserId = null;
                        item.Request.DeniedReasonId = null;

                        item.IsDeleted = true;
                        break;
                    }
                    case ReplyDeniedReasonType.ByLogistDecisionLogistCreatedReply:
                    {
                        item.Request.RequestStatus = RequestStatus.DataInput;
                        item.Request.KrafterRequestStatus = KrafterRequestStatus.DocSignInput;
                        item.Request.InProgressUserId = item.Request.InProgressUserId;
                        item.Request.DeniedReasonId = null;

                        item.IsDeleted = false;
                        break;
                    }
                }


                foreach (var requestReplyFile in item.Files.Where(r =>
                             r.DocumentType == DAL.Models.Enums.DocumentType.RequestContract ||
                             reasonType != ReplyDeniedReasonType.ByLogistDecisionLogistCreatedReply))
                {
                    requestReplyFile.IsDeleted = true;
                }

                item.UpdatedAt = DateTime.Now;
                await logistDbContext.SaveChangesAsync();

                foreach (var rr in item.Request.RoutePoints)
                {
                    rr.ArrivalDateFact = null;
                    rr.LeaveDateFact = null;
                }


                await logistDbContext.SaveChangesAsync()
                    .ContinueWith(t =>
                    {
                        var userIds = new[]
                            {
                                item.Request.AssignedUserId,
                                item.Request.Replies.FirstOrDefault().CreatorId
                            }
                            .Distinct();

                        foreach (var userId in userIds)
                        {
                            BackgroundJob.Enqueue<NotificationsService>(i =>
                                i.AddRequestCancelledByClientNotification(item.RequestId, userId));
                        }

                        BackgroundJob.Enqueue<EmailSenderService>(j => j.SendRequestReplyEmail(item.Id,
                            SubjectType.LogistCancelRequest,
                            item.Kontragent.KontragentUsers
                                .FirstOrDefault(r => r.Discriminator == KontragentUserType.Carrier && r.UserId != null)
                                .User.Email));


                        switch (reasonType)
                        {
                            case ReplyDeniedReasonType.ByLogistDecision:
                                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                                    i.TrackRequestAsync(item.RequestId, RequestActionStatus.LogistAcceptRefused,
                                        ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item.Request)),
                                        userId));
                                break;
                            case ReplyDeniedReasonType.ByClientDecision:
                                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                                    i.TrackRequestAsync(item.RequestId, RequestActionStatus.CancelledByClient,
                                        ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item.Request)),
                                        userId));
                                break;
                            case ReplyDeniedReasonType.ByLogistDecisionLogistCreatedReply:
                                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                                    i.TrackRequestAsync(item.RequestId, RequestActionStatus.LogistAcceptRefused,
                                        ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item.Request)),
                                        userId));
                                break;
                        }
                    });

                await logistDbContext.SaveChangesAsync();
            }

            catch (ApiException<CancelPickUpRequestResponse> e)
            {
                SendErrorNotifications(item.RequestId, recipientIds, e.Result.Messages.First().Text);

                throw;
            }

            catch (ApiException<CancelFreightHaulResponse> e)
            {
                SendErrorNotifications(item.RequestId, recipientIds, e.Result.Messages.First().Text);

                throw;
            }

            catch (ApiException<AttachDocumentToFreightHaulResponse> e)
            {
                SendErrorNotifications(item.RequestId, recipientIds, e.Result.Messages.First().Text);

                throw;
            }
            catch (Exception)
            {
                SendErrorNotifications(item.RequestId, recipientIds, "");

                throw;
            }
        }

        private void SendErrorNotifications(Guid id, IEnumerable<string> userIds, string error)
        {
            foreach (var userId in userIds)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(id, userId, error));
            }
        }

        private async Task<AttachDocumentToFreightHaulResponse> SetCancelRequestReplyDocumentWatermark(
            KrafterRequestReply item, ReplyDeniedReasonType deniedReasonType = ReplyDeniedReasonType.None)
        {
            var document =
                item.Files.FirstOrDefault(i => i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);

            await using var response = await fileService.GetFileAsync(document.Path);

            var typeIsProviderForAgent =
                await kontragentContractService.TypeIsProviderForAgentAsync(item.Request.Reises.First()
                    .KontragentContractId);

            if (deniedReasonType != ReplyDeniedReasonType.None)
            {
                typeIsProviderForAgent = deniedReasonType == ReplyDeniedReasonType.ByClientDecision;
            }

            MemoryStream watermarkDocument = typeIsProviderForAgent
                ? DocumentWatermarkHelper.AddLogistCanceledForProviderAgentWithoutCarrierWatermark(
                    response)
                : DocumentWatermarkHelper.AddLogistCanceledWatermark(response);

            var attachDocumentToFreightHaulResponse = await krafterExternalClient.AttachDocumentAsync(
                item.Request.Reises.First().ExternalId.ToString(), item.Request.ExternalId, new Document()
                {
                    Content = watermarkDocument.GetBuffer(),
                    Name = document.OriginalName,
                    Type = DocumentType.CarrierRequest
                });

            await fileService.PutFileAsync(watermarkDocument, document.Path);
            await logistDbContext.SaveChangesAsync();

            return attachDocumentToFreightHaulResponse;
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task AcceptRequestReplyDocument(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(r => r.Request)
                .IncludeOptimized(i => i.Request.Reises.Where(x => x.StatusId != ReisStatus.Cancelled))
                .IncludeOptimized(i => i.Kontragent.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Request.AssignedUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            var requestContract =
                item.Files.FirstOrDefault(i => i.DocumentType == DAL.Models.Enums.DocumentType.RequestContractTemplate);


            var response = await fileService.GetFileAsync(requestContract.Path);
            var responseContent = new MemoryStream();
            //Если логист согласует заявку сформированную перевозчиком - печати проставляются на этом этапе (G30.2)
            if (item.RequestReplyType == RequestReplyType.Digital)
            {
                responseContent = DocumentWatermarkHelper.AddAcceptedByLogistWatermark(response);

                await fileService.PutFileAsync(responseContent, requestContract.Path);
            }
            else
            {
                using (Stream responseStream = response)
                {
                    responseStream.Position = 0;
                    responseStream.CopyTo(responseContent);
                }
            }

            var recipientIds = new[] { item.Request.InProgressUserId, item.Request.AssignedUserId }.Distinct();

            try
            {
                var acceptRequest = await krafterExternalClient.ConfirmAsync(
                    item.Request.ExternalId, item.Request.Reises.First().ExternalId.ToString(), new[]
                    {
                        new Document
                        {
                            Content = responseContent.GetBuffer(),
                            Name = requestContract.OriginalName,
                            Type = DocumentType.CarrierRequest
                        }
                    });

                requestContract.Accepted = true;
                requestContract.AcceptedDate = DateTime.Now;
                requestContract.AcceptedById = userId;

                await logistDbContext.SaveChangesAsync();

                item.Request.UpdatedAt = DateTime.Now;
                var userCarrier = logistDbContext.KontragentUsers.FirstOrDefault(i =>
                    i.KontragentId == item.KontragentId
                    && i.Discriminator == KontragentUserType.Carrier
                    && i.UserId != null)?.User;
                await SetReis(acceptRequest.FreightHaul, item, userId, true);

                item.Request.RequestStatus = RequestStatus.Done;
                item.Request.KrafterRequestStatus = null;
                

                await logistDbContext.SaveChangesAsync();

                if (userCarrier != null)
                {
                    BackgroundJob.Enqueue<EmailSenderService>(i =>
                        i.SendRequestReplyEmail(item.Id, SubjectType.LogistAccept,
                            userCarrier.Email));
                }

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Id, SubjectType.LogistAccept,
                        item.Request.AssignedUser.Email));

                if (userCarrier != null && item.CreatorId == userCarrier.Id)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddAcceptRequestReplyDocument(item.RequestId, userCarrier.Id));
                }
                else
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddAcceptRequestReplyDocument(item.RequestId, item.Request.InProgressUserId));
                }

                if (userCarrier != null && item.CreatorId != userCarrier.Id &&
                    item.Request.InProgressUserId != item.Request.AssignedUserId)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddAcceptRequestReplyDocument(item.RequestId, item.Request.AssignedUserId));
                }

                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                    i.TrackRequestAsync(item.RequestId, RequestActionStatus.LogistAccept,
                        ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item.Request)),
                        userId));
            }
            catch (ApiException<ConfirmFreightHaulResponse> e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.LogistAcceptError;

                await logistDbContext.SaveChangesAsync();

                foreach (var recipientId in recipientIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyProcessErrorNotification(item.RequestId, recipientId,
                            e.Result.Messages.First().Text));
                }

                throw;
            }
            catch (Exception e)
            {
                item.Request.RequestStatus = RequestStatus.Error;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.LogistAcceptError;

                await logistDbContext.SaveChangesAsync();

                foreach (var recipientId in recipientIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyProcessErrorNotification(item.RequestId, recipientId, ""));
                }

                throw;
            }
        }
        
        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task RejectFileRequestReplyDocument(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key);

            try
            {
                item.Request.RequestStatus = RequestStatus.DataInput;
                item.Request.KrafterRequestStatus = KrafterRequestStatus.DocSignInput;
                item.Request.InProgressUserId = item.Request.InProgressUserId;
                item.Request.DeniedReasonId = null;
                item.IsDeleted = false;
                
                foreach (var requestReplyFile in item.Files.Where(r=>r.DocumentType==DAL.Models.Enums.DocumentType.RequestContract))
                    requestReplyFile.IsDeleted = true;
                
                item.UpdatedAt = DateTime.Now;

                await logistDbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId, ""));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId, ""));

                throw;
            }
        }

        private async Task SetReis(FreightHaul freightHaul, KrafterRequestReply requestReply, string userId,
            bool isActive = false)
        {
            var reis = new Reis();

            if (requestReply.Request.Reises.Any())
            {
                reis = await logistDbContext.Reises.IgnoreQueryFilters()
                    .IncludeOptimized(i => i.RoutePoints)
                    .IncludeOptimized(i => i.RoutePoints.Select(j => j.ReisDispatchingPoints))
                    .IncludeOptimized(i => i.Files)
                    .FirstOrDefaultAsync(i => i.Id == requestReply.Request.Reises.First().Id);
            }

            var request = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == requestReply.RequestId);

            reis.RequestDiscriminator = request.Discriminator;
            reis.RequestCode = request.RequestCode;
            reis.Distance = request.Distance;
            reis.Number1C = request.Number1C;
            reis.RequestId = requestReply.RequestId;
            reis.AcceptorId = userId;
            reis.IsActive = isActive;
            reis.UpdatedAt = DateTime.Now;
            reis.Carrier = await logistDbContext.Kontragents.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.CarrierGuid));
            reis.Client = await logistDbContext.Kontragents.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.ShipperGuid));
            reis.ConnectedTransport = !string.IsNullOrEmpty(freightHaul.SemitrailerGuid)
                ? await logistDbContext.Transports.FirstOrDefaultAsync(rs =>
                    rs.ExternalId == Guid.Parse(freightHaul.SemitrailerGuid))
                : null;
            reis.Transport = await logistDbContext.Transports.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.TruckGuid));
            reis.Driver = await logistDbContext.Drivers.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.DriverGuid));
            reis.ExternalId = Guid.Parse(freightHaul.Guid);
            reis.DeliveryNumber = freightHaul.Number;
            reis.Weight = freightHaul.FreightWeight;
            reis.Volume = freightHaul.FreightVolume;
            reis.Shipment = freightHaul.ShipmentName;
            reis.PackType = freightHaul.PackTypeName;
            reis.ShipmentComment = freightHaul.ShipmentComment;
            reis.RequestId = requestReply.RequestId;
            reis.Price = freightHaul.Price;
            reis.LoadingType = await logistDbContext.LoadingTypes.FirstOrDefaultAsync(rs =>
                rs.Id == requestReply.Request.LoadingTypeId);
            reis.RollingStockType = await logistDbContext.RollingStockTypes.FirstOrDefaultAsync(rs =>
                rs.Id == requestReply.Request.RollingStockTypeId);
            reis.Status = await logistDbContext.ReisStatuses.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.StatusGuid));
            reis.TransportStatus = await logistDbContext.TransportStatuses.FirstOrDefaultAsync(rs =>
                rs.ExternalId == freightHaul.TruckStatusAfterHaulId);

            var kontragentContractId = await logistDbContext.KontragentContracts
                .Where(x => x.Guid1C == Guid.Parse(freightHaul.CarrierContractGuid)).Select(x => new { x.Id })
                .FirstOrDefaultAsync();

            reis.CreatedAt = DateTime.UtcNow.AddHours(3);
            reis.KontragentContractId = kontragentContractId?.Id;

            reis.Files =
                requestReply.Files
                    .Select(f =>
                        new ReisFile
                        {
                            IsDeleted = f.IsDeleted,
                            Accepted = f.Accepted,
                            AcceptedById = f.AcceptedById,
                            AcceptedDate = f.AcceptedDate,
                            ContentType = f.ContentType,
                            CreatedAt = f.CreatedAt,
                            Signed = f.Signed,
                            SignedDate = f.SignedDate,
                            SignedById = f.SignedById,
                            DocumentType = f.DocumentType,
                            OriginalName = f.OriginalName,
                            Path = f.Path
                        }).ToList();

            var orderInRoute = 0;

            reis.RoutePoints = freightHaul.RoutePoints.OrderBy(i => i.ArrivalTimePlan).Select(i =>
            {
                var settlement = logistDbContext.Settlements.FirstOrDefault(r =>
                                     !string.IsNullOrEmpty(r.Title) &&
                                     r.Title == i.Address.Settlement) ??
                                 logistDbContext.Settlements.FirstOrDefault(r =>
                                     !string.IsNullOrEmpty(r.Title) && r.Title == i.Address.City) ??
                                 logistDbContext.Settlements.FirstOrDefault(r =>
                                     !string.IsNullOrEmpty(r.Title) && r.Title == i.Address.Region);

                var region = logistDbContext.Regions.FirstOrDefault(r => r.Title == i.Address.Region);

                // @TODO временный костыль
                var client = string.IsNullOrEmpty(i.ClientGuid)
                    ? ""
                    : logistDbContext.Kontragents.FirstOrDefault(rs => rs.ExternalId == Guid.Parse(i.ClientGuid)).Title;

                orderInRoute++;

                return new ReisRoutePoint()
                {
                    PointType = (PointType)i.PointTypeId,
                    Address = i.Address.AddressString,
                    AddressId = Guid.Parse(i.Address.Guid),
                    Lat = i.Latitude,
                    Lon = i.Longitude,
                    LeaveDatePlan = i.LeaveTimePlan.DateTime,
                    ArrivalDatePlan = i.ArrivalTimePlan.DateTime,
                    Region = region,
                    Settlement = settlement,
                    OrderInRoute = orderInRoute,
                    Client = client
                };
            }).ToList();

            if (!requestReply.Request.Reises.Any())
            {
                await logistDbContext.Reises.AddAsync(reis);
            }

            await logistDbContext.SaveChangesAsync();
        }

        private async Task SetRequestReply(FreightHaul freightHaul, KrafterRequestReply requestReply)
        {
            requestReply.Kontragent = await logistDbContext.Kontragents.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.CarrierGuid));

            requestReply.ConnectedTransport = !string.IsNullOrEmpty(freightHaul.SemitrailerGuid)
                ? await logistDbContext.Transports.FirstOrDefaultAsync(rs =>
                    rs.ExternalId == Guid.Parse(freightHaul.SemitrailerGuid))
                : null;

            requestReply.Transport = await logistDbContext.Transports.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.TruckGuid));

            requestReply.Driver = await logistDbContext.Drivers.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.DriverGuid));

            requestReply.TransportStatus = await logistDbContext.TransportStatuses.FirstOrDefaultAsync(rs =>
                rs.ExternalId == freightHaul.TruckStatusAfterHaulId);

            requestReply.KontragentContact = await logistDbContext.KontragentContacts.FirstOrDefaultAsync(rs =>
                rs.ExternalId == Guid.Parse(freightHaul.CarrierContactGuid));

            requestReply.Price = (decimal)freightHaul.Price;

            await logistDbContext.SaveChangesAsync();
        }
    }
}