﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Dictionaries.Enums;
using DAL.Models.Requests.Krafter.Enums;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Crafter.Client.CIS.Clients;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Attributes;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using DocumentType = DAL.Models.Enums.DocumentType;

namespace Krafter.Logist.WebApp.Services
{
    public class ReisService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IKrafterExternalClient krafterExternalClient;
        private readonly IFileService fileService;
        private readonly KontragentContractService kontragentContractService;

        public ReisService(LogistDbContext logistDbContext,
            IKrafterExternalClient krafterExternalClient, IFileService fileService,
            KontragentContractService kontragentContractService)
        {
            this.logistDbContext = logistDbContext;
            this.krafterExternalClient = krafterExternalClient;
            this.fileService = fileService;
            this.kontragentContractService = kontragentContractService;
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task CancelReis(Guid key, Guid deniedReasonId, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.Reises.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request.Replies.Where(x => !x.IsDeleted).Select(s => new
                {
                    s.KontragentContact,
                    KontragentUsers = s.Kontragent.KontragentUsers.Select(ku => ku.User)
                }))
                .IncludeOptimized(i => i.Files)
                .IncludeOptimized(i => i.Request.RoutePoints)
                .IncludeOptimized(i => i.Request.DeniedReason)
                .FirstOrDefaultAsync(i => i.Id == key);

            var deniedReason = await logistDbContext.DeniedReasons.FirstOrDefaultAsync(i => i.Id == deniedReasonId);

            try
            {
                switch (deniedReason.Type)
                {
                    case DeniedReasonViewType.ByCarrier:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);
                        await SetWatermark(item);

                        item.Request.RequestStatus = RequestStatus.IsActive;
                        item.Request.KrafterRequestStatus = KrafterRequestStatus.InProgress;
                        item.Request.Replies.First().IsDeleted = true;

                        break;
                    }

                    case DeniedReasonViewType.ByClient:
                    {
                        await krafterExternalClient.CancelAsync(item.Request.ExternalId);
                        await SetWatermark(item);

                        await krafterExternalClient.Cancel2Async(item.Request.ExternalId,
                            new CancellationReason
                            {
                                Guid = deniedReason.ExternalId.ToString()
                            });

                        item.Request.RequestStatus = RequestStatus.Cancelled;
                        item.Request.KrafterRequestStatus = KrafterRequestStatus.CancelledByCarrier;

                        break;
                    }
                }

                item.Request.DeniedReasonId = deniedReasonId;
                item.UpdatedAt = DateTime.Now;

                foreach (var rr in item.Request.RoutePoints)
                {
                    rr.ArrivalDateFact = null;
                    rr.LeaveDateFact = null;
                }

                item.StatusId = ReisStatus.Cancelled;
                item.IsActive = true;
                item.IsComplete = true;
                item.IsDeleted = false;

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddReisCancelledByLogistNotification(item.Id, item.Request.InProgressUserId));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddReisCancelledByLogistNotification(item.Id, item.Request.AssignedUserId));


                await logistDbContext.SaveChangesWithTrackingAsync<KrafterRequest>()
                    .ContinueWith(t =>
                    {
                        var kontragentUser = item.Request.Replies.FirstOrDefault()?.Kontragent?.KontragentUsers
                            ?.FirstOrDefault();

                        BackgroundJob.Enqueue<NotificationsService>(i =>
                            i.AddReisCancelledByLogistNotification(item.Id, kontragentUser != null
                                ? kontragentUser.User.Email
                                : item.Request.Replies.First().KontragentContact.Email));

                        BackgroundJob.Enqueue<EmailSenderService>(i =>
                            i.SendRequestReplyEmail(item.Request.Replies.First().Id, SubjectType.LogistCancelRequest,
                                kontragentUser != null
                                    ? kontragentUser.User.Email
                                    : item.Request.Replies.First().KontragentContact.Email));

                        if (deniedReason.Type == DeniedReasonViewType.ByCarrier)
                        {
                            BackgroundJob.Enqueue<ChangesTrackService>(i =>
                                i.TrackRequestReplyAsync(item.Request.Replies.LastOrDefault().Id,
                                    RequestActionStatus.CarrierSignRefused, t.Result, userId));
                        }
                        else
                        {
                            BackgroundJob.Enqueue<ChangesTrackService>(i =>
                                i.TrackRequestAsync(item.RequestId,
                                    RequestActionStatus.CancelledByClient, t.Result, userId));
                        }
                    });
            }

            catch (ApiException<AttachDocumentToFreightHaulResponse> e)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId,
                        e.Result.Messages.First().Text));

                throw;
            }

            catch (ApiException<CancelFreightHaulResponse> e)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId,
                        e.Result.Messages.First().Text));

                throw;
            }

            catch (Exception)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.InProgressUserId, ""));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.RequestId, item.Request.AssignedUserId, ""));

                throw;
            }
        }


        private async Task SetWatermark(Reis item)
        {
            var file = item?.Files.FirstOrDefault(i => i.DocumentType == DocumentType.RequestContract);

            if (file?.ContentType != "application/pdf")
            {
                return;
            }

            await using var response = await fileService.GetFileAsync(file.Path);

            var typeIsProviderForAgent =
                await kontragentContractService.TypeIsProviderForAgentAsync(item.KontragentContractId);

            var watermarkDocument = typeIsProviderForAgent
                ? DocumentWatermarkHelper.AddLogistCanceledForProviderAgentWatermark(response)
                : DocumentWatermarkHelper.AddLogistCanceledWatermark(response);

            await krafterExternalClient.AttachDocumentAsync(
                item.ExternalId.ToString(), item.Request.ExternalId, new Document
                {
                    Content = watermarkDocument.GetBuffer(),
                    Name = file.OriginalName,
                    Type = Crafter.Client.CIS.Clients.DocumentType.CarrierRequest
                });

            await fileService.PutFileAsync(watermarkDocument, file.Path);
        }
    }
}