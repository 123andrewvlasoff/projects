using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Crafter.CarrierOffer.WebApp.Services;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using DAL.Models.Requests.Enums;
using GeoCoordinatePortable;
using Krafter.Logist.WebApp.Models.Api.Request;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.ServiceCommon.Models;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Services;

public interface IRequestDtoFilterService
{
    Task<List<RequestDto>> Filter(IEnumerable<RequestDto> items, PageFilterData<RequestDtoFilter> pageFilterData);
}

public class RequestDtoFilterService : IRequestDtoFilterService
{
    private readonly ICarrierOfferService carrierOfferService;
    private readonly LogistDbContext logistDbContext;
    
    private readonly IDictionary<RequestDtoSortParamNames, Func<RequestDto, object>> sortParams =
        new Dictionary<RequestDtoSortParamNames, Func<RequestDto, object>>
        {
            { RequestDtoSortParamNames.LoadLeaveDate, x => x.Load.LeaveDate },
            { RequestDtoSortParamNames.Number1C, x => x.Number1C }, 
            { RequestDtoSortParamNames.Weight, x => x.Weight }, 
            { RequestDtoSortParamNames.Volume, x => x.Volume },
            { RequestDtoSortParamNames.Price, x => x.Price },
            { RequestDtoSortParamNames.PriceForKm, x => x.PriceForKm },
        };

    public RequestDtoFilterService(ICarrierOfferService carrierOfferService, LogistDbContext logistDbContext)
    {
        this.carrierOfferService = carrierOfferService;
        this.logistDbContext = logistDbContext;
    }
    
    public async Task<List<RequestDto>> Filter(IEnumerable<RequestDto> items, PageFilterData<RequestDtoFilter> pageFilterData)
    {
        var filter = pageFilterData.Filter;
        var requestStatuses = new[] { RequestStatus.IsActive, RequestStatus.DataTreatment, RequestStatus.DataInput };
        items = items
            .Where(x =>
                !filter.RequestStatusName.HasValue || filter.RequestStatusName switch
                {
                    RequestStatusNames.InProgress =>
                        x.InProgressUserId == filter.InProgressUserId
                        && requestStatuses.Contains(x.RequestStatus),
                    RequestStatusNames.Favorite =>
                        x.UserRequests.Any(y => y.UserId == filter.InProgressUserId
                                                && y.Status == UserRequestStatus.Favorite),
                    RequestStatusNames.MyRequests =>
                        x.AssignedUserId == filter.InProgressUserId && x.InProgressUserId != filter.InProgressUserId
                        && x.RequestStatus != RequestStatus.Cancelled,
                    RequestStatusNames.All =>
                        requestStatuses.Contains(x.RequestStatus),
                    _ => true
                })
            .Where(x =>
                !(filter.ClientCode?.Length > 0)
                || x.Client != null && filter.ClientCode.Contains(x.Client.ClientCode))
            .Where(x =>
                !(filter.ClientManager?.Length > 0)
                || x.Client.KontragentUsers.Any(y => filter.ClientManager.Contains(y.User.Employer.Id)))
            .Where(x =>
                string.IsNullOrEmpty(filter.Number1C)
                || x.Number1C.Contains(filter.Number1C, StringComparison.InvariantCultureIgnoreCase) 
                || (x.RequestCode!= null && x.RequestCode.Contains(filter.Number1C, StringComparison.InvariantCultureIgnoreCase)))
            .Where(x =>
                (!filter.LoadActiveUntilFrom.HasValue || !filter.LoadActiveUntilTo.HasValue)
                || x.ActiveUntil >= filter.LoadActiveUntilFrom && x.ActiveUntil <= filter.LoadActiveUntilTo)
            .Where(x =>
                (!filter.UnloadArrivalDateFrom.HasValue || !filter.UnloadArrivalDateTo.HasValue)
                || x.Unload.ArrivalDate >= filter.UnloadArrivalDateFrom &&
                x.Unload.ArrivalDate <= filter.UnloadArrivalDateTo)
            .Where(x =>
                !filter.IsInProgress.HasValue
                || (filter.IsInProgress == true && x.InProgressUserId != null) 
                || (filter.IsInProgress == false && x.InProgressUserId == null))
            .Where(x =>
                !filter.RequestType.HasValue || filter.RequestType switch
                {
                    RequestTypeNames.Express => x.Discriminator == RequestDiscriminator.Krafter 
                                           || (x.Discriminator == RequestDiscriminator.Transporeon 
                                               && x.RequestType == RequestType.Express),
                    RequestTypeNames.Auction => x.Discriminator != RequestDiscriminator.Krafter 
                                           && (x.Discriminator != RequestDiscriminator.Transporeon 
                                               || x.RequestType != RequestType.Express),
                    _ => true
                })
            .Where(x =>
                filter.RollingStockTypeId?.Any() != true
                || filter.RollingStockTypeId.Contains(x.RollingStockTypeId))
            .Where(x =>
                !(filter.WeightMin > 0) && !(filter.WeightMax > 0)
                || ((!(filter.WeightMin > 0) || x.Weight >= filter.WeightMin)
                    && (!(filter.WeightMax > 0) || x.Weight <= filter.WeightMax)))
            .Where(x =>
                !(filter.VolumeMin > 0) && !(filter.VolumeMax > 0)
                || ((!(filter.VolumeMin > 0) || x.Volume >= filter.VolumeMin)
                    && (!(filter.VolumeMax > 0) || x.Volume <= filter.VolumeMax)))
            .Where(x =>
                !(filter.PriceMin > 0) && !(filter.PriceMax > 0)
                || ((!(filter.PriceMin > 0) || x.Price >= filter.PriceMin)
                    && (!(filter.PriceMax > 0) || x.Price <= filter.PriceMax)))
            .Where(x =>
                filter.LoadRegionId?.Any() != true
                || filter.LoadRegionId.Contains(x.Load.RegionId))
            .Where(x =>
                filter.UnloadRegionId?.Any() != true
                || filter.UnloadRegionId.Contains(x.Unload.RegionId));

        var loadSettlement = filter.LoadSettlementId.HasValue
            ? await logistDbContext.Settlements.FirstOrDefaultAsync(x => x.Id == filter.LoadSettlementId)
            : null;
        if (loadSettlement is { Lat: { }, Lon: { } } && filter.LoadRemoteness.HasValue)
        {
            var settlementLat = loadSettlement.Lat.Value;
            var settlementLon = loadSettlement.Lon.Value;
            items = items.Where(x =>
                (((x.Load.Lat.HasValue && x.Load.Lon.HasValue) || (x.Load.Settlement.Lon.HasValue && x.Load.Settlement.Lat.HasValue)) &&
                new GeoCoordinate(x.Load.Lat.HasValue ? x.Load.Lat.Value : (double)x.Load.Settlement.Lat, x.Load.Lon.HasValue ? x.Load.Lon.Value : (double)x.Load.Settlement.Lon).GetDistanceTo(
                    new GeoCoordinate(settlementLat, settlementLon))/1000 < filter.LoadRemoteness.Value));
        }

        var unloadSettlement = filter.UnloadSettlementId.HasValue
            ? await logistDbContext.Settlements.FirstOrDefaultAsync(x => x.Id == filter.UnloadSettlementId)
            : null;
        if (unloadSettlement is { Lat: { }, Lon: { } } && filter.UnloadRemoteness.HasValue)
        {
            var settlementLat = unloadSettlement.Lat.Value;
            var settlementLon = unloadSettlement.Lon.Value;
            items = items.Where(x =>
                (((x.Unload.Lat.HasValue && x.Unload.Lon.HasValue) || (x.Unload.Settlement.Lon.HasValue && x.Unload.Settlement.Lat.HasValue)) &&
                new GeoCoordinate(x.Unload.Lat.HasValue ? x.Unload.Lat.Value : (double)x.Unload.Settlement.Lat, x.Unload.Lon.HasValue ? x.Unload.Lon.Value : (double)x.Unload.Settlement.Lon).GetDistanceTo(
                    new GeoCoordinate(settlementLat, settlementLon)) / 1000 < filter.UnloadRemoteness.Value));
        }
        
        var requests = items.ToList();
        var requestIds = requests.Select(x => x.Id).ToList();
        var carrierOffersCount = await carrierOfferService.GetCarrierOffersCount(requestIds, CancellationToken.None);
        foreach (var request in requests)
            request.CarrierOffersCount = carrierOffersCount.TryGetValue(request.Id, out var count) ? count : 0;

        if (pageFilterData.OrderByParams?.Any() != true) 
            return requests;
        
        var orderByParam = pageFilterData.OrderByParams.First();
        var orderedResult = orderByParam.IsDesc
            ? requests.OrderByDescending(sortParams[orderByParam.ParamName])
            : requests.OrderBy(sortParams[orderByParam.ParamName]);
        if (pageFilterData.OrderByParams.Length > 1)
            orderedResult = pageFilterData.OrderByParams.Skip(1)
                .Aggregate(orderedResult, (current, item) => item.IsDesc
                    ? current.ThenByDescending(sortParams[item.ParamName])
                    : current.ThenBy(sortParams[item.ParamName]));
        requests = orderedResult.ToList();

        return requests;
    }
}