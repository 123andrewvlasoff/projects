using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Actions;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePointAction;
using Krafter.ServiceCommon.Services.ChangesTrackService.Models;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Services
{
    [Queue("logist")]
    public class ChangesTrackService
    {
        private readonly LogistDbContext logistDbContext;

        public ChangesTrackService(LogistDbContext logistDbContext)
        {
            this.logistDbContext = logistDbContext;
        }

        public async Task<RequestAction> TrackRequestAsync(Guid key, RequestActionStatus state,
            IEnumerable<PropertyChanges> changes,
            string userId)
        {
            var item = new KrafterRequestAction
            {
                State = state,
                RequestId = key,
                ActionUserId = userId,
                ActionDate = DateTime.Now,
                Changes = changes
            };

            await logistDbContext.RequestActions.AddAsync(item);

            await logistDbContext.SaveChangesAsync();

            return item;
        }

        public async Task<RequestReplyAction> TrackRequestReplyAsync(Guid key, RequestActionStatus state,
            IEnumerable<PropertyChanges> changes, string userId)
        {
            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            var action = new KrafterRequestReplyAction
            {
                State = state,
                RequestReplyId = item.Id,
                RequestId = item.RequestId,
                ActionUserId = userId,
                ActionDate = DateTime.Now,
                Changes = changes
            };

            await logistDbContext.KrafterRequestReplyActions.AddAsync(action);

            await logistDbContext.SaveChangesAsync();

            return action;
        }

        public async Task TrackRequestRoutePointAsync(Guid key, RequestActionStatus state,
            IEnumerable<PropertyChanges> changes, string userId)
        {
            var item = await logistDbContext.KrafterRequestRoutePoints.FindAsync(key);

            await logistDbContext.KrafterRequestRoutePointActions.AddAsync(new KrafterRequestRoutePointAction
            {
                State = state,
                RequestRoutePointId = item.Id,
                RequestId = item.RequestId,
                ActionUserId = userId,
                ActionDate = DateTime.Now,
                Changes = changes
            });

            await logistDbContext.SaveChangesAsync();
        }
    }
}