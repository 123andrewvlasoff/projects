using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.KontragentUser.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.ServiceCommon.Services;
using Microsoft.EntityFrameworkCore;
using RazorLight;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Services
{
    public class BaseEmailSenderService
    {
        protected  LogistDbContext logistDbContext;
        protected  IEmailService emailService;
        protected  RazorLightEngine engine;

        protected BaseEmailSenderService(RazorLightEngine engine, LogistDbContext logistDbContext,
            IEmailService emailService)
        {
            this.engine = engine;
            this.logistDbContext = logistDbContext;
            this.emailService = emailService;
        }

        protected virtual async Task<string[]> GetAddressesToForUsers(List<string> userIds)
        {
            var users = await logistDbContext.Users
                .IncludeOptimized(r=>r.UserRoles)
                .IncludeOptimized(i=>i.UserRoles.Select(r=>r.Role))
                .IncludeOptimized(r=>r.KontragentUsers)
                .IncludeOptimized(r=>r.KontragentUsers.Select(r=>r.Kontragent))
                .Where(r=>userIds.Contains(r.Id))
                .Select(r=>new
                {
                    r.Email,
                    roles=r.UserRoles.Select(i=>i.Role.Name),
                    r.KontragentUsers
                })
                .ToListAsync();

            var addressesTo = new List<string>();

            foreach (var user in users)
            {
                if (!user.roles.Contains(IdentityServer.Extensions.Constants.UserRoles.CarrierManager.ToUpper()))
                {
                    addressesTo.Add(user.Email);
                    continue;
                }
            
                var kontragentUser =
                    user.KontragentUsers.First(r => r.Discriminator == KontragentUserType.Carrier);
                
                addressesTo.Add(kontragentUser.Kontragent.ContactEmail);
            }

            return addressesTo.ToArray();
        }
        
    }
}