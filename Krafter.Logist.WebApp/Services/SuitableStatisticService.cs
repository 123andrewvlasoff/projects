﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using DAL.Models.Transports.Enums;
using EasyCaching.Core;
using GeoCoordinatePortable;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.ActiveAtrucksRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveBaltikaRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveCargomartRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveRequestSuitableStatistic.RequestStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveTorgTransRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ActiveTransporeonRequestSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.ListWithSuitableReisesSuitableStatistic.ListWithSuitableReisesStatistic;
using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.RequestReplySuitableStatistic.RequestReplyStatistic;
using Krafter.Logist.WebApp.Models.Basic.RequestSuitableTransport;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReplySuitableStatistic;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser.Dto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestRoutePointDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Services
{
    [Queue("logist")]
    public class SuitableStatisticService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IEasyCachingProvider cache;
        private readonly RequestDtoRepository repository;

        public SuitableStatisticService(LogistDbContext logistDbContext, IEasyCachingProvider cache, RequestDtoRepository repository)
        {
            this.logistDbContext = logistDbContext;
            this.cache = cache;
            this.repository = repository;
        }

        [AutomaticRetry(Attempts = 0)]
        public async Task SendRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var cachedItems = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTransportCacheKey_" + userId);
            var enumerable = cachedItems.Value;
            var items = enumerable?.ToList();

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.RequestReplySuitableStatistics
                .AddAsync(new RequestReplySuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = item.CreatedAt ?? DateTime.UtcNow.AddHours(3),
                    TransportId = item.TransportId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    KontragentId = item.KontragentId,
                    Data = new RequestReplyStatistic
                    {
                        SuitableTransportCount = items?.Count ?? 0,
                        SuitableTransportCrafterCount = items?.Count(x => x.Owner == Owner.Crafter) ?? 0,
                        SuitableTransportPartnerCount = items?.Count(x => x.Owner == Owner.Partner) ?? 0,
                        SuitableTransportSelected = items?.Any(a =>
                            a.TransportId == item.TransportId && a.ConnectedTransportId == item.ConnectedTransportId) ?? false,
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendCargomartRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableCargomartTransportCacheKey_" + userId);

            var item = await logistDbContext.CargomartRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.CargomartRequestReplySuitableStatistics
                .AddAsync(new CargomartRequestReplySuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = item.CreatedAt.Value,
                    TransportId = item.TransportId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    KontragentId = item.KontragentId,
                    Data = new RequestReplyStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(x => x.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(x => x.Owner == Owner.Partner),
                        SuitableTransportSelected = items.Value.Any(a =>
                            a.TransportId == item.TransportId && a.ConnectedTransportId == item.ConnectedTransportId)
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendTransporeonRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTransporeonTransportCacheKey_" + userId);

            var item = await logistDbContext.TransporeonRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.TransporeonRequestReplySuitableStatistics
                .AddAsync(new TransporeonRequestReplySuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = item.CreatedAt.Value,
                    TransportId = item.TransportId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    KontragentId = item.KontragentId,
                    Data = new RequestReplyStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(x => x.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(x => x.Owner == Owner.Partner),
                        SuitableTransportSelected = items.Value.Any(a =>
                            a.TransportId == item.TransportId && a.ConnectedTransportId == item.ConnectedTransportId)
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }
        public async Task SendTorgTransRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTorgTransTransportCacheKey_" + userId);

            var item = await logistDbContext.TorgTransRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.TorgTransRequestReplySuitableStatistics
                .AddAsync(new TorgTransRequestReplySuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = item.CreatedAt.Value,
                    TransportId = item.TransportId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    KontragentId = item.KontragentId,
                    Data = new RequestReplyStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(x => x.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(x => x.Owner == Owner.Partner),
                        SuitableTransportSelected = items.Value.Any(a =>
                            a.TransportId == item.TransportId && a.ConnectedTransportId == item.ConnectedTransportId)
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendTrafficRequestReply(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTrafficTransportCacheKey_" + userId);

            var item = await logistDbContext.TrafficRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await logistDbContext.TrafficRequestReplySuitableStatistics
                .AddAsync(new TrafficRequestReplySuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = item.CreatedAt.Value,
                    TransportId = item.TransportId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    KontragentId = item.KontragentId,
                    Data = new RequestReplyStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(x => x.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(x => x.Owner == Owner.Partner),
                        SuitableTransportSelected = items.Value.Any(a =>
                            a.TransportId == item.TransportId && a.ConnectedTransportId == item.ConnectedTransportId)
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTransportCacheKey_" + userId);

            await logistDbContext.ActiveRequestSuitableStatistics
                .AddAsync(new ActiveRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.Now,
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        [AutomaticRetry(Attempts = 0)]
        public async Task SendCargomartRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableCargomartTransportCacheKey_" + userId);

            await logistDbContext.ActiveCargomartRequestSuitableStatistics
                .AddAsync(new ActiveCargomartRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.Now,
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendTransporeonRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTransporeonTransportCacheKey_" + userId);

            await logistDbContext.ActiveTransporeonRequestSuitableStatistics
                .AddAsync(new ActiveTransporeonRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.UtcNow.AddHours(3),
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendTorgTransRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTorgTransTransportCacheKey_" + userId);

            await logistDbContext.ActiveTorgTransRequestSuitableStatistics
                .AddAsync(new ActiveTorgTransRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.Now,
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        public async Task SendTrafficRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableTrafficTransportCacheKey_" + userId);

            await logistDbContext.ActiveTorgTransRequestSuitableStatistics
                .AddAsync(new ActiveTorgTransRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.Now,
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        [AutomaticRetry(Attempts = 0)]
        public async Task SendListWithSuitableReisesSuitableStatistic(string userId)
        {
            const int remotenessParameter = 100;
            const int dateIntervalParameter = 24;

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<TransportKontragentUserDto>>(
                "listWithSuitableReisesSuitableStatistic" + userId);

            var requests = await repository.GetAsync();

            var routePoints = requests
                .Where(i => i.RequestStatus == RequestStatus.IsActive && (i.ActiveUntil > DateTime.UtcNow.AddHours(3) || i.AssignedUserId == userId))
                .Select(s => s.RoutePoints.OrderBy(o => o.OrderInRoute).FirstOrDefault(x => x.PointType == PointType.Load));

            var statictics = new List<Tuple<Guid, int>>();

            foreach (var item in items.Value)
            {
                var count = 0;

                if (item.LastReisDate != null && item.LastReisLat != null && item.LastReisLon != null)
                {
                    count = GetSuitableReisesCount(item, routePoints, dateIntervalParameter, remotenessParameter);
                }

                statictics.Add(new Tuple<Guid, int>(item.TransportId, count));
            }

            await logistDbContext.ListWithSuitableReisesSuitableStatistics
                .AddAsync(new ListWithSuitableReisesSuitableStatistic
                {
                    CreatorId = userId,
                    CreatedAt = DateTime.Now,
                    Data = new ListWithSuitableReisesStatistic
                    {
                        Map = statictics.GroupBy(g => g.Item1)
                            .ToDictionary(s => s.Key, s => s.First().Item2)
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }

        private int GetSuitableReisesCount(TransportKontragentUserDto item, IEnumerable<RequestRoutePointDto> routePointsDto,
            int hoursFromUnload, int remoteness)
        {
            routePointsDto = routePointsDto.Where(r =>
                r.ArrivalDate >= item.LastReisDate.Value.AddHours(hoursFromUnload) &&
                r.ArrivalDate <= item.LastReisDate.Value.AddHours(hoursFromUnload + 36)).ToList();

            routePointsDto = routePointsDto.Where(r => r.Lat != null && r.Lon != null &&
                                                 new GeoCoordinate(r.Lat.Value, r.Lon.Value).GetDistanceTo(
                                                     new GeoCoordinate(item.LastReisLat.Value,
                                                         item.LastReisLon.Value)) / 1000 <= remoteness).ToList();

            return routePointsDto.Count();
        }
        
        public async Task SendBaltikaRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableBaltikaTransportCacheKey_" + userId);

            await logistDbContext.ActiveBaltikaRequestSuitableStatistics
                .AddAsync(new ActiveBaltikaRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.UtcNow.AddHours(3),
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }
        public async Task SendAtrucksRequest(Guid key, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = await cache.GetAsync<IEnumerable<RequestSuitableTransport>>(
                "suitableAtrucksTransportCacheKey_" + userId);

            await logistDbContext.ActiveAtrucksRequestSuitableStatistics
                .AddAsync(new ActiveAtrucksRequestSuitableStatistic
                {
                    RequestId = key,
                    CreatorId = userId,
                    CreatedAt = DateTime.UtcNow.AddHours(3),
                    Data = new RequestStatistic
                    {
                        SuitableTransportCount = items.Value.Count(),
                        SuitableTransportCrafterCount = items.Value.Count(c => c.Owner == Owner.Crafter),
                        SuitableTransportPartnerCount = items.Value.Count(c => c.Owner == Owner.Partner),
                    }
                });

            await logistDbContext.SaveChangesAsync();
        }
    }
}