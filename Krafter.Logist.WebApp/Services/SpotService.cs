﻿using MassTransit;
using System.Threading.Tasks;
using System.Threading;
using System;
using Crafter.Integration.Models.CrafterOnline.Models.Spot;

namespace Krafter.Logist.WebApp.Services
{
    public class SpotService
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;
        public SpotService(ISendEndpointProvider sendEndpointProvider)
        {
            _sendEndpointProvider = sendEndpointProvider;
        }
        public async Task SendCommandAsync(SpotCommandRequest command)
        {
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(15));
            await _sendEndpointProvider.Send<SpotCommandRequest>(command, cts.Token);
        }
    }
}
