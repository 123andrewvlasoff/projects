﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Requests;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Krafter.Enums;
using Hangfire;
using Crafter.Client.CIS.Clients;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;
using DocumentType = DAL.Models.Enums.DocumentType;
using Crafter.Integration.Models.CrafterOnline.Models.Spot;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Krafter.Logist.WebApp.Services
{
    public class KrafterRequestService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IKrafterExternalClient krafterExternalClient;
        private readonly IFileService fileService;
        private readonly KontragentContractService kontragentContractService;
        private readonly IMapper mapper;
        private readonly RequestDtoRepository repository;
        private readonly SpotService _spotService;

        public KrafterRequestService(LogistDbContext logistDbContext,
            IKrafterExternalClient krafterExternalClient, IFileService fileService,
            KontragentContractService kontragentContractService, IMapper mapper, RequestDtoRepository repository,
            SpotService spotService)
        {
            this.logistDbContext = logistDbContext;
            this.krafterExternalClient = krafterExternalClient;
            this.fileService = fileService;
            this.kontragentContractService = kontragentContractService;
            this.mapper = mapper;
            this.repository = repository;
            _spotService = spotService;
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task CancelRequestByCarrier(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Replies.Where(x => !x.IsDeleted).Select(s => new
                {
                    s.Files,
                    s.KontragentContact,
                    s.Kontragent,
                }))
                .IncludeOptimized(i => i.Replies.Where(x => !x.IsDeleted)
                    .Select(s => s.Kontragent.KontragentUsers.Select(s => s.User)))
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(r => r.Reises.Where(x => x.StatusId != Cancelled))
                .FirstOrDefaultAsync(i => i.Id == key);

            var requestReply = item.Replies.First();

            var userIds = new[] { item.InProgressUserId, item.AssignedUserId }.Distinct().ToList();

            try
            {
                await krafterExternalClient.CancelAsync(item.ExternalId);
                // await SetWatermarkAndAttach(item);

                item.RequestStatus = RequestStatus.IsActive;
                item.KrafterRequestStatus = KrafterRequestStatus.InProgress;
                item.UpdatedAt = DateTime.Now;
                requestReply.IsDeleted = true;

                foreach (var rr in item.RoutePoints)
                {
                    rr.ArrivalDateFact = null;
                    rr.LeaveDateFact = null;
                }

                if (item.Reises.Any())
                {
                    item.Reises.First().StatusId = Cancelled;
                    item.Reises.First().IsActive = true;
                    item.Reises.First().IsComplete = true;
                    item.Reises.First().IsDeleted = false;
                }

                await logistDbContext.SaveChangesAsync();


                foreach (var userId in userIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestCancelByCarrierNotification(item.Id, userId));
                }

                if (!item.Replies.First().Kontragent.KontragentUsers.Any())
                {
                    return;
                }

                var kontragentUser = item.Replies.First().Kontragent.KontragentUsers.First();

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestCancelByCarrierNotification(item.Id,
                        item.Replies.First().Kontragent.KontragentUsers.First().UserId));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(item.Replies.FirstOrDefault().Id, SubjectType.LogistCancelRequest,
                        kontragentUser != null
                            ? kontragentUser.User.Email
                            : item.Replies.First().KontragentContact.Email));
            }

            catch (ApiException<AttachDocumentToFreightHaulResponse> e)
            {
                foreach (var userId in userIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyProcessErrorNotification(item.Id, userId,
                            e.Result.Messages.First().Text));
                }

                throw;
            }

            catch (ApiException<CancelFreightHaulResponse> e)
            {
                foreach (var userId in userIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyProcessErrorNotification(item.Id, userId,
                            e.Result.Messages.First().Text));
                }

                throw;
            }

            catch (Exception)
            {
                foreach (var userId in userIds)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyProcessErrorNotification(item.Id, userId, ""));
                }

                throw;
            }
        }

        [Queue("digital")]
        [AutomaticRetry(Attempts = 0)]
        public async Task CancelRequestByClient(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.DeniedReason)
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(r => r.Reises.Where(x => x.StatusId != Cancelled))
                .IncludeOptimized(i => i.InProgressUser)
                .IncludeOptimized(i => i.AssignedUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            var userIds = new[] { item.AssignedUserId, item.InProgressUserId }.Distinct();
            
            try
            {
                await krafterExternalClient.Cancel2Async(item.ExternalId,
                    new CancellationReason
                    {
                        Guid = item.DeniedReason.ExternalId.ToString()
                    });

                // Проверяем если отмена была по копии транспореоновского заказа, то отменяем в транспореоне тоже
                if (item.ExternalId != null)
                {
                    var transporeonRequest = await logistDbContext.TransporeonRequests.IgnoreQueryFilters()
                        .FirstOrDefaultAsync(i =>
                            i.Id == Guid.Parse(item.ExternalId) && i.Discriminator == RequestDiscriminator.Transporeon);

                    if (transporeonRequest != null)
                    {
                        await _spotService.SendCommandAsync(new SpotCommandRequest
                        {
                            SpotCommand = SpotCommand.CancelOnAssignSpot,
                            ExternalId = transporeonRequest.ExternalId,
                            EmployId = item.InProgressUserId != null
                                ? item.InProgressUser.Employer.ExternalId.ToString()
                                : item.AssignedUser.Employer.ExternalId.ToString(),
                            EmployEmail = item.InProgressUserId != null
                                ? item.InProgressUser.Employer.Email
                                : item.AssignedUser.Employer.Email,
                        });
                    }
                }

                item.RequestStatus = RequestStatus.Cancelled;
                item.KrafterRequestStatus = KrafterRequestStatus.CancelledByClient;

                item.UpdatedAt = DateTime.Now;

                foreach (var rr in item.RoutePoints)
                {
                    rr.ArrivalDateFact = null;
                    rr.LeaveDateFact = null;
                }

                if (item.Reises.Any())
                {
                    item.Reises.First().StatusId = Cancelled;
                    item.Reises.First().IsActive = true;
                    item.Reises.First().IsComplete = true;
                    item.Reises.First().IsDeleted = false;
                }

                await logistDbContext.SaveChangesAsync()
                    .ContinueWith(t =>
                    {
                        foreach (var userId in new[] { item.Replies.First().CreatorId, item.AssignedUserId }.Distinct())
                        {
                            BackgroundJob.Enqueue<NotificationsService>(i =>
                                i.AddRequestCancelledByClientNotification(item.Id, userId));
                        }
                    });
            }

            catch (ApiException<CancelPickUpRequestResponse> e)
            {
                SendErrorNotifications(item.Id, userIds, e.Result.Messages.First().Text);
                
                throw;
            }
            catch (ApiException<CancelFreightHaulResponse> e)
            {
                SendErrorNotifications(item.Id, userIds, e.Result.Messages.First().Text);

                throw;
            }
            catch (Exception e)
            {
                SendErrorNotifications(item.Id, userIds, "");

                throw;
            }
        }

        private void SendErrorNotifications(Guid id, IEnumerable<string> userIds,  string error)
        {
            foreach (var userId in userIds)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(id, userId, error));
            }
        } 
        
        
        public async Task SetWatermarkAndAttach(KrafterRequest item)
        {
            var requestReply = item.Replies.FirstOrDefault();

            if (requestReply == null)
            {
                return;
            }

            RequestReplyFile document = null;

            switch (requestReply.RequestReplyType)
            {
                case RequestReplyType.Digital:
                    document = requestReply.Files
                        .FirstOrDefault(i => i.DocumentType == DocumentType.RequestContractTemplate);
                    break;
                case RequestReplyType.Paper:
                    document = requestReply.Files
                        .FirstOrDefault(i => i.DocumentType == DocumentType.RequestContract);
                    break;
            }

            if (document == null)
            {
                return;
            }

            await using var response = await fileService.GetFileAsync(document.Path);

            var typeIsProviderForAgent =
                await kontragentContractService.TypeIsProviderForAgentAsync(item.Reises.First().KontragentContractId);

            var watermarkDocument = typeIsProviderForAgent
                ? DocumentWatermarkHelper.AddLogistCanceledForProviderAgentWithoutCarrierWatermark(
                    response)
                : DocumentWatermarkHelper.AddLogistCanceledWatermark(response);

            await krafterExternalClient.AttachDocumentAsync(
                item.Reises.First().ExternalId.ToString(), item.ExternalId, new Document
                {
                    Content = watermarkDocument.GetBuffer(),
                    Name = document.OriginalName,
                    Type = Crafter.Client.CIS.Clients.DocumentType.CarrierRequest
                });

            await fileService.PutFileAsync(watermarkDocument, document.Path);
        }

        [Queue("logist")]
        [AutomaticRetry(Attempts = 0)]
        public async Task TakeInProgressRequestAsync(Guid key, string userId)
        {
            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Actions)
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.AssignedUser)
                .IncludeOptimized(i => i.InProgressUser.Employer)
                .FirstOrDefaultAsync(ur => ur.Id == key);

            if (item.InProgressUserId == null ||
                item.InProgressUserId == item.AssignedUserId)
            {
                return;
            }

            item.Actions.Add(new KrafterRequestAction
            {
                ActionUserId = userId,
                RequestId = item.Id,
                ActionDate = DateTime.Now,
                State = RequestActionStatus.TakeInProgress,
            });

            await logistDbContext.SaveChangesAsync();

            var request = mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", userId));

            try
            {
                await krafterExternalClient.EmployeeAsync(request.ExternalId, new ResponsibleEmployee
                {
                    EmployeeGuid = item.InProgressUser.Employer.ExternalId.ToString()
                });
            }
            catch (ApiException<GetPickUpRequestResponse> e)
            {
                item.RequestStatus = RequestStatus.Error;
                item.KrafterRequestStatus = KrafterRequestStatus.EmployChangeError;

                await logistDbContext.SaveChangesAsync();

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.Id, item.InProgressUserId,
                        e.Result.Messages.First().Text));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.Id, item.AssignedUserId,
                        e.Result.Messages.First().Text));

                return;
            }
            catch (Exception)
            {
                item.RequestStatus = RequestStatus.Error;
                item.KrafterRequestStatus = KrafterRequestStatus.EmployChangeError;

                await logistDbContext.SaveChangesAsync();

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.Id, item.InProgressUserId, ""));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddRequestReplyProcessErrorNotification(item.Id, item.AssignedUserId, ""));

                return;
            }

            BackgroundJob.Enqueue<NotificationsService>(i =>
                i.AddSetAssignedUserNotification(request.Id, request.InProgressUserId));

            BackgroundJob.Enqueue<NotificationsService>(i =>
                i.AddChangeAssignedUserNotification(request.Id, request.InProgressUserId));

            BackgroundJob.Enqueue<EmailSenderService>(i =>
                i.SendRequestEmail(request.Id, SubjectType.RequestSetAssignedUser,
                    request.InProgressUser.Email, request.InProgressUserId));

            item.AssignedUserId = item.InProgressUserId;
            await logistDbContext.SaveChangesAsync();

            if (request.AssignedUserId != null)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddTakeInProgressForAssignedUserNotification(request.Id, request.AssignedUserId));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestEmail(request.Id, SubjectType.LogistCreateUserRequest, request.AssignedUser.Email,
                        request.InProgressUserId));

                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddChangeAssignedUserNotification(request.Id, request.AssignedUserId));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestEmail(request.Id, SubjectType.RequestChangedAssignedUser,
                        request.AssignedUser.Email, request.InProgressUserId));
            }

            //await repository.SetAsync(request.Id);
        }

        [Queue("logist")]
        [AutomaticRetry(Attempts = 0)]
        public async Task BookRequestAsync(Guid key, string inProgressUserId)
        {
            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Actions)
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.AssignedUser)
                .FirstOrDefaultAsync(ur => ur.Id == key);

            if (inProgressUserId == null ||
                inProgressUserId == item.AssignedUserId)
            {
                return;
            }

            item.Actions.Add(new KrafterRequestAction
            {
                ActionUserId = inProgressUserId,
                RequestId = item.Id,
                ActionDate = DateTime.Now,
                State = RequestActionStatus.TakeInProgress,
            });

            await logistDbContext.SaveChangesAsync();

            var request = mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", inProgressUserId));

            BackgroundJob.Schedule<KrafterRequestService>(i =>
                i.CleanInprogressUserInfo(request.Id), request.LastActionDate.Value - DateTime.Now);

            if (request.AssignedUserId != null)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddTakeInProgressForAssignedUserNotification(request.Id, request.AssignedUserId));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestEmail(request.Id, SubjectType.LogistCreateUserRequest, request.AssignedUser.Email,
                        request.InProgressUserId));
            }

            //await repository.SetAsync(request.Id);
        }

        [Queue("logist")]
        public async Task CleanInprogressUserInfo(Guid requestId)
        {
            var item = logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Replies.Where(x => !x.IsDeleted))
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Actions)
                .IncludeOptimized(i => i.InProgressUser)
                .First(i => i.Id == requestId);

            if (item.InProgressUserId == null)
            {
                return;
            }

            var request =
                mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", item.InProgressUserId));

            if (request.LastActionDate > DateTime.Now ||
                request.InProgressUserId == request.AssignedUserId ||
                request.Replies.Any()
               )
            {
                return;
            }

            item.Actions.Add(new KrafterRequestAction
            {
                ActionDate = DateTime.Now,
                ActionUserId = item.InProgressUserId,
                State = RequestActionStatus.FreeInProgressAutomatically,
            });

            item.InProgressUserId = null;
            item.RequestStatus = RequestStatus.IsActive;
            item.KrafterRequestStatus = null;
            item.UpdatedAt = DateTime.Now;

            await logistDbContext.SaveChangesAsync();

            //await repository.SetAsync(request.Id);

            BackgroundJob.Enqueue<NotificationsService>(i =>
                i.AddRequestReservationExpiredNotification(request.Id, request.InProgressUserId));

            BackgroundJob.Enqueue<EmailSenderService>(i =>
                i.SendRequestEmail(request.Id, SubjectType.RequestReservationExpired, request.InProgressUser.Email,
                    request.InProgressUserId));
        }

        [Queue("logist")]
        public async Task RequestUnblocked(Guid key)
        {
            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.InProgressUser)
                .IncludeOptimized(i => i.Actions)
                .IncludeOptimized(i => i.RoutePoints)
                .FirstOrDefaultAsync(ur => ur.Id == key);

            BackgroundJob.Enqueue<NotificationsService>(i => i.AddRequestUnblockedNotification(item.Id));

            BackgroundJob.Enqueue<EmailSenderService>(i =>
                i.SendRequestEmail(item.Id, SubjectType.RequestUnblocked, item.InProgressUser.Email,
                    item.InProgressUserId));

            var mappedItem =
                mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", item.InProgressUserId));

            if (mappedItem.LastActionDate >= mappedItem.BookUntil)
            {
                await TakeInProgressRequestAsync(item.Id, item.InProgressUserId);
            }
            else
            {
                await BookRequestAsync(item.Id, item.InProgressUserId);
            }
        }
    }
}