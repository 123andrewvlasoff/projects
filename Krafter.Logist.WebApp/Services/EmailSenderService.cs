﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Crafter.CarrierOffer.WebApp.Services;
using DAL.Models.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Krafter.Enums;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply;
using Krafter.Logist.WebApp.Views.Email.CarrierOffer;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.Logist.WebApp.Views.Email.Request;
using Krafter.Logist.WebApp.Views.Email.RequestReply;
using Krafter.Logist.WebApp.Views.Email.SupportTicket;
using Krafter.ServiceCommon.Services;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using RazorLight;
using Z.EntityFramework.Plus;
using User = DAL.Models.Users.User;

namespace Krafter.Logist.WebApp.Services
{
    [Queue("logist")]
    public class EmailSenderService
    {
        private readonly IEmailService emailService;
        private readonly RazorLightEngine engine;
        private readonly LogistDbContext logistDbContext;
        private readonly IFileService fileService;
        private readonly IMapper mapper;
        private readonly ICarrierOfferService carrierOfferService;

        public EmailSenderService(RazorLightEngine engine, LogistDbContext logistDbContext, IEmailService emailService,
            IFileService fileService, IMapper mapper, ICarrierOfferService carrierOfferService)
        {
            this.engine = engine;
            this.logistDbContext = logistDbContext;
            this.emailService = emailService;
            this.fileService = fileService;
            this.mapper = mapper;
            this.carrierOfferService = carrierOfferService;
        }

        public async Task SendNewSupportTicketEmail(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.SupportTickets.IgnoreQueryFilters()
                .IncludeOptimized(i => i.User)
                .IncludeOptimized(i => i.Messages)
                .IncludeOptimized(i => i.MessageType.MessagesEmailTypes.Select(met => new
                {
                    met.SupportUser.Employer
                }))
                .FirstOrDefaultAsync(i => i.Id == key);


            var addressesTo = item.MessageType.MessagesEmailTypes
                .Select(i => i.SupportUser.Employer.Email);

            var subject = "Получено обращение с темой: \"" + item.MessageType.Name + "\"";

            var htmlBody = await engine.CompileRenderAsync("/Views/Email/SupportTicket/SupportTicketView.cshtml",
                new SupportTicketViewModel
                {
                    Author = item.User.FullName,
                    CreatedAt = item.CreatedAt,
                    Type = item.MessageType.Name,
                    Messages = item.Messages.Select(i => i.Text),
                    Url = $"https://support.crafter.online/#/messages/{item.Id}/details"
                });


            await emailService.SendEmailAsync(addressesTo, subject, null, htmlBody);
        }

        public async Task SendRequestReplyEmail(Guid key, SubjectType type, string email)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files.Where(x => !x.IsDeleted))
                .Include(i => i.Request).ThenInclude(j => j.RoutePoints)
                .Include(i => i.Request.Reises).ThenInclude(j => j.RoutePoints)
                .IncludeOptimized(i => i.Request.DeniedReason)
                .IncludeOptimized(i => i.Request.InProgressUser.Employer)
                .IncludeOptimized(i => i.Request.AssignedUser.Employer)
                .IncludeOptimized(i => i.Request.Client)
                .IncludeOptimized(i => i.Request.KontragentContract.ShippingDocsRequirements)
                .IncludeOptimized(i => i.Creator.Employer)
                .IncludeOptimized(i=>i.Creator.KontragentUsers)
                .IncludeOptimized(i => i.Kontragent)
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.ConnectedTransport)
                .IncludeOptimized(i => i.Driver)
                .IncludeOptimized(i=>i.Comments)
                .FirstOrDefaultAsync(i => i.Id == key);

            var reisNumber = "";
            switch (type)
            {
                case SubjectType.LogistCancelRequest:
                    reisNumber=item.Request.Reises.FirstOrDefault(i=>i.StatusId==ReisStatus.Cancelled)?.DeliveryNumber??"";
                    break;
                default:
                    reisNumber=item.Request.Reises.FirstOrDefault(i=>i.StatusId!=ReisStatus.Cancelled)?.DeliveryNumber??"";
                    break;
            }
                

            item = mapper.Map<KrafterRequestReply>(item, options => options.Items.Add("UserId", item.CreatorId));

            var subject = "";

            List<MimePart> mimePart = new List<MimePart>();

            var addressesTo = new List<string>
            {
                "cyberplatform@crafter-tl.ru",
            };

            switch (type)
            {
                case SubjectType.LogistAccept:

                    var requestTsd = item.Request.KontragentContract?.ShippingDocsRequirements;

                    addressesTo.Add(email);
                    subject =
                        $"Заявка № {item.Request.Number1C}. Документ{(item.RequestReplyType==RequestReplyType.Digital?", подписанный ПЭП":"")} согласован логистом {(item.CreatedByCarrier?item.Request.AssignedUser.Employer.FIO:item.Creator.Employer.FIO)}.";

                    if (requestTsd != null)
                    {
                        await using var result = await fileService.GetFileAsync(requestTsd?.Path);

                        mimePart.Add(new MimePart("application", "pdf")
                        {
                            Content = new MimeContent(new MemoryStream(result.GetBuffer())),
                            ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                            ContentTransferEncoding = ContentEncoding.Base64,
                            FileName = "Действующие требования по ТСД.pdf"
                        });
                    }

                    break;

                case SubjectType.LogistCancelRequest:
                    addressesTo.Add(email);
                    subject =
                        $"№ Задание {item.Request.Number1C}. Отклонено логистом {(item.CreatedByCarrier?item.Request.AssignedUser.Employer.FIO:item.Creator.Employer.FIO)}.";
                    break;

                case SubjectType.LogistReplyForEmployer:
                    addressesTo.Add(email);
                    subject =
                        $"№ Задание {item.Request.Number1C}{(!string.IsNullOrEmpty(reisNumber) ? "/№ Рейса " + reisNumber : "")} взято в работу логистом {item.Creator.Employer.FIO}";
                    break;

                case SubjectType.LogistPaperSignWaitingForCarrier:

                    var requestTemplateForCarrier = item.Files
                        .FirstOrDefault(i => i.DocumentType == DocumentType.RequestContractTemplate);

                    addressesTo.Add(email);
                    subject =
                        $"№ Задание {item.Request.Number1C}{(!string.IsNullOrEmpty(reisNumber) ? "/№ Рейса " + reisNumber : "")}. Получена заявка на подписание.";

                    if (requestTemplateForCarrier != null)
                    {
                        await using var result = await fileService.GetFileAsync(requestTemplateForCarrier.Path);

                        mimePart.Add(new MimePart("application", "pdf")
                        {
                            Content = new MimeContent(new MemoryStream(result.GetBuffer())),
                            ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                            ContentTransferEncoding = ContentEncoding.Base64,
                            FileName = "RequestTemplate.pdf"
                        });
                    }

                    break;

                case SubjectType.LogistPaperSignWaitingForLogist:

                    var requestTemplateForLogist =
                        item.Files.FirstOrDefault(i => i.DocumentType == DocumentType.RequestContractTemplate);

                    addressesTo.Add(email);
                    subject =
                        $"№ Задание {item.Request.Number1C}{(!string.IsNullOrEmpty(reisNumber) ? "/№ Рейса " + reisNumber : "")}. Получена заявка на подписание.";

                    if (requestTemplateForLogist != null)
                    {
                        await using var result = await fileService.GetFileAsync(requestTemplateForLogist?.Path);

                        mimePart.Add(new MimePart("application", "pdf")
                        {
                            Content = new MimeContent(new MemoryStream(result.GetBuffer())),
                            ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                            ContentTransferEncoding = ContentEncoding.Base64,
                            FileName = "RequestTemplate.pdf"
                        });
                    }

                    break;

                case SubjectType.LogistDigitalSignWaitingForCarrier:
                    addressesTo.Add(email);
                    subject = 
                        $"Задание № {item.Request.Number1C}{(!string.IsNullOrEmpty(reisNumber) ? "/Рейса № " + reisNumber : "")}. Получена заявка на подписание ПЭП.";
                    break;

                case SubjectType.LogistDigitalSignWaitingForLogist:
                    addressesTo.Add(email);
                    subject =
                        $"Задание № {item.Request.Number1C}{(!string.IsNullOrEmpty(reisNumber) ? "/Рейса № " + reisNumber : "")}. Получена заявка на подписание ПЭП.";
                    break;
            }

            var htmlBody = await GetRequestReplyEmailBodyAsync(item, type);

            await emailService.SendEmailAsync(addressesTo, subject, null, htmlBody, mimePart);
        }

        private async Task<string> GetRequestReplyEmailBodyAsync(KrafterRequestReply requestReply, SubjectType type)
        {
            var model = new RequestReplyViewModel
            {
                RequestId = requestReply.RequestId,
                DeniedReason = requestReply.Request.DeniedReasonId != null
                    ? requestReply.Request.DeniedReason.Title
                    : "",
                RequestNumber1C = requestReply.Request.Number1C,
                ReisNumber1C = requestReply.Request.Reises.FirstOrDefault()?.DeliveryNumber??"",
                CarrierTitle = requestReply.Kontragent.Title,
                ReplyAt = requestReply.CreatedAt.Value,
                Contact = requestReply.CreatedByCarrier? requestReply.Request.AssignedUser.Employer:requestReply.Creator.Employer,
                KontragentContact = requestReply.KontragentContact,
                Transport = requestReply.Transport,
                ConnectedTransport = requestReply.ConnectedTransport,
                Driver = requestReply.Driver,
                Client = requestReply.Request.Client.Title,
                RequestCode = requestReply.Request.RequestCode,
                Load = requestReply.Request.Load,
                Unload = requestReply.Request.Unload,
                Price = requestReply.Request.Reises.FirstOrDefault()?.Price??0,
                LogistComment = requestReply.CreatedByCarrier?requestReply.Comments
                        .FirstOrDefault(i=>i.CreatorId==requestReply.Request.AssignedUserId)?.Text??"":
                    requestReply.Comments
                        .FirstOrDefault(i=>i.CreatorId==requestReply.CreatorId)?.Text??""
            };

            var result = "";

            switch (type)
            {
                case SubjectType.LogistAccept:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistAccept.cshtml", model);
                    break;

                case SubjectType.LogistCancelRequest:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistCancelRequest.cshtml", model);
                    break;

                case SubjectType.LogistReplyForEmployer:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistReplyForEmployer.cshtml", model);
                    break;

                case SubjectType.LogistPaperSignWaitingForCarrier:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistPaperSignWaitingForCarrier.cshtml", model);
                    break;

                case SubjectType.LogistDigitalSignWaitingForCarrier:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistDigitalSignWaitingForCarrier.cshtml", model);
                    break;

                case SubjectType.LogistPaperSignWaitingForLogist:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistPaperSignWaitingForLogist.cshtml", model);
                    break;

                case SubjectType.LogistDigitalSignWaitingForLogist:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/RequestReply/LogistDigitalSignWaitingForLogist.cshtml", model);
                    break;
            }

            return result;
        }

        [AutomaticRetry(Attempts = 40, DelaysInSeconds = new int[] { 30 })] 
        public async Task SendRequestEmail(Guid key, SubjectType type, string email, string userId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.RoutePoints.Select(s => new
                {
                    s.Region,
                    s.Settlement
                }))
                .IncludeOptimized(i => i.InProgressUser.Employer)
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            var user = logistDbContext.Users.First(x => x.Id == userId);

            var settlementLoad =
                item.Load.SettlementId != null ? item.Load.Settlement.Title : "";

            var settlementUnload =
                item.Unload.SettlementId != null ? item.Unload.Settlement.Title : "";

            var subject = "";

            List<MimePart> mimePart = new List<MimePart>();

            var addressesTo = new List<string>
            {
                "cyberplatform@crafter-tl.ru",
            };

            switch (type)
            {
                case SubjectType.LogistCreateUserRequest:
                    addressesTo.Add(email);
                    subject =
                        $"Ваше Задание № {item.Number1C}/Номер транспортировки {item.RequestCode}, {settlementLoad} - {settlementUnload}, {item.Client.Title}" +
                        $" {(item.InProgressUserId == item.AssignedUserId ? "взято в работу логистом" : "забронировано логистом")} {user.FullName}";
                    break;

                case SubjectType.LogistDeleteUserRequest:
                    addressesTo.Add(email);
                    subject =
                        $"Ваше Задание № {item.Number1C}/Номер транспортировки {item.RequestCode}, {settlementLoad} - {settlementUnload}, {item.Client.Title} было освобождено логистом {user.FullName}";
                    break;

                case SubjectType.RequestReservationExpired:
                    addressesTo.Add(email);
                    subject =
                        $"Время бронирования по заданию {item.Number1C}, {settlementLoad} - {settlementUnload}, {item.Client.Title} истекло, задание освобождено автоматически";
                    break;


                case SubjectType.RequestChangedAssignedUser:
                    addressesTo.Add(email);
                    subject =
                        $"Задание № {item.Number1C}, {settlementLoad} - {settlementUnload}, {item.Client.Title} изменен ответственный логист {user.FullName}.";
                    break;

                case SubjectType.RequestSetAssignedUser:
                    addressesTo.Add(email);
                    subject =
                        $"Задание № {item.Number1C}, {settlementLoad} - {settlementUnload}, {item.Client.Title} автоматически закреплено за вами.";
                    break;

                case SubjectType.RequestUnblocked:
                    addressesTo.Add(email);

                    if (item.InProgressUserId == item.AssignedUserId)
                    {
                        var deltaTime = TimeSpan
                            .FromMinutes((item.Load.ArrivalDate - DateTime.Now).TotalMinutes)
                            .ToString(@"hh\:mm\:ss");

                        subject =
                            "Задание разблокировано, можно оформлять заявку с перевозчиком." +
                            $" До погрузки осталось {deltaTime}, Вы назначены ответственным за закрытие данного задания”";
                    }
                    else
                    {
                        subject = "Задание разблокировано оператором, можно оформлять заявку с перевозчиком";
                    }

                    break;
            }

            var htmlBody = await GetRequestEmailBodyAsync(item, type, user);

            await emailService.SendEmailAsync(addressesTo, subject, null, htmlBody, mimePart);
        }

        private async Task<string> GetRequestEmailBodyAsync(KrafterRequest request, SubjectType type, User user)
        {
            var model = new RequestViewModel
            {
                RequestId = request.Id,
                RequestNumber1C = request.Number1C,
                TransportationNumber = request.RequestCode,
                Client = request.Client.Title,
                Load = request.Load,
                Unload = request.Unload,
                Price = request.Price,
                TakenBy = user,
                TakeInProgress = request.InProgressUserId == request.AssignedUserId,
            };

            var result = "";

            switch (type)
            {
                case SubjectType.LogistCreateUserRequest:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/LogistCreateUserRequest.cshtml",
                        model);
                    break;

                case SubjectType.LogistDeleteUserRequest:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/LogistDeleteUserRequest.cshtml",
                        model);
                    break;

                case SubjectType.RequestReservationExpired:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/RequestReservationExpired.cshtml",
                        model);
                    break;

                case SubjectType.RequestChangedAssignedUser:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/RequestChangedAssignedUser.cshtml",
                        model);
                    break;

                case SubjectType.RequestSetAssignedUser:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/RequestChangedAssignedUser.cshtml",
                        model);
                    break;

                case SubjectType.RequestUnblocked:
                    result = await engine.CompileRenderAsync(
                        "/Views/Email/Request/RequestUnblocked.cshtml",
                        model);
                    break;
            }

            return result;
        }
        
        public async Task SendDocsOnVerificationEmail(Guid reisId, string email)
        {
            var reis = await logistDbContext.Reises.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == reisId);
            
            if (reis == null)
            {
                return;
            }
            var text = $"Документы по рейсу №{reis.DeliveryNumber} приняты на проверку";
            
            await emailService.SendEmailAsync(new[] {email}, text, text, null);
        }
        
        public async Task SendDriverDontGaveDocumentsUp(Guid reisId, string email)
        {
            var reis = await logistDbContext.Reises
                .IncludeOptimized(r => r.Client)
                .IncludeOptimized(r => r.Carrier)
                .IncludeOptimized(r => r.Driver)
                .FirstOrDefaultAsync(r=>r.Id==reisId);

            if (reis == null)
            {
                return;
            }
            
            var subj = $"Водитель {reis.Driver.FullName} не сдал документы по рейсу №{reis.DeliveryNumber}";

            var text = subj + $", заказчик \"{reis.Client.Title}\", перевозчик \"{reis.Carrier.Title ?? ""}\"";
        
            await emailService.SendEmailAsync(new[] {email}, subj, text, null);
        }
        
        public async Task SendReisCompletedEmail(Guid reisId, string email)
        {
            var reis = await logistDbContext.Reises.IgnoreQueryFilters()
                .IncludeOptimized(r=>r.Driver)
                .IncludeOptimized(r=>r.Carrier)
                .IncludeOptimized(r=>r.Client)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            
            if (reis == null)
            {
                return;
            }
            
            var text = $"Сдача документов по рейсу №{reis.DeliveryNumber} , Водитель: {reis.Driver.FirstName} {reis.Driver.LastName}; Заказчик: {reis.Client.Title}; Перевозчик: {reis.Carrier.Title} подтверждена. Рейс выполнен";
            
            
            await emailService.SendEmailAsync(new[] {email}, text, text, null);
            
        }
        public async Task SendReisDocsProblemRevealedEmail(Guid reisId, string email)
        {
            var reis = await logistDbContext.Reises.IgnoreQueryFilters()
                .IncludeOptimized(r=>r.Driver)
                .IncludeOptimized(r=>r.Carrier)
                .IncludeOptimized(r=>r.Client)
                .FirstOrDefaultAsync(i => i.Id == reisId);
            
            if (reis == null)
            {
                return;
            }
            
            var text = $"По рейсу №{reis.DeliveryNumber} , Водитель: {reis.Driver.FirstName} {reis.Driver.LastName}; Заказчик: {reis.Client.Title}; Перевозчик: {reis.Carrier.Title} зафиксированы проблемы с ТСД";
            
            
            await emailService.SendEmailAsync(new[] {email}, text, text, null);
            
        }

        public async Task ConfirmCarrierOfferEmail(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;
            var offer = await carrierOfferService.GetCarrierOfferRequest(key, System.Threading.CancellationToken.None);

            var kontragent = await logistDbContext.Kontragents.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == offer.KontragentId);

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
            .IncludeOptimized(i => i.RoutePoints.Select(s => new
            {
                s.Region,
                s.Settlement
            }))
            .IncludeOptimized(i => i.Client)
            .FirstOrDefaultAsync(x => x.Id == offer.RequestId);
            
            var addressesTo = logistDbContext.Users.FirstOrDefault(x => x.Id == offer.CreatedById).Email;

            var subject = $"Ваше предложение по заявке №{ item.Number1C } подтверждено ответственным логистом , { item.Load.ArrivalDate }, { item.Load.Address }, { item.Load.Settlement.Title }";

            var htmlBody = await engine.CompileRenderAsync("/Views/Email/CarrierOffer/CarrierOfferConfirmView.cshtml",
                new CarrierOfferViewModel
                {
                    RequestId = item.Id,
                    RequestNumber1C = item.Number1C,
                    TransportationNumber = item.RequestCode,
                    Client = item.Client.Title,
                    Load = item.Load,
                    Unload = item.Unload,
                    Price = (decimal)offer.Price,
                    isCarrierNDS = kontragent.IsPayNDS ?? false,
                });


            await emailService.SendEmailAsync(new[] { addressesTo }, subject, null, htmlBody);
        }
        public async Task RejectCarrierOfferEmail(Guid key, CarrierOfferStatus reason )
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;
            var offer = await carrierOfferService.GetCarrierOfferRequest(key, System.Threading.CancellationToken.None);

            var kontragent = await logistDbContext.Kontragents.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == offer.KontragentId);

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
            .IncludeOptimized(i => i.RoutePoints.Select(s => new
            {
                s.Region,
                s.Settlement
            }))
            .IncludeOptimized(i => i.Client)
            .FirstOrDefaultAsync(x => x.Id == offer.RequestId);

            var addressesTo = logistDbContext.Users.FirstOrDefault(x => x.Id == offer.CreatedById).Email;

            var subject = $"Ваше предложение по заявке №{item.Number1C} отклонено , {item.Load.ArrivalDate}, {item.Load.Address}, {item.Load.Settlement.Title}";
            
            var htmlBody = await engine.CompileRenderAsync("/Views/Email/CarrierOffer/CarrierOfferRejectView.cshtml",
                new CarrierOfferViewModel
                {
                    RequestId = item.Id,
                    RequestNumber1C = item.Number1C,
                    TransportationNumber = item.RequestCode,
                    Client = item.Client.Title,
                    Load = item.Load,
                    Unload = item.Unload,
                    Price = (decimal)offer.Price,
                    Reason = ((DescriptionAttribute?)typeof(CarrierOfferStatus).GetField(reason.ToString())
                    ?.GetCustomAttributes(false).FirstOrDefault(a => a.GetType() == typeof(DescriptionAttribute)))
                ?.Description,
                    isCarrierNDS = kontragent.IsPayNDS ?? false,
                });

            await emailService.SendEmailAsync(new[] { addressesTo }, subject, null, htmlBody);
        }
    }
}