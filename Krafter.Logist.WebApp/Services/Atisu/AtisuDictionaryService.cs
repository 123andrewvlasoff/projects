using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Krafter.Client.Atisu.Client.Models;
using Krafter.Client.Atisu.Client.Services;
using Krafter.Client.Atisu.Services;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Services.Atisu;

public class AtisuDictionaryService : IAtisuDictionaryService
{
    private readonly LogistDbContext logistDbContext;
    public AtisuDictionaryService(LogistDbContext logistDbContext)
    {
        this.logistDbContext = logistDbContext;
    }

    public async Task<ICollection<AtisuCarType>> GetCarTypes(ICollection<long> carTypeIds)
    {
        return await logistDbContext.AtisuCarTypes.Where(x => carTypeIds.Contains(x.Id))
          .Select(x => new AtisuCarType()
          {
              Key = x.Id,
              Name = x.Name,
              ShortName = x.ShortName,
              TypeId = x.TypeId
          }).ToListAsync();
    }

    public async Task<ICollection<AtisuLoadingType>> GetLoadingTypes(ICollection<long> getLoadingTypeIds)
    {
        return await logistDbContext.AtisuLoadingTypes.Where(x => getLoadingTypeIds.Contains(x.Id))
          .Select(x => new AtisuLoadingType()
          {
              TypeId = x.Id,
              ShortName = x.ShortName,
              Name = x.Name
          }).ToListAsync();
    }

    public async Task<AtisuCityGeolocation> GetCity(int cityId)
    {
        var point = await logistDbContext.AtisuCities.FirstOrDefaultAsync(x => x.Id == cityId);

        if (point == null)
            return default;

        return new AtisuCityGeolocation()
        {
            CityId = point.Id,
            Latitude = point.Latitude,
            Longitude = point.Longitude,
            FullName = point.FullName,
            ShortName = point.ShortName
        };
    }

    public async Task<ICollection<AtisuCityGeolocation>> GetCities(ICollection<int> ids)
    {
        return await logistDbContext.AtisuCities.Where(x => ids.Contains(x.Id))
            .Select(x => new AtisuCityGeolocation()
            {
                CityId = x.Id,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                FullName = x.FullName,
                ShortName = x.ShortName
            }).ToListAsync();
    }

    public async Task<AtisuCityGeolocation> GetCity(double lat, double lon)
    {
        var point = await logistDbContext.AtisuCities.FirstOrDefaultAsync(x => x.Latitude == lat && x.Longitude == lon);

        if (point == null)
            return default;

        return new AtisuCityGeolocation()
        {
            CityId = point.Id,
            Latitude = point.Latitude,
            Longitude = point.Longitude,
            FullName = point.FullName,
            ShortName = point.ShortName
        };
    }

    public async Task<ICollection<AtisuCityGeolocation>> GetNearCities(string name)
    {
        return await logistDbContext.AtisuCities.Where(x => x.CityName.Contains(name))
            .Select(x => new AtisuCityGeolocation()
            {
                CityId = x.Id,
                CityName = x.CityName,
                FullName = x.FullName,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                ShortName = x.ShortName
            }).ToListAsync();
    }

    public async Task<ICollection<AtisuCarType>> GetCarTypes()
    {
        return await logistDbContext.AtisuCarTypes
         .Select(x => new AtisuCarType()
         {
             Key = x.Id,
             Name = x.Name,
             ShortName = x.ShortName,
             TypeId = x.TypeId
         }).ToListAsync();
    }

    public async Task<ICollection<AtisuLoadingType>> GetLoadingTypes()
    {
        return await logistDbContext.AtisuLoadingTypes
          .Select(x => new AtisuLoadingType()
          {
              TypeId = x.Id,
              ShortName = x.ShortName,
              Name = x.Name
          }).ToListAsync();
    }
}