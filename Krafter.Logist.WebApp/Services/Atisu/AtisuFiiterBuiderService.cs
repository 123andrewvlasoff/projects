﻿using Krafter.ServiceCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krafter.Logist.WebApp.Services.Atisu
{
    public static class AtisuFiiterBuiderService
    {
        public static T GetValue<T>(this List<FilterValue> source, string fieldName)
        {
            var item = source.FirstOrDefault(x => x.FieldName == fieldName);
            return item != null ? (T)item.Value : default(T);
        }

        public static double? GetWeightValue(this List<FilterValue> source, string fieldName)
        {
            var item = source.FirstOrDefault(x => x.FieldName == fieldName);

            if (item == null)
            {
                return default;
            }

            var value = Math.Round((double)item.Value, 1);

            //Если тоннаж задания в интервале (20; 22], то подбираем ТС с тоннажом >= 20 
            if (value > 20 && value <= 22)
                value = 20;

            return value;
        }

        public static double? GetMeasureValue(this List<FilterValue> source, string fieldName)
        {
            var item = source.FirstOrDefault(x => x.FieldName == fieldName);
            return item != null ? Math.Round((double)item.Value, 1) : default(double?);
        }

        public static long? GetBitSumValue(this List<FilterValue> source, string fieldName)
        {
            var item = source.FirstOrDefault(x => x.FieldName == fieldName);
            return item != null ? (item.Value as IEnumerable<object>)?.Sum(x => (long)x) : default(long?);
        }
    }
}
