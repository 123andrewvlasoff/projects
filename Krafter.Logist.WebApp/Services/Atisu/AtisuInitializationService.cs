using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Atisu;
using Krafter.Client.Atisu.Client.Services;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Krafter.Logist.WebApp.Services.Atisu;

public class AtisuInitializationService
{
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IAtisuClientService atisuClientService;

    public AtisuInitializationService(IAtisuClientService atisuClientService, IServiceScopeFactory serviceScopeFactory)
    {
        this.atisuClientService = atisuClientService;
        this.serviceScopeFactory = serviceScopeFactory;
    }

    private readonly DateTime currentDate = DateTime.Now;

    public async Task Init()
    {
        using var scope = serviceScopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetService<LogistDbContext>();

        if (dbContext != null)
        {
            await InitEntity(dbContext, "CarType", async (isEmptyTable) =>
            {
                var carTypes = await atisuClientService.GetCarTypes();
                if (!isEmptyTable) dbContext.AtisuCarTypes.RemoveRange(dbContext.AtisuCarTypes);

                await dbContext.AtisuCarTypes.AddRangeAsync(carTypes.Select(x => new AtisuCarType()
                {
                    Id = x.Key,
                    Name = x.Name,
                    ShortName = x.ShortName,
                    TypeId = x.TypeId
                }));
            });

            await InitEntity(dbContext, "LoadingType", async (isEmptyTable) =>
            {
                var loadingTypes = await atisuClientService.GetLoadingTypes();
                if (!isEmptyTable) dbContext.AtisuLoadingTypes.RemoveRange(dbContext.AtisuLoadingTypes);

                await dbContext.AtisuLoadingTypes.AddRangeAsync(loadingTypes.Select(x => new AtisuLoadingType()
                {
                    Id = x.TypeId,
                    Name = x.Name,
                    ShortName = x.ShortName
                }));
            });

            await InitEntity(dbContext, "City", async (isEmptyTable) =>
            {
                var cities = await atisuClientService.GetCities();
                if (!isEmptyTable) dbContext.AtisuCities.RemoveRange(dbContext.AtisuCities);

                await dbContext.AtisuCities.AddRangeAsync(cities.Select(x => new AtisuCity()
                {
                    Id = x.CityId,
                    FullName = x.FullName,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    ShortName = x.ShortName,
                    CityName = x.CityName
                }));
            });
        }
    }

    private async Task InitEntity(LogistDbContext dbContext, string name, Func<bool, Task> action)
    {
        var getHistory =
            await dbContext.AtisuClientHistories
                .Where(x => x.ClientRequestName == name)
                .OrderByDescending(x => x.UpdateDate)
                .FirstOrDefaultAsync();
        
        if (getHistory == null || getHistory.UpdateDate.AddDays(7) < currentDate)
        {
            await action(getHistory == null);

            await dbContext.AtisuClientHistories.AddAsync(new AtisuClientHistory()
            {
                ClientRequestName = name,
                UpdateDate = currentDate
            });

            await dbContext.SaveChangesAsync();
        }
    }
}