using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Reis.Enums;
using DAL.Models.Transports.Enums;
using DelegateDecompiler.EntityFrameworkCore;
using GeoCoordinatePortable;
using Krafter.Logist.WebApp.Models.Api.Basic;
using Krafter.Logist.WebApp.Models.Api.SuitableTransport;
using Krafter.Logist.WebApp.Models.Api.Transport;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.Kontragent;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.KontragentContract;
using Krafter.Logist.WebApp.Models.Basic.RequestSuitableTransport;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;
using static DAL.Models.Transports.Constants.LoadingType;

namespace Krafter.Logist.WebApp.Services;

public interface ISuitableTransportService
{
    Task<ICollection<SuitableTransport>> GetAllAsync(SuitableTransportFilter filter, string userId);
    Task<ICollection<RequestSuitableTransport>> GetAllAsyncOdata(SuitableTransportFilter filter);
    Task<ICollection<PointInfo>> SearchByAddress(string address);
}

public class SuitableTransportService : ISuitableTransportService
{
    private readonly LogistDbContext logistDbContext;

    public SuitableTransportService(LogistDbContext logistDbContext)
    {
        this.logistDbContext = logistDbContext;
    }

    private readonly DateTime fromMinDate = DateTime.Now.AddMonths(-6);

    public async Task<ICollection<SuitableTransport>> GetAllAsync(SuitableTransportFilter filter, string userId)
    {
        if (filter.StartPeriod == null || filter.EndPeriod == null)
            return new List<SuitableTransport>(0);

        if (!filter.IsNds && !filter.IsNotNds)
            return new List<SuitableTransport>(0);

        if (!filter.IsConnectedTransport && !filter.IsNotConnectedTransport)
            return new List<SuitableTransport>(0);

        var baseQuery = logistDbContext.Reises
            .IgnoreQueryFilters()
            .AsNoTracking()
            .Where(t => t.StatusId != Cancelled)
            .Where(i => i.RoutePoints
                .Where(x => x.LeaveDatePlan >= fromMinDate)
                .Any(x => x.PointType == PointType.Unload && x.Lat != null && x.Lon != null)
            );

        if (filter.Owner != Owner.NotSet)
        {
            baseQuery = baseQuery.Where(x => x.Transport.Owner == filter.Owner);
        }

        baseQuery = filter.IsConnectedTransport switch
        {
            true when !filter.IsNotConnectedTransport => baseQuery.Where(x => x.ConnectedTransportId != null),
            false when filter.IsNotConnectedTransport => baseQuery.Where(x => x.ConnectedTransportId == null),
            _ => baseQuery
        };

        //if (filter.VatType.HasValue && filter.VatType != VatType.None)
        //{
        //    baseQuery = baseQuery.Where(x =>
        //        x.Carrier.KontragentContracts.Any(s => s.IsActive == 1 && s.VatType == filter.VatType.Value));
        //}

        baseQuery = filter.IsNds switch
        {
            true when !filter.IsNotNds => baseQuery.Where(x =>
                x.Carrier.KontragentContracts.Any(s => s.IsActive == 1 && (s.VatType != VatType.WithoutNDS))),
            false when filter.IsNotNds => baseQuery.Where(x =>
                x.Carrier.KontragentContracts.Any(s => s.IsActive == 1 && (s.VatType == VatType.WithoutNDS))),
            _ => baseQuery
        };

        var baseSelects = baseQuery.Select(x =>
            new
            {
                x.Id,
                x.TransportId,
                Lat = x.Transport.LastReisLat ?? x.Unload.Lat,
                Lon = x.Transport.LastReisLon ?? x.Unload.Lon,
                UnloadDate = x.Transport.LastReisDate ?? x.Unload.LeaveDatePlan,
                x.Transport.LastReisDate,
                x.Unload.LeaveDatePlan,
                x.ConnectedTransportId,
                Address = x.Transport.LastAddress ?? x.Unload.Address,
                LoadingTypeId = x.ConnectedTransportId == null
                    ? x.Transport.LoadingTypeId
                    : x.ConnectedTransport.LoadingTypeId,
                x.Carrier.VatType,
                Weight = x.ConnectedTransportId != null ? x.ConnectedTransport.Weight : x.Transport.Weight,
                Volume = x.ConnectedTransportId != null ? x.ConnectedTransport.Volume : x.Transport.Volume,
                RollingStockTypeId = x.ConnectedTransport != null
                    ? x.ConnectedTransport.RollingStockTypeId
                    : x.Transport.RollingStockTypeId
            });

        if (filter.RollingStockTypes.Any())
        {
            baseSelects = baseSelects.Where(x => filter.RollingStockTypes.Contains(x.RollingStockTypeId));
        }

        if (filter.MinWeight.HasValue && filter.MaxWeight.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Weight >= filter.MinWeight && x.Weight <= filter.MaxWeight);
        }
        else if (filter.MinWeight.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Weight >= filter.MinWeight);
        }
        else if (filter.MaxWeight.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Weight <= filter.MaxWeight);
        }

        if (filter.MinVolume.HasValue && filter.MaxVolume.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Volume >= filter.MinVolume && x.Volume <= filter.MaxVolume);
        }
        else if (filter.MinVolume.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Volume >= filter.MinVolume);
        }
        else if (filter.MaxVolume.HasValue)
        {
            baseSelects = baseSelects.Where(x => x.Volume <= filter.MaxVolume);
        }

        if (filter.StartPeriod.HasValue && filter.EndPeriod.HasValue)
        {
            baseSelects =
                baseSelects.Where(x => x.UnloadDate >= filter.StartPeriod && x.UnloadDate <= filter.EndPeriod);
        }

        var baseSelectsList = await baseSelects
            .DecompileAsync()
            .ToListAsync();

        var baseResultQuery = baseSelectsList.Select(x =>
            new SuitableTransportBaseSelect
            {
                ReisId = x.Id,
                Remoteness = filter.Lat.HasValue && filter.Lon.HasValue
                    ? new GeoCoordinate(filter.Lat.Value, filter.Lon.Value).GetDistanceTo(
                        new GeoCoordinate((double)x.Lat,
                            (double)x.Lon)) / 1000
                    : null,
                RequestDateInterval = filter.ArrivalDate.HasValue ? filter.ArrivalDate.Value - x.UnloadDate : null,
                TransportId = x.TransportId,
                UnloadDate = x.UnloadDate,
                Lat = x.Lat,
                Lon = x.Lon,
                LastReisDate = x.LastReisDate,
                LeaveDatePlan = x.LeaveDatePlan,
                ConnectedTransportId = x.ConnectedTransportId,
                UnloadAddress = x.Address,
                LoadingTypeId = x.LoadingTypeId
            });

        if (filter.LoadingTypeId.HasValue)
        {
            baseResultQuery = baseResultQuery
                .Where(x => filter.LoadingTypeId == NotDefined || filter.LoadingTypeId == x.LoadingTypeId);
        }

        if (filter.StartInterval.HasValue && filter.EndInterval.HasValue)
        {
            baseResultQuery = baseResultQuery.Where(i =>
                i.RequestDateInterval >= TimeSpan.FromHours(filter.StartInterval.Value) &&
                i.RequestDateInterval <= TimeSpan.FromHours(filter.EndInterval.Value));
        }

        if (filter.Remoteness.HasValue)
        {
            baseResultQuery = baseResultQuery.Where(x => x.Remoteness <= filter.Remoteness.Value);
        }

        var baseResult = baseResultQuery.ToList();

        var transportIds = baseResult.Select(x => x.TransportId).Distinct();

        var transports = await logistDbContext.Transports
            .IgnoreQueryFilters()
            .AsNoTracking()
            .Where(x => transportIds.Contains(x.Id))
            .Where(x => x.Reises.Any(r => r.RoutePoints != null && r.StatusId != Cancelled && r.IsActive
                                          && r.RoutePoints.Any(s =>
                                              s.LeaveDatePlan >= fromMinDate &&
                                              s.PointType == PointType.Unload)
            ))
            .Select(x => new
            {
                x.Id,
                x.LastReisDate,
                LeaveDatePlan = x.Reises.Select(s =>
                        s.RoutePoints.Where(p => p.PointType == PointType.Unload)
                            .OrderByDescending(p => p.LeaveDatePlan)
                            .Select(p => p.LeaveDatePlan).FirstOrDefault())
                    .FirstOrDefault()
            })
            .ToListAsync();


        foreach (var transport in transports)
        {
            var item = baseResult.Where(x => x.TransportId == transport.Id).MinBy(x => x.UnloadDate);
            if (item == null) continue;
            if ((transport.LastReisDate ?? transport.LeaveDatePlan) > item.UnloadDate)
                baseResult.Remove(item);
        }

        var items = baseResult.ToDictionary(i => i.ReisId, i => i);

        if (!baseResult.Any())
            return new List<SuitableTransport>(0);

        var resultIds = baseResult
            .GroupBy(x => new { x.TransportId, x.ConnectedTransportId })
            .Select(x => new
            {
                Reis = x.MaxBy(i => i.UnloadDate)
            })
            .Where(x => x.Reis != null)
            .Select(x => x.Reis.ReisId)
            .Distinct();

        var results = await logistDbContext.Reises
            .IgnoreQueryFilters()
            .AsNoTracking()
            .Where(i => resultIds.Contains(i.Id))
            .Select(x => new SuitableTransport
            {
                Id = x.Id,
                DeliveryNumber = x.DeliveryNumber,
                TransportId = x.TransportId,
                Transport = x.Transport != null
                    ? new TransportApi()
                    {
                        Id = x.Transport.Id,
                        Model = x.Transport.Model,
                        Volume = x.Transport.Volume,
                        RegNumber = x.Transport.RegNumber,
                        IsConnectedTransport = x.Transport.IsConnectedTransport,
                        TransportTypeId = x.Transport.TransportTypeId,
                        Weight = x.Transport.Weight,
                        RollingStockTypeId = x.Transport.RollingStockTypeId
                    }
                    : null,
                ConnectedTransportId = x.ConnectedTransportId,
                ConnectedTransport = x.ConnectedTransportId != null
                    ? new TransportApi()
                    {
                        Id = x.ConnectedTransport.Id,
                        Model = x.ConnectedTransport.Model,
                        RegNumber = x.ConnectedTransport.RegNumber,
                        IsConnectedTransport = x.ConnectedTransport.IsConnectedTransport,
                        TransportTypeId = x.ConnectedTransport.TransportTypeId,
                        Volume = x.ConnectedTransport.Volume,
                        Weight = x.ConnectedTransport.Weight,
                        RollingStockTypeId = x.ConnectedTransport.RollingStockTypeId
                    }
                    : null,
                DriverId = x.DriverId,
                Driver = new DriverApi()
                {
                    Id = x.Driver.Id,
                    FirstName = x.Driver.FirstName,
                    LastName = x.Driver.LastName,
                    Patronymic = x.Driver.Patronymic,
                    PhoneNumber = x.Driver.PhoneNumber,
                    WorkPhone = x.Driver.WorkPhone
                },
                TransportStatusId = x.TransportStatusId,
                TransportStatus = x.TransportStatusId != null
                    ? new TransportStatusApi()
                    {
                        Id = x.TransportStatus.Id,
                        Title = x.TransportStatus.Title
                    }
                    : null,
                CarrierId = x.CarrierId,
                Carrier = new CarrierApi()
                {
                    Id = x.Carrier.Id,
                    Title = x.Carrier.Title,
                    Contacts = x.Carrier.Contacts.Where(i => i.IsActive && !i.IsDeleted)
                        .Select(s => new KontragentContactApi()
                        {
                            Id = s.Id,
                            Patronymic = s.Patronymic,
                            LastName = s.LastName,
                            FirstName = s.FirstName,
                            BirthDate = s.BirthDate,
                            Email = s.Email,
                            Phone = s.Phone,
                            // Position = s.Position != null ? s.Position.Title: null,
                        })
                        .Take(3),
                    VatTypes = x.Carrier.KontragentContracts
                        .Where(s => s.IsActive == 1)
                        .Select(s => s.VatType)
                },
                PinnedEmployer = x.Carrier.KontragentUsers
                    .Where(i => i.RelationType.HasFlag(RelationType.Pinned) && i.User.Employer != null)
                    .Select(i => i.User.Employer)
                    .Select(e => new EmployerApi
                    {
                        Id = e.Id,
                        Branch = e.Branch != null ? new BranchApi() { Id = e.Branch.Id, Title = e.Branch.Title } : null,
                        FIO = e.FIO,
                        Email = e.Email,
                    })
                    .FirstOrDefault(),
                Owner = x.ConnectedTransportId == null ? x.Transport.Owner : x.ConnectedTransport.Owner,
                StatusId = x.StatusId,
                Status = x.StatusId != null
                    ? new ReisStatusApi()
                    {
                        Id = x.Status.Id,
                        Title = x.Status.Title
                    }
                    : null,
                IsCanReadReis = x.AcceptorId == userId
            })
            .DecompileAsync()
            .ToListAsync();

        foreach (var item in results)
        {
            SuitableTransportBaseSelect reisInfo;
            if (!items.TryGetValue(item.Id, out reisInfo)) continue;

            item.Address = reisInfo.UnloadAddress;
            item.UnloadDate = reisInfo.UnloadDate;
            item.RequestDateInterval = reisInfo.RequestDateInterval;
            item.Remoteness = reisInfo.Remoteness;
            item.Lat = reisInfo.Lat;
            item.Lon = reisInfo.Lon;
        }

        return results;
    }

    public async Task<ICollection<RequestSuitableTransport>> GetAllAsyncOdata(SuitableTransportFilter filter)
    {
        var baseQuery = logistDbContext.Reises
            .IgnoreQueryFilters()
            .AsNoTracking()
            .Where(t => t.StatusId != Cancelled)
            .Where(i => i.RoutePoints
                .Where(x => x.LeaveDatePlan >= fromMinDate)
                .Any(x => x.PointType == PointType.Unload && x.Lat != null && x.Lon != null)
            );

        if (filter.Weight.HasValue)
        {
            baseQuery = baseQuery.Where(x => filter.Weight <= (x.ConnectedTransportId == null
                ? x.Transport.Weight
                : x.ConnectedTransport.Weight));
        }

        var baseSelects = await baseQuery.Select(x =>
                new
                {
                    x.Id,
                    x.TransportId,
                    Lat = x.Transport.LastReisLat ?? x.Unload.Lat,
                    Lon = x.Transport.LastReisLon ?? x.Unload.Lon,
                    UnloadDate = x.Transport.LastReisDate ?? x.Unload.LeaveDatePlan,
                    LoadingTypeId = x.ConnectedTransportId == null
                        ? x.Transport.LoadingTypeId
                        : x.ConnectedTransport.LoadingTypeId
                })
            .DecompileAsync()
            .ToListAsync();

        var baseResultQuery = baseSelects.Select(x =>
            new SuitableTransportBaseSelect
            {
                ReisId = x.Id,
                Remoteness = filter.Lat.HasValue && filter.Lon.HasValue
                    ? new GeoCoordinate(filter.Lat.Value, filter.Lon.Value).GetDistanceTo(
                        new GeoCoordinate((double)x.Lat,
                            (double)x.Lon)) / 1000
                    : null,
                RequestDateInterval = filter.ArrivalDate.HasValue ? filter.ArrivalDate.Value - x.UnloadDate : null,
                TransportId = x.TransportId,
                UnloadDate = x.UnloadDate,
                LoadingTypeId = x.LoadingTypeId
            });

        if (filter.LoadingTypeId.HasValue)
        {
            baseResultQuery = baseResultQuery
                .Where(x => filter.LoadingTypeId == NotDefined || filter.LoadingTypeId == x.LoadingTypeId);
        }

        if (filter.StartInterval.HasValue && filter.EndInterval.HasValue)
        {
            baseResultQuery = baseResultQuery.Where(i =>
                i.RequestDateInterval >= TimeSpan.FromHours(filter.StartInterval.Value) &&
                i.RequestDateInterval <= TimeSpan.FromHours(filter.EndInterval.Value));
        }

        if (filter.Remoteness.HasValue)
        {
            baseResultQuery = baseResultQuery.Where(x => x.Remoteness <= filter.Remoteness.Value);
        }

        var baseResult = baseResultQuery.ToList();

        if (!baseResult.Any())
            return new List<RequestSuitableTransport>(0);

        var transportIds = baseResult.Select(x => x.TransportId).Distinct();

        var transports = await logistDbContext.Transports
            .IgnoreQueryFilters()
            .IncludeOptimized(x => x.Reises.Select(s => s.RoutePoints))
            .Where(x => transportIds.Contains(x.Id))
            .ToListAsync();

        foreach (var transport in transports.Where(x => x.LastReis?.Unload != null))
        {
            var item = baseResult.Where(x => x.TransportId == transport.Id)
                .MinBy(x => x.UnloadDate);

            if (item == null) continue;
            if ((transport.LastReisDate ?? transport.LastReis.Unload.LeaveDatePlan) > item.UnloadDate)
                baseResult.Remove(item);
        }

        if (filter.Remoteness.HasValue && filter.ArrivalDate.HasValue)
        {
            var additionalTransports = await logistDbContext.Transports.IgnoreQueryFilters().AsNoTracking()
                .Where(x => x.LastAddress != null && x.LastReisLat != null && x.LastReisLon != null)
                .Where(x => !x.Reises.Any() || x.Reises.All(y => y.StatusId == Completed))
                .Select(i =>
                    new SuitableTransportBaseSelect
                    {
                        ReisId = i.Id,
                        Remoteness = new GeoCoordinate(filter.Lat.Value, filter.Lon.Value).GetDistanceTo(
                            new GeoCoordinate(i.LastReisLat.Value, i.LastReisLon.Value)) / 1000,
                        RequestDateInterval = filter.ArrivalDate.Value - (i.LastReisDate ?? filter.ArrivalDate.Value),
                        TransportId = i.Id,
                        UnloadDate = i.LastReisDate ?? DateTime.Now
                    })
                .DecompileAsync()
                .ToArrayAsync();

            additionalTransports = additionalTransports.Where(x => x.Remoteness <= filter.Remoteness).ToArray();

            foreach (var transport in additionalTransports)
            {
                var item = baseResult.FirstOrDefault(x => x.TransportId == transport.TransportId);
                if (item == null) baseResult.Add(transport);
            }
        }

        var items = baseResult.ToDictionary(i => i.ReisId, i => i);

        var results = await logistDbContext.Reises.IgnoreQueryFilters().AsNoTracking()
            .Where(i => items.Keys.Contains(i.Id))
            .Select(r => new RequestSuitableTransport
            {
                Id = r.Id,
                TransportStatusId = r.TransportStatusId,
                TransportStatus = r.TransportStatus,
                StatusId = r.StatusId,
                Status = r.Status,
                DriverId = r.DriverId,
                Driver = r.Driver,
                TransportId = r.TransportId,
                Transport = r.Transport,
                ConnectedTransport = r.ConnectedTransport,
                ConnectedTransportId = r.ConnectedTransportId,
                CarrierId = r.CarrierId,
                Carrier = new Kontragent
                {
                    Id = r.CarrierId,
                    Title = r.Carrier.Title,
                    Contacts = r.Carrier.Contacts.Where(x => x.IsActive).ToList()
                },
                RollingStockTypeId = r.ConnectedTransportId == null
                    ? r.Transport.RollingStockTypeId
                    : r.ConnectedTransport.RollingStockTypeId,
                UnloadPointLat = r.Unload.Lat,
                UnloadPointLon = r.Unload.Lon,
                UnloadPointAddress = r.Unload.Address,
                UnloadPointDate = r.Unload.LeaveDatePlan,
                PinnedEmployer = r.Carrier.KontragentUsers
                    .Where(i => i.RelationType.HasFlag(RelationType.Pinned) && i.User.Employer != null)
                    .Select(i => i.User.Employer)
                    .Select(e => new Employer
                    {
                        Id = e.Id,
                        UserId = e.UserId,
                        BranchId = e.BranchId,
                        Branch = e.Branch,
                        FIO = e.FIO,
                        PhoneNumber = e.PhoneNumber,
                        Email = e.Email,
                        ParentEmployerId = e.ParentEmployerId,
                        ParentEmployer = e.ParentEmployer,
                    })
                    .FirstOrDefault(),
                Owner = r.ConnectedTransportId == null ? r.Transport.Owner : r.ConnectedTransport.Owner,
                BundleCommentCount = r.Transport.BundleComments != null && r.Transport.BundleComments.Any()
                    ? r.Transport.BundleComments.Count()
                    : null,
                KontragentContactId = r.KontragentContactId
            })
            .DecompileAsync()
            .ToListAsync();
        
        results = results.Where(x => !IsRangeRentDate(filter.ArrivalDate, filter.LeaveDate, x.Transport) &&
                                     !IsRangeRentDate(filter.ArrivalDate, filter.LeaveDate, x.ConnectedTransport))
            .ToList();

        foreach (var reis in results)
        {
            KontragentContact kontragentContact = default;
            if (reis.Transport.LastAddress == null)
            {
                if (reis.KontragentContactId.HasValue)
                {
                    kontragentContact =
                        await logistDbContext.KontragentContacts.Where(x => x.Id == reis.KontragentContactId.Value)
                            .Select(x => new KontragentContact()
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                Patronymic = x.Patronymic,
                                Phone = x.Phone,
                                Email = x.Email
                            }).FirstOrDefaultAsync();
                }
            }
            else
            {
                kontragentContact =
                    await logistDbContext.KontragentContacts.Where(x => x.KontragentId == reis.CarrierId)
                        .Where(x => x.IsActive)
                        .OrderByDescending(x => x.UpdatedAt)
                        .Select(x => new KontragentContact()
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            Patronymic = x.Patronymic,
                            Phone = x.Phone,
                            Email = x.Email
                        }).FirstOrDefaultAsync();
            }
            
            reis.KontragentContact = kontragentContact;
        }
        
        foreach (var item in results)
        {
            SuitableTransportBaseSelect reisInfo;
            if (!items.TryGetValue(item.Id, out reisInfo)) continue;

            item.UnloadAddress = reisInfo.UnloadAddress;
            item.UnloadDate = reisInfo.UnloadDate;
            item.RequestDateInterval = reisInfo.RequestDateInterval ?? default;
            item.Remoteness = reisInfo.Remoteness ?? default;
        }

        results = results.OrderByDescending(i => i.UnloadDate)
            .GroupBy(i => new
            {
                i.TransportId,
                i.ConnectedTransportId
            })
            .Select(g => g.FirstOrDefault())
            .ToList();

        return results;
    }

    public async Task<ICollection<PointInfo>> SearchByAddress(string address)
    {
        var result = await logistDbContext.Reises
            .IgnoreQueryFilters()
            .AsNoTracking()
            .Where(t => t.StatusId != Cancelled)
            .Where(i => i.RoutePoints
                .Where(x => x.LeaveDatePlan >= fromMinDate)
                .Any(x => x.PointType == PointType.Unload && x.Lat != null && x.Lon != null)
            )
            .Where(x => (x.Transport.LastAddress ?? x.Unload.Address).ToLower().Contains(address.ToLower()))
            .Select(x => new PointInfo()
            {
                Id = x.Id,
                Address = x.Transport.LastAddress ?? x.Unload.Address,
                Lat = x.Transport.LastReisLat ?? x.Unload.Lat,
                Lon = x.Transport.LastReisLon ?? x.Unload.Lon,
            })
            .Distinct()
            .Take(5)
            .DecompileAsync()
            .ToListAsync();

        return result;
    }

    private bool IsRangeRentDate(DateTime? arrivalDate, DateTime? leaveDate, Transport transport)
    {
        if (transport.RentStart.HasValue && transport.RentStop.HasValue)
        {
            if ((arrivalDate >= transport.RentStart && arrivalDate < transport.RentStop) ||
                (leaveDate >= transport.RentStart && leaveDate <= transport.RentStop))
                return true;
        }

        return false;
    }


    private class SuitableTransportBaseSelect
    {
        public Guid ReisId { get; set; }
        public double? Remoteness { get; set; }
        public TimeSpan? RequestDateInterval { get; set; }
        public Guid TransportId { get; set; }
        public Guid? ConnectedTransportId { get; set; }
        public DateTime UnloadDate { get; set; }
        public string UnloadAddress { get; set; }
        public DateTime? LastReisDate { get; set; }
        public DateTime LeaveDatePlan { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int? LoadingTypeId { get; set; }
    }
}