﻿using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Krafter.Logist.WebApp.Services
{
    public class KontragentContractService
    {
        private readonly LogistDbContext logistDbContext;

        public KontragentContractService(LogistDbContext logistDbContext)
        {
            this.logistDbContext = logistDbContext;
        }

        public async Task<bool> TypeIsProviderForAgent1CAsync(Guid carrierContractGuid)
        {
            var kontragentContract = await logistDbContext.KontragentContracts.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Guid1C == carrierContractGuid);

            return kontragentContract is {Type: DAL.Models.Requests.Reis.Enums.KontragentContractType.ProviderForAgent};
        }

        public async Task<bool> TypeIsProviderForAgentAsync(Guid? kontragentContractId)
        {
            if (!kontragentContractId.HasValue)
            {
                return false;
            }

            var kontragentContract = await logistDbContext.KontragentContracts.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == kontragentContractId);

            return kontragentContract is {Type: DAL.Models.Requests.Reis.Enums.KontragentContractType.ProviderForAgent};
        }
    }
}