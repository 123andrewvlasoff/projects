﻿using System;
using System.Linq;

namespace Krafter.Logist.WebApp.Helpers
{
    public static class TakeInProgressUntilHelper
    {
        public static DateTime GetTakeUntilDate(DateTime arrivalDate, TimeSpan offset = default)
        {
            var weekends = new[] {DayOfWeek.Saturday, DayOfWeek.Sunday};

            DateTime takeUntilDate = arrivalDate.Add(offset);

            if (!weekends.Contains(arrivalDate.DayOfWeek) && arrivalDate.Hour >= 18)
            {
                takeUntilDate = takeUntilDate
                    .AddHours(-Math.Abs(arrivalDate.Hour - 10))
                    .AddMinutes(-Math.Abs(arrivalDate.Minute))
                    .AddSeconds(-Math.Abs(arrivalDate.Second))
                    .AddMilliseconds(-Math.Abs(arrivalDate.Millisecond));
            }

            if (!weekends.Contains(arrivalDate.DayOfWeek) && arrivalDate.Hour >= 9 && arrivalDate.Hour < 18)
            {
                takeUntilDate = takeUntilDate.AddHours(-24);

                while (weekends.Contains(takeUntilDate.DayOfWeek))
                {
                    takeUntilDate = takeUntilDate.AddHours(-24);
                }

                // Нивелирую лишний час в рамках правильного дня,
                // что бы не попадать на большую погрешность во время поиска правильного
                takeUntilDate = takeUntilDate.AddHours(-1);
            }

            if (arrivalDate.Hour < 9)
            {
                takeUntilDate = takeUntilDate.AddHours(-24);

                while (weekends.Contains(takeUntilDate.DayOfWeek))
                {
                    takeUntilDate = takeUntilDate.AddHours(-24);
                }

                takeUntilDate = takeUntilDate
                    .AddHours(Math.Abs(arrivalDate.Hour - 10))
                    .AddMinutes(Math.Abs(arrivalDate.Minute))
                    .AddSeconds(-Math.Abs(arrivalDate.Second))
                    .AddMilliseconds(-Math.Abs(arrivalDate.Millisecond));

                return takeUntilDate;
            }

            if (weekends.Contains(arrivalDate.DayOfWeek))
            {
                var normalizedDayOfWeek = arrivalDate.DayOfWeek == DayOfWeek.Sunday ? 7 : (int) arrivalDate.DayOfWeek;

                takeUntilDate = takeUntilDate
                    .AddDays(-Math.Abs(normalizedDayOfWeek - (int) DayOfWeek.Friday))
                    .AddHours(-Math.Abs(arrivalDate.Hour - 10))
                    .AddMinutes(-Math.Abs(arrivalDate.Minute))
                    .AddSeconds(-Math.Abs(arrivalDate.Second))
                    .AddMilliseconds(-Math.Abs(arrivalDate.Millisecond));
            }

            return takeUntilDate;
        }
    }
}