using System.IO;
using DAL.Models.Enums;
using Krafter.ServiceCommon.Extensions;

namespace Krafter.Logist.WebApp.Helpers
{
    public static class NormalizeFileNameHelper
    {
        public static string GetName(this string name, DocumentType type, string code)
        {
            return type.GetDisplayName() + "_" + code + Path.GetExtension(name);
        }
    }
}