﻿using Microsoft.Extensions.Configuration;

namespace Krafter.Logist.WebApp.Helpers
{
    public static class TimeIntervalHelper
    {
        public static int GetMinutesInterval(this IConfiguration configuration)
        {
            var value = configuration.GetSection("requesttimeinterval")?.Value;
            return string.IsNullOrEmpty(value) ? 30 : int.Parse(value);
        }
    }
}