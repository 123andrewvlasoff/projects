﻿using System;
using System.IO;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TrafficRequestFileController : BaseController<TrafficRequestFileController>
    {
        private readonly IFileService fileService;

        public TrafficRequestFileController(ILogger<TrafficRequestFileController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            IFileService fileService) : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Get File.
        /// </summary>
        /// <returns>The requested File.</returns>
        /// <response code="200">The File was successfully retrieved.</response>
        /// <response code="404">The Document does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromODataUri] Guid key, DocumentType documentType)
        {
            var item = await logistDbContext.TrafficRequestFiles
                .FirstOrDefaultAsync(i => i.Id == key && i.DocumentType == documentType);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }
    }
}