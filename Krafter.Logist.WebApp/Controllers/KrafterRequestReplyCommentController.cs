﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Comment.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Guaranties.Enums;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyComment;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class KrafterRequestReplyCommentController : BaseController<KrafterRequestReplyCommentController>
    {
        public KrafterRequestReplyCommentController(ILogger<KrafterRequestReplyCommentController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) :
            base(logger, logistDbContext, userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Places a new KrafterRequestReplyComment.
        /// </summary>
        /// <param name="item">The KrafterRequestReplyComment to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created KrafterRequestReplyComment.</returns>
        /// <response code="201">The KrafterRequestReplyComment was successfully placed.</response>
        /// <response code="400">The KrafterRequestReplyComment is invalid.</response>
        [ProducesResponseType(typeof(KrafterRequestReplyComment), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> Post([FromBody] KrafterRequestReplyComment item,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requestReply =
                await logistDbContext.KrafterRequestReplies
                    .IncludeOptimized(i => i.Request)
                    .IncludeOptimized(i => i.Request.InProgressUser)
                    .FirstOrDefaultAsync(i => i.Id == item.RequestReplyId, cancellationToken);

            if (requestReply == null)
            {
                return NotFound();
            }

            if (requestReply.Request.RequestStatus != RequestStatus.IsActive &&
                requestReply.Request.RequestStatus != RequestStatus.DataTreatment &&
                requestReply.Request.RequestStatus != RequestStatus.DataInput &&
                requestReply.Request.RequestStatus!=RequestStatus.Cancelled&&
                requestReply.Request.InProgressUserId != userId)
            {
                return BadRequest(ModelState);
            }

            item.CreatedAt = DateTime.Now;

            item.CreatorId = userId;
            item.CommentType = CommentType.Carrier;

            //Это необходимо, т. к. иначе он попытается добавить в бд вложенный экшн, а он уже существует
            await logistDbContext.RequestReplyComments.AddAsync(new KrafterRequestReplyComment
            {
                ActionId = item.ActionId,
                CommentType = item.CommentType,
                CreatedAt = item.CreatedAt,
                Text = item.Text,
                CreatorId = item.CreatorId,
                RequestReplyId = item.RequestReplyId,
                DeniedReasonId = item.DeniedReasonId
            }, cancellationToken);
            if(item.DeniedReasonId!=null)
            {
                requestReply.Request.DeniedReasonId = item.DeniedReasonId;
            }
            
            await logistDbContext.SaveChangesAsync(cancellationToken);

            if (item?.Action?.State == RequestActionStatus.LogistAcceptRefused)
            {
                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestReplyEmail(requestReply.Id, SubjectType.LogistCancelRequest,
                        requestReply.Request.InProgressUser.Email));
            }

            
            return Created(item);
        }
    }
}