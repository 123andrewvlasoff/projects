﻿using System.Collections.Generic;
using System.Linq;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(ReisRoutePoint))]
    public class ReisRoutePointController : BaseController<ReisRoutePointController>
    {
        public ReisRoutePointController(ILogger<ReisRoutePointController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }


        /// <summary>
        ///     Get ReisRoutePoints.
        /// </summary>
        /// <returns>The requested ReisRoutePoints.</returns>
        /// <response code="200">The ReisRoutePoints was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ReisRoutePoint>), StatusCodes.Status200OK)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<ReisRoutePoint> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return logistDbContext.ReisRoutePoints.IncludeOptimized(i => i.Reis)
                .Where(i => i.Reis != null)
                .Decompile();
        }
    }
}