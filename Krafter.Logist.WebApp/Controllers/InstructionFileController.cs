﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.InstructionFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(InstructionFile))]
    public class InstructionFileController : BaseController<InstructionFileController>
    {
        private readonly IFileService fileService;

        public InstructionFileController(ILogger<InstructionFileController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            IFileService fileService) : base(logger,
            logistDbContext, userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Get Instruction File.
        /// </summary>
        /// <returns>The requested Instruction File.</returns>
        /// <response code="200">The Instruction File was successfully retrieved.</response>
        /// <response code="404">The Instruction Document does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var item = await logistDbContext.InstructionFiles.OrderByDescending(x => x.CreatedAt)
                .FirstOrDefaultAsync(i => i.DocumentType == DocumentType.LogistInstruction);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType);
        }
    }
}