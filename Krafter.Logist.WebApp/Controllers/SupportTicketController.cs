using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.SupportTickets.Enums;
using DelegateDecompiler;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Message;
using Krafter.Logist.WebApp.Models.Basic.SupportTicket;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class SupportTicketController : BaseController<SupportTicketController>
    {
        public SupportTicketController(ILogger<SupportTicketController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get SupportTickets.
        /// </summary>
        /// <returns>The requested SupportTicket.</returns>
        /// <response code="200">The SupportTicket was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<SupportTicket>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 3, HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<SupportTicket> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            logger.LogInformation("Get support tickets");

            return logistDbContext.SupportTickets.IncludeOptimized(i => i.Messages).Decompile();
        }

        /// <summary>
        ///     Get SupportTicket by Id.
        /// </summary>
        /// <returns>The requested SupportTicket.</returns>
        /// <response code="200">The SupportTicket was successfully retrieved.</response>
        /// <response code="404">The SupportTicket does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(SupportTicket), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(MaxExpansionDepth = 3, HandleNullPropagation = HandleNullPropagationOption.False)]
        public async Task<SupportTicket> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return await logistDbContext.SupportTickets
                .IncludeOptimized(r => r.Messages.Select(m => m.Files))
                .IncludeOptimized(r => r.MessageType)
                .IncludeOptimized(r => r.User)
                .IncludeOptimized(r => r.SupportUser)
                .FirstOrDefaultAsync(r => r.Id == key);
        }

        /// <summary>
        ///     Place new or update current SupportTicket.
        /// </summary>
        /// <returns>The current SupportTicket.</returns>
        /// <response code="201">The SupportTicket was successfully placed.</response>
        /// <response code="400">The SupportTicket is invalid.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(SupportTicket), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] SupportTicket item)
        {
            var user = await userManager.GetUserAsync(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.UserId = userManager.GetUserId(User);
            item.Number = logistDbContext.SupportTickets.IgnoreQueryFilters().Count() + 1;
            item.CreatedAt = DateTime.Now;
            item.Status = SupportTicketStatus.NewTicket;
            item.Messages = new List<Message>();

            await logistDbContext.SupportTickets.AddAsync(item);

            await logistDbContext.SaveChangesAsync().ContinueWith(t =>
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddSupportTicketReceivedNotificationAsync(item.Id, user.Id));
                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendNewSupportTicketEmail(item.Id));
            });

            logger.LogInformation($"Created new support ticket: id - {item.Id}");

            return Created(item);
        }

        /// <summary>
        ///     Deletes a SupportTicket
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The SupportTicket was successfully deleted.</response>
        /// <response code="404">The SupportTicket not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = logistDbContext.SupportTickets.Where(i => i.UserId == userId)
                .FirstOrDefault(ur => ur.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.SupportTickets.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }

        /// <summary>
        ///     Updates an existing SupportTicket.
        /// </summary>
        /// <param name="key">The requested SupportTicket identifier.</param>
        /// <param name="delta">The partial SupportTicket to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The SupportTicket was successfully updated.</response>
        /// <response code="400">The SupportTicket is invalid.</response>
        /// <response code="404">The SupportTicket does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(SupportTicket), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<SupportTicket> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.SupportTickets.Where(i => i.UserId == userId)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            delta.Patch(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }
    }
}