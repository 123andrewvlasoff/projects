using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class SettlementController : BaseController<SettlementController>
    {
        public SettlementController(ILogger<SettlementController> logger, LogistDbContext logistDbContext) : base(
            logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get Settlements.
        /// </summary>
        /// <returns>The requested Settlements.</returns>
        /// <response code="200">The Settlements was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Settlement>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<Settlement> Get()
        {
            return logistDbContext.Settlements.AsNoTracking();
        }

        /// <summary>
        ///     Get Settlement by Id.
        /// </summary>
        /// <returns>The requested Settlement.</returns>
        /// <response code="200">The Settlement was successfully retrieved.</response>
        /// <response code="404">The Settlement does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Settlement), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Select)]
        public async Task<ActionResult> Get(int key)
        {
            var item = await logistDbContext.Settlements.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}