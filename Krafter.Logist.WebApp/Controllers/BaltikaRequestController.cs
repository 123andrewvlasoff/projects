﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.Requests.Auction.Enums;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;
using EasyCaching.Core;
using GeoCoordinatePortable;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.Kontragent;
using Krafter.Logist.WebApp.Models.Basic.RequestSuitableTransport;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;
using Crafter.Client.CIS.Clients;
using Krafter.Logist.WebApp.Models.Basic.BaltikaRequest;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(BaltikaRequest))]
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class BaltikaRequestController : BaseController<BaltikaRequestController>
    {
        private readonly IEasyCachingProvider cache;
        private readonly IKrafterExternalClient krafterExternalClient;

        public BaltikaRequestController(ILogger<BaltikaRequestController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IEasyCachingProvider cache,
            IKrafterExternalClient krafterExternalClient) : base(
            logger, logistDbContext,
            userManager, httpContextAccessor)
        {
            this.cache = cache;
            this.krafterExternalClient = krafterExternalClient;
        }
        
        [Produces("application/json")]
        [ProducesResponseType(typeof(BaltikaRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Expand | AllowedQueryOptions.Select)]
        public async Task<IActionResult> Get(Guid key, CancellationToken cancellationToken)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.BaltikaRequests
                .IncludeOptimized(r => r.RoutePoints.Select(s => new
                {
                    s.Region,
                    s.Settlement
                }))
                .IncludeOptimized(r => r.Replies)
                .IncludeOptimized(r => r.Client.Contacts)
                .IncludeOptimized(r => r.InProgressUser.Employer)
                .IncludeOptimized(r => r.AssignedUser.Employer)
                .FirstOrDefaultAsync(r => r.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            logger.LogInformation("Get baltika request: id - {Key}", key);

            return StatusCode(StatusCodes.Status200OK, item);
        }
        
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestSuitableTransport>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<RequestSuitableTransport>> SuitableTransport(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var request = logistDbContext.BaltikaRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.RoutePoints)
                .First(i => i.Id == key);

            var requestLat = request.Load?.Lat;

            var requestLon = request.Load?.Lon;

            var requestLoadDate = request.Load?.ArrivalDate;

            if (requestLoadDate == null || requestLon == null || requestLat == null)
            {
                return new List<RequestSuitableTransport>().AsQueryable();
            }

            var items = logistDbContext.Reises.IgnoreQueryFilters().AsNoTracking()
                .Where(t => t.StatusId == InProgress || t.StatusId == WaitingDocuments ||
                            t.StatusId == OnApproval ||
                            t.StatusId == Agreed || t.StatusId == WaitingLoad)
                .Where(i => i.RoutePoints.Any(a => a.PointType == PointType.Load && a.Lat != null && a.Lon != null) &&
                            i.RoutePoints.Any(a => a.PointType == PointType.Unload && a.Lat != null && a.Lon != null))
                .Where(x => (request.Weight >= 20 && request.Weight <= 22 ? 20 : request.Weight) <=
                            (x.ConnectedTransportId == null
                                ? x.Transport.Weight
                                : x.ConnectedTransport.Weight))
                .Where(x => request.LoadingTypeId == (x.ConnectedTransportId == null
                    ? x.Transport.LoadingTypeId
                    : x.ConnectedTransport.LoadingTypeId))
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Carrier.Contacts)
                .IncludeOptimized(i => i.Carrier.KontragentUsers.Select(s => s.User.Employer.Branch))
                .IncludeOptimized(i => i.Carrier.KontragentUsers.Select(s => s.User.Employer.ParentEmployer))
                .IncludeOptimized(i => i.ConnectedTransport)
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Driver)
                .Select(r => new RequestSuitableTransport
                {
                    Id = r.Id,
                    TransportStatusId = r.TransportStatusId,
                    TransportStatus = r.TransportStatus,
                    StatusId = r.StatusId,
                    Status = r.Status,
                    TransportId = r.TransportId,
                    Transport = r.Transport,
                    ConnectedTransport = r.ConnectedTransport,
                    ConnectedTransportId = r.ConnectedTransportId,
                    Carrier = new Kontragent
                    {
                        Id = r.CarrierId,
                        Title = r.Carrier.Title,
                        Contacts = r.Carrier.Contacts.Where(x => x.IsActive).ToList()
                    },
                    CarrierId = r.CarrierId,
                    UnloadAddress = r.Transport.LastAddress ??
                                    r.Unload.Address,
                    UnloadPointAddress = r.Transport.LastAddress ??
                                    r.Unload.Address,
                    KontragentContact = r.KontragentContact != null
                    ? new KontragentContact()
                    {
                        FirstName = r.KontragentContact.FirstName,
                        LastName = r.KontragentContact.LastName,
                        Patronymic = r.KontragentContact.Patronymic,
                        Phone = r.KontragentContact.Phone,
                        Email = r.KontragentContact.Email
                    }
                    : null,
                    RollingStockTypeId = r.ConnectedTransportId == null
                        ? r.Transport.RollingStockTypeId
                        : r.ConnectedTransport.RollingStockTypeId,
                    RequestDateInterval = requestLoadDate.Value - (r.Transport.LastReisDate ?? r.Unload.LeaveDatePlan),
                    UnloadDate = r.Transport.LastReisDate ??
                                 r.Unload.LeaveDatePlan,
                    PinnedEmployer = r.Carrier.KontragentUsers
                        .Where(i => i.RelationType.HasFlag(RelationType.Pinned) && i.User.Employer != null)
                        .Select(i => i.User.Employer)
                        .Select(e => new Employer
                        {
                            Id = e.Id,
                            BranchId = e.BranchId,
                            Branch = e.Branch,
                            FIO = e.FIO,
                            PhoneNumber = e.PhoneNumber,
                            Email = e.Email,
                            ParentEmployerId = e.ParentEmployerId,
                            ParentEmployer = e.ParentEmployer,
                        })
                        .FirstOrDefault(),
                    Remoteness = new GeoCoordinate(requestLat.Value, requestLon.Value).GetDistanceTo(
                        new GeoCoordinate((double) (r.Transport.LastReisLat ?? r.Unload.Lat),
                            (double) (r.Transport.LastReisLon ?? r.Unload.Lon))) / 1000,
                    Owner = r.ConnectedTransportId == null ? r.Transport.Owner : r.ConnectedTransport.Owner,
                }).OrderByDescending(i => i.UnloadDate).Decompile().ToList()
                .GroupBy(i => new
                {
                    i.TransportId,
                    i.ConnectedTransportId
                })
                .Select(g => g.First());

            await cache.SetAsync("suitableBaltikaTransportCacheKey_" + userId, items, TimeSpan.FromMinutes(30));

            if (request.AuctionRequestStatus == AuctionRequestStatus.ResourceInput)
            {
                BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendBaltikaRequest(request.Id, userId));
            }

            return items;
        }
        
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Instruction")]
        public async Task<IActionResult> GetInstruction(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.BaltikaRequests.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Client)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logger.LogInformation("Get baltika request: id - {Key}", key);


            var instruction =
                await krafterExternalClient.InstructionKontragentAsync(item.Client.ExternalId.ToString(),
                    InstructionType.Logistician);

            return File(instruction.Document.Content, "application/pdf");
        }
    }
}