﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KontragentFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(KontragentFile))]
    public class KontragentFileController : BaseController<KontragentFileController>
    {
        private readonly IFileService fileService;

        public KontragentFileController(ILogger<KontragentFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Get KontragentFile.
        /// </summary>
        /// <returns>The requested KontragentFile.</returns>
        /// <response code="200">The KontragentFile was successfully retrieved.</response>
        /// <response code="404">The KontragentFile does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.KontragentFiles.FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }

        /// <summary>
        ///     Get KontragentFile By Kontragent InventoryId.
        /// </summary>
        /// <returns>The requested KontragentFile.</returns>
        /// <response code="200">The KontragentFile was successfully retrieved.</response>
        /// <response code="404">The KontragentFile does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        [ODataRoute("ByKontragentInventoryId")]
        public async Task<IActionResult> GetByKontragentInventoryId(Guid id, DocumentType type)
        {
            var item = await logistDbContext.KontragentFiles
                .FirstOrDefaultAsync(i => i.Kontragent.ExternalId == id && i.DocumentType == type);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }


        /// <summary>
        ///     SignedByCarrier KontragentFile.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Sign")]
        public async Task<IActionResult> PatchSign(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KontragentFiles
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.DocumentType != DocumentType.UserAgreement &&
                item.DocumentType != DocumentType.PublicOffer &&
                item.DocumentType != DocumentType.AgreementOnTheProcessingOfPersonalData &&
                item.DocumentType != DocumentType.PolicyTheProcessingOfPersonalData)
            {
                return BadRequest(ModelState);
            }

            item.Signed = true;
            item.SignedById = userId;
            item.SignedDate = DateTime.Now;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            logger.LogInformation($"set SignedByCarrier request : id - {item.Id}");

            return StatusCode(StatusCodes.Status201Created, Ok());
        }
    }
}