using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Dictionaries.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using Dapper;
using GeoCoordinatePortable;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser.Dto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using SqlKata.Execution;
using Z.EntityFramework.Plus;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;

namespace Krafter.Logist.WebApp.Controllers
{
    public partial class TransportKontragentUserController
    {
        private readonly int HoursFromUnload = 4;
        private readonly int Remoteness = 100;
        private async Task<IEnumerable<TransportKontragentUserDto>> GetListAsync()
        {
            var basic = db.Query("TransportForKontragents")
                .LeftJoin(
                    db.Query("TransportKontragentUsers")
                        .Select("TransportKontragentUsers.Id", "TransportKontragentUsers.TransportKontragentId",
                            "TransportKontragentUsers.RelationType", "TransportKontragentUsers.Comment",
                            "TransportKontragentUsers.UserId")
                        .As("TransportKontragentUser")
                        .Where("UserId", userManager.GetUserId(User)),
                    j => j.On("TransportKontragentUser.TransportKontragentId", "TransportForKontragents.Id"))
                .LeftJoin(
                    db.Query("KKontragentUsers")
                        .Select("KKontragentUsers.RelationType", "KKontragentUsers.KKontragentID")
                        .Where("UID", userManager.GetUserId(User))
                        .Where("RelationType", ">=", 2)
                        .As("KKontragentUsers"),
                    j => j.On("KKontragentUsers.KKontragentID", "TransportForKontragents.KKontragentID"))
                .WhereNotNull("TransportKontragentUser.UserId")
                .OrWhere("KKontragentUsers.RelationType", ">=", 2)
                .SelectRaw("case when [KKontragentUsers].[RelationType] >= 2 then (2 + COALESCE ([TransportKontragentUser].[RelationType], 0)) else [TransportKontragentUser].[RelationType] end RelationType")
                .SelectRaw("[TransportKontragentUser].[Id] as [TransportKontragentUserId]")
               .Select("TransportForKontragents.TransportId")
                .Join(
                    db.Query("kTransports")
                        .Select("Id",
                            "RegNumber",
                            "IsActive",
                            "TransportTypeId",
                            "Model",
                            "Weight",
                            "Volume",
                            "PalletsQuantity",
                            "LoadingTypeID",
                            "RollingStockTypeID",
                            "LastReisDate",
                            "LastReisLat",
                            "LastReisLon",
                            "LastAddress",
                            "ConnectedTransportId",
                            "RentStart",
                            "RentStop")
                        .As("Transport"),
                    j => j.On("Transport.Id", "TransportId"))
                ;

            var reis = db.Query("Reis")
                    .WhereTrue("IsActive")
                    .WhereFalse("IsDeleted")
                    .WhereNot("StatusId", Cancelled)
                    .Select("Reis.MainTransportId", "Reis.PricepId", "Reis.Id", "Reis.TransportStatusId",
                        "RoutePoints.LeaveDatePlan", "RoutePoints.Address", "RoutePoints.Lat", "RoutePoints.Lon")
                    .RightJoin(
                        db.Query("ReisRoutePoints")
                            .Where("ReisRoutePoints.PointTypeName", PointType.Unload)
                            .Where("ReisRoutePoints.LeaveDatePlan", ">", DateTime.UtcNow.AddHours(3).AddMonths(-6))
                            .Select(
                                "ReisRoutePoints.LeaveDatePlan",
                                "ReisRoutePoints.ReisId",
                                "ReisRoutePoints.Address",
                                "ReisRoutePoints.Lat",
                                "ReisRoutePoints.Lon"
                            )
                            .As("RoutePoints"),
                        j => j.On("Reis.Id", "RoutePoints.ReisId"))
                    .As("Reis");

            var transports = basic.Clone()
            .LeftJoin(reis, j => j.On("Reis.MainTransportId", "TransportId"))
            .Select("TransportForKontragents.KKontragentID as KontragentId")
            .SelectRaw("[TransportForKontragents].[Id] as [TransportKontragentId]")
            .Select(
                "Transport.LastReisDate",
                "Transport.LastReisLat",
                "Transport.LastReisLon",
                "Transport.LastAddress")
            .SelectRaw("null as [TransportStatusId]")
            .SelectRaw("[TransportKontragentUser].[Comment] as [Comment]")
            .Select(
                "Transport.RegNumber",
                "Transport.Model",
                "Transport.Weight",
                "Transport.Volume",
                "Transport.PalletsQuantity",
                "Transport.LoadingTypeID",
                "Transport.RollingStockTypeID",
                "Transport.TransportTypeId",
                "Transport.IsActive",
                "RentStart",
                "RentStop")
            .Select("Transport.ConnectedTransportId")
            .SelectRaw("[Reis].[LeaveDatePlan] as [ReisLeaveDatePlan]");

            var reises = basic.Clone()
            .RightJoin(reis, j => j.On("Reis.MainTransportId", "TransportId"))
            .Select("TransportForKontragents.KKontragentID as KontragentId")
            .SelectRaw("[TransportForKontragents].[Id] as [TransportKontragentId]")
            .SelectRaw("[Reis].[LeaveDatePlan] as [LastReisDate]")
            .SelectRaw("[Reis].[Lat] as [LastReisLat]")
            .SelectRaw("[Reis].[Lon] as [LastReisLon]")
            .SelectRaw("[Reis].[Address] as [LastAddress]")
            .Select("Reis.TransportStatusId")
            .SelectRaw("[TransportKontragentUser].[Comment] as [Comment]")
            .Select(
                "Transport.RegNumber",
                "Transport.Model",
                "Transport.Weight",
                "Transport.Volume",
                "Transport.PalletsQuantity",
                "Transport.LoadingTypeID",
                "Transport.RollingStockTypeID",
                "Transport.TransportTypeId",
                "Transport.IsActive",
                "RentStart",
                "RentStop")
            .SelectRaw("[Reis].[PricepId] as [ConnectedTransportId]")
            .SelectRaw("[Reis].[LeaveDatePlan] as [ReisLeaveDatePlan]");

            var items = db.Query().From(transports.UnionAll(reises), "Transport")
            .LeftJoin(
                db.Query("kTransports")
                    .Select("Id", "RegNumber", "RollingStockTypeID")
                    .As("LastConnectedTransport"),
                j => j.On("LastConnectedTransport.Id", "Transport.ConnectedTransportId"))
            .SelectRaw("distinct on ([TransportKontragentId]) *")
            .Select("TransportKontragentUserId")
            .Select(
                "TransportId",
                "Transport.RegNumber",
                "Transport.Model",
                "Transport.Weight",
                "Transport.Volume",
                "Transport.PalletsQuantity",
                "Transport.LoadingTypeID",
                "Transport.RollingStockTypeID",
                "Transport.TransportTypeId",
                "Transport.IsActive",
                "Transport.RentStart",
                "Transport.RentStop")
            .SelectRaw("[LastConnectedTransport].[Id] as [LastConnectedTransportId]")
            .Select("LastConnectedTransport.*")
            .SelectRaw("[ReisLeaveDatePlan]")
            .OrderByRaw("[TransportKontragentId], [LastReisDate] desc nulls last");

            return await db.Connection.QueryAsync<TransportKontragentUserDto, Transport, TransportKontragentUserDto>(
            db.Compiler.Compile(items).ToString(),
            (transport, connectedTransport) =>
            {
                transport.LastConnectedTransport = connectedTransport;
                return transport;
            }, splitOn: "Id");
        }

        private async Task<IEnumerable<TransportKontragentUserDto>> AddSuitableReisesCount(IEnumerable<TransportKontragentUserDto> transports, string userId)
        {
            if (!transports.Any())
            {
                return transports;
            }

            var requests = await repository.GetAsync();

            if (!requests.Any())
            {
                return transports;
            }

            requests = requests.Where(i => i.RequestStatus == RequestStatus.IsActive ||
                                             (i.RequestStatus == RequestStatus.DataTreatment ||
                                              i.RequestStatus == RequestStatus.DataInput ||
                                              i.RequestStatus == RequestStatus.Cancelled ||
                                              i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            {
                requests = requests.Where(x => x.Discriminator != RequestDiscriminator.Krafter);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            {
                requests = requests.Where(x =>
                    x.Discriminator != RequestDiscriminator.Cargomart &&
                    x.Discriminator != RequestDiscriminator.TrafficOnline &&
                    x.Discriminator != RequestDiscriminator.TorgTrans);
            }

            var transportFilter = transports.Where(x => x.TransportTypeId != (int)TransportType.Semitrailer);

            List<Task> tasks = new List<Task>(transportFilter.Count());

            foreach (var transport in transportFilter)
            {
                tasks.Add(Task.Run(() => AddSuitableReisesCount(requests, transport)));
            }

            await Task.WhenAll(tasks);

            return transports;
        }

        private Task AddSuitableReisesCount(IEnumerable<RequestDto> requests, TransportKontragentUserDto transport)
        {
            var currentQuery = requests;

            if (transport.TransportTypeId == (int)TransportType.Mule && transport.LastConnectedTransportId.HasValue)
            {
                currentQuery = currentQuery.Where(i => i.RollingStockTypeId == transport.LastConnectedTransport.RollingStockTypeId
                                           && (transport.LastConnectedTransport.Volume == 0 || i.Volume <= 1.1 * transport.LastConnectedTransport.Volume)
                                           && (transport.LastConnectedTransport.Weight == 0 || i.Weight <= 1.1 * transport.LastConnectedTransport.Weight));
            }
            else
            {
                currentQuery = currentQuery.Where(i => i.RollingStockTypeId == transport.RollingStockTypeId
                                             && (transport.Volume == 0 || i.Volume <= 1.1 * transport.Volume)
                                             && (transport.Weight == 0 || i.Weight <= 1.1 * transport.Weight));
            }

            if (transport.LastReisDate != null)
            {
                currentQuery = currentQuery.Where(r => r.ActiveUntil >= transport.LastReisDate.Value.AddHours(HoursFromUnload) &&
                                           r.ActiveUntil <= transport.LastReisDate.GetValueOrDefault().AddHours(HoursFromUnload + 36));
            }

            if (transport.LastReisLat != null && transport.LastReisLon != null)
            {
                currentQuery = currentQuery.Where(r => r.Load != null && (new GeoCoordinate(r.Load.Lat.GetValueOrDefault(), r.Load.Lon.GetValueOrDefault()).GetDistanceTo(
                    new GeoCoordinate((double)(transport.LastReisLat), (double)(transport.LastReisLon)))) / 1000 <= Remoteness);
            }

            transport.SuitableReisesCount = currentQuery.Count();

            return Task.CompletedTask;
        }
    }
}