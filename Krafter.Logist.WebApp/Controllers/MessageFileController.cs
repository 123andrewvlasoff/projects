﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.MessageFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(MessageFile))]
    public class MessageFileController : BaseController<MessageFileController>
    {
        private readonly IFileService fileService;

        public MessageFileController(ILogger<MessageFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Place new File.
        /// </summary>
        /// <response code="204">The File was successfully placed.</response>
        /// <response code="404">The Message does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ICollection<MessageFile>> Post([FromQuery] Guid messageId, [FromForm] IFormFile file)
        {
            var item = await logistDbContext.Messages.FindAsync(messageId);

            var fileName = Guid.NewGuid().ToString();

            await using var stream = file.OpenReadStream();
            await fileService.PutFileAsync(stream, Folders.Message, fileName);

            item.Files ??= new List<MessageFile>();

            item.Files.Add(new MessageFile
            {
                CreatedAt = DateTime.Now,
                OriginalName = file.FileName,
                UploaderId = userManager.GetUserId(User),
                ContentType = file.ContentType,
                DocumentType = DocumentType.Screenshot,
                Path = Folders.Message + "/" + fileName
            });

            await logistDbContext.SaveChangesAsync();

            return item.Files;
        }

        /// <summary>
        ///     Get File.
        /// </summary>
        /// <returns>The requested File.</returns>
        /// <response code="200">The File was successfully retrieved.</response>
        /// <response code="404">The Document does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.MessageFiles
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null || !logistDbContext.SupportTickets.Any(x =>
                    x.Messages.SelectMany(sm => sm.Files.Select(s => s.Id)).Contains(key)))
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }
    }
}