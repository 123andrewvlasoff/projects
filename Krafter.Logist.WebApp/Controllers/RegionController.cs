using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class RegionController : BaseController<RegionController>
    {
        public RegionController(ILogger<RegionController> logger, LogistDbContext logistDbContext) : base(
            logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get Regions.
        /// </summary>
        /// <returns>The requested Regions.</returns>
        /// <response code="200">The Regions was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Region>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<Region> Get()
        {
            return logistDbContext.Regions.AsNoTracking();
        }

        /// <summary>
        ///     Get Region by Id.
        /// </summary>
        /// <returns>The requested Region.</returns>
        /// <response code="200">The Region was successfully retrieved.</response>
        /// <response code="404">The Region does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Region), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Select)]
        public async Task<ActionResult> Get(int key)
        {
            var item = await logistDbContext.Regions.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}