using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Enums;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services;
using Krafter.ServiceCommon.Services.ChangesTrackService.Helpers;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.RequestsAccess)]
    public class KrafterRequestReplyFileController : BaseController<KrafterRequestReplyFileController>
    {
        private readonly IFileService fileService;

        public KrafterRequestReplyFileController(ILogger<KrafterRequestReplyFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Get KrafterRequestReplyFile.
        /// </summary>
        /// <returns>The requested KrafterRequestReplyFile.</returns>
        /// <response code="200">The KrafterRequestReplyFile was successfully retrieved.</response>
        /// <response code="404">The KrafterRequestReplyFile does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(ActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplyFiles
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType);
        }

        /// <summary>
        ///     Place new File.
        /// </summary>
        /// <response code="204">The File was successfully placed.</response>
        /// <response code="404">The Message does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(Guid requestReplyId, [FromQuery] DocumentType documentType,
            [FromForm] IFormFile file)
        {
            var userId = userManager.GetUserId(User);

            if (documentType != DocumentType.RequestContract)
            {
                ModelState.AddModelError(nameof(documentType), "Невалидный тип документа");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Files)
                .IncludeOptimized(i => i.Request)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .FirstOrDefaultAsync(i => i.Id == requestReplyId);

            if (item == null)
            {
                return NotFound();
            }

            item.Request.RequestStatus = RequestStatus.DataInput;
            item.Request.KrafterRequestStatus = KrafterRequestStatus.LogistDocumentsApproveInput;

            var fileName = Guid.NewGuid().ToString();

            await using var stream = file.OpenReadStream();
            await fileService.PutFileAsync(stream, Folders.RequestReply, fileName);

            item.Files ??= new List<KrafterRequestReplyFile>();

            item.Files.Add(new KrafterRequestReplyFile
            {
                CreatedAt = DateTime.Now,
                OriginalName = file.FileName,
                UploaderId = userManager.GetUserId(User),
                ContentType = file.ContentType,
                DocumentType = documentType,
                Path = Folders.RequestReply + "/" + fileName,
                Accepted = true,
                AcceptedDate = DateTime.Now,
                AcceptedById = userId,
                Signed = true,
                SignedDate = DateTime.Now,
                SignedById = userId,
            });

            await logistDbContext.SaveChangesAsync()
                .ContinueWith(t =>
                {

                    if (item.Kontragent.KontragentUsers.Any())
                    {
                        BackgroundJob.Enqueue<NotificationsService>(i =>
                            i.AddRequestReplyDocuments(item.RequestId, item.Kontragent.KontragentUsers.First().UserId));
                    }

                    BackgroundJob.Enqueue<ChangesTrackService>(i =>
                        i.TrackRequestAsync(item.RequestId, RequestActionStatus.LogistAddDocuments,
                            ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item.Request)), userId));
                });
            return NoContent();
        }
    }
}