﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using System.Linq;
using DAL.Models.Kontragents.Enums;
using DelegateDecompiler.EntityFrameworkCore;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Extensions;
using Krafter.Client.Atisu.Client.Models;
using Krafter.Client.Atisu.Client.Services;
using Krafter.Logist.WebApp.Models.Basic.Atisu;
using Krafter.Logist.WebApp.Services.Atisu;
using Krafter.ServiceCommon.Helpers;
using Microsoft.AspNetCore.Identity;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Krafter.Client.Atisu.Services;
using Krafter.Client.Atisu.Helpers;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(AtisuTruck))]
    public class AtisuTruckController : BaseController<AtisuTruckController>
    {
        private readonly IAtisuService atisuService;

        public AtisuTruckController(ILogger<AtisuTruckController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            IAtisuService atisuService)
            : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
            this.atisuService = atisuService;
        }

        /// <returns></returns>
        /// <summary>
        /// get list of carriers
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="options"></param>
        /// <param name="fromCity"></param>
        /// <param name="toCity"></param>
        /// <param name="remoteness"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(ICollection<AtisuTruck>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(Guid requestId, int fromCity, int toCity, int remoteness,
            ODataQueryOptions<AtisuTruck> options)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var visitor = new OdataVisitor<AtisuTruck>();
            options.Filter?.FilterClause.Expression.Accept(visitor);

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.RoutePoints)
                .FirstOrDefaultAsync(r => r.Id == requestId);

            var result = await atisuService.SearchTrucks(new AtisuBodyFilter()
            {
                page = options.Skip != null ? (options.Skip.Value / options.Top.Value) + 1 : 1,
                items_per_page = options.Top.Value,
                filter = new AtisuFilter()
                {
                    from = new AtisuFilterPoint()
                    {
                        id = fromCity,
                        radius = remoteness,
                    },
                    to = new AtisuFilterPoint()
                    {
                        id = toCity,
                        radius = remoteness
                    },
                    volume = new AtisuMeasure() { min = visitor.FilterValueList.GetMeasureValue("transport.volume") },
                    weight = new AtisuMeasure() { min = visitor.FilterValueList.GetWeightValue("transport.weight") },
                    truck_type = visitor.FilterValueList.GetBitSumValue("cartype.typeid"),
                    dates = new AtisuDates()
                    {
                        date_from = item.Load.ArrivalDate.AddHours(-24),
                        date_to = item.Load.LeaveDate.AddHours(24),
                        date_option = DateOption.Manual
                    },
                    with_rate = false
                }
            });

            if (result.Total == 0)
            {
                return StatusCode(StatusCodes.Status200OK);
            }

            // @TODO переделать
            var firms = await atisuService.GetFirms(result.Trucks.Select(x => x.Account.AccountId));
            var firmsInn = firms.Where(x => x.Inn != null && x.Inn != "").Select(x => x.Inn).ToList();

            var kontragents = logistDbContext.Kontragents.IgnoreQueryFilters()
                .Where(x => firmsInn.Contains(x.Inn)).Select(x => new
                {
                    x.Inn,
                    x.Status
                });

            await foreach (var kontragent in kontragents.AsAsyncEnumerable())
            {
                var firm = firms.FirstOrDefault(x => x.Inn == kontragent.Inn);
                var accounts = result.Trucks.Where(x => x.Account.AccountId == firm.AtiId).Select(x => x.Account);

                foreach (var account in accounts)
                    account.Status = kontragent.Status == KontragentStatus.Active
                        ? AtisuAccauntStatus.Agreed
                        : AtisuAccauntStatus.NotAgreed;
            }

            Request.ODataFeature().TotalCount = result.Total;
            Request.GetNextPageLink(options.Top.Value);

            return StatusCode(StatusCodes.Status200OK, result.Trucks.Take(options.Top.Value));
        }

        /// <summary>
        /// Get near city
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <param name="name"></param>
        /// <param name="remoteness"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(AtisuCityGeolocation), StatusCodes.Status200OK)]
        [ODataRoute("FindNearCity")]
        public async Task<IActionResult> GetCity(string name, double lat, double lon, double remoteness)
        {
            var cities = await atisuService.GetNearCities(name, lat, lon, remoteness);
            return StatusCode(StatusCodes.Status200OK, cities.OrderBy(x => x.Remoteness).FirstOrDefault());
        }

        /// <summary>
        /// Get city by coordinates
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(AtisuCity), StatusCodes.Status200OK)]
        [ODataRoute("FindCity")]
        public async Task<IActionResult> GetCity(double lat, double lon)
        {
            var city = await atisuService.GetCity(lat, lon);
            return StatusCode(StatusCodes.Status200OK, city);
        }

        /// <summary>
        /// Get CarTypes
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(AtisuCarType), StatusCodes.Status200OK)]
        [ODataRoute("CarTypes")]
        public async Task<IActionResult> GetCarTypes()
        {
            var carTypes = await atisuService.GetCarTypes();
            return StatusCode(StatusCodes.Status200OK, carTypes);
        }

        private async Task<ICollection<AtisuTruck>> GetTrucks(AtisuReis reis, int remoteness)
        {
            var from =
                await atisuService.GetNearCities(reis.Load.Title,
                    reis.Load.Lat.Value,
                    reis.Load.Lon.Value,
                    remoteness);

            var fromCity = from.MaxBy(x => x.Remoteness)?.CityId;

            var to =
                await atisuService.GetNearCities(reis.Unload.Title,
                    reis.Unload.Lat.Value,
                    reis.Unload.Lon.Value,
                    remoteness);

            var toCity = to.MaxBy(x => x.Remoteness)?.CityId;

            var result = await atisuService.SearchTrucks(new AtisuBodyFilter()
            {
                page = 1,
                items_per_page = 100,
                filter = new AtisuFilter()
                {
                    from = fromCity.HasValue
                        ? new AtisuFilterPoint()
                        {
                            id = fromCity.Value,
                            radius = remoteness
                        }
                        : null,
                    to = toCity.HasValue
                        ? new AtisuFilterPoint()
                        {
                            id = toCity.Value,
                            radius = remoteness
                        }
                        : null,
                    volume = new AtisuMeasure() { min = reis.Volume },
                    weight = new AtisuMeasure() { min = reis.Weight },
                    truck_type = AtisuHelper.GetTruckType(reis.RollingStockTypeId),
                    dates = new AtisuDates()
                    {
                        date_from = reis.Load.ArrivalDatePlan.AddHours(-24),
                        date_to = reis.Load.LeaveDatePlan.AddHours(24),
                        date_option = DateOption.Manual
                    },
                    with_rate = false
                }
            });

            return result.Trucks;
        }

        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ICollection<AtisuTruck>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAtisuTrucksBesidePoint([FromBody] AtisuFilterForMap filter)
        {
            var resultList = new List<AtisuTruck>();

            var reises = await logistDbContext.Reises
                .IgnoreQueryFilters()
                .AsNoTracking()
                .Where(x => filter.Ids.Contains(x.Id))
                .Select(x => new
                {
                    x.Id,
                    Load = x.Load != null
                        ? new
                        {
                            Title = x.Load.Settlement != null ? x.Load.Settlement.Title : null,
                            x.Load.Lat,
                            x.Load.Lon,
                            x.Load.ArrivalDatePlan,
                            x.Load.LeaveDatePlan
                        }
                        : null,
                    Unload = x.Unload != null
                        ? new
                        {
                            Title = x.Unload.Settlement != null ? x.Unload.Settlement.Title : null,
                            x.Unload.Lat,
                            x.Unload.Lon
                        }
                        : null,
                    Volume = x.ConnectedTransportId != null ? x.ConnectedTransport.Volume : x.Transport.Volume,
                    Weight = x.ConnectedTransportId != null ? x.ConnectedTransport.Weight : x.Transport.Weight,
                    RollingStockTypeId = x.ConnectedTransportId == null
                        ? x.Transport.RollingStockTypeId
                        : x.ConnectedTransport.RollingStockTypeId
                })
                .DecompileAsync()
                .ToListAsync();

            var reisInfoList = reises.Where(x => x.Load?.Title != null && x.Load.Lat.HasValue && x.Load.Lon.HasValue &&
                                                 x.Unload?.Title != null && x.Unload.Lat.HasValue &&
                                                 x.Unload.Lon.HasValue)
                .Select(x => new AtisuReis()
                {
                    Id = x.Id,
                    RollingStockTypeId = x.RollingStockTypeId,
                    Volume = x.Volume,
                    Weight = x.Weight,
                    Load = new AtisuReis.Point()
                    {
                        Title = x.Load.Title,
                        Lat = x.Load.Lat,
                        Lon = x.Load.Lon,
                        ArrivalDatePlan = x.Load.ArrivalDatePlan,
                        LeaveDatePlan = x.Load.LeaveDatePlan,
                    },
                    Unload = new AtisuReis.Point()
                    {
                        Title = x.Unload.Title,
                        Lat = x.Unload.Lat,
                        Lon = x.Unload.Lon,
                    }
                });

            if (!reisInfoList.Any())
                return StatusCode(StatusCodes.Status200OK, resultList);

            foreach (var reisInfo in reisInfoList)
            {
                var result = await GetTrucks(reisInfo, filter.Remoteness);
                resultList.AddRange(result);
            }
 
            if (resultList.Any())
            {
                var firms = await atisuService.GetFirms(resultList.Select(x => x.Account.AccountId));
                var firmsInn = firms.Where(x => !string.IsNullOrEmpty(x.Inn)).Select(x => x.Inn).ToList();

                var kontragents = logistDbContext.Kontragents
                    .AsNoTracking()
                    .IgnoreQueryFilters()
                    .Where(x => firmsInn.Contains(x.Inn))
                    .Select(x => new
                    {
                        x.Inn,
                        x.Status
                    });

                await foreach (var kontragent in kontragents.AsAsyncEnumerable())
                {
                    var firm = firms.FirstOrDefault(x => x.Inn == kontragent.Inn);
                    var accounts = resultList.Where(x => x.Account.AccountId == firm.AtiId).Select(x => x.Account);

                    foreach (var account in accounts)
                        account.Status = kontragent.Status == KontragentStatus.Active
                            ? AtisuAccauntStatus.Agreed
                            : AtisuAccauntStatus.NotAgreed;
                }
            }
            
            return StatusCode(StatusCodes.Status200OK, resultList.GroupBy(x => new
            {
                x.TruckId,
                x.AtiId
            }).Select(x => x.MaxBy(s => s.UpdatedDate)));
        }
    }
}