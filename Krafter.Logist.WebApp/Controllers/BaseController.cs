using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;

namespace Krafter.Logist.WebApp.Controllers
{
    [ApiVersion("1.0")]
    [Authorize(Roles = UserRoles.Admin + "," + UserRoles.Logist)]
    public abstract class BaseController<TModel> : ODataController
    {
        protected readonly ILogger<TModel> logger;
        protected readonly LogistDbContext logistDbContext;
        protected readonly UserManager<User> userManager;

        protected BaseController(ILogger<TModel> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.logger = logger;
            this.logistDbContext = logistDbContext;
            this.userManager = userManager;

            var userId = this.userManager.GetUserId(httpContextAccessor.HttpContext.User);

            MappedDiagnosticsLogicalContext.Set("UserId", userId);

            this.logistDbContext.UserId = userId;
        }

        protected BaseController(ILogger<TModel> logger,
            LogistDbContext logistDbContext)
        {
            this.logger = logger;
            this.logistDbContext = logistDbContext;
        }
    }
}