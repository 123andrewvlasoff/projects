using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class EmployerController : BaseController<Employer>
    {
        public EmployerController(ILogger<Employer> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get ClientEmployers.
        /// </summary>
        /// <returns>The requested ClientEmployers.</returns>
        /// <response code="200">The ClientEmployers was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Employer>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IEnumerable<Employer> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var items = logistDbContext.Requests.IgnoreQueryFilters()
                .Where(i => i.RequestStatus == RequestStatus.IsActive &&
                            (i.ActiveUntil > DateTime.Now || i.AssignedUserId == userId) ||
                            (i.RequestStatus == RequestStatus.DataTreatment ||
                             i.RequestStatus == RequestStatus.DataInput ||
                             i.RequestStatus == RequestStatus.Cancelled ||
                             i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId)
                .SelectMany(s => s.Client.KontragentUsers.Where(x =>
                    x.RelationType == RelationType.Manager && x.Discriminator == KontragentUserType.Logist))
                .Select(x => x.User.Employer)
                .AsEnumerable()
                .GroupBy(g => g.Id)
                .Select(x => x.First());

            return items;
        }

        /// <summary>
        ///     Get ClientEmployer by Id.
        /// </summary>
        /// <returns>The requested ClientEmployer.</returns>
        /// <response code="200">The ClientEmployer was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Employer), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var item = logistDbContext.Requests.IgnoreQueryFilters()
                .Where(i => i.RequestStatus == RequestStatus.IsActive &&
                            (i.ActiveUntil > DateTime.Now || i.AssignedUserId == userId) ||
                            (i.RequestStatus == RequestStatus.DataTreatment ||
                             i.RequestStatus == RequestStatus.DataInput ||
                             i.RequestStatus == RequestStatus.Cancelled ||
                             i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId)
                .SelectMany(s => s.Client.KontragentUsers.Where(x =>
                    (x.RelationType == RelationType.Pinned || x.RelationType == RelationType.Manager) && x.Discriminator == KontragentUserType.Logist))
                .Select(x => x.User.Employer)
                .AsEnumerable()
                .GroupBy(g => g.Id).Select(x => x.First())
                .FirstOrDefault(x => x.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}