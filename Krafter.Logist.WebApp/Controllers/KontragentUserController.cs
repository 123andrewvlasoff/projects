using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.KontragentUser.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KontragentUser;
using Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(KontragentUser))]
    public class KontragentUserController : BaseController<KontragentUserController>
    {
        public KontragentUserController(ILogger<KontragentUserController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get LogistKontragentUser.
        /// </summary>
        /// <returns>The requested LogistKontragentUser.</returns>
        /// <response code="200">The LogistKontragentUser was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<KontragentUser>), 200)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<KontragentUser> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return logistDbContext.KontragentUsers.OfType<LogistKontragentUser>();
        }

        /// <summary>
        ///     Get LogistKontragentUser by Id.
        /// </summary>
        /// <returns>The requested LogistKontragentUser.</returns>
        /// <response code="200">The LogistKontragentUser was successfully retrieved.</response>
        /// <response code="404">The LogistKontragentUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KontragentUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KontragentUsers.OfType<LogistKontragentUser>()
                .IncludeOptimized(i => i.Kontragent)
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     Places a new LogistKontragentUser.
        /// </summary>
        /// <param name="item">The LogistKontragentUser to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created LogistKontragentUser.</returns>
        /// <response code="201">The LogistKontragentUser was successfully placed.</response>
        /// <response code="400">The LogistKontragentUser is invalid.</response>
        [ProducesResponseType(typeof(KontragentUser), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] KontragentUser item,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.UserId = userManager.GetUserId(User);

            item.RelationType = RelationType.Favorite;
            item.Discriminator = KontragentUserType.Logist;

            await logistDbContext.KontragentUsers.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(item);
        }

        /// <summary>
        ///     Updates an existing LogistKontragentUser.
        /// </summary>
        /// <param name="key">The requested LogistKontragentUser identifier.</param>
        /// <param name="delta">The partial LogistKontragentUser to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="204">The LogistKontragentUser was successfully updated.</response>
        /// <response code="400">The LogistKontragentUser is invalid.</response>
        /// <response code="404">The LogistKontragentUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KontragentUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<KontragentUser> delta,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            item.Comment = delta.GetInstance().Comment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }

        /// <summary>
        ///     delete an LogistKontragentUser.
        /// </summary>
        /// <param name="key">The LogistKontragentUser to cancel.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>None</returns>
        /// <response code="204">The LogistKontragentUser was successfully deleted.</response>
        /// <response code="404">The LogistKontragentUser does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.RelationType.HasFlag(RelationType.Pinned))
            {
                item.RelationType ^= RelationType.Favorite;
            }
            else
            {
                logistDbContext.KontragentContactUsers.RemoveRange(
                    logistDbContext.KontragentContactUsers.Where(x =>
                        x.KontragentContact.KontragentId == item.KontragentId && x.UserId == userId));

                logistDbContext.DriverKontragentUsers.RemoveRange(
                    logistDbContext.DriverKontragentUsers.Where(x =>
                        x.DriverKontragent.KontragentId == item.KontragentId && x.UserId == userId));

                logistDbContext.TransportKontragentUsers.RemoveRange(
                    logistDbContext.TransportKontragentUsers.Where(x =>
                        x.TransportKontragent.KontragentId == item.KontragentId && x.UserId == userId));

                logistDbContext.KontragentUsers.Remove(item);
            }

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}