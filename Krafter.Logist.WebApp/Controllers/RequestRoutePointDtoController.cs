﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Requests.Guaranties.Enums;
using DelegateDecompiler;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestRoutePointDto;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using RequestRoutePoint = Krafter.Logist.WebApp.Models.Basic.RequestRoutePoint.RequestRoutePoint;
using RequestStatus = DAL.Models.Requests.RequestStatus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class RequestRoutePointDtoController : BaseController<RequestRoutePointDtoController>
    {
        private readonly RequestDtoRepository repository;

        public RequestRoutePointDtoController(ILogger<RequestRoutePointDtoController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, RequestDtoRepository repository) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
            this.repository = repository;
        }

        /// <summary>
        ///     Get RequestRoutePointDto.
        /// </summary>
        /// <returns>The requested RequestRoutePointDto.</returns>
        /// <response code="200">The RequestRoutePointDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestRoutePointDto>), StatusCodes.Status200OK)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<RequestRoutePointDto>> Get()
        {
            var items = await repository.GetAsync();
                
            return items.SelectMany(i => i.RoutePoints);
        }
    }
}