﻿using DAL.Models.Enums;
using DelegateDecompiler.EntityFrameworkCore;
using Krafter.Logist.WebApp.Models.Api.Transport;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests.Reis.Enums;
using Krafter.Logist.WebApp.Models.Api.Basic;
using Krafter.Logist.WebApp.Models.Api.SuitableTransport;
using Krafter.Logist.WebApp.Services;

namespace Krafter.Logist.WebApp.Controllers.Api
{
    public class TransportController : BaseApiController<TransportController>
    {
        private readonly ISuitableTransportService suitableTransportService;

        public TransportController(ILogger<TransportController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor, ISuitableTransportService suitableTransportService) : base(logger,
            logistDbContext, userManager, httpContextAccessor)
        {
            this.suitableTransportService = suitableTransportService;
        }

        [Produces("application/json")]
        [ProducesResponseType(typeof(SuitableTransport), StatusCodes.Status200OK)]
        [HttpPost("GetSuitableTransports")]
        public async Task<IActionResult> GetUnloadPoints(SuitableTransportFilter filter)
        {
            var result = await suitableTransportService
                .GetAllAsync(filter, UserId);

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [Produces("application/json")]
        [ProducesResponseType(typeof(ICollection<PointInfo>), StatusCodes.Status200OK)]
        [HttpGet("SearchPoints")]
        public async Task<IActionResult> SearchPoints(string search)
        {
            var result = await suitableTransportService.SearchByAddress(search);

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportPointPreview), StatusCodes.Status200OK)]
        [HttpGet("GetPointPreview")]
        public async Task<IActionResult> GetPointPreview(Guid id)
        {
            var item = await logistDbContext.Reises
                .IgnoreQueryFilters()
                .AsNoTracking()
                .Where(x => x.Id == id)
                .Select(x => new SuitableTransport()
                {
                    Id = x.Id,
                    Address = x.Transport.LastAddress ?? x.Unload.Address,
                    DeliveryNumber = x.DeliveryNumber,
                    Carrier = x.Carrier != null
                        ? new CarrierApi()
                        {
                            Id = x.Carrier.Id,
                            Title = x.Carrier.Title,
                        }
                        : null,
                    PinnedEmployer = x.Carrier.KontragentUsers
                        .Where(i => i.RelationType.HasFlag(RelationType.Pinned) && i.User.Employer != null)
                        .Select(i => i.User.Employer)
                        .Select(e => new EmployerApi
                        {
                            Id = e.Id,
                            Branch = e.Branch != null
                                ? new BranchApi() { Id = e.Branch.Id, Title = e.Branch.Title }
                                : null,
                            FIO = e.FIO,
                            Email = e.Email,
                            Phone = e.PhoneNumber
                        })
                        .FirstOrDefault(),
                    IsCanReadReis = x.AcceptorId == UserId
                })
                .DecompileAsync()
                .FirstOrDefaultAsync();

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}