﻿using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;

namespace Krafter.Logist.WebApp.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [Authorize(Roles = UserRoles.Admin + "," + UserRoles.Logist)]
    public abstract class BaseApiController<TModel> : ControllerBase
    {
        protected readonly ILogger<TModel> logger;
        protected readonly LogistDbContext logistDbContext;
        protected readonly UserManager<User> userManager;

        public string UserId {get; set;}

        protected BaseApiController(ILogger<TModel> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.logger = logger;
            this.logistDbContext = logistDbContext;
            this.userManager = userManager;

            var userId = this.userManager.GetUserId(httpContextAccessor.HttpContext.User);

            MappedDiagnosticsLogicalContext.Set("UserId", userId);

            this.logistDbContext.UserId = userId;
            this.UserId = userId;
        }

        protected BaseApiController(ILogger<TModel> logger,
            LogistDbContext logistDbContext)
        {
            this.logger = logger;
            this.logistDbContext = logistDbContext;
        }
    }
}