using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using GeoCoordinatePortable;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Api.Request;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers.Api;

public class RequestDtoApiController : BaseApiController<RequestDtoApiController>
{
    private readonly RequestDtoRepository repository;
    private readonly IRequestDtoFilterService requestDtoFilterService;
    private readonly IMapper mapper;

    public RequestDtoApiController(ILogger<RequestDtoApiController> logger, LogistDbContext logistDbContext,
        UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, RequestDtoRepository repository,
        IRequestDtoFilterService requestDtoFilterService, IMapper mapper) : base(logger, logistDbContext, userManager,
        httpContextAccessor)
    {
        this.repository = repository;
        this.requestDtoFilterService = requestDtoFilterService;
        this.mapper = mapper;
    }

    [Produces("application/json")]
    [ProducesResponseType(typeof(PageResponseModel<IEnumerable<RequestDto>>), StatusCodes.Status200OK)]
    [HttpPost("Filter")]
    public async Task<IActionResult> Post([FromBody]PageFilterData<RequestDtoFilter> pageFilterData, double? lat, double? lon, int? remoteness = null)
    {
        var userId = userManager.GetUserId(User);
        var items = await repository.GetAsync();

        items = items
            .Where(i => i.RequestStatus == RequestStatus.IsActive ||
                        (i.RequestStatus == RequestStatus.DataTreatment ||
                         i.RequestStatus == RequestStatus.DataInput ||
                         i.RequestStatus == RequestStatus.Cancelled ||
                         i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

        if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            items = items
                .Where(x => x.Discriminator != RequestDiscriminator.Krafter || x.AssignedUserId == userId);

        if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            items = items
                .Where(x =>
                x.Discriminator != RequestDiscriminator.Cargomart &&
                x.Discriminator != RequestDiscriminator.TrafficOnline &&
                x.Discriminator != RequestDiscriminator.TorgTrans);

        if (remoteness != null && lat.HasValue && lon.HasValue)
            items = items
                .Where(x => x.Load?.Lat != null && x.Load?.Lon != null)
                .Where(x => new GeoCoordinate(x.Load.Lat.Value, x.Load.Lon.Value)
                    .GetDistanceTo(new GeoCoordinate((double)lat, (double)lon)) / 1000 <= remoteness);

        var requests = await requestDtoFilterService.Filter(items, pageFilterData);        

        var total = requests.Count;
        requests = requests.Skip(pageFilterData.Skip).Take(pageFilterData.Take).ToList();
        var result = mapper.Map<List<RequestDto>>(requests, options => options.Items.Add("UserId", userId));
        
        return Ok(PageResponseModel<IEnumerable<RequestDto>>.Success(result, total));
    }
}