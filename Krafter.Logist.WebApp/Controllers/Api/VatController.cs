﻿using Crafter.Vat.Helpers;
using Crafter.Vat.Models;
using Crafter.Vat.Services;
using DAL.Models.Requests.Reis.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Krafter.Logist.WebApp.Controllers
{
    public class VatController : BaseApiController<VatController>
    {
        private readonly VatCacheService vatCacheService;
        private readonly RequestDtoRepository repository;
        public VatController(ILogger<VatController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, RequestDtoRepository repository, VatCacheService vatCacheService)
            : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
            this.repository = repository;
            this.vatCacheService = vatCacheService;
        }

        private async Task<KrafterRequestDto> GetRequestDto(Guid requestId)
        {
            return await repository.GetAsync<KrafterRequestDto>(requestId) ??
                   await logistDbContext.Requests
                       .Where(x => x.Id == requestId)
                       .Select(x => new KrafterRequestDto()
                       {
                           VatType = x.VatType,
                           Id = x.Id,
                           PredictedFinalPrice = x.PredictedFinalPrice,
                           Distance = x.Distance,
                           ClientPrice = x.ClientPrice,
                       }).FirstOrDefaultAsync();
        }


        [Produces("application/json")]
        [ProducesResponseType(typeof(CalculatedRequestPrices), StatusCodes.Status200OK)]
        [HttpGet("CalculatePrices")]
        public async Task<IActionResult> CalculatedPrice(Guid requestId)
        {
            var request = await GetRequestDto(requestId);
            CalculatedRequestPrices result = default;

            if (request != null && request.VatType.HasValue)
            {
                var _actualVat = await vatCacheService.GetActualVat(() => logistDbContext.Vat.GetActualVat());
                var _actualMarginality = await vatCacheService.GetMarginality(() => logistDbContext.Marginality.GetActualMarginality());

                result = VatCalculateService.CalculatePrices(request.VatType.Value,
                    request.ClientPrice,
                    _actualVat.Value,
                    _actualMarginality.MarginValue,
                    request.PredictedFinalPrice,
                    request.Distance);
            }

            return StatusCode(StatusCodes.Status200OK, result);
        }


        [Produces("application/json")]
        [ProducesResponseType(typeof(CalculatedVatData), StatusCodes.Status200OK)]
        [HttpGet("CalculateVat")]
        public async Task<IActionResult> CalculateVat(Guid requestId, string changeProperty, decimal value, VatType? vatType)
        {
            var request = await GetRequestDto(requestId);
            CalculatedVatData result = new();

            if (request != null && request.VatType.HasValue)
            {
                var _actualVat = await vatCacheService.GetActualVat(() => logistDbContext.Vat.GetActualVat());
                var _actualMarginality = await vatCacheService.GetMarginality(() => logistDbContext.Marginality.GetActualMarginality());

                var _calculatedPrice = VatCalculateService.CalculatePrices(request.VatType.Value,
                    request.ClientPrice,
                    _actualVat.Value,
                    _actualMarginality.MarginValue,
                    request.PredictedFinalPrice,
                    request.Distance);

                result = VatCalculateService.CalculateVat(
                    (request.VatType, vatType),
                    (_calculatedPrice.ClientPriceWithVat == 0
                        ? _calculatedPrice.ClientPriceWithoutVat
                        : _calculatedPrice.ClientPriceWithVat, _calculatedPrice.ClientPriceWithoutVat),
                    changeProperty,
                    value,
                    _actualVat.Value);
            }

            return StatusCode(StatusCodes.Status200OK, result);
        }
    }
}
