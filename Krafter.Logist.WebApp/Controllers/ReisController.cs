using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Enums;
using DAL.Models.Kontragents.Dictionaries;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests.Dictionaries;
using DAL.Models.Requests.Enums;
using Dapper;
using DelegateDecompiler;
using GeoCoordinatePortable;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.BundleComment;
using Krafter.Logist.WebApp.Models.Basic.Driver;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.Kontragent;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Models.Basic.Reis.Dto;
using Krafter.Logist.WebApp.Models.Basic.ReisRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.SqlKataOdata;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlKata.Execution;
using Z.EntityFramework.Plus;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(Reis))]
    public class ReisController : BaseController<ReisController>
    {
        private readonly QueryFactory db;
        private readonly IMapper mapper;

        public ReisController(ILogger<ReisController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, QueryFactory db,
            IMapper mapper) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.db = db;
            this.mapper = mapper;
        }

        /// <summary>
        ///     Get Reises.
        /// </summary>
        /// <returns>The requested Reises.</returns>
        /// <response code="200">The Reises was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Reis>), StatusCodes.Status200OK)]
        public IQueryable<Reis> Get(ODataQueryOptions<Reis> options)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            logger.LogInformation("Get reises");

            return options.ApplyTo(logistDbContext.Reises.Where(i => i.IsActive)
                    .IncludeOptimized(i => i.RoutePoints.Select(rp => new
                    {
                        rp.Region,
                        rp.Settlement
                    }))
                    .IncludeOptimized(i => i.KontragentContact)
                    .IncludeOptimized(i => i.Carrier)
                    .IncludeOptimized(i => i.Client)
                    .IncludeOptimized(i => i.Problems)
                    .IncludeOptimized(i => i.Transport)
                    .IncludeOptimized(i => i.ConnectedTransport)
                    .IncludeOptimized(i => i.Driver)
                    .IncludeOptimized(i => i.PaymentTransactions)
                    .IncludeOptimized(i => i.Claims)
                    .IncludeOptimized(i => i.Status).Decompile(), new ODataQuerySettings()
                {
                    HandleNullPropagation = HandleNullPropagationOption.False
                },
                AllowedQueryOptions.Expand) as IQueryable<Reis>;
        }

        /// <summary>
        ///     Get Reis by Id.
        /// </summary>
        /// <returns>The requested Reis.</returns>
        /// <response code="200">The Reis was successfully retrieved.</response>
        /// <response code="404">The Reis does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Reis), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.Reises
                .Where(x => x.IsActive||x.AcceptorId==userManager.GetUserId(User))
                .IncludeOptimized(i => i.RoutePoints.Select(rp => new
                {
                    rp.Region,
                    rp.Settlement
                }))
                .IncludeOptimized(i => i.Problems)
                .IncludeOptimized(i => i.Carrier)
                .IncludeOptimized(i => i.Client)
                .IncludeOptimized(i => i.Transport.TransportType)
                .IncludeOptimized(i => i.ConnectedTransport)
                .IncludeOptimized(i => i.LoadingType)
                .IncludeOptimized(i => i.RollingStockType)
                .IncludeOptimized(i => i.Driver)
                .IncludeOptimized(i => i.TransportStatus)
                .IncludeOptimized(i => i.Status)
                .IncludeOptimized(i => i.PaymentTransactions.Select(s => s.PayType))
                .IncludeOptimized(i => i.Files.Select(f => f.SignedBy))
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logger.LogInformation($"Get reis: id - {key}");

            return StatusCode(StatusCodes.Status200OK, mapper.Map<Reis>(item));
        }

        /// <summary>
        ///     Get ReisesDto.
        /// </summary>
        /// <returns>The requested ReisesDto.</returns>
        /// <response code="200">The ReisesDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ReisDto>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.True)]
        [ODataRoute("List")]
        public async Task<IEnumerable<ReisDto>> GetList(Guid key, ODataQueryOptions<ReisDto> options)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.RoutePoints)
                .First(i => i.Id == key);

            var requestLat = item.Load?.Lat;

            var requestLon = item.Load?.Lon;

            var filtredIds = options.FilterTo(db.Query("Reis")
                    .WhereTrue("IsActive")
                    .WhereFalse("IsDeleted")
                    .WhereIn("StatusId", new[]
                    {
                        InProgress, WaitingDocuments,
                        OnApproval, Agreed,
                        WaitingLoad, Completed
                    })
                    .RightJoin(
                        db.Query("ReisRoutePoints")
                            .Select("PointTypeName",
                                "ArrivalDatePlan",
                                "SettlementId",
                                "kRegionId",
                                "ReisId")
                            .As("Load")
                            .Where("PointTypeName", PointType.Load),
                        j => j.On("Reis.Id", "Load.ReisId"))
                    .RightJoin(
                        db.Query("ReisRoutePoints")
                            .Select("PointTypeName",
                                "ArrivalDatePlan",
                                "SettlementId",
                                "kRegionId",
                                "ReisId")
                            .As("Unload")
                            .Where("PointTypeName", PointType.Unload),
                        j => j.On("Reis.Id", "Unload.ReisId")), "Reis")
                .Select("Reis.Id")
                .Get<Guid>();

            var query = db.Query().From("Reis")
                .WhereIn("Reis.Id", filtredIds)
                .SelectRaw("[Reis].[KCarrierId] as [CarrierId]")
                .SelectRaw("[Reis].[KClientId] as [ClientId]")
                .SelectRaw("[Reis].[MainTransportId] as [TransportId]")
                .SelectRaw("[Reis].[PricepId] as [ConnectedTransportId]")
                .Join(
                    db.Query("ReisRoutePoints")
                        .Select(
                            "Id",
                            "ArrivalDatePlan",
                            "ArrivalDateFact",
                            "LeaveDatePlan",
                            "LeaveDateFact",
                            "SettlementId",
                            "Address",
                            "ReisId")
                        .SelectRaw("[kRegionId] as [RegionId]")
                        .SelectRaw("[PointTypeName] as [PointType]")
                        .As("RoutePoints"),
                    j => j.On("Reis.Id", "RoutePoints.ReisId"))
                .Join(
                    db.Query("kRegions")
                        .SelectRaw("[Id] as [ID]")
                        .Select("Title")
                        .As("Region"),
                    j => j.On("RoutePoints.RegionId", "Region.ID"))
                .Join(
                    db.Query("Settlements")
                        .SelectRaw("[Id] as [ID]")
                        .Select("Title")
                        .As("Settlement"),
                    j => j.On("RoutePoints.SettlementId", "Settlement.ID"))
                .Join(
                    db.Query("kTransports")
                        .Select("Id",
                            "RegNumber",
                            "TransportTypeId",
                            "IsPricep",
                            "RollingStockTypeID",
                            "LastReisDate",
                            "LastReisLat",
                            "LastReisLon",
                            "LastAddress",
                            "Model",
                            "Volume",
                            "Weight",
                            "RollingStockTypeID",
                            "Temperature",
                            "RentStart",
                            "RentStop"
                        )
                        .As("Transport"),
                    j => j.On("Transport.Id", "Reis.MainTransportId"))
                .Join(
                    db.Query("kTransports")
                        .Select("Id",
                            "RegNumber",
                            "TransportTypeId",
                            "IsPricep",
                            "RollingStockTypeID",
                            "LastReisDate",
                            "LastReisLat",
                            "LastReisLon",
                            "LastAddress",
                            "Model",
                            "Volume",
                            "Weight",
                            "RollingStockTypeID",
                            "Temperature",
                            "RentStart",
                            "RentStop"
                        )
                        .As("ConnectedTransport"),
                    j => j.On("ConnectedTransport.Id", "Reis.PricepId"))
                .Join(
                    db.Query("kDrivers")
                        .Select(
                            "Id",
                            "LastName",
                            "FirstName",
                            "Patronymic",
                            "PhoneNumber",
                            "WorkPhone"
                        )
                        .As("Driver"),
                    j => j.On("Driver.Id", "Reis.DriverId"))
                .Join(
                    db.Query("Kontragents")
                        .Select(
                            "Id",
                            "Title",
                            "ContactName",
                            "ContactEmail",
                            "ContactPhone")
                        .As("Client"),
                    j => j.On("Reis.KClientId", "Client.Id"))
                .Join(
                    db.Query("Status")
                        .SelectRaw("[Id]")
                        .Select("Title")
                        .As("Status"),
                    j => j.On("Status.Id", "Reis.StatusId"))
                .Join(
                    db.Query("KKontragentUsers")
                        .Where("RelationType", RelationType.Pinned)
                        .Where("Discriminator", KontragentUserType.Logist)
                        .Select(
                            "ID",
                            "UID",
                            "KKontragentID",
                            "Discriminator",
                            "RelationType")
                        .As("KontragentUsers"),
                    j => j.On("Reis.KCarrierId", "KontragentUsers.KKontragentID"))
                .Join(
                    db.Query("AspNetUsers")
                        .WhereTrue("IsActive")
                        .Select("Id")
                        .As("User"),
                    j => j.On("KontragentUsers.UID", "User.Id"))
                .Join(
                    db.Query("Employs")
                        .WhereTrue("IsActive")
                        .Select(
                            "Id",
                            "FIO",
                            "Email",
                            "PhoneNumber",
                            "BranchId",
                            "ParentEmployerId",
                            "UserId")
                        .As("Employer"),
                    j => j.On("User.Id", "Employer.UserId"))
                .Join(
                    db.Query("Branch")
                        .Select(
                            "Id",
                            "Title")
                        .As("Branch"),
                    j => j.On("Employer.BranchId", "Branch.Id"))
                .Join(
                    db.Query("Employs")
                        .Select(
                            "Id",
                            "UserId",
                            "FIO",
                            "Email",
                            "PhoneNumber",
                            "BranchId",
                            "ParentEmployerId",
                            "UserId")
                        .As("ParentEmployer"),
                    j => j.On("Employer.ParentEmployerId", "ParentEmployer.Id"))
                .Join(
                    db.Query("Kontragents")
                        .Select("Id")
                        .Select("Title")
                        .As("Carrier"),
                    j => j.On("Carrier.Id", "Reis.KCarrierId"))
                .LeftJoin(
                    db.Query("KontragentContacts")
                        .WhereTrue("IsActive")
                        .Select("Id")
                        .SelectRaw("[KKontragentId] as [KontragentId]")
                        .Select("Email")
                        .Select("Phone")
                        .Select("FirstName")
                        .Select("LastName")
                        .Select("Patronymic")
                        .As("Contact"),
                    j => j.On("Contact.Id", "Reis.KontragentContactId"))
                .LeftJoin(
                    db.Query("Comments")
                        .Where("ConnectedTransportId" == "[ConnectedTransport].[Id]")
                        .Select("Id")
                        .Select("Text")
                        .Select("RequestId")
                        .Select("TransportId")
                        .Select("ConnectedTransportId")
                        .Select("CreatedAt")
                        .Select("UpdatedAt")
                        .Select("IsDeleted")
                        .Select("UpdaterId")
                        .SelectRaw("[CreatedBy] as [CreatorId]")
                        .As("Comment"),
                    j => j.On("Comment.TransportId", "Transport.Id"))
                .LeftJoin(
                    db.Query("Requests")
                        .Select("Id")
                        .Select("Number1C")
                        .As("Request"),
                    j => j.On("Request.Id", "Comment.RequestId"))
                .LeftJoin(
                    db.Query("RequestRoutePoints")
                        .Select(
                            "Id",
                            "Lat",
                            "Lon",
                            "SettlementId",
                            "PointType",
                            "RequestId",
                            "ArrivalDate",
                            "LeaveDate",
                            "ArrivalDateFact",
                            "LeaveDateFact")
                        .SelectRaw("[kRegionId] as [RegionId]")
                        .As("RequestRoutePoint"),
                    j => j.On("Request.Id", "RequestRoutePoint.RequestId"))
                .LeftJoin(
                    db.Query("kRegions")
                        .SelectRaw("[Id] as [ID]")
                        .Select("Title")
                        .As("RequestRegion"),
                    j => j.On("RequestRoutePoint.RegionId", "RequestRegion.ID"))
                .LeftJoin(
                    db.Query("Settlements")
                        .SelectRaw("[Id] as [ID]")
                        .Select("Title")
                        .As("RequestSettlement"),
                    j => j.On("RequestRoutePoint.SettlementId", "RequestSettlement.ID"))
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .WhereTrue("IsActive")
                        .Select("Id", "PersonName", "LastName", "Patronymic")
                        .As("Creator"),
                    j => j.On("Comment.CreatorId", "Creator.Id"))
                .LeftJoin(
                    db.Query("AspNetUsers")
                        .WhereTrue("IsActive")
                        .Select("Id", "PersonName", "LastName", "Patronymic")
                        .As("Updater"),
                    j => j.On("Comment.UpdaterId", "Updater.Id"))
                .Select("Reis.*", "RoutePoints.*", "Region.*", "Settlement.*", "Transport.*", "ConnectedTransport.*",
                    "Driver.*", "Client.*", "Status.*", "Employer.*", "Branch.*", "ParentEmployer.*", "Carrier.*",
                    "Contact.*", "Comment.*", "Request.*", "RequestRoutePoint.*", "RequestRegion.*",
                    "RequestSettlement.*",
                    "Creator.*", "Updater.*");

            var reisDictionary = new Dictionary<Guid, ReisDto>();

            logger.LogInformation($"({db.Compiler.Compile(query)})");

            await db.Connection.QueryAsync(
                db.Compiler.Compile(query).ToString(),
                new[]
                {
                    typeof(ReisDto),
                    typeof(ReisRoutePoint),
                    typeof(Region),
                    typeof(Settlement),
                    typeof(Transport),
                    typeof(Transport),
                    typeof(Driver),
                    typeof(Kontragent),
                    typeof(DAL.Models.Requests.Reis.Dictionaries.ReisStatus),
                    typeof(Employer),
                    typeof(Branch),
                    typeof(Employer),
                    typeof(Kontragent),
                    typeof(KontragentContact),
                    typeof(BundleComment),
                    typeof(KrafterRequest),
                    typeof(KrafterRequestRoutePoint),
                    typeof(Region),
                    typeof(Settlement),
                    typeof(User),
                    typeof(User),
                },
                item =>
                {
                    ReisDto reisEntry;

                    var reis = item[0] as ReisDto;
                    var routePoint = item[1] as ReisRoutePoint;
                    var region = item[2] as Region;
                    var settlement = item[3] as Settlement;
                    var transport = item[4] as Transport;
                    var connectedTransport = item[5] as Transport;
                    var driver = item[6] as Driver;
                    var client = item[7] as Kontragent;
                    var status = item[8] as DAL.Models.Requests.Reis.Dictionaries.ReisStatus;
                    var employer = item[9] as Employer;
                    var branch = item[10] as Branch;
                    var parentEmployer = item[11] as Employer;
                    var carrier = item[12] as Kontragent;
                    var contact = item[13] as KontragentContact;
                    var bundleComment = item[14] as BundleComment;
                    var request = item[15] as KrafterRequest;
                    var requestRoutePoint = item[16] as KrafterRequestRoutePoint;
                    var requestRoutePointRegion = item[17] as Region;
                    var requestRoutePointSettlement = item[18] as Settlement;
                    var creator = item[19] as User;
                    var updater = item[20] as User;

                    if (!reisDictionary.TryGetValue(reis.Id, out reisEntry))
                    {
                        reisEntry = reis;
                        employer.Branch = branch;
                        employer.ParentEmployer = parentEmployer;
                        routePoint.Region = region;
                        routePoint.Settlement = settlement;
                        reisEntry.Client = client;
                        reisEntry.Status = status;
                        reisEntry.PinnedEmployer = employer;
                        reisEntry.Carrier = carrier;
                        reisEntry.Carrier.Contacts = new List<KontragentContact>();
                        reisEntry.Driver = driver;
                        reisEntry.Transport = transport;
                        reisEntry.ConnectedTransport = connectedTransport;
                        reisEntry.RoutePoints = new List<ReisRoutePoint>();
                        reisEntry.BundleComments = new List<BundleComment>();
                        reisEntry.KontragentContact = contact;
                        reisDictionary.Add(reisEntry.Id, reisEntry);
                    }

                    routePoint = reisEntry.RoutePoints.FirstOrDefault(i => i.Id == routePoint.Id);

                    if (routePoint == null)
                    {
                        routePoint = item[1] as ReisRoutePoint;
                        routePoint.Region = region;
                        routePoint.Settlement = settlement;
                        reisEntry.RoutePoints.Add(routePoint);
                    }

                    if (contact != null)
                    {
                        contact = reisEntry.Carrier.Contacts.FirstOrDefault(i => i.KontragentId == carrier.Id);

                        if (contact == null)
                        {
                            contact = item[13] as KontragentContact;
                            reisEntry.Carrier.Contacts.Add(contact);
                        }
                    }

                    if (bundleComment != null)
                    {
                        bundleComment = reisEntry.BundleComments.FirstOrDefault(i => i.Id == bundleComment.Id);

                        if (bundleComment == null)
                        {
                            bundleComment = item[14] as BundleComment;
                            bundleComment.Transport = transport;
                            bundleComment.ConnectedTransport = connectedTransport;
                            bundleComment.Request = request;
                            bundleComment.Creator = creator;
                            bundleComment.Updater = updater;
                            bundleComment.Request.RoutePoints = new List<KrafterRequestRoutePoint>();
                            reisEntry.BundleComments.Add(bundleComment);
                        }

                        requestRoutePoint = bundleComment.Request.RoutePoints
                            .FirstOrDefault(i => i.Id == requestRoutePoint.Id);

                        if (requestRoutePoint == null)
                        {
                            requestRoutePoint = item[16] as KrafterRequestRoutePoint;
                            requestRoutePoint.Region = requestRoutePointRegion;
                            requestRoutePoint.Settlement = requestRoutePointSettlement;
                            bundleComment.Request.RoutePoints.Add(requestRoutePoint);
                        }
                    }

                    return reisEntry;
                });

            var result = reisDictionary.Values.AsQueryable() as IEnumerable<ReisDto>;

            if (requestLat != null && requestLon != null)
            {
                result = result.Select(s =>
                {
                    if (s.BundleComments.Any(c => c.Request.RoutePoints.Count > 2))
                    {
                        s.BundleComments = s.BundleComments.Where(x =>
                            new GeoCoordinate(requestLat.Value, requestLon.Value)
                                .GetDistanceTo(new GeoCoordinate((double) x.Request.Load.Lat,
                                    (double) x.Request.Load.Lon)) / 1000 <= 100).ToList();
                    }

                    return s;
                });
            }

            return result;
        }

        /// <summary>
        ///     CancelledByCarrier Reis.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Cancel")]
        public async Task<IActionResult> PatchCancel([FromODataUri] Guid key, Guid deniedReasonId,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.Reises.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request.Replies)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.StatusId == Cancelled)
            {
                return BadRequest("Невозможно повторно отменить рейс!");
            }

            if (item.RequestId == null)
            {
                return BadRequest("Рейс без заявки!");
            }

            if (!item.Request.Replies.Any())
            {
                return BadRequest("Отклик не существует!");
            }

            if (deniedReasonId == Guid.Empty)
            {
                return BadRequest("Отсутсвует причина!");
            }

            BackgroundJob.Enqueue<ReisService>(i => i.CancelReis(item.Id, deniedReasonId, userId));

            return NoContent();
        }
    }
}