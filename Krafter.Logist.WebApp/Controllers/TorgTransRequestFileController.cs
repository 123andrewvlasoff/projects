﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TorgTransRequestFileController : BaseController<TorgTransRequestFileController>
    {
        private readonly IFileService fileService;

        public TorgTransRequestFileController(ILogger<TorgTransRequestFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Place new File.
        /// </summary>
        /// <response code="204">The File was successfully placed.</response>
        /// <response code="404">The Message does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromQuery] Guid requestId, [FromQuery] DocumentType documentType,
            [FromForm] IFormFile file)
        {
            if (documentType != DocumentType.Proposal)
            {
                ModelState.AddModelError(nameof(documentType), "Невалидный тип документа");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TorgTransRequests
                .IncludeOptimized(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == requestId);

            if (item == null)
            {
                return NotFound();
            }

            var fileName = Guid.NewGuid().ToString();

            await using var stream = file.OpenReadStream();

            await fileService.PutFileAsync(stream, Folders.TorgTrans, fileName);

            item.Files ??= new List<TorgTransRequestFile>();

            item.Files.Add(new TorgTransRequestFile
            {
                CreatedAt = DateTime.Now,
                OriginalName = file.FileName,
                UploaderId = userManager.GetUserId(User),
                ContentType = file.ContentType,
                DocumentType = documentType,
                Path = Folders.TorgTrans + "/" + fileName
            });

            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     Get File.
        /// </summary>
        /// <returns>The requested File.</returns>
        /// <response code="200">The File was successfully retrieved.</response>
        /// <response code="404">The Document does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromODataUri] Guid key, DocumentType documentType)
        {
            var item = await logistDbContext.TorgTransRequestFiles
                .FirstOrDefaultAsync(i => i.Id == key && i.DocumentType == documentType);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }
    }
}