using System;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Driver;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class DriverController : BaseController<DriverController>
    {
        public DriverController(ILogger<DriverController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get Driver by Id.
        /// </summary>
        /// <returns>The requested Driver.</returns>
        /// <response code="200">The Driver was successfully retrieved.</response>
        /// <response code="404">The Driver does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Driver), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.Drivers
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}