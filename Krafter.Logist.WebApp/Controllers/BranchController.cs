﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Kontragents.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class BranchController : BaseController<BranchController>
    {
        public BranchController(ILogger<BranchController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get Branches.
        /// </summary>
        /// <returns>The requested Branches.</returns>
        /// <response code="200">The Branches was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Branch>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<Branch> Get()
        {
            return logistDbContext.Branches.AsQueryable();
        }

        /// <summary>
        ///     Get Branch by Id.
        /// </summary>
        /// <returns>The requested Branch.</returns>
        /// <response code="200">The Branch was successfully retrieved.</response>
        /// <response code="404">The Branch does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Branch), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.Branches.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}