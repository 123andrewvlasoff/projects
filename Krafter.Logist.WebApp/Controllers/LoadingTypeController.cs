﻿using System.Collections.Generic;
using System.Linq;
using DAL.Models.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace Krafter.Logist.WebApp.Controllers
{
    public class LoadingTypeController : BaseController<LoadingTypeController>
    {
        public LoadingTypeController(ILogger<LoadingTypeController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get LoadingTypes.
        /// </summary>
        /// <returns>The requested LoadingType.</returns>
        /// <response code="200">The LoadingType was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<LoadingType>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<LoadingType> Get()
        {
            return logistDbContext.LoadingTypes.AsNoTracking().AsQueryable();
        }

        /// <summary>
        ///     Get LoadingType by Id.
        /// </summary>
        /// <returns>The requested LoadingType.</returns>
        /// <response code="200">The LoadingType was successfully retrieved.</response>
        /// <response code="404">The LoadingType does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(LoadingType), 200)]
        [ProducesResponseType(404)]
        [EnableQuery(AllowedQueryOptions = Select)]
        public SingleResult<LoadingType> Get(int key)
        {
            return SingleResult.Create(logistDbContext.LoadingTypes.AsNoTracking().Where(r => r.Id == key)
                .AsQueryable());
        }
    }
}