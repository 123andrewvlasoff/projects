﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Crafter.Integration.Models.CrafterOnline.Models.Spot;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestReply;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;


namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(TransporeonRequestReply))]
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TransporeonRequestReplyController : BaseController<TransporeonRequestReplyController>
    {
        private readonly SpotService _spotService;
        public TransporeonRequestReplyController(ILogger<TransporeonRequestReplyController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, SpotService spotService) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
            _spotService = spotService;
        }

        /// <summary>
        ///     Make a new TransporeonRequestReply Bid.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("Bid")]
        public async Task<IActionResult> PostBid([FromBody] TransporeonRequestReply transporeonRequestReply,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var emloyer = logistDbContext.Employers.AsNoTracking().FirstOrDefault( x => x.UserId == userId );

            var item = await logistDbContext.TransporeonRequests
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Replies)
                .FirstOrDefaultAsync(i => i.Id == transporeonRequestReply.RequestId,
                    cancellationToken);

            if (item == null || emloyer == null)
            {
                return NotFound();
            }

            if (item.RequestStatus != RequestStatus.IsActive)
            {
                return BadRequest();
            }

            await _spotService.SendCommandAsync(new SpotCommandRequest
            {
                SpotCommand = SpotCommand.TakeSpot,
                ExternalId = item.ExternalId,
                EmployId = emloyer.ExternalId.ToString(),
                EmployEmail = emloyer.Email,
            });

            item.InProgressUserId = userId;
            item.AssignedUserId = userId;
            item.ActualPrice = item.CurrentPrice - item.Step;
            item.BidCount++;
            item.UpdatedAt = DateTime.UtcNow.AddHours(3);

            // @TODO Нужно переделать логику
            if (!item.Replies.Any())
            {
                item.Replies ??= new List<TransporeonRequestReply>();
                item.Replies.Add(new TransporeonRequestReply
                {
                    CreatedAt = DateTime.UtcNow.AddHours(3),
                    Price = item.ActualPrice,
                    CreatorId = userId,
                    Status = 0,
                    AutoBidding = transporeonRequestReply.AutoBidding,
                    MinPrice = transporeonRequestReply.MinPrice
                });
            }
            else
            {
                item.Replies.First().CreatedAt = DateTime.UtcNow.AddHours(3);
                item.Replies.First().Price = item.ActualPrice;
                item.Replies.First().AutoBidding = transporeonRequestReply.AutoBidding;
                item.Replies.First().MinPrice = transporeonRequestReply.MinPrice;
            }

            item.UpdatedAt = DateTime.UtcNow.AddHours(3);
            item.RequestStatus = RequestStatus.IsActive;
            item.AuctionRequestStatus = AuctionRequestStatus.BiddingActive;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(transporeonRequestReply);
        }

        /// <summary>
        ///     Updates an existing TransporeonRequest.
        /// </summary>
        /// <param name="key">The requested TransporeonRequest identifier.</param>
        /// <param name="delta">The partial TransporeonRequest to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The TransporeonRequest was successfully updated.</response>
        /// <response code="400">The TransporeonRequest is invalid.</response>
        /// <response code="404">The TransporeonRequest does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransporeonRequestReply), 200)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<TransporeonRequestReply> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TransporeonRequestReplies.IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.RequestStatus != RequestStatus.IsActive &&
                item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.Error &&
                item.Request.RequestStatus != RequestStatus.DataTreatment)
            {
                return BadRequest();
            }

            delta.Patch(item);
            // Выбрали исполнителя, кнопка Продолжить не активна
            item.Request.AuctionRequestStatus = AuctionRequestStatus.ResourceTreatment;
            item.Request.RequestStatus = RequestStatus.DataTreatment;


            // Выбрали исполнителя, активируем кнопку Продолжить
            item.Request.AuctionRequestStatus = AuctionRequestStatus.DocSignInput;
            item.Request.RequestStatus = RequestStatus.DataInput;


            await logistDbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendTransporeonRequestReply(item.Id, userId));

            return Updated(item);
        }
    }
}
