﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestRoutePointDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class AuctionRequestRoutePointDtoController : BaseController<AuctionRequestRoutePointDtoController>
    {
        private readonly RequestDtoRepository repository;

        public AuctionRequestRoutePointDtoController(ILogger<AuctionRequestRoutePointDtoController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            RequestDtoRepository repository) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
            this.repository = repository;
        }

        /// <summary>
        ///     Get AuctionRequestRoutePointDto.
        /// </summary>
        /// <returns>The requested AuctionRequestRoutePointDto.</returns>
        /// <response code="200">The AuctionRequestRoutePointDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<AuctionRequestRoutePointDto>), StatusCodes.Status200OK)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<AuctionRequestRoutePointDto>> Get()
        {
            var items = await repository.GetAsync();

            return items.SelectMany(i => i.RoutePoints).OfType<AuctionRequestRoutePointDto>();
        }
    }
}