using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.BundleComment;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class BundleCommentController : BaseController<BundleCommentController>
    {
        public BundleCommentController(ILogger<BundleCommentController> logger,
            LogistDbContext securityDbContext, UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor) : base(logger,
            securityDbContext, userManager, httpContextAccessor)
        {
        }


        /// <summary>
        ///     Places a new BundleComment.
        /// </summary>
        /// <param name="item">The BundleComment to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created BundleComment.</returns>
        /// <response code="201">The BundleComment was successfully placed.</response>
        /// <response code="400">The DriverDraftComment is invalid.</response>
        [ProducesResponseType(typeof(BundleComment), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] BundleComment item, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.CreatedAt = DateTime.Now;
            item.CreatorId = userId;

            await logistDbContext.BundleComments.AddAsync(item, cancellationToken);
            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(item);
        }

        /// <summary>
        ///     Updates an existing BundleComment.
        /// </summary>
        /// <param name="key">The requested BundleComment identifier.</param>
        /// <param name="delta">The partial BundleComment to update.</param>
        /// <param name="cancellationToken">  </param>
        /// <returns>The created order.</returns>
        /// <response code="200">The BundleComment was successfully updated.</response>
        /// <response code="400">The BundleComment is invalid.</response>
        /// <response code="404">The BundleComment does not exist.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<BundleComment> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (delta.GetChangedPropertyNames().Contains(nameof(BundleComment.RequestId)))
            {
                ModelState.AddModelError(nameof(BundleComment.Request), "Запрещено менять привязанное задание!");
            }

            if (delta.GetChangedPropertyNames().Contains(nameof(BundleComment.TransportId)))
            {
                ModelState.AddModelError(nameof(BundleComment.Transport), "Запрещено менять главное ТС!");
            }

            if (delta.GetChangedPropertyNames().Contains(nameof(BundleComment.ConnectedTransportId)))
            {
                ModelState.AddModelError(nameof(BundleComment.ConnectedTransport), "Запрещено менять прицеп!");
            }

            var item = await logistDbContext.BundleComments
                .IncludeOptimized(i =>
                    i.Transport.TransportKontragents.Select(s =>
                        s.Kontragent.KontragentUsers.Select(s => s.User.Employer)))
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.IsDeleted)
            {
                BadRequest("Комментарий удален!");
            }

            if (item.CreatorId != userId && item.Transport.TransportKontragents.FirstOrDefault()?.Kontragent
                    ?.KontragentUsers.FirstOrDefault(x => x.RelationType == RelationType.Pinned)?.User?.Employer
                    .UserId != userId)
            {
                BadRequest("Вы не являетесь автором комментария или ответственным логистом!");
            }

            delta.Patch(item);

            item.UpdaterId = userId;
            item.UpdatedAt = DateTime.Now;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     Delete a BundleComment
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The BundleComment was successfully deleted.</response>
        /// <response code="404">The BundleComment not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.BundleComments.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Transport.TransportKontragents
                    .Select(s => s.Kontragent.KontragentUsers))
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.IsDeleted)
            {
                BadRequest("Комментарий удален!");
            }

            if (item.CreatorId != userId && item.Transport.TransportKontragents.FirstOrDefault()?.Kontragent
                    ?.KontragentUsers.FirstOrDefault(x => x.RelationType == RelationType.Pinned)?.User?.Employer
                    .UserId != userId)
            {
                BadRequest("Вы не являетесь автором комментария или ответсвенным логистом данного контрагента!");
            }

            item.IsDeleted = true;
            item.UpdaterId = userId;
            item.UpdatedAt = DateTime.Now;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}