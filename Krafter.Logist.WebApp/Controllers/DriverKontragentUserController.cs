using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.DriverKontragent;
using Krafter.Logist.WebApp.Models.Basic.DriverKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(DriverKontragentUser))]
    public class DriverKontragentUserController : BaseController<DriverKontragentUserController>
    {
        public DriverKontragentUserController(ILogger<DriverKontragentUserController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get DriverKontragentUsers.
        /// </summary>
        /// <returns>The requested DriverKontragentUsers.</returns>
        /// <response code="200">The DriverKontragentUsers was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<DriverKontragentUser>), StatusCodes.Status200OK)]
        [ExpandIgnoreEnableQuery(HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<DriverKontragentUser> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            return logistDbContext.DriverKontragents
                .IncludeOptimized(i => i.Driver)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.DriverKontragentUsers)
                .Where(i => i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                .Count(ku => ku.RelationType.HasFlag(RelationType.Pinned)) > 0 ||
                            i.DriverKontragentUsers.Count > 0)
                .Select(i => new DriverKontragentUser()
                {
                    Id = i.DriverKontragentUsers.Select(tku => tku.Id).FirstOrDefault(),
                    Comment = i.DriverKontragentUsers.Select(tku => tku.Comment).FirstOrDefault(),
                    RelationType = i.DriverKontragentUsers.Select(tku => tku.RelationType).FirstOrDefault()
                                   | i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                       .Where(ku => ku.RelationType.HasFlag(RelationType.Pinned))
                                       .Select(ku => RelationType.Pinned)
                                       .FirstOrDefault(),
                    DriverKontragent = new DriverKontragent()
                    {
                        Id = i.Id,
                        Kontragent = i.Kontragent,
                        KontragentId = i.KontragentId,
                        Driver = i.Driver,
                        DriverId = i.DriverId
                    },
                    DriverKontragentId = i.Id,
                    UserId = userId
                    // Поправить Unable to cast object of type 'System.String' to type 'Microsoft.AspNet.OData.Query.Expressions.GroupByWrapper'.
                }).ToList().AsQueryable();
        }

        /// <summary>
        ///     add all DriverKontragentUser to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created DriverKontragentUser.</returns>
        /// <response code="200">The DriverKontragentUser was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> PostAllToFavorite(Guid key)
        {
            var userId = userManager.GetUserId(User);

            var ids = logistDbContext.DriverKontragents
                .IncludeOptimized(i => i.DriverKontragentUsers)
                .Where(i => i.KontragentId == key && !i.DriverKontragentUsers.Any())
                .Select(i => i.Id)
                .AsAsyncEnumerable();

            var items = new List<DriverKontragentUser>();

            await foreach (var id in ids)
            {
                items.Add(new DriverKontragentUser
                {
                    RelationType = RelationType.Favorite,
                    DriverKontragentId = id,
                    UserId = userId
                });
            }

            await logistDbContext.DriverKontragentUsers.AddRangeAsync(items);
            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     delete all DriverKontragentUsers to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created DriverKontragentUsers.</returns>
        /// <response code="200">The DriverKontragentUsers was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> DeleteAllToFavorite(Guid key)
        {
            var items = await logistDbContext.DriverKontragentUsers
                .IncludeOptimized(i => i.DriverKontragent)
                .Where(i => i.DriverKontragent.KontragentId == key)
                .ToListAsync();

            logistDbContext.DriverKontragentUsers.RemoveRange(items);
            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     Places a new DriverKontragentUser.
        /// </summary>
        /// <param name="item">The DriverKontragentUser to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created DriverKontragentUser.</returns>
        /// <response code="201">The DriverKontragentUser was successfully placed.</response>
        /// <response code="400">The DriverKontragentUser is invalid.</response>
        [ProducesResponseType(typeof(DriverKontragentUser), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] DriverKontragentUser item,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.UserId = userManager.GetUserId(User);
            item.RelationType = RelationType.Favorite;

            await logistDbContext.DriverKontragentUsers.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(item);
        }

        /// <summary>
        ///     Updates an existing DriverKontragentUser.
        /// </summary>
        /// <param name="key">The requested DriverKontragentUser identifier.</param>
        /// <param name="delta">The partial DriverKontragentUser to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="204">The DriverKontragentUser was successfully updated.</response>
        /// <response code="400">The DriverKontragentUser is invalid.</response>
        /// <response code="404">The DriverKontragentUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(DriverKontragentUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<DriverKontragentUser> delta,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.DriverKontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            item.Comment = delta.GetInstance().Comment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }

        /// <summary>
        ///     delete an DriverKontragentUser.
        /// </summary>
        /// <param name="key">The DriverKontragentUser to cancel.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>None</returns>
        /// <response code="204">The DriverKontragentUser was successfully deleted.</response>
        /// <response code="404">The DriverKontragentUser does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item = await logistDbContext.DriverKontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.DriverKontragentUsers.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}