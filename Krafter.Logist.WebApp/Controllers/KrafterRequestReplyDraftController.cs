using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Requests.Krafter;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyDraft;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class KrafterRequestReplyDraftController : BaseController<KrafterRequestReplyDraftController>
    {
        public KrafterRequestReplyDraftController(ILogger<KrafterRequestReplyDraftController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get KrafterRequestReplyDraft.
        /// </summary>
        /// <returns>The requested KrafterRequestReplyDraft.</returns>
        /// <response code="200">The KrafterRequestReplyDraft was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<KrafterRequestReplyDraft>), StatusCodes.Status200OK)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<KrafterRequestReplyDraft> Get()
        {
            return logistDbContext.KrafterRequestReplyDrafts;
        }

        /// <summary>
        ///     Places a new KrafterRequestReplyDraft.
        /// </summary>
        /// <param name="item">The KrafterRequestReplyDraft to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created KrafterRequestReplyDraft.</returns>
        /// <response code="201">The KrafterRequestReplyDraft was successfully placed.</response>
        /// <response code="400">The KrafterRequestReplyDraft is invalid.</response>
        [ProducesResponseType(typeof(RequestReplyDraft), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [EnableQuery]
        public async Task<IActionResult> Post([FromBody] KrafterRequestReplyDraft item,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var request =
                await logistDbContext.KrafterRequests.FirstOrDefaultAsync(i => i.Id == item.RequestId,
                    cancellationToken);

            if (request == null)
            {
                return BadRequest(ModelState);
            }

            if (request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await logistDbContext.KrafterRequestReplyDrafts.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            await logistDbContext.Entry(item).Reference(i => i.Transport).LoadAsync(cancellationToken);
            await logistDbContext.Entry(item.Transport).Reference(i => i.RollingStockType).LoadAsync(cancellationToken);
            await logistDbContext.Entry(item.Transport).Reference(i => i.TransportType).LoadAsync(cancellationToken);

            await logistDbContext.Entry(item).Reference(i => i.Kontragent).LoadAsync(cancellationToken);

            if (item.ConnectedTransportId.HasValue)
            {
                await logistDbContext.Entry(item).Reference(i => i.ConnectedTransport).LoadAsync(cancellationToken);
                await logistDbContext.Entry(item.ConnectedTransport).Reference(i => i.RollingStockType)
                    .LoadAsync(cancellationToken);
                await logistDbContext.Entry(item.ConnectedTransport).Reference(i => i.TransportType)
                    .LoadAsync(cancellationToken);
            }

            if (item.KontragentContactId.HasValue)
            {
                await logistDbContext.Entry(item).Reference(i => i.KontragentContact).LoadAsync(cancellationToken);
            }

            if (item.DriverId.HasValue)
            {
                await logistDbContext.Entry(item).Reference(i => i.Driver).LoadAsync(cancellationToken);
            }

            logger.LogInformation($"Created request reply draft: id - {item.Id}");

            return StatusCode(StatusCodes.Status201Created, item);
        }

        /// <summary>
        ///     Updates an existing KrafterRequestReplyDraft.
        /// </summary>
        /// <param name="key">The requested KrafterRequestReplyDraft identifier.</param>
        /// <param name="delta">The partial KrafterRequestReplyDraft to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The KrafterRequestReplyDraft was successfully updated.</response>
        /// <response code="400">The KrafterRequestReplyDraft is invalid.</response>
        /// <response code="404">The KrafterRequestReplyDraft does not exist.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<KrafterRequestReplyDraft> delta,
            CancellationToken cancellationToken)
        {
            if (delta.GetChangedPropertyNames().Contains(nameof(KrafterRequestReplyDraft.RequestId)))
            {
                ModelState.AddModelError(nameof(RequestReplyDraft.RequestId),
                    "Запрещено менять привязанную к черновику заявку");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KrafterRequestReplyDrafts.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            delta.Patch(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }

        /// <summary>
        ///     Deletes a KrafterRequestReplyDraft
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The KrafterRequestReplyDraft was successfully deleted.</response>
        /// <response code="404">The KrafterRequestReplyDraft not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item = await logistDbContext.KrafterRequestReplyDrafts
                .FirstOrDefaultAsync(ur => ur.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.KrafterRequestReplyDrafts.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}