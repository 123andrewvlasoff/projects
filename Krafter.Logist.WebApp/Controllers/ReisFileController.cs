using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.ReisFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(ReisFile))]
    public class ReisFileController : BaseController<ReisFileController>
    {
        private readonly IFileService fileService;

        public ReisFileController(ILogger<ReisFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Get ReisFile.
        /// </summary>
        /// <returns>The requested ReisFile.</returns>
        /// <response code="200">The ReisFile was successfully retrieved.</response>
        /// <response code="404">The ReisFile does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(ActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.Reises
                .IncludeOptimized(i => i.Files)
                .SelectMany(i => i.Files)
                .FirstOrDefaultAsync(x => x.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            await using var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }
    }
}