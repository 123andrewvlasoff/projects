using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Crafter.CarrierOffer.WebApp.Services;
using Crafter.Client.CIS.Clients;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Actions;
using DAL.Models.Requests.Krafter.Enums;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.DeniedReason;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply.Enums;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyAction;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly.ActionDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestActionDto;
using Krafter.Logist.WebApp.Services;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Services.ChangesTrackService.Helpers;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using DocumentType = DAL.Models.Enums.DocumentType;
using RequestStatus = DAL.Models.Requests.RequestStatus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(KrafterRequestReply))]
    [Authorize(Policy = UserPolicies.RequestsAccess)]
    public class KrafterRequestReplyController : BaseController<KrafterRequestReplyController>
    {
        private readonly KrafterRequestReplyService requestReplyService;

        private readonly KrafterRequestReplyValidator validator;
        private readonly IKrafterExternalClient krafterExternalClient;
        private readonly IMapper mapper;
        private readonly ChangesTrackService changesTrackService;
        private readonly ICarrierOfferService carrierOfferService;

        public KrafterRequestReplyController(ILogger<KrafterRequestReplyController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            KrafterRequestReplyService krafterRequestReplyService, KrafterRequestReplyValidator validator,
            IKrafterExternalClient krafterExternalClient, IMapper mapper,
            ChangesTrackService changesTrackService, ICarrierOfferService carrierOfferService) : base(
            logger,
            logistDbContext, userManager,
            httpContextAccessor)
        {
            this.requestReplyService = krafterRequestReplyService;
            this.validator = validator;
            this.krafterExternalClient = krafterExternalClient;
            this.mapper = mapper;
            this.changesTrackService = changesTrackService;
            this.carrierOfferService = carrierOfferService;
        }

        /// <summary>
        ///     Places a new KrafterRequestReply.
        /// </summary>
        /// <param name="item">The KrafterRequestReply to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created KrafterRequestReply.</returns>
        /// <response code="201">The KrafterRequestReply was successfully placed.</response>
        /// <response code="400">The KrafterRequestReply is invalid.</response>
        [ProducesResponseType(typeof(KrafterRequestReply), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] KrafterRequestReply item, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var request = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Replies)
                .IncludeOptimized(i => i.InProgressUser)
                .FirstOrDefaultAsync(i => i.Id == item.RequestId, cancellationToken);

            if (request == null)
            {
                return NotFound("Не найдена заявка");
            }

            var results = await validator.ValidateAsync(item, cancellationToken);

            if (!results.IsValid)
            {
                return BadRequest(results.Errors.Select(s => s.ErrorMessage));
            }

            if (request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            if (!request.IsActive && request.InProgressUserId != request.AssignedUserId)
            {
                return NotFound("Заявка не активна!");
            }

            if (request.RequestStatus != RequestStatus.IsActive &&
                request.RequestStatus != RequestStatus.Error &&
                !(request.RequestStatus == RequestStatus.DataInput &&
                  request.KrafterRequestStatus == KrafterRequestStatus.CancelledByCarrier)
               )
            {
                return BadRequest("Ошибка. Некорректный статус заявки!");
            }

            item.RequestId = request.Id;
            item.CreatorId = userId;
            item.UpdaterId = userId;
            item.IsAccepted = false;
            item.CreatedAt = DateTime.Now;
            item.UpdatedAt = DateTime.Now;

            request.RequestStatus = RequestStatus.DataTreatment;
            request.KrafterRequestStatus = KrafterRequestStatus.PrepareDocTreatment;

            await logistDbContext.KrafterRequestReplies.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesWithTrackingAsync<KrafterRequestReply>(cancellationToken).ContinueWith(t =>
            {
                BackgroundJob.Enqueue<KrafterRequestReplyService>(i =>
                    i.PrepareRequestReplyDocument(item.Id, userId, RequestReplyDocumentType.Create));

                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                    i.TrackRequestReplyAsync(item.Id, RequestActionStatus.Verification, t.Result, userId));
            }, cancellationToken);

            logger.LogInformation($"Created request reply: id - {item.Id}");

            return Created(item);
        }

        /// <summary>
        ///     SendToSign request.
        /// </summary>
        [ProducesResponseType(typeof(KrafterRequestReply), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ODataRoute("({key})/SendToSign")]
        public async Task<IActionResult> PatchSendToSign(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Request)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound("Не найдена заявка");
            }

            if (item.Request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            if (item.Request.KrafterRequestStatus != KrafterRequestStatus.Verification)
            {
                return BadRequest("Ошибка. Некорректный статус заявки!");
            }


            item.UpdatedAt = DateTime.Now;
            item.IsAccepted = true;
            item.Request.KrafterRequestStatus = KrafterRequestStatus.DocSignInput;
            item.Request.RequestStatus = RequestStatus.DataInput;

            logistDbContext.KrafterRequestReplies.Update(item);

            await logistDbContext.SaveChangesWithTrackingAsync<KrafterRequestReply>(cancellationToken).ContinueWith(t =>
            {
                BackgroundJob.Enqueue<KrafterRequestReplyService>(i => i.SendRequestReplyEmails(item.Id));

                BackgroundJob.Enqueue<ChangesTrackService>(i =>
                    i.TrackRequestReplyAsync(item.Id, RequestActionStatus.SignWaiting, t.Result, userId));

                if (item.Kontragent.KontragentUsers.FirstOrDefault() != null)
                {
                    BackgroundJob.Enqueue<NotificationsService>(i =>
                        i.AddRequestReplyReceivedNotification(item.RequestId,
                            item.Kontragent.KontragentUsers.First().UserId));
                }
            }, cancellationToken);

            await carrierOfferService.RejectAnotherCarrierOffers(item.RequestId, cancellationToken);

            logger.LogInformation($"Created request reply: id - {item.Id}");

            return Updated(item);
        }

        /// <summary>
        ///     ReloadByLogist from 1C request.
        /// </summary>
        [ProducesResponseType(typeof(RequestReply), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ODataRoute("({key})/Reload")]
        public async Task<IActionResult> PatchReload(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound("Не найдена заявка");
            }

            if (item.Request.KrafterRequestStatus != KrafterRequestStatus.Verification)
            {
                return BadRequest("Ошибка. Некорректный статус заявки!");
            }

            if (item.Request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            await requestReplyService.ReloadReisInfoFrom1C(key, userId);

            return Ok();
        }

        /// <summary>
        /// Cancel request
        /// 
        /// </summary>
        [ProducesResponseType(typeof(RequestReply), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(rr => rr.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound("Не найден отклик");
            }

            if (item.Request.KrafterRequestStatus != KrafterRequestStatus.Verification)
            {
                return BadRequest("Ошибка. Некорректный статус заявки!");
            }

            if (item.Request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            BackgroundJob.Enqueue<KrafterRequestReplyService>(i => i.DeleteRequestReply(item.Id, userId));

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Ok();
        }

        /// <summary>
        ///     Updates an existing KrafterRequestReply.
        /// </summary>
        /// <param name="key">The requested KrafterRequestReply identifier.</param>
        /// <param name="delta">The partial KrafterRequestReply to update.</param>
        /// <param name="cancellationToken">  </param>
        /// <returns>The created order.</returns>
        /// <response code="200">The KrafterRequestReply was successfully updated.</response>
        /// <response code="400">The KrafterRequestReply is invalid.</response>
        /// <response code="404">The KrafterRequestReply does not exist.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<KrafterRequestReply> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (delta.GetChangedPropertyNames().Contains(nameof(KrafterRequestReply.RequestId)))
            {
                ModelState.AddModelError(nameof(KrafterRequestReply.Request), "Запрещено менять привязанное задание");
            }

            var item = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Request)
                .IncludeOptimized(i => i.Comments)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.InProgressUserId != userId)
            {
                return BadRequest("Ошибка. Заявка не взята в работу!");
            }

            if (item.Request.RequestStatus != RequestStatus.IsActive &&
                item.Request.RequestStatus != RequestStatus.Error &&
                item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.DataTreatment)
            {
                return BadRequest(ModelState);
            }

            item.Request.RequestStatus = RequestStatus.DataTreatment;
            item.Request.KrafterRequestStatus = KrafterRequestStatus.PrepareDocTreatment;
            item.IsAccepted = false;
            item.UpdatedAt = DateTime.Now;

            if (delta.GetChangedPropertyNames().Contains(nameof(KrafterRequestReply.KontragentId)))
            {
                foreach (var i in item.Comments)
                {
                    i.IsDeleted = true;
                }
            }

            delta.Patch(item);

            var results = await validator.ValidateAsync(item, cancellationToken);

            if (!results.IsValid)
            {
                return BadRequest(results.Errors.Select(s => s.ErrorMessage));
            }

            await logistDbContext.SaveChangesWithTrackingAsync<KrafterRequestReply>(cancellationToken)
                .ContinueWith(t =>
                {
                    BackgroundJob.Enqueue<KrafterRequestReplyService>(i =>
                        i.PrepareRequestReplyDocument(item.Id, userId, RequestReplyDocumentType.Update));

                    BackgroundJob.Enqueue<ChangesTrackService>(i =>
                        i.TrackRequestReplyAsync(item.Id, RequestActionStatus.Verification, t.Result, userId));
                }, cancellationToken);

            logger.LogInformation($"Edited request reply: id - {item.Id}");

            return Updated(item);
        }

        /// <summary>
        ///     AcceptByLogist Request.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Accept")]
        public async Task<IActionResult> PatchAccept(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Request)
                .IncludeOptimized(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == key
                                          && (i.Request.AssignedUserId == userId ||
                                              i.CreatorId == userId), cancellationToken);


            if (item == null)
            {
                return NotFound();
            }

            var file = item.Files.FirstOrDefault(i => i.DocumentType == DocumentType.RequestContractTemplate);

            if (file == null)
            {
                return BadRequest();
            }

            BackgroundJob.Enqueue<KrafterRequestReplyService>(i => i.AcceptRequestReplyDocument(item.Id, userId));

            return NoContent();
        }

        /// <summary>
        ///     DeniedByLogist Request.
        /// </summary>
        [ProducesResponseType(typeof(KrafterRequestReplyAction), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Denied")]
        public async Task<IActionResult> PatchDenied(Guid key, CancellationToken cancellationToken)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KrafterRequestReplies
                .IncludeOptimized(i => i.Request)
                .IncludeOptimized(r => r.Request.RoutePoints)
                .IncludeOptimized(r => r.Request.InProgressUser)
                .IncludeOptimized(r => r.Request.AssignedUser)
                .IncludeOptimized(i => i.KontragentContact)
                .IncludeOptimized(i => i.Kontragent)
                .IncludeOptimized(i => i.Request.Replies)
                .IncludeOptimized(i => i.Creator)
                .IncludeOptimized(i => i.Creator.KontragentUsers)
                .IncludeOptimized(i => i.Creator.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.Kontragent.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.Files.Where(x => x.DocumentType == DocumentType.RequestContractTemplate))
                .FirstOrDefaultAsync(i =>
                    i.Id == key && (i.CreatorId == userId || i.Request.InProgressUserId == userId), cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            var mappedItem = mapper.Map<KrafterRequestReply>(item, options => options.Items.Add("UserId", userId));


            await logistDbContext.SaveChangesAsync(cancellationToken)
                .ContinueWith(t =>
                {
                    if (!mappedItem.CreatedByCarrier && item.RequestReplyType == RequestReplyType.Paper)
                    {
                        BackgroundJob.Enqueue<KrafterRequestReplyService>(r =>
                            r.CancelRequestReplyDocument(item.Id, userId,
                                ReplyDeniedReasonType.ByLogistDecisionLogistCreatedReply));
                    }
                    else
                    {
                        BackgroundJob.Enqueue<KrafterRequestReplyService>(r =>
                            r.CancelRequestReplyDocument(item.Id, userId, ReplyDeniedReasonType.ByLogistDecision));
                    }

                    foreach (var userId in new[] { item.Request.AssignedUserId, item.Request.Replies.First().CreatorId }.Distinct())
                    {
                        BackgroundJob.Enqueue<NotificationsService>(i =>
                            i.AddRequestReplyDeniedByLogistNotification(item.Id, userId));
                    }
                }, cancellationToken);

            var action = await changesTrackService.TrackRequestReplyAsync(item.Id,
                RequestActionStatus.LogistAcceptRefused,
                ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item)), userId);

            return Ok(mapper.Map<MinimalRequestActionDto>(action));
        }

        /// <summary>
        ///     RejectFileByLogist Request.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/RejectFile")]
        public async Task<IActionResult> PatchRejectFile(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);
            var item = await logistDbContext.KrafterRequestReplies
                .FirstOrDefaultAsync(i =>
                    i.Id == key && (i.CreatorId == userId || i.Request.InProgressUserId == userId), cancellationToken);

            if (item == null)
                return NotFound();
            if (!item.CreatedByCarrier && item.RequestReplyType == RequestReplyType.Paper)
                BackgroundJob.Enqueue<KrafterRequestReplyService>(r =>
                    r.RejectFileRequestReplyDocument(item.Id, userId));
            return Ok();
        }
    }
}