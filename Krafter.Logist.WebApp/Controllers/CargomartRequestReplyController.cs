﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestReply;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(CargomartRequestReply))]
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class CargomartRequestReplyController : BaseController<CargomartRequestReplyController>
    {
        public CargomartRequestReplyController(ILogger<CargomartRequestReplyController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Make a new CargomartRequestReply Bid.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("Bid")]
        public async Task<IActionResult> PostBid([FromBody] CargomartRequestReply cargomartRequestReply,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.CargomartRequests
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Replies)
                .FirstOrDefaultAsync(i => i.Id == cargomartRequestReply.RequestId,
                    cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.RequestStatus != RequestStatus.IsActive)
            {
                return BadRequest();
            }

            if (item.Replies.Any(x => x.Price < item.CurrentPrice))
            {
                return BadRequest();
            }

            if (item.CurrentPrice - item.Step < item.MinPrice)
            {
                return BadRequest();
            }

            if (item.Replies.Any(a => a.AutoBidding == 1 && a.CreatorId != userId))
            {
                return BadRequest("Автоторг включен у другого пользователя");
            }

            if (cargomartRequestReply.MinPrice != null && cargomartRequestReply.MinPrice <= item.MinPrice)
            {
                return BadRequest("Превышен минимальный порог для данного пользователя");
            }

            item.InProgressUserId = userId;
            item.ActualPrice = item.CurrentPrice - item.Step;
            item.BidCount++;
            item.UpdatedAt = DateTime.Now;

            // @TODO Нужно переделать логику
            if (!item.Replies.Any())
            {
                item.Replies ??= new List<CargomartRequestReply>();
                item.Replies.Add(new CargomartRequestReply
                {
                    CreatedAt = DateTime.Now,
                    Price = item.ActualPrice,
                    CreatorId = userId,
                    Status = 0,
                    AutoBidding = cargomartRequestReply.AutoBidding,
                    MinPrice = cargomartRequestReply.MinPrice
                });
            }
            else
            {
                item.Replies.First().CreatedAt = DateTime.Now;
                item.Replies.First().Price = item.ActualPrice;
                item.Replies.First().AutoBidding = cargomartRequestReply.AutoBidding;
                item.Replies.First().MinPrice = cargomartRequestReply.MinPrice;
            }

            item.UpdatedAt = DateTime.Now;
            item.RequestStatus = RequestStatus.IsActive;
            item.AuctionRequestStatus = AuctionRequestStatus.BiddingActive;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(cargomartRequestReply);
        }

        /// <summary>
        ///     Updates an existing CargomartRequest.
        /// </summary>
        /// <param name="key">The requested CargomartRequest identifier.</param>
        /// <param name="delta">The partial CargomartRequest to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The CargomartRequest was successfully updated.</response>
        /// <response code="400">The CargomartRequest is invalid.</response>
        /// <response code="404">The CargomartRequest does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(CargomartRequestReply), 200)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<CargomartRequestReply> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.CargomartRequestReplies.IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.RequestStatus != RequestStatus.IsActive &&
                item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.Error &&
                item.Request.RequestStatus != RequestStatus.DataTreatment)
            {
                return BadRequest();
            }

            delta.Patch(item);

            item.Request.AuctionRequestStatus = AuctionRequestStatus.ResourceTreatment;
            item.Request.RequestStatus = RequestStatus.DataTreatment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendCargomartRequestReply(item.Id, userId));

            return Updated(item);
        }
    }
}