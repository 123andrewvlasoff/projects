using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.KontragentUser.Enums;
using DelegateDecompiler;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(Transport))]
    public partial class TransportController : BaseController<TransportController>
    {
        private readonly RequestDtoRepository repository;

        public TransportController(ILogger<TransportController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, RequestDtoRepository repository) :
            base(logger, logistDbContext, userManager,
                httpContextAccessor)
        {
            this.repository = repository;
        }

        /// <summary>
        ///     Get Transport by Id.
        /// </summary>
        /// <returns>The requested Transport.</returns>
        /// <response code="200">The Transport was successfully retrieved.</response>
        /// <response code="404">The Transport does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Transport), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<Transport> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return await logistDbContext.Transports
                .IncludeOptimized(i => i.TransportType)
                .IncludeOptimized(i => i.RollingStockType)
                .FirstOrDefaultAsync(r => r.Id == key);
        }

        /// <summary>
        ///     Get Transports.
        /// </summary>
        /// <returns>The requested Transports.</returns>
        /// <response code="200">The Transports was successfully retrieved.</response>
        /// <response code="404">The Transports does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Transport>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public IQueryable<Transport> Get()
        {
            return logistDbContext.Transports.AsNoTracking().Decompile();
        }

        /// <summary>
        ///     Patch TransportLoadInfo.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<Transport> delta, CancellationToken cancellationToken)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var allowedProperties = new[]
            {
                nameof(Transport.LastReisLon),
                nameof(Transport.LastReisLat),
                nameof(Transport.LastAddress),
                nameof(Transport.LastReisDate),
                nameof(Transport.ConnectedTransportId)
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = logistDbContext.Transports.IgnoreQueryFilters()
                .IncludeOptimized(i => i.TransportKontragents.Select(s => s.Kontragent.KontragentUsers))
                .IncludeOptimized(i => i.Reises.Select(s => s.RoutePoints))
                .Computed()
                .FirstOrDefault(i => i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            if (!delta.GetChangedPropertyNames().All(a => allowedProperties.Contains(a)))
            {
                return BadRequest("Запрет на редактирование свойства");
            }

            delta.Patch(item);

            if (delta.GetChangedPropertyNames().Contains(nameof(Transport.LastReisDate)) &&
                item.LastReisDate < item.LastReis?.Unload?.LeaveDateFact)
            {
                return BadRequest("Введена некорректная дата");
            }

            await logistDbContext.SaveChangesAsync(cancellationToken);

            var ku = item.TransportKontragents
                .SelectMany(s => s.Kontragent.KontragentUsers)
                .FirstOrDefault(x => x.Discriminator == KontragentUserType.Carrier);

            if (ku != null)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddChangeUnloadInfoNotification(item.Id, userId, ku.UserId));
            }

            var mainLogist = item.TransportKontragents
                .SelectMany(s => s.Kontragent.KontragentUsers)
                .FirstOrDefault(x => x.Discriminator == KontragentUserType.Logist && x.RelationType == DAL.Models.Enums.RelationType.Pinned);

            if (mainLogist != null && userId != mainLogist.UserId)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddChangeUnloadInfoNotificationLogist(item.Id, userId, mainLogist.UserId));
            }

            BackgroundJob.Schedule<TransportService>(i => i.CleanUnloadInfo(item.Id),
                TimeSpan.FromHours((item.LastReisDate.Value - DateTime.Now).TotalHours + 48));

            return Updated(item);
        }
    }
}