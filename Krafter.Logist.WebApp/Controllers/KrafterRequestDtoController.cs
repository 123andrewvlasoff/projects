﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Requests;
using DAL.Models.Requests.Krafter.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.RequestsAccess)]
    [ODataRoutePrefix(nameof(KrafterRequestDto))]
    public class KrafterRequestDtoController : BaseController<KrafterRequestDtoController>
    {
        private readonly RequestDtoRepository repository;
        private readonly IMapper mapper;
        private readonly LogistReadOnlyDbContext logistReadOnlyDbContext;

        public KrafterRequestDtoController(ILogger<KrafterRequestDtoController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            RequestDtoRepository repository, IMapper mapper, LogistReadOnlyDbContext logistReadOnlyDbContext) : base(
            logger,
            logistDbContext, userManager, httpContextAccessor)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.logistReadOnlyDbContext = logistReadOnlyDbContext;
        }

        /// <summary>
        ///     Get KrafterRequestDto.
        /// </summary>
        /// <returns>The requested KrafterRequestDto.</returns>
        /// <response code="200">The KrafterRequestDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<KrafterRequestDto>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<KrafterRequestDto>> Get()
        {
            var userId = userManager.GetUserId(User);

            var items = await repository.GetAsync();

            items = items.OfType<KrafterRequestDto>().Where(i =>
                i.RequestStatus == RequestStatus.IsActive ||
                (i.RequestStatus == RequestStatus.DataTreatment ||
                 i.RequestStatus == RequestStatus.DataInput ||
                 i.RequestStatus == RequestStatus.Cancelled ||
                 i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

            return mapper.Map<IEnumerable<KrafterRequestDto>>(items, options => options.Items.Add("UserId", userId));
        }
    }
}