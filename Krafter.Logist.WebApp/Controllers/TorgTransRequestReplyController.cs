﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestReply;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    [ODataRoutePrefix(nameof(TorgTransRequestReply))]
    public class TorgTransRequestReplyController : BaseController<TorgTransRequestReplyController>
    {
        public TorgTransRequestReplyController(ILogger<TorgTransRequestReplyController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Make a new TorgTransRequestReply Bid.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("Bid")]
        public async Task<IActionResult> PostBid([FromBody] TorgTransRequestReply torgTransRequestReply,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.TorgTransRequests
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Replies)
                .FirstOrDefaultAsync(i => i.Id == torgTransRequestReply.RequestId,
                    cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.RequestStatus == RequestStatus.IsActive && item.ActiveUntil < DateTime.Now)
            {
                return BadRequest();
            }

            if (item.Replies.Any(x => x.Price < item.CurrentPrice))
            {
                return BadRequest();
            }

            if (item.CurrentPrice - item.Step < item.MinPrice)
            {
                return BadRequest();
            }

            if (torgTransRequestReply.MinPrice != null &&
                torgTransRequestReply.AutoBidding != null &&
                torgTransRequestReply.AutoBidding != 0 &&
                torgTransRequestReply.MinPrice <= item.MinPrice)
            {
                return BadRequest("Превышен минимальный порог для данного пользователя");
            }

            item.InProgressUserId = userId;
            item.ActualPrice = item.CurrentPrice - item.Step;
            item.BidCount++;
            item.UpdatedAt = DateTime.Now;

            // @TODO поправить логику
            if (!item.Replies.Any())
            {
                item.Replies ??= new List<TorgTransRequestReply>();
                item.Replies.Add(new TorgTransRequestReply
                {
                    CreatedAt = DateTime.Now,
                    Price = item.ActualPrice,
                    CreatorId = userId,
                    Status = 0,
                    AutoBidding = torgTransRequestReply.AutoBidding,
                    MinPrice = torgTransRequestReply.MinPrice
                });
            }
            else
            {
                item.Replies.First().CreatedAt = DateTime.Now;
                item.Replies.First().Price = item.ActualPrice;
                item.Replies.First().AutoBidding = torgTransRequestReply.AutoBidding;
                item.Replies.First().MinPrice = torgTransRequestReply.MinPrice;
            }

            item.UpdatedAt = DateTime.Now;
            item.RequestStatus = RequestStatus.IsActive;
            item.AuctionRequestStatus = AuctionRequestStatus.BiddingActive;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(torgTransRequestReply);
        }

        /// <summary>
        ///     Updates an existing TorgTransRequestReply.
        /// </summary>
        /// <param name="key">The requested TorgTransRequestReply identifier.</param>
        /// <param name="delta">The partial TorgTransRequestReply to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The TorgTransRequestReply was successfully updated.</response>
        /// <response code="400">The TorgTransRequestReply is invalid.</response>
        /// <response code="404">The TorgTransRequestReply does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TorgTransRequestReply), 200)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<TorgTransRequestReply> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TorgTransRequestReplies.IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.Error &&
                item.Request.RequestStatus != RequestStatus.DataTreatment)
            {
                return BadRequest();
            }

            delta.Patch(item);

            item.Request.AuctionRequestStatus = AuctionRequestStatus.ResourceTreatment;
            item.Request.RequestStatus = RequestStatus.DataTreatment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendTorgTransRequestReply(item.Id, userId));

            return Updated(item);
        }
    }
}