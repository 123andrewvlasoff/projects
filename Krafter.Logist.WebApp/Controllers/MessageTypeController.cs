﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.MessageType;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class MessageTypeController : BaseController<MessageTypeController>
    {
        public MessageTypeController(ILogger<MessageTypeController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get MessageTypes.
        /// </summary>
        /// <returns>The requested MessageType.</returns>
        /// <response code="200">The MessageType was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<MessageType>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IEnumerable<MessageType> Get()
        {
            return logistDbContext.MessageTypes.AsNoTracking().Decompile();
        }

        /// <summary>
        ///     Get MessageType by Id.
        /// </summary>
        /// <returns>The requested MessageType.</returns>
        /// <response code="200">The MessageType was successfully retrieved.</response>
        /// <response code="404">The MessageType does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(MessageType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.MessageTypes.AsNoTracking()
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}