﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Krafter.Enums;
using DelegateDecompiler.EntityFrameworkCore;
using EasyCaching.Core;
using Hangfire;
using GeoCoordinatePortable;
using Krafter.IdentityServer.Extensions.Constants;
using Crafter.Client.CIS.Clients;
using Krafter.Logist.WebApp.Models.Api.SuitableTransport;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.BundleComment;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply.Enums;
using Krafter.Logist.WebApp.Models.Basic.ReisesPriceByQuarter;
using Krafter.Logist.WebApp.Models.Basic.RequestSuitableTransport;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.UserDto;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Krafter.Logist.WebApp.Views.Email.Enums;
using Krafter.ServiceCommon.Services.ChangesTrackService.Helpers;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlKata.Execution;
using Z.EntityFramework.Plus;
using MathNet.Numerics.Statistics;
using static DAL.Models.Requests.Reis.Constants.ReisStatus;
using KrafterRequest = Krafter.Logist.WebApp.Models.Basic.KrafterRequest.KrafterRequest;
using KrafterRequestAction = Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction.KrafterRequestAction;
using KrafterRequestRoutePoint = Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint.KrafterRequestRoutePoint;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(KrafterRequest))]
    [Authorize(Policy = UserPolicies.RequestsAccess)]
    public class KrafterRequestController : BaseController<KrafterRequestController>
    {
        private readonly IKrafterExternalClient krafterExternalClient;
        private readonly KrafterRequestService krafterRequestService;
        private readonly IEasyCachingProvider cache;
        private readonly ChangesTrackService changesTrackService;
        private readonly QueryFactory db;
        private readonly RequestDtoRepository repository;
        private readonly IMapper mapper;
        private readonly ISuitableTransportService suitableTransportService;

        public KrafterRequestController(ILogger<KrafterRequestController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            IKrafterExternalClient krafterExternalClient, ChangesTrackService changesTrackService,
            IEasyCachingProvider cache, QueryFactory db, IMapper mapper, RequestDtoRepository repository,
            KrafterRequestService krafterRequestService, ISuitableTransportService suitableTransportService) : base(
            logger,
            logistDbContext, userManager, httpContextAccessor)
        {
            this.krafterExternalClient = krafterExternalClient;
            this.changesTrackService = changesTrackService;
            this.cache = cache;
            this.db = db;
            this.mapper = mapper;
            this.repository = repository;
            this.krafterRequestService = krafterRequestService;
            this.suitableTransportService = suitableTransportService;
        }

        /// <summary>
        ///     Get KrafterRequest by Id.
        /// </summary>
        /// <returns>The requested KrafterRequest.</returns>
        /// <response code="200">The KrafterRequest was successfully retrieved.</response>
        /// <response code="404">The KrafterRequest does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KrafterRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True, MaxExpansionDepth = 4)]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.RoutePoints.Select(rp => new
                {
                    rp.Region,
                    rp.Settlement
                }))
                .IncludeOptimized(r => r.Reises
                    .Select(s => s.RoutePoints.Select(rp => new
                    {
                        rp.Region,
                        rp.Settlement,
                    })))
                .IncludeOptimized(r => r.Replies
                    .Select(s => new
                    {
                        s.Files,
                        s.Comments,
                        s.Creator,
                        s.Creator.KontragentUsers
                    }))
                .IncludeOptimized(r => r.Replies)
                .IncludeOptimized(r => r.Actions)
                .IncludeOptimized(r => r.Client.Contacts)
                .IncludeOptimized(r => r.AssignedUser.Employer)
                .IncludeOptimized(r => r.InProgressUser.Employer)
                .IncludeOptimized(r => r.KontragentContract)
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logger.LogInformation($"Get request: id - {key}");

            return StatusCode(StatusCodes.Status200OK,
                mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", userManager.GetUserId(User))));
        }

        /// <summary>
        ///     Get KrafterRequest Instruction by Id.
        /// </summary>
        /// <returns>The requested KrafterRequest Instruction.</returns>
        /// <response code="200">The KrafterRequest Instruction was successfully retrieved.</response>
        /// <response code="404">The KrafterRequest Instruction does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Instruction")]
        public async Task<IActionResult> GetInstruction(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logger.LogInformation($"Get request: id - {key}");

            var instruction =
                await krafterExternalClient.InstructionAsync(item.ExternalId, InstructionType.Logistician);

            return File(instruction.Document.Content, "application/pdf");
        }

        /// <summary>
        ///     Get Suitable Transports for KrafterRequests.
        /// </summary>
        /// <returns>The requested Suitable Transports for KrafterRequests.</returns>
        /// <response code="200">The Suitable Transports for KrafterRequests was successfully retrieved.</response>
        /// <response code="404">The KrafterRequests does not exist.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestSuitableTransport>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<RequestSuitableTransport>> SuitableTransport(Guid key, int from, int to,
            int remoteness)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var request = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.RoutePoints)
                .FirstAsync(i => i.Id == key);

            var requestLat = request.Load?.Lat;

            var requestLon = request.Load?.Lon;

            var requestLoadDate = request.Load?.ArrivalDate;
            var requestUnloadDate = request.Unload?.ArrivalDate;

            var requestWeight = request.Weight >= 20 && request.Weight <= 22 ? 20 : request.Weight;

            if (requestLoadDate == null || requestLon == null || requestLat == null)
            {
                return new List<RequestSuitableTransport>().AsQueryable();
            }

            var results = await suitableTransportService.GetAllAsyncOdata(
                new SuitableTransportFilter()
                {
                    Remoteness = remoteness,
                    Weight = requestWeight,
                    StartInterval = from,
                    EndInterval = to,
                    LoadingTypeId = request.LoadingTypeId,
                    ArrivalDate = requestLoadDate,
                    Lat = requestLat,
                    Lon = requestLon,
                    LeaveDate = requestUnloadDate,
                });

            await cache.SetAsync("suitableTransportCacheKey_" + userId, results, TimeSpan.FromHours(1));

            if (request.RequestStatus == RequestStatus.IsActive)
            {
                BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendRequest(request.Id, userId));
            }

            return results;
        }

        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<BundleComment>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<BundleComment>> GetBundleComments(Guid key)
        {
            var result = await logistDbContext.Reises
                .IgnoreQueryFilters()
                .AsNoTracking()
                .Select(r => new
                {
                    r.Id,
                    BundleComments = r.Transport.BundleComments
                        .Where(x => x.ConnectedTransportId == r.ConnectedTransportId)
                        .Select(s => new BundleComment
                        {
                            Id = s.Id,
                            Text = s.Text,
                            IsDeleted = s.IsDeleted,
                            CreatorId = s.CreatorId,
                            Creator = s.Creator,
                            UpdaterId = s.UpdaterId,
                            Updater = s.Updater,
                            RequestId = s.RequestId,
                            CreatedAt = s.CreatedAt,
                            UpdatedAt = s.UpdatedAt,
                            Request = new KrafterRequest
                            {
                                Id = s.RequestId,
                                RoutePoints = s.Request.RoutePoints
                                    .Select(x => new KrafterRequestRoutePoint
                                    {
                                        Id = x.Id,
                                        PointType = x.PointType,
                                        SettlementId = x.SettlementId,
                                        ArrivalDate = x.ArrivalDate,
                                        ArrivalDateFact = x.ArrivalDateFact,
                                        LeaveDate = x.LeaveDate,
                                        LeaveDateFact = x.LeaveDateFact,
                                        Settlement = x.Settlement,
                                        RegionId = x.RegionId,
                                        Region = x.Region,
                                        Lat = x.Lat,
                                        Lon = x.Lon
                                    }).ToList()
                            },
                            // @TODO зачем тут это 
                            TransportId = r.TransportId,
                            Transport = r.Transport,
                            ConnectedTransportId = r.ConnectedTransportId,
                            ConnectedTransport = r.ConnectedTransport,
                            Remoteness = new GeoCoordinate(r.Unload.Lat.Value, r.Unload.Lon.Value)
                                .GetDistanceTo(new GeoCoordinate((double)s.Request.Load.Lat,
                                    (double)s.Request.Load.Lon)) / 1000,
                        }).ToList()
                })
                .DecompileAsync()
                .FirstOrDefaultAsync(x => x.Id == key);

            return result?.BundleComments ?? new List<BundleComment>(0);
        }

        /// <summary>
        ///     Get Reises Price (min, max, avg) on simple direction by Quarter.
        /// </summary>
        /// <returns>The requested Reises Price.</returns>
        /// <response code="200">The Reises Price was successfully retrieved.</response>
        /// <response code="404">The KrafterRequest does not exist.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ReisesPriceByQuarter>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<ReisesPriceByQuarter>> ReisesPriceByQuarter(Guid key)
        {
            var request = await logistDbContext.KrafterRequests.IncludeOptimized(i => i.RoutePoints)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (request == null)
            {
                return new List<ReisesPriceByQuarter>();
            }

            var distance = request.Distance ?? 0;
            var load = request.Load.PostalCode;
            var unload = request.Unload.PostalCode;
            var endDate = DateTime.Now.AddMonths(-12 - (DateTime.Now.Date
                .Month + 2) / 3);

            var items = await db.Query("Reis")
                .WhereNotNull("Distance")
                .WhereNot("Distance", 0)
                .WhereTrue("IsActive")
                .WhereFalse("IsDeleted")
                .Where("RollingStockTypeId", request.RollingStockTypeId)
                .Where("Weight", "<=", request.Weight * 1.15)
                .Where("Weight", ">=", request.Weight * 0.85)
                .Where("Volume", "<=", request.Volume * 1.15)
                .Where("Volume", ">=", request.Volume * 0.85)
                .WhereIn("StatusId", new[] { Completed, WaitingDocuments })
                .RightJoin(
                    db.Query("ReisRoutePoints")
                        .Select("ReisRoutePoints.Id", "ReisRoutePoints.ArrivalDatePlan",
                            "ReisRoutePoints.PostalCode", "ReisRoutePoints.ReisId", "ReisRoutePoints.PointTypeName")
                        .As("RoutePoint")
                        .WhereDate("ArrivalDatePlan", ">=", endDate)
                        .Where(q =>
                            q.Where(i => i.Where("PointTypeName", PointType.Load).Where("PostalCode", load))
                                .OrWhere(i => i.Where("PointTypeName", PointType.Unload).Where("PostalCode", unload)))
                    ,
                    j => j.On("Reis.Id", "RoutePoint.ReisId"))
                .GroupBy("Reis.Id", "Reis.Price", "Reis.Distance").HavingRaw("count([RoutePoint]) = 2")
                .SelectRaw($"[Reis].[Price] / [Reis].[Distance] * {distance} as [Price]")
                .SelectRaw("min([RoutePoint].[ArrivalDatePlan]) as [Date]")
                .GetAsync<ReisPrice>();

            return items
                .GroupBy(x =>
                    new
                    {
                        x.Date
                            .Year,
                        Quarter = (x.Date
                            .Month + 2) / 3
                    })
                .Select(i => new ReisesPriceByQuarter
                    {
                        YearQuarter = new YearQuarter
                        {
                            Year = i.Key.Year,
                            Quarter = i.Key.Quarter
                        },
                        Min = i.Min(r => r.Price),
                        Max = i.Max(r => r.Price),
                        Average = i.Select(rp => rp.Price).Median()
                    }
                );
        }

        /// <summary>
        ///     CancelledByClient KrafterRequest.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/CancelByClient")]
        public async Task<IActionResult> PatchCancelByClient([FromODataUri] Guid key, Guid deniedReasonId,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Replies.Select(s => s.Files))
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.RequestStatus == RequestStatus.DataTreatment)

            {
                return BadRequest(ModelState);
            }

            item.DeniedReasonId = deniedReasonId;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            var action = await changesTrackService.TrackRequestAsync(item.Id,
                RequestActionStatus.CancelledByClient,
                ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item)), userId);

            var requestReply = item.Replies.FirstOrDefault();


            if (requestReply == null || item.RequestStatus == RequestStatus.Error)
            {
                BackgroundJob.Enqueue<KrafterRequestService>(i => i.CancelRequestByClient(item.Id));
                return Ok(action);
            }

            item.InProgressUserId = requestReply.CreatorId;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<KrafterRequestReplyService>(i =>
                i.CancelRequestReplyDocument(requestReply.Id, userId, ReplyDeniedReasonType.ByClientDecision));

            return Ok(action);
        }

        /// <summary>
        ///     CancelByCarrier KrafterRequests.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/CancelByCarrier")]
        public async Task<IActionResult> PatchCancelByCarrier(Guid key, Guid deniedReasonId,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Replies)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.RequestStatus == RequestStatus.IsActive ||
                item.RequestStatus == RequestStatus.DataTreatment ||
                item.KrafterRequestStatus == KrafterRequestStatus.Verification ||
                item.KrafterRequestStatus == KrafterRequestStatus.InProgress)
            {
                return BadRequest(ModelState);
            }

            item.DeniedReasonId = deniedReasonId;
            await logistDbContext.SaveChangesAsync(cancellationToken);

            var action = await changesTrackService.TrackRequestAsync(item.Id,
                RequestActionStatus.IsActive,
                ChangesTrackHelper.TrackChanges(logistDbContext.Entry(item)), userId);

            BackgroundJob.Enqueue<KrafterRequestService>(i => i.CancelRequestByCarrier(item.Id));

            return Ok(action);
        }

        /// <summary>
        ///     Get Reis history.
        /// </summary>
        /// <returns>The requested Reis history.</returns>
        /// <response code="200">The Reis history was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<KrafterRequestAction>), StatusCodes.Status200OK)]
        [EnableQuery]
        [ODataRoute("({key})/History")]
        public async Task<ActionResult> GetHistory(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests.IgnoreQueryFilters()
                .Include(i => i.Actions)
                .ThenInclude(i => i.User)
                .Include(i => i.RoutePoints)
                .ThenInclude(i => i.Actions)
                .ThenInclude(i => i.User)
                .Include(i => i.Replies)
                .ThenInclude(i => i.Actions)
                .ThenInclude(i => i.RequestReplyComments)
                .Include(i => i.Replies)
                .ThenInclude(i => i.Actions)
                .ThenInclude(i => i.User)
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, new List<KrafterRequestAction>()
                {
                    new KrafterRequestAction
                    {
                        State = RequestActionStatus.New,
                        ActionDate = item.CreatedAt.Value
                    }
                }.Concat(item.Actions).Concat(
                    item.RoutePoints.SelectMany(
                        i => i.Actions.Select(a => new KrafterRequestAction
                        {
                            Changes = a.Changes,
                            ActionDate = a.ActionDate,
                            Discriminator = a.Discriminator,
                            Id = a.Id,
                            State = a.State,
                            User = a.User,
                            ActionUserId = a.ActionUserId
                        }))).Concat(
                    item.Replies.Where(x => !x.IsDeleted).SelectMany(i => i.Actions.Select(a =>
                        new KrafterRequestAction
                        {
                            Changes = a.Changes,
                            ActionDate = a.ActionDate,
                            Discriminator = a.Discriminator,
                            Id = a.Id,
                            State = a.State,
                            User = a.User,
                            ActionUserId = a.ActionUserId
                        })))
                .OrderBy(i => i.ActionDate)
            );
        }


        /// <summary>
        ///     Patch a TakeInProgress
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The request was successfully TakeInProgress.</response>
        /// <response code="404">The request not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/TakeInProgress")]
        [EnableQuery]
        public async Task<IActionResult> PatchTakeInProgress(Guid key, CancellationToken cancellationToken)
        {
            var user = await userManager.GetUserAsync(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Actions)
                .Include(i => i.RoutePoints)
                .FirstOrDefaultAsync(ur => ur.Id == key, cancellationToken);

            var mappedItem = mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", user.Id));

            if (item == null)
            {
                return NotFound();
            }

            if (item.InProgressUserId != null && item.InProgressUserId != user.Id)
            {
                return BadRequest("Заявка уже в работе!");
            }

            if (mappedItem.AssignedUserId != user.Id && mappedItem.ActionCount < 2 &&
                DateTime.Now < mappedItem.BookUntil)
            {
                return BadRequest("Заявка доступна для бронирования!");
            }

            item.InProgressUserId = user.Id;
            item.KrafterRequestStatus = KrafterRequestStatus.InProgress;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            if (item.IsActive)
            {
                await krafterRequestService.TakeInProgressRequestAsync(item.Id, user.Id);
            }

            // @TODO поправить кривую логику
            var request = await repository.GetAsync<KrafterRequestDto>(item.Id);
            request.InProgressUserId = user.Id;
            request.InProgressUser = mapper.Map<UserDto>(user);
            request.RequestStatus = RequestStatus.IsActive;
            request.KrafterRequestStatus = KrafterRequestStatus.InProgress;
            await repository.SetAsync(request);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            logger.LogInformation($"Get request to progress: id - {item.Id}");

            return NoContent();
        }


        /// <summary>
        ///     Patch Book Request
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The request was successfully TakeInProgress.</response>
        /// <response code="404">The request not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Book")]
        [EnableQuery]
        public async Task<IActionResult> PatchBook(Guid key, CancellationToken cancellationToken)
        {
            var user = await userManager.GetUserAsync(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.Actions)
                .IncludeOptimized(i => i.RoutePoints)
                .FirstOrDefaultAsync(ur => ur.Id == key, cancellationToken);

            var mappedItem = mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", user.Id));

            if (item == null)
            {
                return NotFound();
            }

            if (mappedItem.InProgressUserId != null && mappedItem.InProgressUserId != user.Id)
            {
                return BadRequest("Заявка уже в работе!");
            }

            if (mappedItem.InProgressUserId != null && mappedItem.InProgressUserId == mappedItem.AssignedUserId)
            {
                return BadRequest("Вы являетесь ответственным логистом заявки!");
            }

            if (mappedItem.ActionCount > 1)
            {
                return BadRequest("Бронирование не доступно!");
            }

            if (DateTime.Now >= mappedItem.BookUntil)
            {
                return BadRequest("Время бронирования истекло!");
            }

            item.InProgressUserId = user.Id;
            item.KrafterRequestStatus = KrafterRequestStatus.InProgress;

            if (item.IsActive)
            {
                await krafterRequestService.BookRequestAsync(item.Id, user.Id);
            }

            await logistDbContext.SaveChangesAsync(cancellationToken);

            // @TODO поправить кривую логику
            var request = await repository.GetAsync<KrafterRequestDto>(item.Id);
            request.InProgressUserId = user.Id;
            request.InProgressUser = mapper.Map<UserDto>(user);
            request.RequestStatus = RequestStatus.IsActive;
            request.KrafterRequestStatus = KrafterRequestStatus.InProgress;
            await repository.SetAsync(request);

            logger.LogInformation($"Get request to book: id - {item.Id}");

            return NoContent();
        }

        /// <summary>
        ///     Patch a FreeInProgress
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The request was successfully FreeInProgress.</response>
        /// <response code="404">The request not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/FreeInProgress")]
        public async Task<IActionResult> PatchFreeInProgress(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.KrafterRequests
                .IncludeOptimized(i => i.AssignedUser)
                .IncludeOptimized(i => i.Actions)
                .FirstOrDefaultAsync(ur => ur.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.InProgressUserId != userId)
            {
                return BadRequest("Заявка не у Вас в работе!");
            }

            item.InProgressUserId = null;
            item.RequestStatus = RequestStatus.IsActive;
            item.KrafterRequestStatus = null;

            item.Actions.Add(new KrafterRequestAction
            {
                ActionDate = DateTime.Now,
                ActionUserId = userId,
                State = RequestActionStatus.FreeInProgress,
            });

            await logistDbContext.SaveChangesAsync(cancellationToken);

            if (item.AssignedUserId != null)
            {
                BackgroundJob.Enqueue<NotificationsService>(i =>
                    i.AddFreeInProgressForAssignedUserNotification(item.Id, userId, item.AssignedUserId));

                BackgroundJob.Enqueue<EmailSenderService>(i =>
                    i.SendRequestEmail(item.Id, SubjectType.LogistDeleteUserRequest, item.AssignedUser.Email, userId));
            }

            // @TODO поправить кривую логику
            var request = await repository.GetAsync<KrafterRequestDto>(item.Id);
            request.InProgressUserId = null;
            request.InProgressUser = null;
            request.RequestStatus = RequestStatus.IsActive;
            request.KrafterRequestStatus = null;
            await repository.SetAsync(request);

            logger.LogInformation($"Delete request from progress: id - {item.Id}");

            return NoContent();
        }

        /// <summary>
        ///     Get NextRequest for KrafterRequests.
        /// </summary>
        /// <returns>The requested NextRequest for KrafterRequests.</returns>
        /// <response code="200">The NextRequest for KrafterRequests was successfully retrieved.</response>
        /// <response code="404">The NextRequest does not exist.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<RequestDto>> NextRequest(Guid key, int from, int to, int remoteness)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var request = logistDbContext.KrafterRequests.IncludeOptimized(i => i.RoutePoints)
                .First(i => i.Id == key);

            var requestLat = request.Unload?.Lat;

            var requestLon = request.Unload?.Lon;

            var requestDate = request.Unload?.LeaveDate;

            if (requestDate == null || requestLon == null || requestLat == null)
            {
                return new List<RequestDto>().AsQueryable();
            }

            var items = await repository.GetAsync();

            items = items.Where(i => i.ActiveUntil > DateTime.Now && i.RequestStatus == RequestStatus.IsActive &&
                                     (i.InProgressUserId == null || i.InProgressUserId == userId));

            items = items.Where(i => i.Load.ArrivalDate >= requestDate);

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            {
                items = items.Where(x => x.Discriminator != RequestDiscriminator.Krafter);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            {
                items = items.Where(x =>
                    x.Discriminator != RequestDiscriminator.Cargomart &&
                    x.Discriminator != RequestDiscriminator.TrafficOnline &&
                    x.Discriminator != RequestDiscriminator.TorgTrans);
            }

            items = items.Where(x => x.RoutePoints.All(a => a.Lon != null && a.Lat != null))
                .Select(s =>
                {
                    s.Remoteness = new GeoCoordinate(requestLat.Value, requestLon.Value).GetDistanceTo(
                        new GeoCoordinate(s.Load.Lat!.Value, s.Load.Lon!.Value)) / 1000;
                    s.RequestDateInterval = s.Load.ArrivalDate - requestDate.Value;
                    return s;
                });

            items = items.Where(i => i.Remoteness <= remoteness &&
                                     i.RequestDateInterval >= TimeSpan.FromHours(from) &&
                                     i.RequestDateInterval <= TimeSpan.FromHours(to));

            return mapper.Map<IEnumerable<RequestDto>>(items, options => options.Items.Add("UserId", userId));
        }
    }
}