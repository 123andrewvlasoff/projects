﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Dictionaries.Enums;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using DelegateDecompiler;
using GeoCoordinatePortable;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public partial class TransportController
    {
        private async Task<int> GetSuitableReisesCount(Guid key)
        {
            var userId = userManager.GetUserId(User);

            var hoursFromUnload = 4;

            var remoteness = 100;

            var item = logistDbContext.Transports
                .IncludeOptimized(i => i.Reises.Select(s => new
                {
                    s.Transport,
                    s.ConnectedTransport,
                    s.RoutePoints
                }))
                .IncludeOptimized(i => i.ConnectedTransport)
                .First(x => x.Id == key);

            var bundle = new List<Transport>
            {
                new Transport
                {
                    Id = item.Id,
                    Volume = item.Volume,
                    Weight = item.Weight,
                    RollingStockTypeId = item.RollingStockTypeId,
                    ConnectedTransportId = item.ConnectedTransportId,
                    ConnectedTransport = item.ConnectedTransport,
                    LastReisDate = item.LastReisDate,
                    LastAddress = item.LastAddress,
                    LastReisLon = item.LastReisLon,
                    LastReisLat = item.LastReisLat
                },
                new Transport
                {
                    Id = item.Id,
                    Volume = (double) item?.Volume,
                    Weight = (double) item?.Weight,
                    RollingStockTypeId = item?.RollingStockTypeId,
                    ConnectedTransportId = item?.ConnectedTransportId,
                    ConnectedTransport = item?.ConnectedTransport,
                    LastReisDate = item.LastReis?.Unload?.LeaveDatePlan,
                    LastAddress = item.LastReis?.Unload?.Address,
                    LastReisLon =item.LastReis?.Unload?.Lon,
                    LastReisLat = item.LastReis?.Unload?.Lat
                }
            }.OrderByDescending(o => o.LastReisDate).First();

            var requests = await repository.GetAsync();
            
            var result = requests.Where(i => i.RequestStatus == RequestStatus.IsActive &&
                                            (i.ActiveUntil > DateTime.Now || i.AssignedUserId == userId) ||
                                            (i.RequestStatus == RequestStatus.DataTreatment ||
                                             i.RequestStatus == RequestStatus.DataInput ||
                                             i.RequestStatus == RequestStatus.Cancelled ||
                                             i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

            if (item.TransportTypeId == (int) TransportType.Mule && bundle.ConnectedTransportId.HasValue)
            {
                result = result.Where(i => i.RollingStockTypeId == bundle.ConnectedTransport.RollingStockTypeId 
                                           && i.Volume <= 1.1 * bundle.ConnectedTransport.Volume 
                                           && i.Weight <= 1.1 * bundle.ConnectedTransport.Weight);
            }
            else
            {
                result = result.Where(i => i.RollingStockTypeId == bundle.RollingStockTypeId 
                                           && i.Volume <= 1.1 * bundle.Volume 
                                           && i.Weight <= 1.1 * bundle.Weight);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            {
                result = result.Where(x => x.Discriminator != RequestDiscriminator.Krafter);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            {
                result = result.Where(x =>
                    x.Discriminator != RequestDiscriminator.Cargomart &&
                    x.Discriminator != RequestDiscriminator.TrafficOnline &&
                    x.Discriminator != RequestDiscriminator.TorgTrans);
            }
            
            if (bundle.LastReisDate != null)
            {
                result = result.Where(r=>r.ActiveUntil >= bundle.LastReisDate.Value.AddHours(hoursFromUnload) && 
                                         r.ActiveUntil <= bundle.LastReisDate.Value.AddHours(hoursFromUnload + 36));
            }

            if (bundle.LastReisLat != null && bundle.LastReisLon != null)
            {
                result = result.Where(r => (new GeoCoordinate(r.Load.Lat.Value, r.Load.Lon.Value).GetDistanceTo(
                    new GeoCoordinate((double) (bundle.LastReisLat), (double) (bundle.LastReisLon)))) / 1000 <= remoteness);
            }

            return result.Count();
        }
    }
}