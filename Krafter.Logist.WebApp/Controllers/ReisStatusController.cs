﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests.Reis.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class ReisStatusController : BaseController<ReisStatusController>
    {
        public ReisStatusController(ILogger<ReisStatusController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get ReisStatuses.
        /// </summary>
        /// <returns>The requested ReisStatuses.</returns>
        /// <response code="200">The ReisStatuses was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ReisStatus>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 3, HandleNullPropagation = HandleNullPropagationOption.False)]
        public IQueryable<ReisStatus> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return logistDbContext.ReisStatuses.AsNoTracking().AsQueryable();
        }

        /// <summary>
        ///     Get ReisStatuses by Id.
        /// </summary>
        /// <returns>The requested ReisStatuses.</returns>
        /// <response code="200">The ReisStatuses was successfully retrieved.</response>
        /// <response code="404">The ReisStatuses does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReisStatus), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.ReisStatuses.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}