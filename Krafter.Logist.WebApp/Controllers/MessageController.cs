﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.SupportTickets.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Message;
using Krafter.Logist.WebApp.Models.Basic.MessageFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class MessageController : BaseController<MessageController>
    {
        public MessageController(ILogger<MessageController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Place new Message.
        /// </summary>
        /// <returns>The Message.</returns>
        /// <response code="201">The Message was successfully placed.</response>
        /// <response code="400">The Message is invalid.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Message), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Post([FromBody] Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            message.UserId = userManager.GetUserId(User);
            message.CreatedAt = DateTime.Now;
            message.Files = new List<MessageFile>();

            await logistDbContext.Messages.AddAsync(message);

            var item = await logistDbContext.SupportTickets
                .IncludeOptimized(i => i.Messages)
                .FirstOrDefaultAsync(x => x.Id == message.SupportTicketId);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Status == SupportTicketStatus.Closed)
            {
                return BadRequest();
            }

            var lastTwoMessages = item.Messages.OrderByDescending(o => o.CreatedAt).Take(2).ToList();

            var firstMessage = lastTwoMessages.FirstOrDefault();

            var secondMessage = lastTwoMessages.LastOrDefault();

            if (firstMessage != null && secondMessage != null && firstMessage.UserId != secondMessage.UserId)
            {
                item.Status = SupportTicketStatus.NewTicket;
            }

            await logistDbContext.SaveChangesAsync();

            logger.LogInformation($"Created new support ticket massage: id - {message.Id}");

            return Created(message);
        }

        /// <summary>
        ///     Deletes a Message
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The Message was successfully deleted.</response>
        /// <response code="404">The Message not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item =
                logistDbContext.Messages.FirstOrDefault(ur => ur.Id == key);

            if (item == null) return NotFound();

            logistDbContext.Messages.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Ok();
        }
    }
}