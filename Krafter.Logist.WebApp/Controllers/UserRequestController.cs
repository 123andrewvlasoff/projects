﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.Requests;
using DAL.Models.Requests.Krafter.Enums;
using DAL.Models.Requests.UserRequest.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.Basic.UserRequest;
using Krafter.Logist.WebApp.Models.ReadOnly.UserRequestDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(UserRequest))]
    public class UserRequestController : BaseController<UserRequestController>
    {
        private readonly RequestDtoRepository repository;
        private readonly IMapper mapper;

        public UserRequestController(ILogger<UserRequestController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            RequestDtoRepository repository, IMapper mapper)
            : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        /// <summary>
        ///     Place new or update current UserRequest.
        /// </summary>
        /// <returns>The current UserRequest.</returns>
        /// <response code="201">The UserRequest was successfully placed.</response>
        /// <response code="400">The UserRequest is invalid.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserRequest), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] UserRequest userRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            userRequest.UserId = userManager.GetUserId(User);
            userRequest.CreatedAt = DateTime.Now;

            await logistDbContext.UserRequests.AddAsync(userRequest);

            await logistDbContext.SaveChangesAsync();

            // @TODO поправить кривую логику
            var request = await repository.GetAsync(userRequest.RequestId);

            request.UserRequests.Add(mapper.Map<UserRequestDto>(userRequest));

            await repository.SetAsync(request);

            return Created(userRequest);
        }

        /// <summary>
        ///     Deletes a UserRequest
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <response code="204">The UserRequest was successfully deleted.</response>
        /// <response code="404">The UserRequest not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item = logistDbContext.UserRequests.FirstOrDefault(ur => ur.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.UserRequests.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            // @TODO поправить кривую логику
            var request = await repository.GetAsync(item.RequestId);

            request.UserRequests = request.UserRequests.Where(i => i.Id != item.Id).ToList();

            await repository.SetAsync(request);

            return NoContent();
        }
    }
}