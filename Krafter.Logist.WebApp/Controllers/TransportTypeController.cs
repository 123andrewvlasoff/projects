﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class TransportTypeController : BaseController<TransportTypeController>
    {
        public TransportTypeController(ILogger<TransportTypeController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get TransportTypes.
        /// </summary>
        /// <returns>The requested TransportType.</returns>
        /// <response code="200">The TransportType was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<TransportType>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<TransportType> Get()
        {
            return logistDbContext.TransportTypes.AsNoTracking().AsQueryable();
        }

        /// <summary>
        ///     Get TransportType by Id.
        /// </summary>
        /// <returns>The requested TransportType.</returns>
        /// <response code="200">The TransportType was successfully retrieved.</response>
        /// <response code="404">The TransportType does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int key)
        {
            var item = await logistDbContext.TransportTypes.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}