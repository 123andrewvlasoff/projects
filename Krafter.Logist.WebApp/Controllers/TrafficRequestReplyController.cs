﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestReply;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(TrafficRequestReply))]
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TrafficRequestReplyController : BaseController<TrafficRequestReplyController>
    {
        public TrafficRequestReplyController(ILogger<TrafficRequestReplyController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Places a new TrafficRequestReply.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created TrafficRequestReply.</returns>
        /// <response code="201">The TrafficRequestReply was successfully placed.</response>
        /// <response code="400">The TrafficRequestReply is invalid.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ODataRoute("({key})/Bid")]
        public async Task<IActionResult> PostBid(Guid key, CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            var item = await logistDbContext.TrafficRequests
                .IncludeOptimized(i => i.RoutePoints)
                .IncludeOptimized(i => i.Replies)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return BadRequest();
            }

            if (item.InProgressUserId != null)
            {
                return BadRequest();
            }

            if (item.RequestStatus != RequestStatus.IsActive)
            {
                return BadRequest();
            }

            item.InProgressUserId = userId;

            if (item.Replies.Any())
            {
                return BadRequest();
            }

            item.Replies.Add(new TrafficRequestReply
            {
                CreatedAt = DateTime.Now,
                CreatorId = userId,
            });

            item.UpdatedAt = DateTime.Now;
            item.RequestStatus = RequestStatus.IsActive;
            item.AuctionRequestStatus = AuctionRequestStatus.BiddingActive;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }


        /// <summary>
        ///     Updates an existing TrafficRequestReply.
        /// </summary>
        /// <param name="key">The requested TrafficRequestReply identifier.</param>
        /// <param name="delta">The partial TrafficRequestReply to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The TrafficRequestReply was successfully updated.</response>
        /// <response code="400">The TrafficRequestReply is invalid.</response>
        /// <response code="404">The TrafficRequestReply does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TrafficRequestReply), 200)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<TrafficRequestReply> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TrafficRequestReplies
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.Error &&
                item.Request.RequestStatus != RequestStatus.DataTreatment)
            {
                return BadRequest();
            }

            delta.Patch(item);

            item.Request.AuctionRequestStatus = AuctionRequestStatus.ResourceTreatment;
            item.Request.RequestStatus = RequestStatus.DataTreatment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendTrafficRequestReply(item.Id, userId));

            return Updated(item);
        }
    }
}