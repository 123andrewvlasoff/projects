﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DAL.Models.Enums;
using DAL.Models.Requests.Auction.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.AuctionRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.Basic.User.UserProfileDto;
using Krafter.Logist.WebApp.Models.Basic.User.UserStaticDto;
using Krafter.Logist.WebApp.Models.Basic.User.UserStaticDto.Enums;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using ClaimTypes = Krafter.IdentityServer.Extensions.Constants.ClaimTypes;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(User))]
    public class UserController : BaseController<UserController>
    {
        private readonly XLWorkbook xlWorkbook;

        public UserController(ILogger<UserController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, XLWorkbook xlWorkbook) : base(
            logger, logistDbContext,
            userManager, httpContextAccessor)
        {
            this.xlWorkbook = xlWorkbook;
        }

        
        /// <summary>
        ///     Get current User.
        /// </summary>
        /// <returns>The current User.</returns>
        /// <response code="200">The User was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ODataRoute("Profile")]
        public async Task<IActionResult> GetProfile()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var user = await logistDbContext.Users
                .IncludeOptimized(i => i.UserRoles.Select(ur => ur.Role))
                .Select(i => new UserProfileDto
                {
                    Id = i.Id,
                    Email = i.Email,
                    PersonName = i.PersonName,
                    LastName = i.LastName,
                    Patronymic = i.Patronymic,
                    PhoneNumber = i.PhoneNumber,
                    UserName = i.UserName,
                    Roles = i.UserRoles.Select(ur => ur.Role.Name).ToList(),
                }).FirstOrDefaultAsync(i => i.Id == userId);

            if (user == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, user);
        }

        /// <summary>
        ///     Update User Password.
        /// </summary>
        /// <response code="200">The User password was successfully changed.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IdentityResult), StatusCodes.Status200OK)]
        [ODataRoute("ChangePassword")]
        public async Task<IdentityResult> PostChangePassword(string oldPassword, string newPassword)
        {
            var user = await userManager.GetUserAsync(User);

            return await userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }


        /// <summary>
        ///     Get UsersStatisticDiagram model.
        /// </summary>
        /// <returns>UsersStatisticDiagram model.</returns>
        /// <response code="200">The UsersStatisticDiagram model was successfully returned.</response>
        [Produces("application/json")]
        [ODataRoute("StatisticDiagram")]
        public async Task<UsersStatisticDiagramViewModel> GetStatisticDiagram(DateTime start, DateTime end)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var roles = User.FindAll(System.Security.Claims.ClaimTypes.Role).Select(i => i.Value);

            if (roles.All(r => r != UserRoles.Admin))
            {
                return new UsersStatisticDiagramViewModel();
            }

            var auctionStatusList = new List<AuctionRequestStatus>
            {
                AuctionRequestStatus.BiddingActive,
                AuctionRequestStatus.ResourceInput,
                AuctionRequestStatus.ResourceTreatment,
                AuctionRequestStatus.DocSignInput,
                AuctionRequestStatus.DocSignTreatment,
                AuctionRequestStatus.TimeWindowInput,
                AuctionRequestStatus.TimeWindowTreatment,
            };

            var request = logistDbContext.Requests.IgnoreQueryFilters().AsNoTracking().AsSplitQuery()
                .Where(x => x.CreatedAt >= start && x.CreatedAt <= end);

            var krafterRequests = request.OfType<KrafterRequest>()
                .IncludeOptimized(i =>
                    i.Replies.Where(rr => !rr.IsDeleted).Select(rr => rr.Files.Where(f => !f.IsDeleted)))
                .Select(s => new UserStaticDto
                {
                    Type = UserStatisticTypes.Request,
                    Status = s.Replies
                        .Any(i => i.Files.Any(f => f.DocumentType == DocumentType.RequestContract)),
                    CreatedAt = s.CreatedAt,
                });

            var auctionRequests = request.OfType<AuctionRequest>()
                .Select(s => new UserStaticDto
                {
                    Type = UserStatisticTypes.Auction,
                    Status = auctionStatusList.Contains(s.AuctionRequestStatus.Value) && s.ActiveUntil >= start &&
                             s.ActiveUntil <= end,
                    CreatedAt = s.CreatedAt,
                });

            return new UsersStatisticDiagramViewModel
            {
                RequestCount = await krafterRequests.CountAsync(),
                RequestDoneCount = await krafterRequests.CountAsync(c => c.Status),
                AuctionCount = await auctionRequests.CountAsync(),
                AuctionDoneCount = await auctionRequests.CountAsync(c => c.Status)
            };
        }

        /// <summary>
        ///     Download UsersStatistic in Excel spreadsheet format.
        /// </summary>
        /// <returns>Spreadsheet with requested UsersStatistic.</returns>
        /// <response code="200">The spreadsheet was successfully built.</response>
        [Produces("application/json")]
        [ODataRoute("ExportStatisticToExcel")]
        public async Task<IActionResult> GetExportStatisticToExcel(DateTime start, DateTime end)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var roles = User.FindAll(System.Security.Claims.ClaimTypes.Role).Select(i => i.Value);

            if (roles.All(r => r != UserRoles.Admin))
            {
                return BadRequest();
            }

            var auctionStatusList = new List<AuctionRequestStatus>
            {
                AuctionRequestStatus.BiddingActive,
                AuctionRequestStatus.ResourceInput,
                AuctionRequestStatus.ResourceTreatment,
                AuctionRequestStatus.DocSignInput,
                AuctionRequestStatus.DocSignTreatment,
                AuctionRequestStatus.TimeWindowInput,
                AuctionRequestStatus.TimeWindowTreatment,
            };

            var request = logistDbContext.Requests.IgnoreQueryFilters().AsNoTracking().AsSplitQuery()
                .Where(x => x.CreatedAt >= start && x.CreatedAt <= end);

            var krafterRequests = request.OfType<KrafterRequest>()
                .IncludeOptimized(i =>
                    i.Replies.Where(rr => !rr.IsDeleted).Select(rr => rr.Files.Where(f => !f.IsDeleted)))
                .Select(s => new UserStaticDto
                {
                    Type = UserStatisticTypes.Request,
                    Status = s.Replies
                        .Any(i => i.Files.Any(f => f.DocumentType == DocumentType.RequestContract)),
                    CreatedAt = s.CreatedAt,
                }).AsEnumerable();

            var auctionRequests = request.OfType<AuctionRequest>()
                .Select(s => new UserStaticDto
                {
                    Type = UserStatisticTypes.Auction,
                    Status = auctionStatusList.Contains(s.AuctionRequestStatus.Value) && s.ActiveUntil >= start &&
                             s.ActiveUntil <= end,
                    CreatedAt = s.CreatedAt,
                }).AsEnumerable();

            var items = krafterRequests.Concat(auctionRequests).OrderBy(o => o.CreatedAt);

            var ws = xlWorkbook.AddWorksheet("Статистика по пользователям");
            ws.Cell(1, 1).Value =
                "Статистика за период с " + start.ToShortDateString() + " по " + end.ToShortDateString();
            ws.Cell(3, 2).Value = "Гарантии";
            ws.Cell(4, 2).Value = "Опубликовано";
            ws.Cell(4, 3).Value = "Взято";
            ws.Cell(3, 4).Value = "Аукционы";
            ws.Cell(4, 4).Value = "Опубликовано";
            ws.Cell(4, 5).Value = "Выйграно";

            var pointerRequest = 5;

            foreach (var item in items.GroupBy(g => g.CreatedAt.Value.Date)
                .ToDictionary(t => t.Key, t => t))
            {
                ws.Cell(pointerRequest, 1).Value = item.Key.ToShortDateString();
                ws.Cell(pointerRequest, 2).Value = item.Value.Count(x => x.Type == UserStatisticTypes.Request);
                ws.Cell(pointerRequest, 3).Value = item.Value.Count(x => x.Type == UserStatisticTypes.Request
                                                                         && x.Status);
                ws.Cell(pointerRequest, 4).Value = item.Value.Count(x => x.Type == UserStatisticTypes.Auction);
                ws.Cell(pointerRequest, 5).Value = item.Value.Count(x => x.Type == UserStatisticTypes.Auction
                                                                         && x.Status);
                pointerRequest++;
            }

            var memoryStream = new MemoryStream();
            xlWorkbook.SaveAs(memoryStream);
            memoryStream.Seek(0L, SeekOrigin.Begin);

            return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "UsersStatistic.xlsx");
        }
    }
}