﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Crafter.CarrierOffer.WebApp.Models;
using Crafter.CarrierOffer.WebApp.Services;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Krafter.Enums;
using GeoCoordinatePortable;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.KrafterRequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto.RequestsTypesCountDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.OData.UriParser;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(RequestDto))]
    public class RequestDtoController : BaseController<RequestDtoController>
    {
        private readonly RequestDtoRepository repository;
        private readonly IMapper mapper;
        private readonly ICarrierOfferService carrierOfferService;

        public RequestDtoController(ILogger<RequestDtoController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            RequestDtoRepository repository, IMapper mapper, ICarrierOfferService carrierOfferService) : base(logger, logistDbContext, userManager,
            httpContextAccessor)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.carrierOfferService = carrierOfferService;
        }

        /// <summary>
        ///     Get RequestDto.
        /// </summary>
        /// <returns>The requested RequestDto.</returns>
        /// <response code="200">The RequestDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestDto>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 4)]
        public async Task<IEnumerable<RequestDto>> Get(double? lat, double? lon, int? remoteness = null)
        {
            var userId = userManager.GetUserId(User);
            var items = await repository.GetAsync();

            items = items.Where(i => i.RequestStatus == RequestStatus.IsActive ||
                                     (i.RequestStatus == RequestStatus.DataTreatment ||
                                      i.RequestStatus == RequestStatus.DataInput ||
                                      i.RequestStatus == RequestStatus.Cancelled ||
                                      i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            {
                items = items.Where(x => x.Discriminator != RequestDiscriminator.Krafter || x.AssignedUserId == userId);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            {
                items = items.Where(x =>
                    x.Discriminator != RequestDiscriminator.Cargomart &&
                    x.Discriminator != RequestDiscriminator.TrafficOnline &&
                    x.Discriminator != RequestDiscriminator.TorgTrans);
            }

            if (remoteness != null && lat != null && lon != null)
            {
                items = items.Where(i => i.Load is { Lat: { }, Lon: { } })
                    .Where(x => new GeoCoordinate(x.Load.Lat.Value, x.Load.Lon.Value)
                        .GetDistanceTo(new GeoCoordinate((double)lat, (double)lon)) / 1000 <= remoteness);
            }

            var requests = items.ToList();
            var requestIds = requests.Select(x => x.Id).ToList();
            var carrierOffersCount = await carrierOfferService.GetCarrierOffersCount(requestIds, CancellationToken.None);
            foreach (var request in requests)
                request.CarrierOffersCount = carrierOffersCount.TryGetValue(request.Id, out var count) ? count : 0;

            return mapper.Map<IEnumerable<RequestDto>>(requests, options => options.Items.Add("UserId", userId));
        }

        /// <summary>
        ///     Get Requests types count.
        /// </summary>
        /// <returns>The Requests types count.</returns>
        /// <response code="200">The Requests types count was successfully retrieved.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RequestsTypesCountDto>), StatusCodes.Status200OK)]
        public async Task<RequestsTypesCountDto> TypesCount()
        {
            var userId = userManager.GetUserId(User);
            var items = await repository.GetAsync();

            items = items.Where(i => i.RequestStatus == RequestStatus.IsActive  ||
                                     (i.RequestStatus == RequestStatus.DataTreatment ||
                                      i.RequestStatus == RequestStatus.DataInput ||
                                      i.RequestStatus == RequestStatus.Cancelled ||
                                      i.RequestStatus == RequestStatus.Error) && i.InProgressUserId == userId);

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.RequestsAccess)))
            {
                items = items.Where(x => x.Discriminator != RequestDiscriminator.Krafter || x.AssignedUserId == userId);
            }

            if (!User.HasClaim(i => i.Value.Contains(UserPolicies.AuctionsAccess)))
            {
                items = items.Where(x =>
                    x.Discriminator != RequestDiscriminator.Cargomart &&
                    x.Discriminator != RequestDiscriminator.TrafficOnline &&
                    x.Discriminator != RequestDiscriminator.TorgTrans);
            }

            return new RequestsTypesCountDto
            {
                All = items.Count(i =>
                    new[] {RequestStatus.IsActive, RequestStatus.DataInput, RequestStatus.DataTreatment}.Contains(
                        i.RequestStatus)),
                Favorite = items.Count(i =>
                    i.UserRequests.Count(c =>
                        c.Status == UserRequestStatus.Favorite && c.UserId == userId) > 0),
                MyRequest = items.Count(i =>
                    i.AssignedUserId == userId && i.InProgressUserId != userId &&
                    i.RequestStatus != RequestStatus.Cancelled),
                InProgress = items
                    .Count(i =>
                        i.InProgressUserId == userId &&
                        new[] {RequestStatus.IsActive, RequestStatus.DataInput, RequestStatus.DataTreatment}.Contains(
                            i.RequestStatus)),
                OnSign = items.OfType<KrafterRequestDto>().Count(i =>
                    i.RequestStatus == RequestStatus.DataInput &&
                    i.KrafterRequestStatus == KrafterRequestStatus.DocSignInput &&
                    i.InProgressUserId == userId),
                ApproveInput = items.OfType<KrafterRequestDto>()
                    .Count(i =>
                        i.KrafterRequestStatus == KrafterRequestStatus.LogistDocumentsApproveInput &&
                        i.InProgressUserId == userId),
                Cancelled = items.OfType<KrafterRequestDto>().Count(i =>
                    i.UserRequests.All(a => a.Status != UserRequestStatus.Hidden) &&
                    (i.RequestStatus == RequestStatus.Cancelled &&
                     (i.KrafterRequestStatus == KrafterRequestStatus.CarrierChangeRequestInput ||
                      i.KrafterRequestStatus == KrafterRequestStatus.CancelledByCarrier
                      || i.KrafterRequestStatus == KrafterRequestStatus.CancelledByClient) 
                   //  || i.RequestStatus == RequestStatus.Error
                     ) &&
                    (/*i.InProgressUserId == userId ||*/ i.AssignedUserId == userId)),
                Bidding = items.OfType<AuctionRequestDto>().Count(i =>
                    (i.RequestStatus == RequestStatus.IsActive ||
                     i.AuctionRequestStatus == AuctionRequestStatus.BiddingActive) && i.InProgressUserId == userId),
            };
        }
    }
}