﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Requests.Guaranties.Enums;
using DelegateDecompiler;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using RequestStatus = DAL.Models.Requests.RequestStatus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class KrafterRequestRoutePointController : BaseController<KrafterRequestRoutePointController>
    {
        public KrafterRequestRoutePointController(ILogger<KrafterRequestRoutePointController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Updates an existing RequestRoutePoint.
        /// </summary>
        /// <param name="key">The requested RequestRoutePoint identifier.</param>
        /// <param name="delta">The partial RequestRoutePoint to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="200">The RequestRoutePoint was successfully updated.</response>
        /// <response code="400">The RequestRoutePoint is invalid.</response>
        /// <response code="404">The RequestRoutePoint does not exist.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<KrafterRequestRoutePoint> delta,
            CancellationToken cancellationToken)
        {
            var userId = userManager.GetUserId(User);

            if (delta.GetChangedPropertyNames().Contains(nameof(KrafterRequestRoutePoint.RequestId)))
            {
                ModelState.AddModelError(nameof(KrafterRequestRoutePoint.Request),
                    "Запрещено менять привязанное задание");
            }

            var item = await logistDbContext.KrafterRequestRoutePoints
                .IncludeOptimized(i => i.Request)
                .FirstOrDefaultAsync(i => i.Id == key, cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            if (item.Request.RequestStatus != RequestStatus.IsActive &&
                item.Request.RequestStatus != RequestStatus.DataTreatment &&
                item.Request.RequestStatus != RequestStatus.DataInput &&
                item.Request.RequestStatus != RequestStatus.Error)
            {
                return BadRequest(ModelState);
            }

            if (item.Request.InProgressUserId != userId)
            {
                return BadRequest(ModelState);
            }

            delta.Patch(item);

            if (item.ArrivalDateFact < DateTime.Now)
            {
                ModelState.AddModelError(nameof(KrafterRequestRoutePoint.ArrivalDateFact),
                    "Введена некорректная дата!");
            }

            if (item.LeaveDateFact < DateTime.Now)
            {
                ModelState.AddModelError(nameof(KrafterRequestRoutePoint.LeaveDateFact),
                    "Введена некорректная дата!");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await logistDbContext.SaveChangesWithTrackingAsync<KrafterRequestRoutePoint>(cancellationToken)
                .ContinueWith(t =>
                {
                    if (!t.Result.Any())
                    {
                        return;
                    }

                    BackgroundJob.Enqueue<ChangesTrackService>(
                        i =>
                            i.TrackRequestRoutePointAsync(item.Id, RequestActionStatus.AddChanges, t.Result, userId));
                }, cancellationToken);

            logger.LogInformation($"Edited request route point: id - {item.Id}");

            return Updated(item);
        }
    }
}