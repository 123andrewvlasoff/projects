﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests.Krafter.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    public class DeniedReasonController : BaseController<DeniedReasonController>
    {
        public DeniedReasonController(ILogger<DeniedReasonController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get DeniedReasones.
        /// </summary>
        /// <returns>The requested DeniedReasones.</returns>
        /// <response code="200">The DeniedReasones was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<DeniedReason>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<DeniedReason> Get()
        {
            return logistDbContext.DeniedReasons.AsQueryable();
        }

        /// <summary>
        ///     Get TransportStatus by Id.
        /// </summary>
        /// <returns>The requested TransportStatus.</returns>
        /// <response code="200">The TransportStatus was successfully retrieved.</response>
        /// <response code="404">The TransportStatus does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(DeniedReason), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.DeniedReasons.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}