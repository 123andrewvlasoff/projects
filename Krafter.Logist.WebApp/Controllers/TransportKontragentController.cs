using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Dictionaries.Enums;
using DAL.Models.Enums;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragent;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragent.SuitableInfoDto;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(TransportKontragent))]
    public class TransportKontragentController : BaseController<TransportKontragentController>
    {
        public TransportKontragentController(ILogger<TransportKontragentController> logger,
            LogistDbContext logistDbContext, UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get TransportKontragents.
        /// </summary>
        /// <returns>The requested TransportKontragents.</returns>
        /// <response code="200">The TransportKontragent was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IQueryable<TransportKontragent>), 200)]
        [ExpandIgnoreEnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.False)]
        public IEnumerable<TransportKontragent> Get()
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return logistDbContext.TransportKontragents
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.TransportKontragentUsers)
                .IncludeOptimized(i => i.Transport)
                .Select(s => new TransportKontragent
                {
                    Id = s.Id,
                    KontragentId = s.KontragentId,
                    Kontragent = s.Kontragent,
                    TransportId = s.TransportId,
                    Transport = s.Transport,
                    TransportKontragentUsers = s.TransportKontragentUsers,
                    RelationType = s.TransportKontragentUsers.Select(i => i.RelationType).FirstOrDefault()
                                   | s.Kontragent.KontragentUsers.Where(i => i.UserId == userId).Select(i =>
                                           i.RelationType == RelationType.Pinned
                                               ? RelationType.Pinned
                                               : RelationType.None)
                                       .FirstOrDefault()
                });
        }


        /// <summary>
        ///     Get TransportKontragent by Transport Id and Kontragent Id.
        /// </summary>
        /// <returns>The requested TransportKontragent.</returns>
        /// <response code="200">The TransportKontragent was successfully retrieved.</response>
        /// <response code="404">The TransportKontragent does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportKontragent), 200)]
        [ProducesResponseType(404)]
        [EnableQuery(MaxExpansionDepth = 3)]
        [ODataRoute("({transportId},{kontragentId})")]
        public async Task<IActionResult> Get(Guid transportId, Guid kontragentId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TransportKontragents.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.TransportKontragentUsers)
                .IncludeOptimized(i => i.Transport.RollingStockType)
                .IncludeOptimized(i => i.Transport.TransportType)
                .IncludeOptimized(i => i.Transport.Reises.Select(r => r.RoutePoints))
                .Where(i => i.TransportId == transportId && i.KontragentId == kontragentId)
                .FirstOrDefaultAsync();

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     Get SuitableInfoDto.
        /// </summary>
        /// <returns>The SuitableInfoDto.</returns>
        /// <response code="200">The SuitableInfoDto  was successfully retrieved.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(SuitableInfoDto), StatusCodes.Status200OK)]
        [EnableQuery]
        [ODataRoute("SuitableInfo")]
        public async Task<SuitableInfoDto> SuitableInfo(Guid key, CancellationToken cancellationToken)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var tk = await logistDbContext.TransportKontragents
                .IncludeOptimized(i => i.Transport.Reises.Select(s => new
                {
                    s.Transport,
                    s.ConnectedTransport,
                    s.RoutePoints
                }))
                .IncludeOptimized(i => i.Transport.ConnectedTransport)
                .FirstOrDefaultAsync(x => x.Id == key, cancellationToken);

            var result = new SuitableInfoDto();

            if (tk == null)
            {
                return result;
            }

            var bundle = new List<Transport>
            {
                new Transport
                {
                    Id = tk.Transport.Id,
                    Volume = tk.Transport.Volume,
                    Weight = tk.Transport.Weight,
                    RollingStockTypeId = tk.Transport.RollingStockTypeId,
                    ConnectedTransportId = tk.Transport.ConnectedTransportId,
                    ConnectedTransport = tk.Transport.ConnectedTransport,
                    LastReisDate = tk.Transport.LastReisDate,
                    LastAddress = tk.Transport.LastAddress,
                    LastReisLon = tk.Transport.LastReisLon,
                    LastReisLat = tk.Transport.LastReisLat
                },
                new Transport
                {
                    Id = tk.Transport.Id,
                    Volume = (double) tk.Transport?.Volume,
                    Weight = (double) tk.Transport?.Weight,
                    RollingStockTypeId = tk.Transport?.RollingStockTypeId,
                    ConnectedTransportId = tk.Transport.LastReis?.ConnectedTransportId,
                    ConnectedTransport = tk.Transport.LastReis?.ConnectedTransport,
                    LastReisDate = tk.Transport.LastReis?.Unload?.LeaveDatePlan,
                    LastAddress = tk.Transport.LastReis?.Unload?.Address,
                    LastReisLon = tk.Transport.LastReis?.Unload?.Lon,
                    LastReisLat = tk.Transport.LastReis?.Unload?.Lat
                }
            }.OrderByDescending(o => o.LastReisDate).First();

            switch (tk.Transport.TransportTypeId)
            {
                case (int)TransportType.Mule:

                    result.TransportKontragent = logistDbContext.TransportKontragents
                        .FirstOrDefault(x => x.TransportId == bundle.ConnectedTransportId);
                    result.RollingStockType = bundle.RollingStockTypeId ?? 0;
                    result.LastReisDate = bundle.LastReisDate;
                    result.LastAddress = bundle.LastAddress;
                    result.LastReisLat = bundle.LastReisLat;
                    result.LastReisLon = bundle.LastReisLon;

                    break;

                case (int)TransportType.Van:

                    result.TransportKontragent = tk;
                    result.RollingStockType = bundle.RollingStockTypeId ?? 0;
                    result.Volume = bundle.Volume;
                    result.Weight = bundle.Weight;
                    result.LastReisDate = bundle.LastReisDate;
                    result.LastAddress = bundle.LastAddress;
                    result.LastReisLat = bundle.LastReisLat;
                    result.LastReisLon = bundle.LastReisLon;
                    break;

                case (int)TransportType.Semitrailer:

                    result.TransportKontragent = tk;
                    result.RollingStockType = bundle.RollingStockTypeId ?? 0;
                    result.Volume = bundle.Volume;
                    result.Weight = bundle.Weight;

                    break;
            }

            return result;
        }
    }
}