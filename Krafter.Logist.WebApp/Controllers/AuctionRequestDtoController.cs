﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DelegateDecompiler;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Krafter.Logist.WebApp.Models.ReadOnly.AuctionRequestDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class AuctionRequestDtoController : BaseController<AuctionRequestDtoController>
    {
        private readonly RequestDtoRepository repository;

        public AuctionRequestDtoController(ILogger<AuctionRequestDtoController> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor,
            RequestDtoRepository repository) : base(logger,
            logistDbContext, userManager, httpContextAccessor)
        {
            this.repository = repository;
        }

        /// <summary>
        ///     Get AuctionRequestDto.
        /// </summary>
        /// <returns>The requested AuctionRequestDto.</returns>
        /// <response code="200">The AuctionRequestDto was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<AuctionRequestDto>), StatusCodes.Status200OK)]
        [EnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.True)]
        public async Task<IEnumerable<AuctionRequestDto>> Get()
        {
            var userId = userManager.GetUserId(User);

            var items = await repository.GetAsync();

            return items.OfType<AuctionRequestDto>().Where(i =>
                i.RequestStatus == RequestStatus.IsActive && (i.ActiveUntil >
                    DateTime.Now || i.AssignedUserId == userId) ||
                i.RequestStatus != RequestStatus.IsActive &&
                i.InProgressUserId == userId);
        }
    }
}