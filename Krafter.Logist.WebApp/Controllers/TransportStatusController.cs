﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Transports.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace Krafter.Logist.WebApp.Controllers
{
    public class TransportStatusController : BaseController<TransportStatusController>
    {
        public TransportStatusController(ILogger<TransportStatusController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get TransportStatuses.
        /// </summary>
        /// <returns>The requested TransportStatuses.</returns>
        /// <response code="200">The TransportStatuses was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<TransportStatus>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<TransportStatus> Get()
        {
            return logistDbContext.TransportStatuses.AsNoTracking().AsQueryable();
        }

        /// <summary>
        ///     Get TransportStatus by Id.
        /// </summary>
        /// <returns>The requested TransportStatus.</returns>
        /// <response code="200">The TransportStatus was successfully retrieved.</response>
        /// <response code="404">The TransportStatus does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportStatus), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.TransportStatuses.FindAsync(key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}