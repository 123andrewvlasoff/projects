﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DAL.Models.Enums;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.TransporeonRequestFile;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Services;
using Krafter.ServiceCommon.Services.Files;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TransporeonRequestFileController : BaseController<TransporeonRequestFileController>
    {
        private readonly IFileService fileService;

        public TransporeonRequestFileController(ILogger<TransporeonRequestFileController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IFileService fileService) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
            this.fileService = fileService;
        }

        /// <summary>
        ///     Place new File.
        /// </summary>
        /// <response code="204">The File was successfully placed.</response>
        /// <response code="404">The Message does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromQuery] Guid requestId, [FromQuery] DocumentType documentType,
            [FromForm] IFormFile file)
        {
            if (documentType != DocumentType.Proposal && documentType != DocumentType.Proxy)
            {
                ModelState.AddModelError(nameof(documentType), "Невалидный тип документа");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TransporeonRequests.IncludeOptimized(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == requestId);

            if (item == null)
            {
                return NotFound();
            }

            var fileName = Guid.NewGuid().ToString();

            await using var stream = file.OpenReadStream();

            await fileService.PutFileAsync(stream, Folders.Cargomart, fileName);

            item.Files ??= new List<TransporeonRequestFile>();

            item.Files.Add(new TransporeonRequestFile
            {
                CreatedAt = DateTime.UtcNow.AddHours(3),
                OriginalName = file.FileName,
                UploaderId = userManager.GetUserId(User),
                ContentType = file.ContentType,
                DocumentType = documentType,
                Path = Folders.Cargomart + "/" + fileName
            });

            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     Get File.
        /// </summary>
        /// <returns>The requested File.</returns>
        /// <response code="200">The File was successfully retrieved.</response>
        /// <response code="404">The Document does not exist.</response>
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromODataUri] Guid key, DocumentType documentType)
        {
            var item = await logistDbContext.TransporeonRequestFiles
                .FirstOrDefaultAsync(i => i.DocumentType == documentType && i.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            var response = await fileService.GetFileAsync(item.Path);

            return File(response.GetBuffer(), item.ContentType, item.OriginalName);
        }
    }
}
