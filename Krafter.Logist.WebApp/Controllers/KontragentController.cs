using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DAL.Models.Kontragents.Enums;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests;
using DelegateDecompiler.EntityFrameworkCore;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Employer;
using Krafter.Logist.WebApp.Models.Basic.Kontragent;
using Krafter.Logist.WebApp.Models.Basic.KontragentUser;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(Kontragent))]
    public class KontragentController : BaseController<Kontragent>
    {
        public KontragentController(ILogger<Kontragent> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get Kontragents.
        /// </summary>
        /// <returns>The requested Kontragents.</returns>
        /// <response code="200">The Kontragents was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Kontragent>), StatusCodes.Status200OK)]
        [ExpandIgnoreEnableQuery(MaxExpansionDepth = 3)]
        public IEnumerable<Kontragent> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            return logistDbContext.Kontragents.IgnoreQueryFilters()
                .Where(x => x.Status != KontragentStatus.Blocked)
                .Include(dk => dk.KontragentUsers)
                .DecompileAsync()
                .Select(s => new Kontragent
                {
                    Id = s.Id,
                    IsCarrier = s.IsCarrier,
                    IsClient = s.IsClient,
                    Status = s.Status,
                    Inn = s.Inn,
                    Title = s.Title,
                    KontragentGrade = s.KontragentGrade,
                    ClientGrade = s.ClientGrade,
                    RelationType = s.KontragentUsers
                        .Any(ku => ku.UserId == userId)
                        ? s.KontragentUsers
                            .Where(ku => ku.UserId == userId).Select(i => i.RelationType).FirstOrDefault()
                        : RelationType.None,
                    KontragentUsers = s.KontragentUsers
                        .Where(ku => ku.UserId == userId || ku.Discriminator == KontragentUserType.Carrier)
                        .Select(ku => new KontragentUser
                        {
                            Id = ku.Id,
                            KontragentId = ku.KontragentId,
                            UserId = ku.UserId,
                            User = ku.User,
                            Discriminator = ku.Discriminator,
                            RelationType = ku.RelationType,
                        }).ToList()
                });
        }

        /// <summary>
        ///     Get Kontragent by Id.
        /// </summary>
        /// <returns>The requested Kontragent.</returns>
        /// <response code="200">The Kontragent was successfully retrieved.</response>
        /// <response code="404">The Kontragent does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Kontragent), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.Kontragents
                .Where(x => x.Status != KontragentStatus.Blocked)
                .IncludeOptimized(i => i.KontragentUsers)
                .IncludeOptimized(i => i.KontragentUsers.Select(s => s.User))
                .IncludeOptimized(i => i.KontragentFiles)
                .IncludeOptimized(i => i.KontragentContracts.Select(s => s.CrafterCompany))
                .DecompileAsync()
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     Get UserReisClients.
        /// </summary>
        /// <returns>The requested UserReisClients.</returns>
        /// <response code="200">The UserReisClients was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Kontragent>), StatusCodes.Status200OK)]
        [ODataRoute("UserReisClients")]
        [EnableQuery]
        public async Task<IEnumerable<Kontragent>> GetUserReisClients()
        {
            return logistDbContext.Reises.AsNoTracking()
                .Where(x => x.IsActive && x.AcceptorId == userManager.GetUserId(User))
                .Select(s => s.Client).AsEnumerable()
                .GroupBy(g => g.Id).Select(x => x.First());
        }

        /// <summary>
        ///     Get RequestsClients.
        /// </summary>
        /// <returns>The requested RequestsClients.</returns>
        /// <response code="200">The RequestsClients was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Kontragent>), StatusCodes.Status200OK)]
        [ODataRoute("RequestsClients")]
        [EnableQuery]
        public async Task<IEnumerable<Kontragent>> GetRequestsClients()
        {
            return logistDbContext.Requests.AsNoTracking()
                .Select(s => s.Client).AsEnumerable()
                .GroupBy(g => g.ClientCode).Select(x => x.First());
        }
    }
}