using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.KontragentContactUser;
using Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(KontragentContactUser))]
    public class KontragentContactUserController : BaseController<KontragentContactUserController>
    {
        public KontragentContactUserController(ILogger<KontragentContactUserController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get KontragentContactUsers.
        /// </summary>
        /// <returns>The requested KontragentContactUsers.</returns>
        /// <response code="200">The TransportType was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<KontragentContactUser>), 200)]
        [EnableQuery]
        public IQueryable<KontragentContactUser> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            return logistDbContext.KontragentContacts
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.KontragentContactUsers)
                .Where(i => i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                .Count(ku => ku.RelationType.HasFlag(RelationType.Pinned)) > 0 ||
                            i.KontragentContactUsers.Count > 0)
                .Select(i => new KontragentContactUser()
                {
                    Id = i.KontragentContactUsers.Select(tku => tku.Id).FirstOrDefault(),
                    Comment = i.KontragentContactUsers.Select(tku => tku.Comment).FirstOrDefault(),
                    RelationType = i.KontragentContactUsers.Select(tku => tku.RelationType).FirstOrDefault()
                                   | i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                       .Where(ku => ku.RelationType.HasFlag(RelationType.Pinned))
                                       .Select(ku => RelationType.Pinned)
                                       .FirstOrDefault(),
                    KontragentContact = new KontragentContact()
                    {
                        Id = i.Id,
                        Kontragent = i.Kontragent,
                        KontragentId = i.KontragentId,
                        FirstName = i.FirstName,
                        LastName = i.LastName,
                        Patronymic = i.Patronymic,
                        Email = i.Email,
                        Phone = i.Phone
                    },
                    KontragentContactId = i.Id,
                    UserId = userId
                    // Поправить Unable to cast object of type 'System.String' to type 'Microsoft.AspNet.OData.Query.Expressions.GroupByWrapper'.
                }).ToList().AsQueryable();
        }

        /// <summary>
        ///     Get KontragentContactUser by Id.
        /// </summary>
        /// <returns>The requested KontragentContactUser.</returns>
        /// <response code="200">The KontragentContactUser was successfully retrieved.</response>
        /// <response code="404">The KontragentContactUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KontragentContactUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = Select)]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.KontragentContactUsers
                .IncludeOptimized(i => i.KontragentContact)
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     add all KontragentContactUser to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created KontragentContactUser.</returns>
        /// <response code="200">The KontragentContactUsers was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> PostAllToFavorite(Guid key)
        {
            var userId = userManager.GetUserId(User);

            var ids = logistDbContext.KontragentContacts
                .IncludeOptimized(i => i.KontragentContactUsers)
                .Where(i => i.KontragentId == key && !i.KontragentContactUsers.Any())
                .Select(i => i.Id)
                .AsAsyncEnumerable();

            var items = new List<KontragentContactUser>();

            await foreach (var id in ids)
            {
                items.Add(new KontragentContactUser
                {
                    RelationType = RelationType.Favorite,
                    KontragentContactId = id,
                    UserId = userId
                });
            }

            await logistDbContext.KontragentContactUsers.AddRangeAsync(items);
            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     delete all KontragentContactUsers to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created KontragentContactUser.</returns>
        /// <response code="200">The KontragentContactUsers was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> DeleteAllToFavorite(Guid key)
        {
            var items = await logistDbContext.KontragentContactUsers
                .IncludeOptimized(i => i.KontragentContact)
                .Where(i => i.KontragentContact.KontragentId == key)
                .ToListAsync();

            logistDbContext.KontragentContactUsers.RemoveRange(items);

            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     Places a new KontragentContactUser.
        /// </summary>
        /// <param name="item">The KontragentContactUser to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created KontragentContactUser.</returns>
        /// <response code="201">The KontragentContactUser was successfully placed.</response>
        /// <response code="400">The KontragentContactUser is invalid.</response>
        [ProducesResponseType(typeof(KontragentContactUser), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] KontragentContactUser item,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.UserId = userManager.GetUserId(User);

            item.RelationType = RelationType.Favorite;

            await logistDbContext.KontragentContactUsers.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(item);
        }

        /// <summary>
        ///     Updates an existing KontragentContactUser.
        /// </summary>
        /// <param name="key">The requested KontragentContactUser identifier.</param>
        /// <param name="delta">The partial KontragentContactUser to update.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created order.</returns>
        /// <response code="204">The KontragentContactUser was successfully updated.</response>
        /// <response code="400">The KontragentContactUser is invalid.</response>
        /// <response code="404">The KontragentContactUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KontragentContactUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<KontragentContactUser> delta,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.KontragentContactUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            item.Comment = delta.GetInstance().Comment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }

        /// <summary>
        ///     delete an KontragentContactUsers.
        /// </summary>
        /// <param name="key">The KontragentContactUsers to cancel.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>None</returns>
        /// <response code="204">The KontragentContactUsers was successfully deleted.</response>
        /// <response code="404">The KontragentContactUsers does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item = await logistDbContext.KontragentContactUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.KontragentContactUsers.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}