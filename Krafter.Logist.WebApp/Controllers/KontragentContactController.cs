using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    public class KontragentContactController : BaseController<KontragentContact>
    {
        public KontragentContactController(ILogger<KontragentContact> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get KontragentContacts.
        /// </summary>
        /// <returns>The requested KontragentContacts.</returns>
        /// <response code="200">The KontragentContacts was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IQueryable<KontragentContact>), 200)]
        [ExpandIgnoreEnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.False)]
        public IEnumerable<KontragentContact> Get(ODataQueryOptions<KontragentContact> options)
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var items = logistDbContext.KontragentContacts
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.KontragentContactUsers);

            if (options.Filter != null)
            {
                items = options.Filter.ApplyTo(items.Decompile(),
                    new ODataQuerySettings
                    {
                        HandleNullPropagation = HandleNullPropagationOption.False
                    }) as IQueryable<KontragentContact>;
            }

            return items!
                .Select(s => new KontragentContact
                {
                    Id = s.Id,
                    IsActive = s.IsActive,
                    Email = s.Email,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Patronymic = s.Patronymic,
                    KontragentId = s.KontragentId,
                    Kontragent = s.Kontragent,
                    KontragentContactUsers = s.KontragentContactUsers,
                    RelationType = s.KontragentContactUsers.Select(i => i.RelationType).FirstOrDefault()
                                   | s.Kontragent.KontragentUsers.Where(i => i.UserId == userId).Select(i =>
                                           i.RelationType == RelationType.Pinned
                                               ? RelationType.Pinned
                                               : RelationType.None)
                                       .FirstOrDefault()
                }).ToList();
        }


        /// <summary>
        ///     Get KontragentContact by Id.
        /// </summary>
        /// <returns>The requested KontragentContact.</returns>
        /// <response code="200">The KontragentContact was successfully retrieved.</response>
        /// <response code="404">The KontragentContact does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(KontragentContact), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ExpandIgnoreEnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.False)]
        public async Task<IActionResult> Get(int key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KontragentContacts.Where(i => i.IsActive)
                .IncludeOptimized(dk => dk.Kontragent.KontragentUsers)
                .IncludeOptimized(dk => dk.KontragentContactUsers)
                .FirstOrDefaultAsync(r => r.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}