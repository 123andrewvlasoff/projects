﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Models.Enums;
using EasyCaching.Core;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.LogistKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragent;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser;
using Krafter.Logist.WebApp.Models.Basic.TransportKontragentUser.Dto;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlKata.Execution;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(TransportKontragentUser))]
    public partial class TransportKontragentUserController : BaseController<TransportKontragentUser>
    {
        private readonly QueryFactory db;
        private readonly IEasyCachingProvider cache;
        private readonly RequestDtoRepository repository;

        public TransportKontragentUserController(
            ILogger<TransportKontragentUser> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor,
            QueryFactory db,
            IEasyCachingProvider cache,
            RequestDtoRepository repository) : base(logger, logistDbContext, userManager, httpContextAccessor)
        {
            this.db = db;
            this.cache = cache;
            this.repository = repository;
        }

        /// <summary>
        ///     Get TransportUser dto list with suitable Reises.
        /// </summary>
        /// <returns>The requested TransportUser.</returns>
        /// <response code="200">The TransportUser was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<TransportKontragentUserDto>), StatusCodes.Status200OK)]
        [EnableQuery]
        [ODataRoute("ListWithSuitableReises")]
        public async Task<IEnumerable<TransportKontragentUserDto>> GetListWithSuitableReises()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            var items = await GetListAsync();

            items = await AddSuitableReisesCount(items, userId);

            await cache.SetAsync("listWithSuitableReisesSuitableStatistic" + userId, items, TimeSpan.FromMinutes(30));

            BackgroundJob.Enqueue<SuitableStatisticService>(i => i.SendListWithSuitableReisesSuitableStatistic(userId));

            return items;
        }

        /// <summary>
        ///     Get FavoriteTransportKontragentUsers.
        /// </summary>
        /// <returns>The requested TransportKontragentUsers.</returns>
        /// <response code="200">The TransportKontragentUsers was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<TransportKontragentUser>), StatusCodes.Status200OK)]
        [EnableQuery]
        public async Task<IEnumerable<TransportKontragentUser>> Get()
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var userId = userManager.GetUserId(User);

            return logistDbContext.TransportKontragents
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.TransportKontragentUsers)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .Select(i => new TransportKontragentUser()
                {
                    Id = i.TransportKontragentUsers.Select(tku => tku.Id).FirstOrDefault(),
                    RelationType = i.TransportKontragentUsers.Select(tku => tku.RelationType).FirstOrDefault()
                                   | i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                       .Where(ku => ku.UserId == userId && ku.RelationType.HasFlag(RelationType.Pinned))
                                       .Select(ku => RelationType.Pinned)
                                       .FirstOrDefault(),
                    TransportKontragentId = i.Id,
                    TransportKontragent = i,
                });
        }

        /// <summary>
        ///     Get TransportUser by Id.
        /// </summary>
        /// <returns>The requested TransportUser.</returns>
        /// <response code="200">The TransportUser was successfully retrieved.</response>
        /// <response code="404">The TransportUser does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportKontragentUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(MaxExpansionDepth = 4)]
        public async Task<IActionResult> Get(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TransportKontragents
                .IgnoreQueryFilters()
                .IncludeOptimized(i => i.Transport)
                .IncludeOptimized(i => i.TransportKontragentUsers)
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .Where(i => i.Id == key)
                .Select(i => new TransportKontragentUser()
                {
                    Id = i.TransportKontragentUsers.Select(tku => tku.Id).FirstOrDefault(),
                    Comment = i.TransportKontragentUsers.Select(tku => tku.Comment).FirstOrDefault(),
                    RelationType = i.TransportKontragentUsers.Select(tku => tku.RelationType).FirstOrDefault()
                                   | i.Kontragent.KontragentUsers.OfType<LogistKontragentUser>()
                                       .Where(ku => ku.RelationType.HasFlag(RelationType.Pinned))
                                       .Select(ku => RelationType.Pinned)
                                       .FirstOrDefault(),
                    TransportKontragentId = i.Id,
                    TransportKontragent = new TransportKontragent()
                    {
                        TransportId = i.TransportId,
                        Id = i.Id,
                        Transport = i.Transport
                    },
                }).FirstOrDefaultAsync();

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///     Places a new FavoriteTransportKontragent.
        /// </summary>
        /// <param name="item">The FavoriteTransportKontragent to place.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The created FavoriteTransportKontragent.</returns>
        /// <response code="201">The FavoriteTransportKontragent was successfully placed.</response>
        /// <response code="400">The FavoriteTransportKontragent is invalid.</response>
        [ProducesResponseType(typeof(TransportKontragentUser), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] TransportKontragentUser item,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.UserId = userManager.GetUserId(User);

            item.RelationType = RelationType.Favorite;

            await logistDbContext.TransportKontragentUsers.AddAsync(item, cancellationToken);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Created(item);
        }

        /// <summary>
        ///     add all FavoriteTransportKontragent to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created FavoriteTransportKontragent.</returns>
        /// <response code="200">The FavoriteTransportKontragent was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> PostAllToFavorite(Guid key)
        {
            var userId = userManager.GetUserId(User);

            var ids = logistDbContext.TransportKontragents
                .IncludeOptimized(i => i.TransportKontragentUsers)
                .Where(i => i.KontragentId == key && !i.TransportKontragentUsers.Any())
                .Select(i => i.Id)
                .AsAsyncEnumerable();

            var items = new List<TransportKontragentUser>();

            await foreach (var id in ids)
            {
                items.Add(new TransportKontragentUser
                {
                    RelationType = RelationType.Favorite,
                    TransportKontragentId = id,
                    UserId = userId
                });
            }

            await logistDbContext.TransportKontragentUsers.AddRangeAsync(items);
            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     delete all KontragentContact to favorite by Kontragent id.
        /// </summary>
        /// <param name="key">Kontragent id.</param>
        /// <returns>The created FavoriteKontragentContact.</returns>
        /// <response code="200">The FavoriteKontragentContacts was successfully placed.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ODataRoute("({key})/AllToFavorite")]
        public async Task<IActionResult> DeleteAllToFavorite(Guid key)
        {
            var items = await logistDbContext.TransportKontragentUsers
                .IncludeOptimized(i => i.TransportKontragent)
                .Where(i => i.TransportKontragent.KontragentId == key)
                .ToListAsync();

            logistDbContext.TransportKontragentUsers.RemoveRange(items);

            await logistDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        ///     Updates an existing FavoriteTransportKontragent.
        /// </summary>
        /// <param name="key">The requested FavoriteTransportKontragent identifier.</param>
        /// <param name="delta">The partial FavoriteTransportKontragent to update.</param>
        /// <returns>The created order.</returns>
        /// <response code="204">The FavoriteTransportKontragent was successfully updated.</response>
        /// <response code="400">The FavoriteTransportKontragent is invalid.</response>
        /// <response code="404">The FavoriteTransportKontragent does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(TransportKontragentUser), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(Guid key, Delta<TransportKontragentUser> delta,
            CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var item = await logistDbContext.TransportKontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            item.Comment = delta.GetInstance().Comment;

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return Updated(item);
        }

        /// <summary>
        ///     Delete an FavoriteTransportKontragent.
        /// </summary>
        /// <param name="key">The FavoriteTransportKontragent to cancel.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>None</returns>
        /// <response code="204">The FavoriteTransportKontragent was successfully deleted.</response>
        /// <response code="404">The FavoriteTransportKontragent does not exist.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid key, CancellationToken cancellationToken)
        {
            var item = await logistDbContext.TransportKontragentUsers.FindAsync(new object[] { key },
                cancellationToken);

            if (item == null)
            {
                return NotFound();
            }

            logistDbContext.TransportKontragentUsers.Remove(item);

            await logistDbContext.SaveChangesAsync(cancellationToken);

            return NoContent();
        }
    }
}