﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.Notification;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(Notification))]
    public class NotificationController : BaseController<NotificationController>
    {
        public NotificationController(ILogger<NotificationController> logger,
            LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger,
            logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get Notification.
        /// </summary>
        /// <returns>The requested Notifications</returns>
        /// <response code="200">Notifications were successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Notification>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<Notification> Get()
        {
            return logistDbContext.Notifications;
        }

        /// <summary>
        ///     Get Notification by Id.
        /// </summary>
        /// <returns>The requested Notification.</returns>
        /// <response code="200">The Notification was successfully retrieved.</response>
        /// <response code="404">The Notification does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Notification), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        public async Task<IActionResult> Get(Guid key)
        {
            var item = await logistDbContext.Notifications.FirstOrDefaultAsync(n => n.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }

        /// <summary>
        ///   Read One Notification By User.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("({key})/Read")]
        public async Task<IActionResult> PatchRead(Guid key)
        {
            var item = await logistDbContext.Notifications.FirstOrDefaultAsync(n => n.Id == key);

            if (item == null)
            {
                return NotFound();
            }

            item.HasRead = true;
            await logistDbContext.SaveChangesAsync();

            return Updated(item);
        }


        /// <summary>
        ///   Read All Notifications by User.
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ODataRoute("ReadAll")]
        public async Task<IActionResult> PatchReadAll()
        {
            var items = logistDbContext.Notifications.ToList();

            foreach (var item in items)
            {
                item.HasRead = true;
            }

            await logistDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}