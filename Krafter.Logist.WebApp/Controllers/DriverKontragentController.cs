using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Enums;
using DelegateDecompiler;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.DriverKontragent;
using Krafter.Logist.WebApp.Models.Basic.User;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(DriverKontragent))]
    public class DriverKontragentController : BaseController<DriverKontragent>
    {
        public DriverKontragentController(ILogger<DriverKontragent> logger, LogistDbContext logistDbContext,
            UserManager<User> userManager, IHttpContextAccessor httpContextAccessor) : base(logger, logistDbContext,
            userManager, httpContextAccessor)
        {
        }

        /// <summary>
        ///     Get DriverKontragents.
        /// </summary>
        /// <returns>The requested DriverKontragents.</returns>
        /// <response code="200">The DriverKontragents was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IQueryable<DriverKontragent>), 200)]
        [ExpandIgnoreEnableQuery(MaxExpansionDepth = 4, HandleNullPropagation = HandleNullPropagationOption.False)]
        public IEnumerable<DriverKontragent> Get()
        {
            var userId = userManager.GetUserId(User);

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            return logistDbContext.DriverKontragents
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .IncludeOptimized(i => i.DriverKontragentUsers)
                .IncludeOptimized(i => i.Driver)
                .Select(s => new DriverKontragent
                {
                    Id = s.Id,
                    KontragentId = s.KontragentId,
                    Kontragent = s.Kontragent,
                    DriverId = s.DriverId,
                    Driver = s.Driver,
                    DriverKontragentUsers = s.DriverKontragentUsers,
                    RelationType = s.DriverKontragentUsers.Select(i => i.RelationType).FirstOrDefault()
                                   | s.Kontragent.KontragentUsers.Where(i => i.UserId == userId).Select(i =>
                                           i.RelationType == RelationType.Pinned
                                               ? RelationType.Pinned
                                               : RelationType.None)
                                       .FirstOrDefault()
                });
        }

        /// <summary>
        ///     Get DriverKontragent by Driver Id and Kontragent Id.
        /// </summary>
        /// <returns>The requested DriverKontragent.</returns>
        /// <response code="200">The DriverKontragent was successfully retrieved.</response>
        /// <response code="404">The DriverKontragent does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(DriverKontragent), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery]
        [ODataRoute("({driverId},{kontragentId})")]
        public async Task<IActionResult> Get(Guid driverId, Guid kontragentId)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.DriverKontragents.IgnoreQueryFilters()
                .IncludeOptimized(i => i.DriverKontragentUsers)
                .IncludeOptimized(i => i.Driver)
                .FirstOrDefaultAsync(i => i.DriverId == driverId && i.KontragentId == kontragentId);

            if (item == null)
            {
                return NotFound();
            }

            return StatusCode(StatusCodes.Status200OK, item);
        }
    }
}