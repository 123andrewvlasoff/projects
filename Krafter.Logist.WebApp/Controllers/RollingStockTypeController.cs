﻿using System.Collections.Generic;
using System.Linq;
using DAL.Models.Dictionaries;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace Krafter.Logist.WebApp.Controllers
{
    [ODataRoutePrefix(nameof(RollingStockType))]
    public class RollingStockTypeController : BaseController<RollingStockTypeController>
    {
        public RollingStockTypeController(ILogger<RollingStockTypeController> logger, LogistDbContext logistDbContext) :
            base(logger, logistDbContext)
        {
        }

        /// <summary>
        ///     Get RollingStockTypes.
        /// </summary>
        /// <returns>The requested RollingStockTypes.</returns>
        /// <response code="200">The RollingStockTypes was successfully retrieved.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RollingStockType>), StatusCodes.Status200OK)]
        [EnableQuery]
        public IQueryable<RollingStockType> Get()
        {
            return logistDbContext.RollingStockTypes.AsNoTracking().AsQueryable();
        }

        /// <summary>
        ///     Get RollingStockType by Id.
        /// </summary>
        /// <returns>The requested RollingStockType.</returns>
        /// <response code="200">The RollingStockType was successfully retrieved.</response>
        /// <response code="404">The RollingStockType does not exist.</response>
        [Produces("application/json")]
        [ProducesResponseType(typeof(RollingStockType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [EnableQuery(AllowedQueryOptions = Select)]
        public RollingStockType Get(int key)
        {
            return logistDbContext.RollingStockTypes.FirstOrDefault(r => r.Id == key);
        }
    }
}