using System;
using System.Collections.Generic;

namespace Krafter.Logist.WebApp.Views.Email.SupportTicket
{
    public class SupportTicketViewModel
    {
        public string Author { get; set; }

        public IEnumerable<string> Messages { get; set; }

        public string Url { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Type { get; set; }
    }
}