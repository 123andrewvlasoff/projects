﻿namespace Krafter.Logist.WebApp.Views.Email.Enums
{
    public enum SubjectType
    {
        LogistAccept,
        LogistCancelRequest,
        LogistReplyForEmployer,

        LogistPaperSignWaitingForCarrier,
        LogistPaperSignWaitingForLogist,

        LogistDigitalSignWaitingForCarrier,
        LogistDigitalSignWaitingForLogist,

        LogistCreateUserRequest,
        LogistDeleteUserRequest,

        RequestReservationExpired,
        RequestChangedAssignedUser,
        RequestSetAssignedUser,
        RequestUnblocked
    }
}