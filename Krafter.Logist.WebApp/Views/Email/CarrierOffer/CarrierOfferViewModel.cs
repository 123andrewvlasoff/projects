﻿using System;
using DAL.Models.Users;
using KrafterRequestRoutePoint = Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint.KrafterRequestRoutePoint;

namespace Krafter.Logist.WebApp.Views.Email.CarrierOffer
{
    public class CarrierOfferViewModel
    {
        public Guid RequestId { get; set; }

        public string RequestNumber1C { get; set; }

        public string TransportationNumber { get; set; }

        public string Client { get; set; }

        public KrafterRequestRoutePoint Load { get; set; }

        public KrafterRequestRoutePoint Unload { get; set; }

        public decimal Price { get; set; }

        public string Reason { get; set; }

        public bool isCarrierNDS { get; set; }
    }
}