using System;
using DAL.Models.Users;
using Krafter.Logist.WebApp.Models.Basic.Driver;
using Krafter.Logist.WebApp.Models.Basic.KontragentContact;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReply;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Models.Basic.Transport;
using KrafterRequestRoutePoint = Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint.KrafterRequestRoutePoint;

namespace Krafter.Logist.WebApp.Views.Email.RequestReply
{
    public class RequestReplyViewModel
    {
        public Guid RequestId { get; set; }

        public string DeniedReason { get; set; }

        public string RequestNumber1C { get; set; }

        public string CarrierTitle { get; set; }

        public DateTime ReplyAt { get; set; }

        public string CarrierComment { get; set; }

        public Employer Contact { get; set; }

        public KontragentContact KontragentContact { get; set; }

        public Transport Transport { get; set; }

        public Transport ConnectedTransport { get; set; }

        public Driver Driver { get; set; }

        public string ReisNumber1C { get; set; }

        public int RequestNumber { get; set; }

        public string RequestCode { get; set; }

        public string Client { get; set; }
        
        public string LogistComment { get; set; }

        public KrafterRequestRoutePoint Load { get; set; }

        public KrafterRequestRoutePoint Unload { get; set; }

        public double Price { get; set; }
    }
}