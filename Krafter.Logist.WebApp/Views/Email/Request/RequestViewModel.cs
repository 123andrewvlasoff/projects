﻿using System;
using DAL.Models.Users;
using KrafterRequestRoutePoint = Krafter.Logist.WebApp.Models.Basic.KrafterRequestRoutePoint.KrafterRequestRoutePoint;

namespace Krafter.Logist.WebApp.Views.Email.Request
{
    public class RequestViewModel
    {
        public Guid RequestId { get; set; }

        public string RequestNumber1C { get; set; }
        
        public string TransportationNumber { get; set; }

        public string Client { get; set; }

        public KrafterRequestRoutePoint Load { get; set; }

        public KrafterRequestRoutePoint Unload { get; set; }

        public decimal Price { get; set; }

        public User TakenBy { get; set; }

        public bool TakeInProgress { get; set; }
    }
}