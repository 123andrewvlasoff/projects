﻿using System;
using System.Threading.Tasks;
using EasyCaching.Core;
using Krafter.IdentityServer.Extensions.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TransporeonsHub : Hub
    {
        private readonly IEasyCachingProvider cache;

        public TransporeonsHub(IEasyCachingProvider cache)
        {
            this.cache = cache;
        }

        public async Task Subscribe(Guid requestId)
        {
            await cache.SetAsync(nameof(TransporeonsHub) + $"_${Context.ConnectionId}", requestId, TimeSpan.FromMinutes(20));
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await cache.RemoveAsync(nameof(TransporeonsHub) + $"_${Context.ConnectionId}");
        }
    }
}
