using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize]
    public class MessagesHub : Hub
    {
    }
}