using System;
using System.Linq;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize]
    public class NotificationsHub : Hub
    {
        private readonly LogistDbContext logistDbContext;

        public NotificationsHub(LogistDbContext logistDbContext)
        {
            this.logistDbContext = logistDbContext;
        }

        public async Task ReadById(Guid key)
        {
            var item = await logistDbContext.Notifications.IgnoreQueryFilters()
                .FirstOrDefaultAsync(n => n.Id == key);

            item.HasRead = true;
            await logistDbContext.SaveChangesAsync();
        }

        public async Task ReadAll(string userId)
        {
            var items = logistDbContext.Notifications.IgnoreQueryFilters()
                .Where(x => x.UserId == userId);

            foreach (var item in items)
            {
                item.HasRead = true;
            }

            await logistDbContext.SaveChangesAsync();
        }
    }
}