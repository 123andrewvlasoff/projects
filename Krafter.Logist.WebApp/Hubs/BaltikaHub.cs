﻿using System;
using System.Threading.Tasks;
using EasyCaching.Core;
using Krafter.IdentityServer.Extensions.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class BaltikaHub : Hub
    {
        private readonly IEasyCachingProvider cache;

        public BaltikaHub(IEasyCachingProvider cache)
        {
            this.cache = cache;
        }

        public async Task Subscribe(Guid requestId)
        {
            await cache.SetAsync(nameof(BaltikaHub) + $"_${Context.ConnectionId}", requestId, TimeSpan.FromMinutes(20));
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await cache.RemoveAsync(nameof(BaltikaHub) + $"_${Context.ConnectionId}");
        }
    }
}
