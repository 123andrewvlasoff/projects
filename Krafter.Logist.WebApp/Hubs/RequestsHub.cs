using System;
using System.Threading.Tasks;
using EasyCaching.Core;
using Krafter.Logist.WebApp.Hubs.Services;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize]
    public class RequestsHub : Hub
    {
        private readonly CacheSubscriberService cache;
        private readonly UserManager<User> userManager;

        public RequestsHub(CacheSubscriberService cache, UserManager<User> userManager)
        {
            this.cache = cache;
            this.userManager = userManager;
        }

        public async Task Subscribe(Guid requestId)
        {
            var userId = userManager.GetUserId(Context.User);

            await cache.AddRequestSubscriberAsync($"{nameof(RequestsHub)}_{Context.ConnectionId}",
                new RequestSubscriber
                {
                    RequestId = requestId,
                    UserId = userId
                });
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await cache.DeleteRequestSubscriberAsync($"{nameof(RequestsHub)}_{Context.ConnectionId}");
        }
    }
}