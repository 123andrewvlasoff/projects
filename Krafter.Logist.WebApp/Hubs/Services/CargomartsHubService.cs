using System;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class CargomartsHubService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly CacheSubscriberService cache;
        private readonly IHubContext<CargomartsHub> hubContext;

        public CargomartsHubService(LogistDbContext logistDbContext, CacheSubscriberService cache,
            IHubContext<CargomartsHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.cache = cache;
            this.hubContext = hubContext;
        }

        public async Task SendRequestAsync(Guid key)
        {
            var subscribers = await cache.GetRequestSubscribersAsync(nameof(CargomartsHub), key);

            if (!subscribers.Any())
            {
                return;
            }

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.CargomartRequests
                .IgnoreQueryFilters()
                .IncludeOptimized(r => r.RoutePoints)
                .IncludeOptimized(r => r.Replies)
                .IncludeOptimized(r => r.Files)
                .IncludeOptimized(r => r.LoadingType)
                .IncludeOptimized(r => r.RollingStockType)
                .IncludeOptimized(r => r.TransportType)
                .IncludeOptimized(r => r.Client.Contacts)
                .IncludeOptimized(r => r.InProgressUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            foreach (var userId in subscribers.Select(i => i.UserId))
            {
                await hubContext.Clients.User(userId).SendAsync("ReceiveCargomartRequest", item);
            }
        }

        public async Task SendRequestReplyAsync(Guid key)
        {
            var item = await logistDbContext.CargomartRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == key);

            await hubContext.Clients.All.SendAsync("ReceiveCargomartRequestReply", item);
        }
    }
}