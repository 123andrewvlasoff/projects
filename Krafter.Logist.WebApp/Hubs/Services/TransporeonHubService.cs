﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using EasyCaching.Core;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class TransporeonHubService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<TransporeonsHub> hubContext;

        public TransporeonHubService(LogistDbContext logistDbContext, IHubContext<TransporeonsHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.hubContext = hubContext;
        }

        public async Task SendRequestAsync(Guid key)
        {

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TransporeonRequests.IgnoreQueryFilters()
                .IncludeOptimized(r => r.RoutePoints)
                .IncludeOptimized(r => r.Files)
                .IncludeOptimized(r => r.LoadingType)
                .IncludeOptimized(r => r.RollingStockType)
                .IncludeOptimized(r => r.TransportType)
                .IncludeOptimized(r => r.Client.Contacts)
                .IncludeOptimized(r => r.InProgressUser.UserRoles)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            if (item.RequestStatus == RequestStatus.IsActive || (item.AuctionRequestStatus == AuctionRequestStatus.CancellationTreatment &&
                                                                 item.RequestStatus == RequestStatus.DataTreatment)
                                                              ||(item.RequestStatus == RequestStatus.Cancelled && item.InProgressUserId == null))
            {
                await hubContext.Clients.All.SendAsync("ReceiveTransporeonRequest", item);
                return;
            }

            await hubContext.Clients.User(item.InProgressUserId).SendAsync("ReceiveTransporeonRequest", item);
        }

        public async Task SendRequestReplyAsync(Guid key)
        {
            var item = await logistDbContext.TransporeonRequestReplies.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            await hubContext.Clients.User(item.CreatorId).SendAsync("ReceiveTransporeonRequestReply", item);
        }

        public async Task SendTransporeonRequestFile(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TransporeonRequestFiles.IgnoreQueryFilters().AsNoTracking()
            .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            var request = await logistDbContext.TransporeonRequests.IgnoreQueryFilters()
            .FirstOrDefaultAsync(i => i.Id == item.RequestId);

            await hubContext.Clients.User(request.InProgressUserId).SendAsync("ReceiveTransporeonRequestFile", item);
            
        }
    }
}
