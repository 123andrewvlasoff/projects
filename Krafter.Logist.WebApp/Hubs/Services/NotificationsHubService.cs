using System;
using System.Threading.Tasks;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class NotificationsHubService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<NotificationsHub> hubContext;

        public NotificationsHubService(LogistDbContext logistDbContext, IHubContext<NotificationsHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.hubContext = hubContext;
        }
        
        public async Task SendAsync(Guid key)
        {
            var item = await logistDbContext.Notifications.IgnoreQueryFilters()
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            await hubContext.Clients.User(item.UserId).SendAsync("ReceiveNotification", item);
        }
    }
}