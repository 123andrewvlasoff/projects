using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Models.KontragentUser.Enums;
using DAL.Models.Requests.Krafter;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.ServiceCommon.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MimeKit.Text;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using KrafterRequest = Krafter.Logist.WebApp.Models.Basic.KrafterRequest.KrafterRequest;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class RequestsHubService
    {
        private readonly CacheSubscriberService cache;
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<RequestsHub> hubContext;
        private readonly IMapper mapper;
        private readonly IEmailService _email;
        private readonly ILogger<RequestsHubService> _logger;

        public RequestsHubService(CacheSubscriberService cache, LogistDbContext logistDbContext,
            IHubContext<RequestsHub> hubContext, IMapper mapper, IEmailService email,
            ILogger<RequestsHubService> logger)
        {
            this.cache = cache;
            this.logistDbContext = logistDbContext;
            this.hubContext = hubContext;
            this.mapper = mapper;
            _email = email;
            _logger = logger;
    }

    public async Task SendRequestAsync(Guid key)
        {
            var subscribers = await cache.GetRequestSubscribersAsync(nameof(RequestsHub), key);

            if (!subscribers.Any())
            {
                _logger.LogError($"SendRequestAsync subscribers not find request :{key}");
                return;
            }

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequests
                .IgnoreQueryFilters()
                .IncludeOptimized(r => r.RoutePoints.Select(i => new
                {
                    i.Region,
                    i.Settlement
                }))
                .IncludeOptimized(r =>
                    r.Reises.Where(x => x.StatusId != ReisStatus.Cancelled)
                        .Select(s => s.RoutePoints.Select(s => new
                        {
                            s.Region,
                            s.Settlement
                        })))
                .IncludeOptimized(r => r.Client)
                .IncludeOptimized(r => r.Actions)
                .IncludeOptimized(r => r.AssignedUser.Employer)
                .IncludeOptimized(r => r.InProgressUser.Employer)
                //.AsNoTracking()
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            if (item.Reises.Any() && item.Reises.First().IsDeleted)
            {
                item.Reises = new List<Reis>();
            }


            foreach (var userId in subscribers.GroupBy(x => x.UserId))
            {
                var request = mapper.Map<KrafterRequest>(item, options => options.Items.Add("UserId", userId.Key));
                await hubContext.Clients.User(userId.Key).SendAsync("ReceiveRequest", request);
            }
        }

        public async Task SendRequestReplyAsync(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;
            var item = await logistDbContext.KrafterRequestReplies
                .IgnoreQueryFilters()
                .IncludeOptimized(r => r.Files.Where(x => !x.IsDeleted))
                .IncludeOptimized(r => r.Transport)
                .IncludeOptimized(r => r.TransportStatus)
                .IncludeOptimized(r => r.Comments)
                .IncludeOptimized(r => r.Driver)
                .IncludeOptimized(r => r.ConnectedTransport)
                .IncludeOptimized(r => r.KontragentContact)
                .IncludeOptimized(r => r.Kontragent)
                .IncludeOptimized(r => r.Creator.Employer)
                //.AsNoTracking()
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            if (item.IsDeleted)
            {
                await hubContext.Clients.All.SendAsync("DeleteRequestReply", item.Id);
                return;
            }

            await hubContext.Clients.All.SendAsync("ReceiveRequestReply", item);
        }

        public async Task SendRequestReplyFileAsync(Guid key)
        {
            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.KrafterRequestReplyFiles.IgnoreQueryFilters()
                //.AsNoTracking()
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            // @TODO ����, ����� ���������
            var userId = await logistDbContext.KrafterRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Kontragent.KontragentUsers)
                .Where(i => i.Id == item.ReplyId)
                .SelectMany(i => i.Kontragent.KontragentUsers)
                .Where(i => i.Discriminator == KontragentUserType.Logist)
                .Select(i => i.UserId)
                //.AsNoTracking()
                .FirstOrDefaultAsync();
                
            if (userId == null)
                return;
            
            var krafterRequestReplyFile = mapper.Map<KrafterRequestReplyFile>(item);

            await hubContext.Clients.User(userId)
                .SendAsync("ReceiveRequestReplyFile", krafterRequestReplyFile);
        }
    }
}