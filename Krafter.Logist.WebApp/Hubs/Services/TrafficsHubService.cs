using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using EasyCaching.Core;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class TrafficsHubService
    {
        private readonly CacheSubscriberService cache;
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<TrafficsHub> hubContext;

        public TrafficsHubService(LogistDbContext logistDbContext, CacheSubscriberService cache,
            IHubContext<TrafficsHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.cache = cache;
            this.hubContext = hubContext;
        }

        public async Task SendRequestAsync(Guid key)
        {
            var subscribers = await cache.GetRequestSubscribersAsync(nameof(TrafficsHub), key);

            if (!subscribers.Any())
            {
                return;
            }

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TrafficRequests.IgnoreQueryFilters()
                .IncludeOptimized(r => r.Files)
                .Include(r => r.Replies)
                .Include(r => r.RoutePoints)
                .Include(r => r.Client.Contacts)
                .Include(r => r.InProgressUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            foreach (var userId in subscribers.Select(i => i.UserId))
            {
                await hubContext.Clients.User(userId).SendAsync("ReceiveTorgTransRequest", item);
            }
        }

        public async Task SendRequestReplyAsync(Guid key)
        {
            var item = await logistDbContext.TrafficRequestReplies.IgnoreQueryFilters()
                .Include(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == key);

            await hubContext.Clients.All.SendAsync("ReceiveTrafficRequestReply", item);
        }
    }
}