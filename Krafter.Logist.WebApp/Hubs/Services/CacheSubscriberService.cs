using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyCaching.Core;
using Krafter.Logist.WebApp.Configurations;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    public class CacheSubscriberService
    {
        private static readonly TimeSpan CacheLifeTime = TimeSpan.FromMinutes(20);//TimeSpan.FromHours(2);

        private readonly IEasyCachingProvider cache;
        private readonly ILogger<CacheSubscriberService> _logger;

        public CacheSubscriberService(IEasyCachingProviderFactory cache,ILogger<CacheSubscriberService> logger)
        {
            this.cache = cache.GetCachingProvider(CacheConfiguration.ProviderSessions);
            _logger = logger;
        }

        public async Task<IEnumerable<RequestSubscriber>> GetRequestSubscribersAsync(string key, Guid requestId)
        {
            var keys = await cache.GetByPrefixAsync<RequestSubscriber>(key);
            _logger.LogInformation($"GetList key :{key} requestId:{requestId}");
            foreach (var keyItem in keys)
            {
                _logger.LogInformation($"GetRequestSubscribersAsync Item {key} key :{keyItem.Key} UserId:{keyItem.Value?.Value.UserId} RequestId:{keyItem.Value?.Value.RequestId}");
            }

            return keys.Select(i => i.Value?.Value).Where(i => i?.RequestId == requestId);
        }

        public async Task AddRequestSubscriberAsync(string key, RequestSubscriber subscriber)
        {
            _logger.LogInformation($"AddRequestSubscriberAsync key :{key} UserId:{subscriber.UserId} RequestId:{subscriber.RequestId}");
            await cache.SetAsync(key,
                subscriber, CacheLifeTime);
        }

        public async Task DeleteRequestSubscriberAsync(string key)
        {
            _logger.LogInformation($"DeleteRequestSubscriberAsync key :{key}");
            await cache.RemoveAsync(key);
        }
    }
}