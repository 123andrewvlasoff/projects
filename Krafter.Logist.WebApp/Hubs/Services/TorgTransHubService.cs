using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DocumentFormat.OpenXml.InkML;
using EasyCaching.Core;
using Hangfire;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class TorgTransHubService
    {
        private readonly CacheSubscriberService cache;
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<TorgTransHub> hubContext;

        public TorgTransHubService(LogistDbContext logistDbContext, CacheSubscriberService cache,
            IHubContext<TorgTransHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.cache = cache;
            this.hubContext = hubContext;
        }

        public async Task SendRequestAsync(Guid key)
        {
            var subscribers = await cache.GetRequestSubscribersAsync(nameof(TorgTransHub), key);

            if (!subscribers.Any())
            {
                return;
            }

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.TorgTransRequests.IgnoreQueryFilters()
                .IncludeOptimized(r => r.Files)
                .Include(r => r.RoutePoints)
                .Include(r => r.Client.Contacts)
                .Include(r => r.InProgressUser)
                .FirstOrDefaultAsync(i => i.Id == key);

            foreach (var userId in subscribers.Select(i => i.UserId))
            {
                await hubContext.Clients.User(userId).SendAsync("ReceiveTorgTransRequest", item);
            }
        }

        public async Task SendRequestReplyAsync(Guid key)
        {
            var item = await logistDbContext.TorgTransRequestReplies.IgnoreQueryFilters()
                .IncludeOptimized(i => i.Files)
                .FirstOrDefaultAsync(i => i.Id == key);

            await hubContext.Clients.All.SendAsync("ReceiveTorgTransRequestReply", item);
        }
    }
}