﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models.Requests;
using DAL.Models.Requests.Auction.Enums;
using EasyCaching.Core;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class BaltikaHubService
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<BaltikaHub> hubContext;

        public BaltikaHubService(LogistDbContext logistDbContext, IHubContext<BaltikaHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.hubContext = hubContext;
        }

        public async Task SendRequestAsync(Guid key)
        {

            QueryIncludeOptimizedManager.AllowIncludeSubPath = true;

            var item = await logistDbContext.BaltikaRequests.IgnoreQueryFilters()
                .IncludeOptimized(r => r.RoutePoints)
                .IncludeOptimized(r => r.LoadingType)
                .IncludeOptimized(r => r.RollingStockType)
                .IncludeOptimized(r => r.TransportType)
                .IncludeOptimized(r => r.Client.Contacts)
                .IncludeOptimized(r => r.InProgressUser.UserRoles)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            if (item.RequestStatus == RequestStatus.IsActive || (item.AuctionRequestStatus == AuctionRequestStatus.CancellationTreatment &&
                                                                 item.RequestStatus == RequestStatus.DataTreatment)
                                                              ||(item.RequestStatus == RequestStatus.Cancelled && item.InProgressUserId == null))
            {
                await hubContext.Clients.All.SendAsync("ReceiveBaltikaRequest", item);
                return;
            }

            await hubContext.Clients.User(item.InProgressUserId).SendAsync("ReceiveBaltikaRequest", item);
        }
    }
}
