using System;
using System.Threading.Tasks;
using Hangfire;
using Krafter.Logist.WebApp.Models.Basic;
using Krafter.ServiceCommon.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Krafter.Logist.WebApp.Hubs.Services
{
    [Queue("to_front")]
    public class MessagesHubService 
    {
        private readonly LogistDbContext logistDbContext;
        private readonly IHubContext<RequestsHub> hubContext;

        public MessagesHubService(LogistDbContext logistDbContext, IHubContext<RequestsHub> hubContext)
        {
            this.logistDbContext = logistDbContext;
            this.hubContext = hubContext;
        }
        
        public async Task SendAsync(Guid key)
        {
            var item = await logistDbContext.Messages.IgnoreQueryFilters()
                .IncludeOptimized(r => r.Files)
                .IncludeOptimized(r => r.User)
                .IncludeOptimized(r => r.SupportTicket)
                .FirstOrDefaultAsync(i => i.Id == key);

            if (item == null)
                return;
            
            await hubContext.Clients.User(item.SupportTicket.UserId).SendAsync("ReceiveMessage", item);
        }
    }
}