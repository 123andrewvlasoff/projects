﻿using System;

namespace Krafter.Logist.WebApp.Hubs
{
    public class RequestSubscriber
    {
        public Guid RequestId { get; set; }

        public string UserId { get; set; }
    }
}