using System;
using System.Threading.Tasks;
using Krafter.IdentityServer.Extensions.Constants;
using Krafter.Logist.WebApp.Hubs.Services;
using Krafter.Logist.WebApp.Models.Basic.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;

namespace Krafter.Logist.WebApp.Hubs
{
    [Authorize(Policy = UserPolicies.AuctionsAccess)]
    public class TorgTransHub : Hub
    {
        private readonly CacheSubscriberService cache;
        private readonly UserManager<User> userManager;

        public TorgTransHub(CacheSubscriberService cache, UserManager<User> userManager)
        {
            this.cache = cache;
            this.userManager = userManager;
        }

        public async Task Subscribe(Guid requestId)
        {
            var userId = userManager.GetUserId(Context.User);

            await cache.AddRequestSubscriberAsync($"{nameof(TorgTransHub)}_{Context.ConnectionId}",
                new RequestSubscriber
                {
                    RequestId = requestId,
                    UserId = userId
                });
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await cache.DeleteRequestSubscriberAsync($"{nameof(TorgTransHub)}_{Context.ConnectionId}");
        }
    }
}