﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class MassTransitInMemoryConfiguration
    {
        public static void ConfigureMassTransitInMemory(this IServiceCollection services,
    IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.UsingInMemory((context, cfg) =>
                {
                    cfg.ConfigureEndpoints(context);
                });
            });
        }
    }
}
