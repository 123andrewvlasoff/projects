using Krafter.Client.Atisu.Configuration;
using Krafter.Client.Atisu.Services;
using Krafter.Logist.WebApp.Services.Atisu;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Krafter.Logist.WebApp.Configurations.Atisu
{
    public static class AtisuConfiguration
    {
        public static IServiceCollection ConfigureAtisu(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.ConfigureAtisuClient(configuration)
                .AddAtisuEasyCache();
            
            services.AddSingleton<AtisuInitializationService>();
            services.AddScoped<IAtisuDictionaryService, AtisuDictionaryService>();
            services.AddHostedService<AtisuHostedService>();

            return services;
        }
    }
}