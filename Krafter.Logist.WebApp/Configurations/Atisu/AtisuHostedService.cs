using System.Threading;
using System.Threading.Tasks;
using Krafter.Logist.WebApp.Services.Atisu;
using Microsoft.Extensions.Hosting;

namespace Krafter.Logist.WebApp.Configurations.Atisu;

public class AtisuHostedService: BackgroundService
{
    private readonly AtisuInitializationService atisuInitializationService;

    public AtisuHostedService(AtisuInitializationService atisuInitializationService)
    {
        this.atisuInitializationService = atisuInitializationService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await atisuInitializationService.Init();
    }
}