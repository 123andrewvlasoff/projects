using System;
using System.Collections.Generic;
using System.Linq;
using Crafter.Vat.Services;
using DAL.Models.Requests;
using DAL.Models.Requests.Enums;
using DAL.Models.Requests.Guaranties.Enums;
using DAL.Models.Requests.Reis.Constants;
using Hangfire;
using Krafter.Logist.WebApp.Helpers;
using Krafter.Logist.WebApp.Hubs.Services;
using Krafter.Logist.WebApp.Models.Basic.CargomartRequestFile;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequest;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestAction;
using Krafter.Logist.WebApp.Models.Basic.KrafterRequestReplyFile;
using Krafter.Logist.WebApp.Models.Basic.Problem;
using Krafter.Logist.WebApp.Models.Basic.Reis;
using Krafter.Logist.WebApp.Models.Basic.ReisDispatchingPoint;
using Krafter.Logist.WebApp.Models.Basic.RequestReply;
using Krafter.Logist.WebApp.Models.Basic.RequestReplyComment;
using Krafter.Logist.WebApp.Models.Basic.TorgTransRequestFile;
using Krafter.Logist.WebApp.Models.Basic.TrafficRequestFile;
using Krafter.Logist.WebApp.Repositories;
using Krafter.Logist.WebApp.Services;
using Krafter.ServiceCommon.Helpers;
using Krafter.ServiceCommon.Services.DbEventHandlerService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class DbEventHostedServiceConfiguration
    {
        public static void ConfigureDbEventHostedService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHostedService(provider =>
            {
                var logger = provider.GetService<ILogger<DbEventHostedService>>();
                var connectionString = configuration.GetConnectionString("Krafter");
                

                return new DbEventHostedService(new List<DbEventHandler>()
                {
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Notifications" &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<NotificationsHubService>(j => j.SendAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.Krafter == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<RequestsHubService>(j => j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.Krafter == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation) &&
                            !p.GetOld<KrafterRequest>().IsActive && p.GetNew<KrafterRequest>().IsActive &&
                            !string.IsNullOrEmpty(p.GetNew<KrafterRequest>().InProgressUserId),
                        Handler = e =>
                            BackgroundJob.Enqueue<KrafterRequestService>(i => i.RequestUnblocked(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler()
                    {
                        Pattern = p =>
                            p.TableName == "Files" && p.Discriminator == nameof(KrafterRequestReplyFile) &&
                            new List<DbOperation>() { DbOperation.Insert, DbOperation.Update }.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<RequestsHubService>(j =>
                                j.SendRequestReplyFileAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            new List<DbOperation> {DbOperation.Update, DbOperation.Insert}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<RequestDtoRepository>(j => j.SetAsync(Guid.Parse( e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.Cargomart == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<CargomartsHubService>(j =>
                                j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.Transporeon == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TransporeonHubService>(j =>
                                j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.Baltika == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<BaltikaHubService>(j =>
                                j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.TrafficOnline == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TrafficsHubService>(j => j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Requests" &&
                            RequestDiscriminator.TorgTrans == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TorgTransHubService>(j =>
                                j.SendRequestAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "RequestReplies" &&
                            RequestDiscriminator.Cargomart == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<CargomartsHubService>(j =>
                                j.SendRequestReplyAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "RequestReplies" &&
                            RequestDiscriminator.Transporeon == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TransporeonHubService>(j =>
                                j.SendRequestReplyAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "RequestReplies" &&
                            RequestDiscriminator.TrafficOnline == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TrafficsHubService>(j =>
                                j.SendRequestReplyAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "RequestReplies" &&
                            RequestDiscriminator.TorgTrans == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TorgTransHubService>(j =>
                                j.SendRequestReplyAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "RequestReplies" &&
                            RequestDiscriminator.Krafter == (RequestDiscriminator) int.Parse(p.Discriminator) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e => 
                        BackgroundJob.Enqueue<RequestsHubService>(j =>
                                j.SendRequestAsync(e.GetNew<Models.Basic.RequestReply.RequestReply>().RequestId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "NewMessages" &&
                            new List<DbOperation> {DbOperation.Insert}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<MessagesHubService>(j => j.SendAsync(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Files" && p.Discriminator == nameof(KrafterRequestReplyFile) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            // await requestHubService.SendRequestReplyAsync(e.GetNew<KrafterRequestReplyFile>().ReplyId)
                            BackgroundJob.Enqueue<RequestsHubService>(j =>
                                j.SendRequestReplyAsync(e.GetNew<KrafterRequestReplyFile>().ReplyId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Files" && p.Discriminator == nameof(CargomartRequestFile) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<CargomartsHubService>(j =>
                                j.SendRequestAsync(e.GetNew<CargomartRequestFile>().RequestId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Files" && p.Discriminator == nameof(TorgTransRequestFile) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TorgTransHubService>(j =>
                                j.SendRequestAsync(e.GetNew<TorgTransRequestFile>().RequestId))
                    },

                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Files" && p.Discriminator == nameof(TrafficRequestFile) &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<TrafficsHubService>(j =>
                                j.SendRequestAsync(e.GetNew<TrafficRequestFile>().RequestId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Comments" &&
                            new List<DbOperation> {DbOperation.Insert, DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<RequestsHubService>(j =>
                                j.SendRequestReplyAsync(e.GetNew<RequestReplyComment>().RequestReplyId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Actions" &&
                            p.Discriminator == nameof(KrafterRequestAction) &&
                            new List<DbOperation> {DbOperation.Insert}.Contains(p.Operation) &&
                            p.GetNew<KrafterRequestAction>().State == RequestActionStatus.TakeInProgress,
                        Handler = e =>
                            BackgroundJob.Enqueue<RequestsHubService>(j =>
                                j.SendRequestAsync(e.GetNew<KrafterRequestAction>().RequestId))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Vat" &&
                            new List<DbOperation> {DbOperation.Insert}.Contains(p.Operation),
                        Handler = e =>
                                BackgroundJob.Enqueue<VatCacheService>(i => i.ResetVatCache())
                    },   
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "Marginality" &&
                            new List<DbOperation> {DbOperation.Insert}.Contains(p.Operation),
                        Handler = e =>
                                BackgroundJob.Enqueue<VatCacheService>(i => i.ResetMarginalityCache())
                    },
                    new DbEventHandler()
                    {
                        Pattern = p =>
                            p.TableName == "Reis" && new List<DbOperation>()
                            {
                                DbOperation.Update
                            }.Contains(p.Operation) &&
                            p.GetNew<Reis>()?.StatusId == ReisStatus.Completed &&
                            p.GetOld<Reis>()?.StatusId != ReisStatus.Completed,
                        Handler = e =>
                        {
                            BackgroundJob.Enqueue<NotificationDispatcherService>(j =>
                                j.NotifyReisCompleted(e.GetNew<Reis>().Id));
                        }
                    },
                    new DbEventHandler()
                    {
                        Pattern = p =>
                            p.TableName == "Reis" && new List<DbOperation>()
                            {
                                DbOperation.Update
                            }.Contains(p.Operation) &&
                            p.GetNew<Reis>()?.Rvd != null &&
                            p.GetOld<Reis>()?.Rvd == null,
                        Handler = e =>
                        {
                            var reis = e.GetNew<Reis>();
                            
                            BackgroundJob.Enqueue<NotificationDispatcherService>(
                                j => j.NotifyReisDocsOnVerification(reis.Id));
                        }
                    },
                    new DbEventHandler()
                    {
                        Pattern = p =>
                            p.TableName == "ReisDispatchingPoints" && new List<DbOperation>()
                            {
                                DbOperation.Insert,
                                DbOperation.Update
                            }.Contains(p.Operation) &&
                            p.GetNew<ReisDispatchingPoint>()?.MonitoringPointTypeId ==
                            MonitoringPointTypeGuids.DeliveryOfDocumentsIsConfirmedbyTheDriver &&
                            p.GetNew<ReisDispatchingPoint>()?.ValidityDateStart != null &&
                            p.GetNew<ReisDispatchingPoint>()?.MonitoringDateFact == null,
                        Handler = e =>
                        {
                            var item = e.GetNew<ReisDispatchingPoint>();

                            BackgroundJob.Schedule<NotificationDispatcherService>(
                                j => j.NotifyDriverDontGaveDocumentsUp(item.ReisId),
                                item.ValidityDateStart!.Value.Date.AddDays(1));
                        }
                    },
                    new DbEventHandler()
                    {
                        Pattern = p =>
                            p.TableName == "Problems" &&
                            new List<DbOperation>()
                            {
                                DbOperation.Insert
                            }.Contains(p.Operation)
                            && p.GetNew<Problem>()?.ReisId.HasValue == true,
                        Handler = e =>
                        {
                            BackgroundJob.Enqueue<NotificationDispatcherService>(j =>
                                j.NotifyReisProblemRevealed(e.GetNew<Problem>().ReisId.Value));
                        }
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "CarrierOffers" &&
                            new List<CarrierOfferStatus> {CarrierOfferStatus.Confirm}.Contains(p.GetNew<CarrierOffers>().Status) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<EmailSenderService>(j => j.ConfirmCarrierOfferEmail(Guid.Parse(e.EntryId)))
                    },
                    new DbEventHandler
                    {
                        Pattern = p =>
                            p.TableName == "CarrierOffers" &&
                            new List<CarrierOfferStatus> {CarrierOfferStatus.RejectByLoadingDateTime,
                                CarrierOfferStatus.RejectByPrice,CarrierOfferStatus.RejectByPriceAndLoadingDateTime,
                                CarrierOfferStatus.RejectCauseAnotherCarrier}.Contains(p.GetNew<CarrierOffers>().Status) &&
                            new List<DbOperation> {DbOperation.Update}.Contains(p.Operation),
                        Handler = e =>
                            BackgroundJob.Enqueue<EmailSenderService>(j => j.RejectCarrierOfferEmail(Guid.Parse(e.EntryId),e.GetNew<CarrierOffers>().Status))
                    },
                }, connectionString, logger);
            });
        }
    }
}