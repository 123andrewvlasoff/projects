using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Krafter.IdentityServer.Extensions.Constants;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ClaimTypes = Krafter.IdentityServer.Extensions.Constants.ClaimTypes;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class IdentityServerClientConfiguration
    {
        public static IServiceCollection ConfigureIdentityServerClient(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAuthentication(options => { options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme; })
                .AddJwtBearer(options =>
                {
                    options.Authority = configuration.GetValue<string>("Web:IDENTITY_AUTHORITY");
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = false,
                        ValidateAudience = false
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            if (!string.IsNullOrEmpty(accessToken) && context.HttpContext.WebSockets.IsWebSocketRequest)
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(UserPolicies.RequestsAccess,
                    policy => policy.RequireClaim(ClaimTypes.UserPolicy, UserPolicies.RequestsAccess));

                options.AddPolicy(UserPolicies.AuctionsAccess,
                    policy => policy.RequireClaim(ClaimTypes.UserPolicy, UserPolicies.AuctionsAccess));
            });

            return services;
        }
    }
}