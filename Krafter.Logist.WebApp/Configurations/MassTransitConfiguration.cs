﻿using System;
using Crafter.Integration.Models.CrafterOnline.Models.Spot;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class MassTransitConfiguration
    {
        public static void ConfigureMassTransit(this IServiceCollection services,
            IConfiguration configuration)
        {
            var config = configuration.GetSection("RabbitMq");
            services.AddMassTransit(x =>
            {
                    
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(config.GetValue<string>("Host"),
                        cfgHost =>
                        {
                            cfgHost.Username(config.GetValue<string>("Username"));
                            cfgHost.Password(config.GetValue<string>("Password"));
                        });
                    cfg.ConfigureEndpoints(context);

                    EndpointConvention.Map<SpotCommandRequest>(new Uri("queue:" + config.GetValue<string>("CommandQueue")));
                });

                x.AddRequestClient<SpotCommandRequest>(new Uri("queue:" + config.GetValue<string>("CommandQueue")), TimeSpan.FromSeconds(config.GetValue<int>("CommandTimeOut")));

            });
        }
    }
}