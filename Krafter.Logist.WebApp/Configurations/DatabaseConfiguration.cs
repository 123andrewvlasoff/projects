using Krafter.Logist.WebApp.Models.Basic;
using Krafter.Logist.WebApp.Models.ReadOnly;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class DatabaseConfiguration
    {
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<LogistDbContext>((serviceProvider, options) =>
            {
                var connectionString = configuration.GetConnectionString("Krafter");
                options.UseNpgsql(connectionString, builder => builder.UseNetTopologySuite());
            });

            services.AddDbContext<LogistReadOnlyDbContext>((serviceProvider, options) =>
            {
                var connectionString = configuration.GetConnectionString("Krafter");
                options.UseNpgsql(connectionString, builder => builder.UseNetTopologySuite());
            });

            NpgsqlConnection.GlobalTypeMapper.UseNetTopologySuite(geographyAsDefault: true);

            return services;
        }
    }
}