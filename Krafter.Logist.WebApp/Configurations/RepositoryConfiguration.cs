using System;
using System.Collections.Concurrent;
using System.Threading;
using Hangfire;
using Krafter.Logist.WebApp.Models.ReadOnly.RequestDto;
using Krafter.Logist.WebApp.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Nito.AsyncEx;

namespace Krafter.Logist.WebApp.Configurations
{
    public static class RepositoryConfiguration
    {
        public static TimeSpan RepositoryReload { get; set; } = TimeSpan.FromSeconds(10);

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<RequestDtoRepository>();

            services.AddSingleton(new AsyncReaderWriterLock());
            services.AddSingleton(new ConcurrentDictionary<Guid, RequestDto>());
        }

        public static void UseRepositoriesAutoUpdate(this IApplicationBuilder app)
        {
            RecurringJob.AddOrUpdate<RequestDtoRepository>(repository => repository.SetAsync(),
                $"*/{RepositoryReload.TotalSeconds} * * * * *");

            BackgroundJob.Enqueue<RequestDtoRepository>(repository => repository.SetAsync());
        }
    }
}