#nullable enable

using Base.Api;
using IdentityServer4.AccessTokenValidation;
using QPay.Integration.Api.Data;
using MongoDB.Driver;
using QPay.Integration.Api.Models;
using QPay.Integration.Api.Services;
using Dto.Layer.Payment;
using Refit;
using Transport.Layer;
using QPay.Integration.Api.Infrasturcture;
using Core.Layer;

namespace QPay.Integration.Api;

public class Startup : DefaultStartup
{
  private Func<Task<string>> _GetTokenCallback { get; set; }
  public Startup(IWebHostEnvironment env) : base(env)
  {
  }

  protected override Type OnDatabaseMigratorType => null;
  protected override void OnUseTransportHandlers(IApplicationBuilder app)
  {
  }

  protected override void OnConfigureAuthentication(IdentityServerAuthenticationOptions o)
  {
    o.ApiName = Constants.Scopes.Payment;
    o.ApiSecret = $"{Constants.Scopes.Payment}.secret";
    o.CacheDuration = TimeSpan.FromMinutes(Configuration.GetValue<int>("TokenCacheDurationMinutes", 10));
    o.RequireHttpsMetadata = Configuration.GetValue<bool>("IdentityServerRequireHttps");
    o.EnableCaching = true;
    o.SupportedTokens = SupportedTokens.Both;
  }

  protected override void OnAddServices(IServiceCollection services)
  {
    services.AddTransient<IInvoiceService, InvoiceService>();
    services.AddTransient<ICustomerService, CustomerService>();
    
    var basepath = Configuration.GetValue<string>("QpayBasePath");
    var baseUri = new Uri(basepath);

    services.AddSingleton<IQpayAuthorisationService, QpayAuthorisationService>();
    services.AddRefitClient<IQpayAuthClient>().ConfigureHttpClient(c => c.BaseAddress = baseUri);
    services.AddRefitClient<IQpayClient>(new RefitSettings()
    {
      AuthorizationHeaderValueGetter = async () => await _GetTokenCallback!.Invoke()
    })
      .ConfigureHttpClient(c => c.BaseAddress = baseUri);

    services.AddTransportInvoker<CustomerSubscriptionCustomerResponseDto, CustomerSubscriptionCustomerRequestDto>(SubscriptionsSdk.Constants.Customer);
    services.AddTransportInvoker<UpdateExternalBalanceResponseDto, UpdateExternalBalanceRequestDto>(Core.Layer.Constants.Transport.Payment.ExternalUpdateBalance);
    services.AddAutoMapper(typeof(Startup));
  }

  protected override void OnAddDatabase(IServiceCollection services)
  {
    services.AddTransient(s => s.GetRequiredService<IMongoDatabase>().GetCollection<Invoice>("invoice"));
    services.AddTransient<IInvoiceRepository, InvoiceRepository>();
  }

  protected override void OnUseDatabaseIndexes(IServiceProvider applicationServices)
  {
    applicationServices.UseMongoIndexes();
  }
  
  protected override void OnUseCustom(IApplicationBuilder app)
  {
    app.UsePathBase(Configuration.GetValue<string>("BasePath"));
    _GetTokenCallback = app.ApplicationServices.GetRequiredService<IQpayAuthorisationService>().GetToken;
  }
}