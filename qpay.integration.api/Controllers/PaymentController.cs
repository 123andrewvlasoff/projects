using Microsoft.AspNetCore.Mvc;
using QPay.Integration.Api.Models;
using QPay.Integration.Api.Services;
using Base.Api.Extentions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace QPay.Integration.Api.Controllers
{
  [Route("api/qpay")]
  public class PaymentController : Controller
  {
    private readonly IInvoiceService _InvoiceService;
    private readonly ILogger<PaymentController> _Logger;

    public PaymentController(IInvoiceService invoiceService, ILogger<PaymentController> logger)
    {
      _InvoiceService = invoiceService;
      _Logger = logger;
    }

    /// <summary>
    /// Создает платеж в QPay и возвращает ссылки для оплаты
    /// </summary>
    /// <param name="request"><see cref="InvoiceRequest"/></param>
    /// <returns>Модель, содержащая сссылки для оплаты <see cref="InvoiceLinksModel"/></returns>
    [HttpPost("links")]
    [ProducesResponseType(typeof(InvoiceLinksModel), 200)]
    [ProducesResponseType(typeof(InvoiceLinksModel), 400)]
    [Authorize]
    public async Task<IActionResult> CreateInvoice([FromBody] InvoiceRequest request)
    {
      _Logger.LogInformation($"received {nameof(CreateInvoice)} request, parameters: anount {request.Amount}");
      if (!ModelState.IsValid)
      {
        return BadRequest(new InvoiceLinksModel
        {
          Errors = ModelState
          .Keys.Where(key => ModelState[key].ValidationState == ModelValidationState.Invalid)
            .Select(key => new KeyValuePair<string, string[]>(
              key, 
              ModelState[key].Errors.Select(err => err.ErrorMessage).ToArray())
            )
        });
      }

      var phone = HttpContext.GetPhoneNumber();

      try
      {
        var response = await _InvoiceService.CreateInvoice(amount: request.Amount, userPhone: phone);
        return Ok(response);
      }
      catch (Exception ex)
      {
        _Logger.LogError(ex, $"Exeption at pulling invoice, user phone: {phone}; amount: {request.Amount}");
        return BadRequest(ex);
      }
    }

    /// <summary>
    /// Колбэк для пополнения счета через QPay
    /// </summary>
    /// <param name="internalId">Внутренний Id платежа</param>
    [HttpGet("topup/{internalId}")]
    [ProducesResponseType(typeof(OkResult), 200)]
    public async Task<IActionResult> TopUp([FromRoute] string internalId)
    {
      _Logger.LogInformation($"received {nameof(TopUp)} request, parameters: internalId {internalId}");
      await _InvoiceService.TopUp(internalId);
      return Ok();
    }

    /// <summary>
    /// Метод проверки статуса платежа
    /// </summary>
    /// <param name="internalId">Внутренний Id платежа</param>
    /// <returns>Статус платежа <see cref="CheckStatusModel"/></returns>
    [HttpGet("check-status/{internalId}")]
    [ProducesResponseType(typeof(CheckStatusModel), 200)]
    public async Task<IActionResult> CheckStatus(string internalId)
    {
      _Logger.LogInformation($"received {nameof(CheckStatus)} request, parameters: internalId {internalId}");
      var res = await _InvoiceService.CheckStatus(internalId);
      return Ok(res);
    }
  }
}