#nullable enable

using AutoMapper;
using QPay.Integration.Api.Models;

namespace QPay.Integration.Api.Mappers;

public class MappingProfile: Profile
{
  public MappingProfile() 
  {
    CreateMap<CreateInvoiceResult, InvoiceLinksModel>();
  }
}