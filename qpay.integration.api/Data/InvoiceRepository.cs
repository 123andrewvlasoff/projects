#nullable enable

using Base.Api.Data;
using Base.Api.Services;
using MongoDB.Driver;
using QPay.Integration.Api.Models;

namespace QPay.Integration.Api.Data;

public interface IInvoiceRepository : IRepository<Invoice>
{
  Task<string> CreateAndInsertInvoice(decimal amount, string accountId);
  Task UpdateInvoiceStatus(string internalId, InvoiceStatus status);
  Task AddExternalId(string internalId, string externalId);
}
public class InvoiceRepository : Repository<Invoice>, IInvoiceRepository
{
  public InvoiceRepository(IMongoCollection<Invoice> collection, ITimeService timeService) : base(collection, timeService)
  {
  }

  public async Task<string> CreateAndInsertInvoice(decimal amount, string accountId)
  {
    var invoice = new Invoice()
    {
      Amount = amount,
      Status = InvoiceStatus.Created,
      AccountId = accountId,
    };
    var id = invoice.Id;
    await InsertOneAsync(invoice);
    return id;
  }

  public async Task UpdateInvoiceStatus(string internalId, InvoiceStatus status)
  {
    await FindOneAndUpdateAsync(i => i.Id == internalId, 
      Builders<Invoice>.Update
        .Set(u => u.Status, status)
    );
  }

  public async Task AddExternalId(string internalId, string externalId)
  {
    await FindOneAndUpdateAsync(i => i.Id == internalId,
      Builders<Invoice>.Update
        .Set(i => i.ExternalId, externalId)
    );
  }
}