﻿using QPay.Integration.Api.Infrasturcture;
using QPay.Integration.Api.Models;
using System.Text;

namespace QPay.Integration.Api.Services
{
  public interface IQpayAuthorisationService
  {
    Task<string> GetToken();
  }

  public class QpayAuthorisationService : IQpayAuthorisationService
  {
    private readonly IQpayAuthClient _AuthClient;
    private readonly IConfiguration _Config;

    private TokenDto? _token;
    private DateTime? _expireDate = null;
    private DateTime? _refreshExpireDate = null;

    public QpayAuthorisationService(IQpayAuthClient authClient, IConfiguration configuration)
    {
      _AuthClient = authClient;
      _Config = configuration;
    }

    public async Task<string> GetToken()
    {
      TimeSpan? expireDiff = _expireDate.HasValue ? _expireDate.Value - DateTime.UtcNow.ToLocalTime() : null;
      TimeSpan? refreshExpireDiff = _refreshExpireDate.HasValue ? _refreshExpireDate.Value - DateTime.UtcNow.ToLocalTime() : null;

      var refreshTsBeforeExpire = TimeSpan.FromMinutes(
        _Config.GetValue<double>("TokenRefreshBeforeExpireMinutes")
      );

      if (_token == null || 
        !expireDiff.HasValue || 
        expireDiff.Value.TotalMilliseconds <= 0 ||
        !refreshExpireDiff.HasValue ||
        refreshExpireDiff.Value.TotalMilliseconds <= 0)
      {
        _token = await _AuthClient.GetToken(GetCredentials());
        UpdateExpireDate(_token);
      }
      else if (refreshExpireDiff.HasValue && refreshExpireDiff.Value <= refreshTsBeforeExpire)
      {
        _token = await _AuthClient.RefreshToken(_token.RefreshToken);
        UpdateExpireDate(_token);
      }
      return _token.AccessToken;
    }

    private string GetCredentials()
    {
      var sellerUsername = _Config.GetSection("Credentials").GetValue<string>("Username");
      var sellerPass = _Config.GetSection("Credentials").GetValue<string>("Password"); 
      var authCreds = string.Join(':', sellerUsername, sellerPass);
      var credsBytes = Encoding.UTF8.GetBytes(authCreds);
      var base64creds = Convert.ToBase64String(credsBytes);
      return base64creds;
    }

    private void UpdateExpireDate(TokenDto newToken)
    {
      _expireDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
        .AddSeconds(newToken.ExpiresIn)
        .ToLocalTime();
      _refreshExpireDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
        .AddSeconds(newToken.RefreshExpiresIn)
        .ToLocalTime();
    }
  }
}
