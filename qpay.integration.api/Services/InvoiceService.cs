﻿#nullable enable

using System.Globalization;
using AutoMapper;
using Dto.Layer.Payment;
using Newtonsoft.Json;
using QPay.Integration.Api.Data;
using QPay.Integration.Api.Infrasturcture;
using QPay.Integration.Api.Models;
using Transport.Layer;

namespace QPay.Integration.Api.Services;

public interface IInvoiceService
{
  Task<InvoiceLinksModel> CreateInvoice(decimal amount, string userPhone);
  Task TopUp(string internalId);
  Task<CheckStatusModel> CheckStatus(string internalId);
}

public class InvoiceService : IInvoiceService
{
  private readonly IInvoiceRepository _InvoiceRepository;
  private readonly IQpayClient _QpayClient;
  private readonly ILogger<InvoiceService> _Logger;
  private readonly IMapper _Mapper;
  private readonly IInvoker<UpdateExternalBalanceResponseDto, UpdateExternalBalanceRequestDto> _UpdateBalanceInvoker;
  private readonly ICustomerService _CustomerService;

  private readonly string _CallbackUrlBase;
  private readonly string _CallbackMethod = "api/qpay/topup";

  public InvoiceService(
    IInvoiceRepository invoiceRepository,
    IQpayClient qpayClient,
    ILogger<InvoiceService> logger,
    IMapper mapper,
    IInvoker<UpdateExternalBalanceResponseDto, UpdateExternalBalanceRequestDto> updateBalanceInvoker,
    ICustomerService customerService,
    IConfiguration config)
  {
    _InvoiceRepository = invoiceRepository;
    _QpayClient = qpayClient;
    _Logger = logger;
    _Mapper = mapper;
    _UpdateBalanceInvoker = updateBalanceInvoker;
    _CustomerService = customerService;

    _CallbackUrlBase = config.GetValue<string>("CallbackUrlBase");
  }

  public async Task<CheckStatusModel> CheckStatus(string internalId)
  {
    _Logger.LogInformation($"checking status of invoice with internalId {internalId}");
    var invoice = (await _InvoiceRepository.FindAnyAsync(i => i.Id == internalId)).FirstOrDefault();

    if (invoice == null)
    {
      _Logger.LogError($"Failed to check invoice status: invoice with id {internalId} not found");
      return new CheckStatusModel
      {
        InvoiceId = internalId,
        Status = InvoiceStatus.InvoiceNotFound
      };
    }

    var res = await _QpayClient.CheckStatus(new CheckStatusRequest()
    {
      ObjectType = "INVOICE",
      InvoiceId = invoice.ExternalId!,
    });
    _Logger.LogInformation($"checking status of invoice with internalId {internalId} in QPay service");

    var resStatus = res.Rows.FirstOrDefault()?.Status;

    _Logger.LogInformation($"{resStatus ?? "NULL"} status returned as a result of checking invoice status with internalId {internalId} in QPay service.");

    if (!res.Rows.Any())
    {
      return new CheckStatusModel
      {
        InvoiceId = internalId,
        Status = InvoiceStatus.Created
      };
    }

    if (string.IsNullOrEmpty(resStatus) || resStatus.Equals("FAILED"))
    {
      return new CheckStatusModel
      {
        InvoiceId = internalId,
        Status = InvoiceStatus.Failed
      };
    }

    if (res.PaidAmount.Equals(invoice.Amount) && resStatus.Equals("PAID"))
    {
      await _InvoiceRepository.UpdateInvoiceStatus(internalId, InvoiceStatus.Fulfilled);
      return new CheckStatusModel
      {
        InvoiceId = internalId,
        Status = InvoiceStatus.Fulfilled
      };
    }
    return new CheckStatusModel
    {
      InvoiceId = internalId,
      Status = InvoiceStatus.Created
    };
  }

  public async Task<InvoiceLinksModel> CreateInvoice(decimal amount, string userPhone)
  {
    var checkStatusResult = await _CustomerService.CheckUserStatus(userPhone);

    if (checkStatusResult.Result != PaymentResultCodes.Success)
    {
      _Logger.LogError($"Can`t create invoice, user with phone {userPhone} not available: status {checkStatusResult.Result}; comment {checkStatusResult.Comment}");
      return new InvoiceLinksModel
      {
        Comment = checkStatusResult.Comment ?? checkStatusResult.Result.ToString(),
      };
    }


    var id = await _InvoiceRepository.CreateAndInsertInvoice(amount, checkStatusResult.UserAccountId);
    var callbackUrl = $"{_CallbackUrlBase}/{_CallbackMethod}/{id}";

    var dto = new CreateInvoiceDto()
    {
      Amount = amount,
      CallbackUrl = callbackUrl,
      InvoiceCode = "JET_INVOICE",
      InvoiceDescription = "Jet topup",
      InvoiceReceiverCode = id,
      SenderInvoiceNo = checkStatusResult.UserAccountId,
    };
    _Logger
      .WithUserAccountId(checkStatusResult.UserAccountId)
      .LogInformation($"creating invoice in QPay service. {JsonConvert.SerializeObject(dto)}");
    var res = await _QpayClient.CreateInvoice(dto);
    _Logger
      .WithUserAccountId(checkStatusResult.UserAccountId)
      .LogInformation($"response from qpay. {JsonConvert.SerializeObject(res)}");

    await _InvoiceRepository.AddExternalId(id, res.ExternalId);

    var model = _Mapper.Map<InvoiceLinksModel>(res);

    model.InternalId = id;

    return model;
  }

  public async Task TopUp(string internalId)
  {
    var invoiceStatus = await CheckStatus(internalId);
    if (invoiceStatus.Status == InvoiceStatus.Fulfilled)
    {
      var invoice = await _InvoiceRepository.FindOneAsync(i => i.Id == internalId);
      if (invoice == null)
      {
        _Logger.LogError($"Cant top up invoice: failed to find invoice with id {internalId}");
        return;
      }

      var updateResponse = await _UpdateBalanceInvoker.InvokeAsync(new UpdateExternalBalanceRequestDto
      {
        UserAccountId = invoice.AccountId,
        Bonus = new MoneyDto { Value = invoice.Amount, Culture = new CultureInfo("mn-MN").ToString() },
        Message = string.Format("+{0} bonus {1}Qpay ашиглан төлбөр хийгдэн!", invoice.Amount, Environment.NewLine),
        Notify = true,
        TransactionType = TransactionTypeDto.QpayBonuses,        
        Comment = Resources.SharedResources.TransactionMessage,
        Payload = new Dictionary<PayloadTypeDto, string>() { { PayloadTypeDto.QPayTransactionId, invoice.ExternalId.ToString() } }
      });
      if (!updateResponse.Success)
      {
        _Logger.LogError($"Failed to update external balance, invoiceId: {internalId}, errorMessage: {updateResponse.ErrorMessage}");
      } else
      {
        _Logger.LogInformation($"Topped up invoice with internalId {internalId}", updateResponse);
      }
    }
  }
}