#nullable enable
using Dto.Layer.Payment;
using QPay.Integration.Api.Models;
using Transport.Layer;

namespace QPay.Integration.Api.Services;

/// <summary>
/// Интерфейс сервиса для работы с абонентом
/// </summary>
public interface ICustomerService
{
  /// <summary>
  /// Метод проверки состояния счета абонента
  /// </summary>
  /// <param name="phoneNumber">Номер телефона абонента</param>
  /// <returns>Модель ответа метода проверки состояния счета абонента</returns>
  Task<CheckCustomerStatusResultDto> CheckUserStatus(string phoneNumber);

}

internal class CustomerService : ICustomerService
{
  private readonly IInvoker<CustomerSubscriptionCustomerResponseDto, CustomerSubscriptionCustomerRequestDto> _InvokerGetCustomerByPhone;
  private readonly ILogger<CustomerService> _Logger;

  public CustomerService(IInvoker<CustomerSubscriptionCustomerResponseDto, CustomerSubscriptionCustomerRequestDto> invokerGetCustomerByPhone, ILogger<CustomerService> logger)
  {
    _InvokerGetCustomerByPhone = invokerGetCustomerByPhone;
    _Logger = logger;
  }

  public async Task<CheckCustomerStatusResultDto> CheckUserStatus(string phoneNumber)
  {
    _Logger.LogInformation($"getting user with phone number {phoneNumber}");
    var getCustomerByPhoneResult = await _InvokerGetCustomerByPhone.InvokeAsync(new CustomerSubscriptionCustomerRequestDto
    {
      CustomerPhoneNumber = phoneNumber
    });

    if (!getCustomerByPhoneResult.Success || string.IsNullOrEmpty(getCustomerByPhoneResult.UserAccountId))
    {
      _Logger.LogWarning($"Can`t get user with phone number {phoneNumber}, error message: {getCustomerByPhoneResult.ErrorMessage} ");
      return new CheckCustomerStatusResultDto
      {
        Result = PaymentResultCodes.NotFound
      };
    }

    if (getCustomerByPhoneResult.IsBlocked)
    {
      _Logger.LogWarning($"Can`t get user with phone number {phoneNumber}, user is blocked: {getCustomerByPhoneResult.IsBlocked}");
      return new CheckCustomerStatusResultDto
      {
        Result = PaymentResultCodes.InternalServerError,
        //Comment = Resources.SharedResources.ErrorCustomerBlocked
      };
    }

    return new CheckCustomerStatusResultDto
    {
      Result = PaymentResultCodes.Success,
      UserAccountId = getCustomerByPhoneResult.UserAccountId
    };
  }
}
