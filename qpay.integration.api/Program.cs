namespace QPay.Integration.Api;

public class Program
{
  public static void Main(string[] args)
  {
    Base.Api.MainHost.Run<Startup>(args);
  }
}