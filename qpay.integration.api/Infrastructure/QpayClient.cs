using QPay.Integration.Api.Models;
using Refit;

namespace QPay.Integration.Api.Infrasturcture;

[Headers("Authorization: Bearer")]
public interface IQpayClient
{
  [Post("/invoice")]
  Task<CreateInvoiceResult> CreateInvoice([Body] CreateInvoiceDto invoiceDto);

  [Delete("/invoice/{id}")]
  Task CancelInvoice([AliasAs("id")] string invoiceId);

  [Post("/payment/check")]
  Task<CheckStatusResult> CheckStatus([Body] CheckStatusRequest request);
}