#nullable enable
using QPay.Integration.Api.Models;
using Refit;

namespace QPay.Integration.Api.Infrasturcture;

public interface IQpayAuthClient
{
  [Post("/auth/token")]
  Task<TokenDto> GetToken([Authorize("Basic")] string credentials);

  [Post("/auth/refresh")]
  Task<TokenDto> RefreshToken([Authorize("Bearer")] string refreshToken);
}