#nullable enable

using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace QPay.Integration.Api.Models;

/// <summary>
/// Модель запроса в метод пополнения лицевого счета
/// </summary>
public class InvoiceRequest
{
  /// <summary>
  /// Сумма к зачислению на лицевой счет абонента
  /// </summary>
  /// <example>500.00</example>
  [Elastic.Apm.Api.Constraints.Required]
  [FromQuery(Name = "amount")]
  [Range(10, double.MaxValue)]
  public decimal Amount { get; set; }
}