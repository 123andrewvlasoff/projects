#nullable enable

namespace QPay.Integration.Api.Models;

/// <summary>
/// Модель ответа метода проверки состояния счета абонента
/// </summary>
public class CheckCustomerStatusResultDto
{
  /// <summary>
  /// Код ответа. Возможные значения:<see cref="PaymentResultCodes"/>
  /// </summary>
  public PaymentResultCodes Result { get; set; }

  /// <summary>
  /// Комментарий
  /// </summary>
  public string? Comment { get; set; }

  /// <summary>
  /// Идентификатор лицевого счета абонента
  /// </summary>
  public string? UserAccountId { get; set; }
}