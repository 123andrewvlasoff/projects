#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class CheckStatusRow
{
  [JsonPropertyName("payment_id")]
  public string PaymentId { get; set; }

  /// <summary>
  /// <see cref="PaymentStatusConstants"/>
  /// </summary>
  [JsonPropertyName("payment_status")]
  public string Status { get; set; }

  [JsonPropertyName("payment_date")]
  public DateTime PaymentDate { get; set; }

  [JsonPropertyName("payment_fee")]
  public double PaymentFee { get; set; }

  [JsonPropertyName("payment_amount")]
  [JsonNumberHandling(JsonNumberHandling.AllowReadingFromString)]
  public decimal PaymentAmount { get; set; }

  [JsonPropertyName("payment_currency")]
  public string PaymentCurency { get; set; }

  [JsonPropertyName("payment_wallet")]
  public string PaymentWallet { get; set; }

  [JsonPropertyName("transaction_type")]
  public string TransactionType { get; set; }

  public static class PaymentStatusConstants
  {
    public static readonly string New = "NEW";
    public static readonly string Paid = "PAID";
    public static readonly string Failed = "FAILED";
    public static readonly string Refunded = "REFUNDED";
  }
}