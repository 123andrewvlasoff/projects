#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class CheckStatusResult
{
  [JsonPropertyName("count")]
  public int Count { get; set; }
  [JsonPropertyName("paid_amount")]
  public decimal PaidAmount { get; set; }
  [JsonPropertyName("rows")]
  public CheckStatusRow[] Rows { get; set; }
}