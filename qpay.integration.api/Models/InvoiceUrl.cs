#nullable enable

namespace QPay.Integration.Api.Models;

public class InvoiceUrl
{
  public string Name { get; set; }

  public string Description { get; set; }

  public string Logo { get; set; }

  public string Link { get; set; }
}