#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class CreateInvoiceResult
{
  [JsonPropertyName("invoice_id")]
  public string ExternalId { get; set; }
  [JsonPropertyName("urls")]
  public InvoiceUrl[] Urls { get; set; }
  [JsonPropertyName("qPay_shortUrl")]
  public string ShortUrl { get; set; }
}