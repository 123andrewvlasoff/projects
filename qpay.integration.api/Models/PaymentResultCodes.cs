#nullable enable

namespace QPay.Integration.Api.Models;

public enum PaymentResultCodes
{
  /// <summary>
  /// Лицевой счет абонента может быть пополнен / Успешное завершение операции пополнения баланса
  /// </summary>
  Success = 0,

  /// <summary>
  /// Абонент не найден или заблокирован / Заказ не найден
  /// </summary>
  NotFound = 1,

  /// <summary>
  /// Заказ уже оплачен
  /// </summary>
  AlreadyPaid = 3,

  /// <summary>
  /// Ошибка валидации формы
  /// </summary>
  ModelValidationError = 4,

  /// <summary>
  /// Внутренняя ошибка сервера
  /// </summary>
  InternalServerError = 5
}