#nullable enable

using Base.Api.Models;

namespace QPay.Integration.Api.Models;

public class InvoiceLinksModel : BaseErrorHandlerViewModel
{
  public string InternalId { get; set; }

  public InvoiceUrl[] Urls { get; set; }

  public string Comment { get; set; }
  public string ShortUrl { get; set; }
}