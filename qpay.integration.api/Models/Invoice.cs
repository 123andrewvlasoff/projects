#nullable enable

using Base.Api.Models;

namespace QPay.Integration.Api.Models;

public class Invoice : BaseDocument
{
  public string AccountId { get; set; }
  public decimal Amount { get; set; }
  public InvoiceStatus Status { get; set; }
  public string? ExternalId { get; set; }
}

public enum InvoiceStatus
{
  Created = 0,
  Fulfilled = 1,
  InvoiceNotFound = -1,
  Failed = 2,
}