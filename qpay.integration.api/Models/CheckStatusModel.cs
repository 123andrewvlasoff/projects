#nullable enable

namespace QPay.Integration.Api.Models;

public class CheckStatusModel
{
  public string InvoiceId { get; set; }
  public InvoiceStatus Status { get; set; }
}