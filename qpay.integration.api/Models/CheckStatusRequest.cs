#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class CheckStatusRequest
{
  [JsonPropertyName("object_type")]
  public string ObjectType { get; set; }
  [JsonPropertyName("object_id")]
  public string InvoiceId { get; set; }
}