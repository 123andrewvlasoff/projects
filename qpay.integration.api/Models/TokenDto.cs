#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class TokenDto
{
  [JsonPropertyName("token_type")]
  public string TokenType { get; set; }

  [JsonPropertyName("refresh_expires_in")]
  public int RefreshExpiresIn { get; set; }

  [JsonPropertyName("refresh_token")]
  public string RefreshToken { get; set; }

  [JsonPropertyName("access_token")]
  public string AccessToken { get; set; }

  [JsonPropertyName("expires_in")]
  public int ExpiresIn { get; set; }

  [JsonPropertyName("scope")]
  public string Scope { get; set; }

  [JsonPropertyName("not-before-policy")]
  public string NotBeforePolicy { get; set; }

  [JsonPropertyName("session_state")]
  public string SessionState { get; set; }
}