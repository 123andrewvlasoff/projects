#nullable enable

using System.Text.Json.Serialization;

namespace QPay.Integration.Api.Models;

public class CreateInvoiceDto
{
  [JsonPropertyName("invoice_code")]
  public string InvoiceCode { get; set; }

  [JsonPropertyName("sender_invoice_no")]
  public string SenderInvoiceNo { get; set; }

  [JsonPropertyName("invoice_receiver_code")]
  public string InvoiceReceiverCode { get; set; }

  [JsonPropertyName("invoice_description")]
  public string InvoiceDescription { get; set; }

  [JsonPropertyName("amount")]
  public decimal Amount { get; set; }

  [JsonPropertyName("callback_url")]
  public string CallbackUrl { get; set; }
}