#nullable enable

namespace QPay.Integration.Api.Models;

public class CheckInvoiceStatusRequest
{
  public string UserPhone { get; set; }
  public string InvoiceId { get; set; }
}